package com.netizen.medicom.viewModel

import android.app.Application
import androidx.lifecycle.*
import com.netizen.medicom.model.AddPatientFamilyMember
import com.netizen.medicom.model.PurchasePoint
import com.netizen.medicom.repository.PatientRepository
import com.netizen.medicom.utils.Loaders

class PatientViewModel(application: Application) : AndroidViewModel(application) {

    private val patientRepository = PatientRepository(application)
    val fullName = patientRepository.fullName
    val PhoneNo = patientRepository.phoneNo

    var isRelation = MutableLiveData<Boolean>()
    var isName = MutableLiveData<Boolean>()
    var isMobile = MutableLiveData<Boolean>()
    var isEmail = MutableLiveData<Boolean>()
    var isBirthday = MutableLiveData<Boolean>()
    var isGender = MutableLiveData<Boolean>()
    var isBloodGroup = MutableLiveData<Boolean>()
    var isReligion = MutableLiveData<Boolean>()
    val familyMemberArrayList=patientRepository.patientFamilyMemberArrayList

    fun checkReferenceId(netiId: String?) {
        when {
            netiId?.isEmpty()!! -> {
                Loaders.error.value = "Custom Neti ID can't left empty."
            }
            else -> {
                patientRepository.getPersonInfoForPatient(netiId)
            }
        }
    }

    fun patientRegSubmit(netiId: String?) {
        when {
            netiId?.isEmpty()!! -> {
                Loaders.error.value = "Custom Neti ID can't left empty."
            }
            else -> {
                patientRepository.patientRegistration(netiId)
            }
        }
    }

    fun onAddPatient(
        relation: String?,
        name: String?,
        mobile: String?,
        email: String?,
        birthday: String?,
        gender: String?,
        blood: String?,
        religion: String?

    ) {
        val addPatientFamilyMember =
            AddPatientFamilyMember(relation, name, mobile, email, birthday, gender, blood, religion)

        when {
            relation.isNullOrEmpty() -> {
                isRelation.value = true
            }
            name.isNullOrEmpty() -> {
                isName.value = true
            }
            mobile.isNullOrEmpty() -> {
                isMobile.value = true
            }

            email.isNullOrEmpty() -> {
                isEmail.value = true
            }

            birthday.isNullOrEmpty() -> {
                isBirthday.value = true
            }

            gender.isNullOrEmpty() -> {
                isGender.value = true
            }

            blood.isNullOrEmpty() -> {
                isBloodGroup.value = true
            }

            religion.isNullOrEmpty() -> {
                isReligion.value = true
            }

            else -> {
                patientRepository.addPatientFamilyMember(addPatientFamilyMember)
            }

        }

    }

    fun getFamilyMemberList(){
        patientRepository.getPatientFamilyMemberData()
    }

    @Suppress("UNCHECKED_CAST")
    class PatientViewModelFactory(private val application: Application) :
        ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(PatientViewModel::class.java)) {
                return PatientViewModel(application) as T
            }

            throw IllegalStateException("Unknown ViewModel Class")
        }
    }
}