package com.netizen.medicom.viewModel

import android.app.Application
import androidx.lifecycle.*
import com.netizen.medicom.model.DoctorDesignation
import com.netizen.medicom.model.DoctorEnroll
import com.netizen.medicom.repository.DoctorRepository

class DoctorViewModel(application: Application) : AndroidViewModel(application) {

    private val doctorRepository = DoctorRepository(application)

    var isPartnerReferenceId = MutableLiveData<Boolean>()
    var isDesignationId = MutableLiveData<Boolean>()
    var isSpecialistId = MutableLiveData<Boolean>()
    var isInstituteName = MutableLiveData<Boolean>()
    var isBmdcNumber = MutableLiveData<Boolean>()
    var isDescription = MutableLiveData<Boolean>()
    var isDegreeInfo = MutableLiveData<Boolean>()
    var isNewPatientFeesAmount = MutableLiveData<Boolean>()
    var isReturnPatientFeesAmount = MutableLiveData<Boolean>()
    var isReportPatientFeesAmount = MutableLiveData<Boolean>()
    private val doctorDesignationArrayList = doctorRepository.doctorDesignationArrayList
    private val doctorSpecialistArrayList = doctorRepository.doctorDesignationArrayList
    var doctorDesignationId: String? = null

    //Doctor Designation -----------------------------------------------------------------
    val tempDoctorDesignationList = Transformations.map(doctorDesignationArrayList) {
        getTempDoctorDesignationList(it)
    }

    private fun getTempDoctorDesignationList(designationList: List<DoctorDesignation>): ArrayList<String> {
        val tempDoctorDesignationList = ArrayList<String>()
        tempDoctorDesignationList.add("Select Designation")

        designationList.forEach { designation -> tempDoctorDesignationList.add(designation.getCategoryName().toString()) }

        return tempDoctorDesignationList
    }

    //Doctor Specialist -----------------------------------------------------------------
    val tempSpecialistList = Transformations.map(doctorSpecialistArrayList) {
        getTempSpecialisList(it)
    }

    private fun getTempSpecialisList(specialistList: List<DoctorDesignation>): ArrayList<String> {
        val tempSpecialistList = ArrayList<String>()
        tempSpecialistList.add("Select Specialist")

        specialistList.forEach { specialist -> tempSpecialistList.add(specialist.getCategoryName().toString()) }

        return tempSpecialistList
    }

    //get Doctor Designation Id------------------
    fun getDoctorDesignationId(selectItemName: String) {
        doctorDesignationArrayList.value!!.forEach { selectName ->
            if (selectName.getCategoryName().toString().equals(selectItemName, true)) {
                doctorDesignationId = selectName.getCoreCategoryID().toString()
                return
            }
        }
    }

    fun onSignUpClick(
        partnerReferenceId: String?,
        designationId: String?,
        specialistId: String?,
        instituteName: String?,
        bmdcNumber: String?,
        description: String?,
        degreeInfo: String?,
        newPatientFeesAmount: String?,
        returnPatientFeesAmount: String?,
        reportPatientFeesAmount: String?
    ) {
        val signUpDoctor = DoctorEnroll(
            partnerReferenceId,
            designationId,
            specialistId,
            instituteName,
            bmdcNumber,
            description,
            degreeInfo,
            newPatientFeesAmount,
            returnPatientFeesAmount,
            reportPatientFeesAmount
        )

     when {
            partnerReferenceId.isNullOrEmpty() -> {
                isPartnerReferenceId.value = true
            }
            designationId.isNullOrEmpty() -> {
                isDesignationId.value = true
            }
            specialistId.isNullOrEmpty() -> {
                isSpecialistId.value = true
            }

            instituteName.isNullOrEmpty() -> {
                isInstituteName.value = true
            }

            bmdcNumber.isNullOrEmpty() -> {
                isBmdcNumber.value = true
            }

            description.isNullOrEmpty() -> {
                isDescription.value = true
            }

            degreeInfo.isNullOrEmpty() -> {
                isDegreeInfo.value = true
            }

            newPatientFeesAmount.isNullOrEmpty() -> {
                isNewPatientFeesAmount.value = true
            }
            returnPatientFeesAmount.isNullOrEmpty() -> {
                isReturnPatientFeesAmount.value = true
            }

            reportPatientFeesAmount.isNullOrEmpty() -> {
                isReportPatientFeesAmount.value = true
            }
            else -> {
                doctorRepository.doctorRegistration(signUpDoctor)
            }
       }
    }

    fun getDoctorDesignation(){
        doctorRepository.getDoctorDesignation("T129", "0")
    }
    fun getSpecialist(){
        doctorRepository.getDoctorDesignation("T130", "0")
    }

    @Suppress("UNCHECKED_CAST")
    class DoctorViewModelFactory(private val application: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(DoctorViewModel::class.java)) {
                return DoctorViewModel(application) as T
            }

            throw IllegalStateException("Unknown ViewModel Class")
        }
    }
}