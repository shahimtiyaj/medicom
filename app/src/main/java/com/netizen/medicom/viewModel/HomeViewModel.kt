package com.netizen.medicom.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.netizen.medicom.repository.HomeRepository

class HomeViewModel(application: Application) : AndroidViewModel(application) {

    private val homeRepository = HomeRepository(application)

    val homePageInfoLiveData = homeRepository.homePageInfoLiveData
    val isPatientDashboardClicked = MutableLiveData<Boolean>()
    val isAddPatientClicked = MutableLiveData<Boolean>()
    val isPatientListClicked = MutableLiveData<Boolean>()
    val isStartCallClicked = MutableLiveData<Boolean>()
    val isOfferProductClicked = MutableLiveData<Boolean>()
    val isMessageLogClicked = MutableLiveData<Boolean>()
    val isBankClicked = MutableLiveData<Boolean>()
    val isRevenueReportClicked = MutableLiveData<Boolean>()
    val isGeneralProductReportClicked = MutableLiveData<Boolean>()
    val isStatementReportClicked = MutableLiveData<Boolean>()
    val isPurchaseCodeReportClicked = MutableLiveData<Boolean>()
    val isOfferProductReportClicked = MutableLiveData<Boolean>()
    val isWalletLogReportClicked = MutableLiveData<Boolean>()

    fun getHomePageInfo() {
        if (homePageInfoLiveData.value == null) {
            homeRepository.getHomePageInfo()
        }
    }

    @Suppress("UNCHECKED_CAST")
    class HomeViewModelFactory(private val application: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(HomeViewModel::class.java)) {
                return HomeViewModel(application) as T
            }

            throw IllegalStateException("Unknown ViewModel Class")
        }
    }
}