package com.netizen.medicom.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.medicom.R
import com.netizen.medicom.databinding.SinglePatientLayoutBinding
import com.netizen.medicom.model.PatientFamilyMemberList

class FamilyMemberAdapter(
    private val context: Context,
    private val familyMemberList: List<PatientFamilyMemberList.Item?>
) : RecyclerView.Adapter<FamilyMemberAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.single_patient_layout,
                parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return familyMemberList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            holder.bind(familyMemberList[position])
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
        }
    }

    class ViewHolder(private val itemBinding: SinglePatientLayoutBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(patientFamilyMemberList: PatientFamilyMemberList.Item?) {
            itemBinding.txtName.text = patientFamilyMemberList?.getPatientName()
            itemBinding.txtRelation.text = patientFamilyMemberList?.getRelation()
            itemBinding.txtAge.text = patientFamilyMemberList?.getAge()
            itemBinding.txtBloodGroup.text = patientFamilyMemberList?.getBloodGroup()
            itemBinding.txtId.text = patientFamilyMemberList?.getPatientId().toString()
            itemBinding.txtMobile.text = patientFamilyMemberList?.getPatientMobile()
            itemBinding.txtEmail.text = patientFamilyMemberList?.getPatientEmail()
            itemBinding.txtGender.text = patientFamilyMemberList?.getGender()
        }
    }

    companion object {
        private val TAG = FamilyMemberAdapter::class.java.simpleName
    }
}