package com.netizen.medicom.model


class MobileBankList {
    private var moblieBankAccountInfos: List<BankAccountInfo>? = null

    fun getBankLIst(): List<BankAccountInfo>? {
        return moblieBankAccountInfos
    }

    fun setBankList(moblieBankAccountInfos: List<BankAccountInfo>) {
        this.moblieBankAccountInfos = moblieBankAccountInfos
    }
}