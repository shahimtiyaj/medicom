package com.netizen.medicom.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class DepositPostData {

    @SerializedName("attachFileContent")
    @Expose
    private var attachFileContent: String? = null
    @SerializedName("attachFileName")
    @Expose
    private var attachFileName: String? = null
    @SerializedName("attachFileSaveOrEditable")
    @Expose
    private var attachFileSaveOrEditable: Boolean? = null
    @SerializedName("coreBankAccountInfoDTO")
    @Expose
    private var coreBankAccountInfoDTO: CoreBankAccountInfoDTO? = null
    @SerializedName("requestedAmount")
    @Expose
    private var requestedAmount: String? = null
    @SerializedName("requestNote")
    @Expose
    private var requestNote: Any? = null
    @SerializedName("fromWhere")
    @Expose
    private var fromWhere: String? = null
    @SerializedName("transactionNumber")
    @Expose
    private var transactionNumber: String? = null
    @SerializedName("transactionDate")
    @Expose
    private var transactionDate: String? = null
    @SerializedName("paymentType")
    @Expose
    private var paymentType: Any? = null

    fun getAttachFileContent(): String? {
        return attachFileContent
    }

    fun setAttachFileContent(attachFileContent: String) {
        this.attachFileContent = attachFileContent
    }

    fun getAttachFileName(): String? {
        return attachFileName
    }

    fun setAttachFileName(attachFileName: String) {
        this.attachFileName = attachFileName
    }

    fun getAttachFileSaveOrEditable(): Boolean? {
        return attachFileSaveOrEditable
    }

    fun setAttachFileSaveOrEditable(attachFileSaveOrEditable: Boolean) {
        this.attachFileSaveOrEditable = attachFileSaveOrEditable
    }

    fun getCoreBankAccountInfoDTO(): CoreBankAccountInfoDTO? {
        return coreBankAccountInfoDTO
    }

    fun setCoreBankAccountInfoDTO(coreBankAccountInfoDTO: CoreBankAccountInfoDTO) {
        this.coreBankAccountInfoDTO = coreBankAccountInfoDTO
    }

    fun getRequestedAmount(): String? {
        return requestedAmount
    }

    fun setRequestedAmount(requestedAmount: String) {
        this.requestedAmount = requestedAmount
    }

    fun getRequestNote(): Any? {
        return requestNote
    }

    fun setRequestNote(requestNote: Any) {
        this.requestNote = requestNote
    }

    fun getFromWhere(): String? {
        return fromWhere
    }

    fun setFromWhere(fromWhere: String) {
        this.fromWhere = fromWhere
    }

    fun getTransactionNumber(): String? {
        return transactionNumber
    }

    fun setTransactionNumber(transactionNumber: String) {
        this.transactionNumber = transactionNumber
    }

    fun getTransactionDate(): String? {
        return transactionDate
    }

    fun setTransactionDate(transactionDate: String) {
        this.transactionDate = transactionDate
    }

    fun getPaymentType(): Any? {
        return paymentType
    }

    fun setPaymentType(paymentType: Any) {
        this.paymentType = paymentType
    }

    class CoreBankAccountInfoDTO {

        @SerializedName("coreBankAccId")
        @Expose
        private var coreBankAccId: Int? = null

        fun getCoreBankAccId(): Int? {
            return coreBankAccId
        }

        fun setCoreBankAccId(coreBankAccId: Int?) {
            this.coreBankAccId = coreBankAccId
        }
    }
}