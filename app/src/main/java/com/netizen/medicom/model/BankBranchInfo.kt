package com.netizen.medicom.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class BankBranchInfo {

    @SerializedName("branchID")
    @Expose
    private var branchID: Int? = null
    @SerializedName("branchName")
    @Expose
    private var branchName: String? = null
    @SerializedName("routingNumber")
    @Expose
    private var routingNumber: String? = null
    @SerializedName("detailsNote")
    @Expose
    private var detailsNote: String? = null

    fun getBranchID(): Int? {
        return branchID
    }

    fun setBranchID(branchID: Int?) {
        this.branchID = branchID
    }

    fun getBranchName(): String? {
        return branchName
    }

    fun setBranchName(branchName: String?) {
        this.branchName = branchName
    }

    fun getRoutingNumber(): String? {
        return routingNumber
    }

    fun setRoutingNumber(routingNumber: String?) {
        this.routingNumber = routingNumber
    }

    fun getDetailsNote(): String? {
        return detailsNote
    }

    fun setDetailsNote(detailsNote: String?) {
        this.detailsNote = detailsNote
    }
}