package com.netizen.medicom.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class MessageType1 {

    @SerializedName("productRoleAssignID")
    @Expose
     var productRoleAssignID: String? = null

    @SerializedName("productInfoDTO")
    @Expose
     var productInfoDTO: ProductInfoDTO? = null

    constructor(productRoleAssignID: String?, productInfoDTO: ProductInfoDTO?) {
        this.productRoleAssignID = productRoleAssignID
        this.productInfoDTO = productInfoDTO
    }

    class ProductInfoDTO {

        @SerializedName("productID")
        @Expose
        private var productID: Long? = null

        @SerializedName("productName")
        @Expose
        private var productName: String? = null

        @SerializedName("salesPrice")
        @Expose
        private var salesPrice: Double? = null

        @SerializedName("percentVat")
        @Expose
        private var percentVat: Double? = null


        constructor() {
        }

        constructor(productID: Long?, productName: String?) {
            this.productID = productID
            this.productName = productName
        }


        fun getProductID(): Long? {
            return productID
        }

        fun setProductID(productID: Long?) {
            this.productID = productID
        }

        fun getProductName(): String? {
            return productName
        }

        fun setProductName(productName: String?) {
            this.productName = productName
        }

        fun getSalesPrice(): Double? {
            return salesPrice
        }

        fun setSalesPrice(salesPrice: Double?) {
            this.salesPrice = salesPrice
        }

        fun getPercentVat(): Double? {
            return percentVat
        }

        fun setPercentVat(percentVat: Double?) {
            this.percentVat = percentVat
        }

    }
}