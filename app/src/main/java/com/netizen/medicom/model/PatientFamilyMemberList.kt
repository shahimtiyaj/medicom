package com.netizen.medicom.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class PatientFamilyMemberList {
    @SerializedName("message")
    @Expose
    private var message: String? = null

    @SerializedName("msgType")
    @Expose
    private var msgType: Int? = null

    @SerializedName("item")
    @Expose
    private var item: List<Item?>? = null

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String?) {
        this.message = message
    }

    fun getMsgType(): Int? {
        return msgType
    }

    fun setMsgType(msgType: Int?) {
        this.msgType = msgType
    }

    fun getItem(): List<Item?>? {
        return item
    }

    fun setItem(item: List<Item?>?) {
        this.item = item
    }


    class Item {
        @SerializedName("patientId")
        @Expose
        private var patientId: Int? = null

        @SerializedName("patientName")
        @Expose
        private var patientName: String? = null

        @SerializedName("patientEmail")
        @Expose
        private var patientEmail: String? = null

        @SerializedName("patientMobile")
        @Expose
        private var patientMobile: String? = null

        @SerializedName("patientDob")
        @Expose
        private var patientDob: String? = null

        @SerializedName("gender")
        @Expose
        private var gender: String? = null

        @SerializedName("bloodGroup")
        @Expose
        private var bloodGroup: String? = null

        @SerializedName("relation")
        @Expose
        private var relation: String? = null

        @SerializedName("religion")
        @Expose
        private var religion: String? = null

        @SerializedName("age")
        @Expose
        private var age: String? = null

        fun getPatientId(): Int? {
            return patientId
        }

        fun setPatientId(patientId: Int?) {
            this.patientId = patientId
        }

        fun getPatientName(): String? {
            return patientName
        }

        fun setPatientName(patientName: String?) {
            this.patientName = patientName
        }

        fun getPatientEmail(): String? {
            return patientEmail
        }

        fun setPatientEmail(patientEmail: String?) {
            this.patientEmail = patientEmail
        }

        fun getPatientMobile(): String? {
            return patientMobile
        }

        fun setPatientMobile(patientMobile: String?) {
            this.patientMobile = patientMobile
        }

        fun getPatientDob(): String? {
            return patientDob
        }

        fun setPatientDob(patientDob: String?) {
            this.patientDob = patientDob
        }

        fun getGender(): String? {
            return gender
        }

        fun setGender(gender: String?) {
            this.gender = gender
        }

        fun getBloodGroup(): String? {
            return bloodGroup
        }

        fun setBloodGroup(bloodGroup: String?) {
            this.bloodGroup = bloodGroup
        }

        fun getRelation(): String? {
            return relation
        }

        fun setRelation(relation: String?) {
            this.relation = relation
        }

        fun getReligion(): String? {
            return religion
        }

        fun setReligion(religion: String?) {
            this.religion = religion
        }

        fun getAge(): String? {
            return age
        }

        fun setAge(age: String?) {
            this.age = age
        }

    }
}