package com.netizen.medicom.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class AddPatientFamilyMember {

    @SerializedName("bloodGroup")
    @Expose
    private var bloodGroup: String? = null

    @SerializedName("gender")
    @Expose
    private var gender: String? = null

    @SerializedName("patientDob")
    @Expose
    private var patientDob: String? = null

    @SerializedName("patientEmail")
    @Expose
    private var patientEmail: String? = null

    @SerializedName("patientMobile")
    @Expose
    private var patientMobile: String? = null

    @SerializedName("patientName")
    @Expose
    private var patientName: String? = null

    @SerializedName("relation")
    @Expose
    private var relation: String? = null

    @SerializedName("religion")
    @Expose
    private var religion: String? = null

    constructor() {
    }

    constructor(
        relation: String?,
        patientName: String?,
        patientMobile: String?,
        patientEmail: String?,
        patientDob: String?,
        gender: String?,
        bloodGroup: String?,
        religion: String?

    ) {
        this.relation = relation
        this.patientName = patientName
        this.patientMobile = patientMobile
        this.patientEmail = patientEmail
        this.patientDob = patientDob
        this.bloodGroup = bloodGroup
        this.gender = gender
        this.religion = religion
    }

    fun getBloodGroup(): String? {
        return bloodGroup
    }

    fun setBloodGroup(bloodGroup: String?) {
        this.bloodGroup = bloodGroup
    }

    fun getGender(): String? {
        return gender
    }

    fun setGender(gender: String?) {
        this.gender = gender
    }

    fun getPatientDob(): String? {
        return patientDob
    }

    fun setPatientDob(patientDob: String?) {
        this.patientDob = patientDob
    }

    fun getPatientEmail(): String? {
        return patientEmail
    }

    fun setPatientEmail(patientEmail: String?) {
        this.patientEmail = patientEmail
    }

    fun getPatientMobile(): String? {
        return patientMobile
    }

    fun setPatientMobile(patientMobile: String?) {
        this.patientMobile = patientMobile
    }

    fun getPatientName(): String? {
        return patientName
    }

    fun setPatientName(patientName: String?) {
        this.patientName = patientName
    }

    fun getRelation(): String? {
        return relation
    }

    fun setRelation(relation: String?) {
        this.relation = relation
    }

    fun getReligion(): String? {
        return religion
    }

    fun setReligion(religion: String?) {
        this.religion = religion
    }

}