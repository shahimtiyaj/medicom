package com.netizen.medicom.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class BalanceDepositReportGetData {

    @SerializedName("balanceRequestLogId")
    @Expose
    private var balanceRequestLogId: Int? = null
    @SerializedName("requestStatus")
    @Expose
    private var requestStatus: String? = null
    @SerializedName("requestType")
    @Expose
    private var requestType: String? = null
    @SerializedName("requestDate")
    @Expose
    private var requestDate: String? = null
    @SerializedName("transactionDate")
    @Expose
    private var transactionDate: String? = null
    @SerializedName("requestedAmount")
    @Expose
    private var requestedAmount: Int? = null
    @SerializedName("actualAmount")
    @Expose
    private var actualAmount: Int? = null
    @SerializedName("serviceChargeAmount")
    @Expose
    private var serviceChargeAmount: Int? = null
    @SerializedName("requestNote")
    @Expose
    private var requestNote: String? = null
    @SerializedName("approveDate")
    @Expose
    private var approveDate: Any? = null
    @SerializedName("approveNote")
    @Expose
    private var approveNote: Any? = null
    @SerializedName("attachFileName")
    @Expose
    private var attachFileName: String? = null
    @SerializedName("attachFilePath")
    @Expose
    private var attachFilePath: String? = null
    @SerializedName("transactionNumber")
    @Expose
    private var transactionNumber: Any? = null
    @SerializedName("fromWhere")
    @Expose
    private var fromWhere: String? = null
    @SerializedName("bank")
    @Expose
    private var bank: String? = null
    @SerializedName("accountNumber")
    @Expose
    private var accountNumber: String? = null

    fun getBalanceRequestLogId(): Int? {
        return balanceRequestLogId
    }

    fun setBalanceRequestLogId(balanceRequestLogId: Int?) {
        this.balanceRequestLogId = balanceRequestLogId
    }

    fun getRequestStatus(): String? {
        return requestStatus
    }

    fun setRequestStatus(requestStatus: String?) {
        this.requestStatus = requestStatus
    }

    fun getRequestType(): String? {
        return requestType
    }

    fun setRequestType(requestType: String?) {
        this.requestType = requestType
    }

    fun getRequestDate(): String? {
        return requestDate
    }

    fun setRequestDate(requestDate: String?) {
        this.requestDate = requestDate
    }

    fun getTransactionDate(): String? {
        return transactionDate
    }

    fun setTransactionDate(transactionDate: String?) {
        this.transactionDate = transactionDate
    }

    fun getRequestedAmount(): Int? {
        return requestedAmount
    }

    fun setRequestedAmount(requestedAmount: Int?) {
        this.requestedAmount = requestedAmount
    }

    fun getActualAmount(): Int? {
        return actualAmount
    }

    fun setActualAmount(actualAmount: Int?) {
        this.actualAmount = actualAmount
    }

    fun getServiceChargeAmount(): Int? {
        return serviceChargeAmount
    }

    fun setServiceChargeAmount(serviceChargeAmount: Int?) {
        this.serviceChargeAmount = serviceChargeAmount
    }

    fun getRequestNote(): String? {
        return requestNote
    }

    fun setRequestNote(requestNote: String?) {
        this.requestNote = requestNote
    }

    fun getApproveDate(): Any? {
        return approveDate
    }

    fun setApproveDate(approveDate: Any?) {
        this.approveDate = approveDate
    }

    fun getApproveNote(): Any? {
        return approveNote
    }

    fun setApproveNote(approveNote: Any?) {
        this.approveNote = approveNote
    }

    fun getAttachFileName(): String? {
        return attachFileName
    }

    fun setAttachFileName(attachFileName: String?) {
        this.attachFileName = attachFileName
    }

    fun getAttachFilePath(): String? {
        return attachFilePath
    }

    fun setAttachFilePath(attachFilePath: String?) {
        this.attachFilePath = attachFilePath
    }

    fun getTransactionNumber(): Any? {
        return transactionNumber
    }

    fun setTransactionNumber(transactionNumber: Any?) {
        this.transactionNumber = transactionNumber
    }

    fun getFromWhere(): String? {
        return fromWhere
    }

    fun setFromWhere(fromWhere: String?) {
        this.fromWhere = fromWhere
    }

    fun getBank(): String? {
        return bank
    }

    fun setBank(bank: String?) {
        this.bank = bank
    }

    fun getAccountNumber(): String? {
        return accountNumber
    }

    fun setAccountNumber(accountNumber: String?) {
        this.accountNumber = accountNumber
    }
}