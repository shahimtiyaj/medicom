package com.netizen.medicom.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class WithdrawPostData {

    @SerializedName("requestedAmount")
    @Expose
    private var requestedAmount: String? = null
    @SerializedName("requestNote")
    @Expose
    private var requestNote: String? = null
    @SerializedName("userBankAccountInfoDTO")
    @Expose
    private var userBankAccountInfoDTO: UserBankAccountInfoDTO? = null

    fun getRequestedAmount(): String? {
        return requestedAmount
    }

    fun setRequestedAmount(requestedAmount: String?) {
        this.requestedAmount = requestedAmount
    }

    fun getRequestNote(): String? {
        return requestNote
    }

    fun setRequestNote(requestNote: String?) {
        this.requestNote = requestNote
    }

    fun getUserBankAccountInfoDTO(): UserBankAccountInfoDTO? {
        return userBankAccountInfoDTO
    }

    fun setUserBankAccountInfoDTO(userBankAccountInfoDTO: UserBankAccountInfoDTO?) {
        this.userBankAccountInfoDTO = userBankAccountInfoDTO
    }

    class UserBankAccountInfoDTO {

        @SerializedName("userBankAccId")
        @Expose
        private var userBankAccId: Int? = null

        fun getUserBankAccId(): Int? {
            return userBankAccId
        }

        fun setUserBankAccId(userBankAccId: Int?) {
            this.userBankAccId = userBankAccId
        }
    }
}