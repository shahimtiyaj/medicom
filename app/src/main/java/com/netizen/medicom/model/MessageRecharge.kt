package com.netizen.medicom.model

import com.google.gson.annotations.SerializedName

class MessageRecharge {

    @SerializedName("quantity")
    var requestedAmount: Int? = 0

    @SerializedName("productInfoDTO")
    var productInfoDTO: ProductInfoDTO? = null

    @SerializedName("productPurchaseLogDTO")
    var productPurchaseLogDTO: ProductPurchaseLogDTO? = null



    constructor() {
    }

    constructor(
        requestedAmount: Int?,
        productInfoDTO: ProductInfoDTO?,
        productPurchaseLogDTO: ProductPurchaseLogDTO
    ) {
        this.requestedAmount = requestedAmount
        this.productInfoDTO = productInfoDTO
        this.productPurchaseLogDTO = productPurchaseLogDTO
    }
}


class ProductInfoDTO {
    @SerializedName("productID")
    var productID: Int? = 0

    constructor(productID: Int?) {
        this.productID = productID
    }
}

class ProductRoleAssignDTO {
    @SerializedName("productRoleAssignID")
    var productRoleAssignID: Int? = 0

    constructor(productRoleAssignID: Int?) {
        this.productRoleAssignID = productRoleAssignID
    }
}

class ProductPurchaseLogDTO {

    @SerializedName("productRoleAssignDTO")
    var productRoleAssignDTO: ProductRoleAssignDTO? = null


    constructor(
        productRoleAssignDTO: ProductRoleAssignDTO?
    ) {
        this.productRoleAssignDTO = productRoleAssignDTO
    }
}