package com.netizen.medicom.model

import com.google.gson.annotations.SerializedName


class BalanceTransferGetData {

    @SerializedName("transactionDate")
    var transactionDate: String? = null

    @SerializedName("transactionFor")
    var transactionFor: String? = null

    @SerializedName("customID")
    var netiID: Long? = null

    @SerializedName("fullName")
    var fullName: String? = null

    @SerializedName("basicMobile")
    var basicMobile: String? = null

    @SerializedName("note")
    var note: String? = null

    @SerializedName("amount")
    var amount: Double? = null

    constructor() {

    }

    constructor(
        transactionDate: String?,
        transactionFor: String?,
        netiID: Long?,
        fullName: String?,
        basicMobile: String?,
        note: String?,
        amount: Double?
    ) {
        this.transactionDate = transactionDate
        this.transactionFor = transactionFor
        this.netiID = netiID
        this.fullName = fullName
        this.basicMobile = basicMobile
        this.note = note
        this.amount = amount
    }

}

