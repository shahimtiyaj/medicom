package com.netizen.medicom.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DoctorEnroll {

    @SerializedName("parterReferenceId")
    @Expose
    private var parterReferenceId: String? = null

    @SerializedName("designationId")
    @Expose
    private var designationId: String? = null

    @SerializedName("specialistId")
    @Expose
    private var specialistId: String? = null

    @SerializedName("instituteName")
    @Expose
    private var instituteName: String? = null

    @SerializedName("bmdcNumber")
    @Expose
    private var bmdcNumber: String? = null

    @SerializedName("description")
    @Expose
    private var description: String? = null

    @SerializedName("degreeInfo")
    @Expose
    private var degreeInfo: String? = null

    @SerializedName("newPatientFeesAmount")
    @Expose
    private var newPatientFeesAmount: String? = null

    @SerializedName("returnPatientFeesAmount")
    @Expose
    private var returnPatientFeesAmount: String? = null

    @SerializedName("reportPatientFeesAmount")
    @Expose
    private var reportPatientFeesAmount: String? = null

    constructor() {}
    constructor(
        parterReferenceId: String?,
        designationId: String?,
        specialistId: String?,
        instituteName: String?,
        bmdcNumber: String?,
        description: String?,
        degreeInfo: String?,
        newPatientFeesAmount: String?,
        returnPatientFeesAmount: String?,
        reportPatientFeesAmount: String?
    ) {
        this.parterReferenceId = parterReferenceId
        this.designationId = designationId
        this.specialistId = specialistId
        this.instituteName = instituteName
        this.bmdcNumber = bmdcNumber
        this.description = description
        this.degreeInfo = degreeInfo
        this.newPatientFeesAmount = newPatientFeesAmount
        this.returnPatientFeesAmount = returnPatientFeesAmount
        this.reportPatientFeesAmount = reportPatientFeesAmount
    }


    fun getParterReferenceId(): String? {
        return parterReferenceId
    }

    fun setParterReferenceId(parterReferenceId: String?) {
        this.parterReferenceId = parterReferenceId
    }

    fun getDesignationId(): String? {
        return designationId
    }

    fun setDesignationId(designationId: String?) {
        this.designationId = designationId
    }

    fun getSpecialistId(): String? {
        return specialistId
    }

    fun setSpecialistId(specialistId: String?) {
        this.specialistId = specialistId
    }

    fun getInstituteName(): String? {
        return instituteName
    }

    fun setInstituteName(instituteName: String?) {
        this.instituteName = instituteName
    }

    fun getBmdcNumber(): String? {
        return bmdcNumber
    }

    fun setBmdcNumber(bmdcNumber: String?) {
        this.bmdcNumber = bmdcNumber
    }

    fun getDescription(): String? {
        return description
    }

    fun setDescription(description: String?) {
        this.description = description
    }

    fun getDegreeInfo(): String? {
        return degreeInfo
    }

    fun setDegreeInfo(degreeInfo: String?) {
        this.degreeInfo = degreeInfo
    }

    fun getNewPatientFeesAmount(): String? {
        return newPatientFeesAmount
    }

    fun setNewPatientFeesAmount(newPatientFeesAmount: String?) {
        this.newPatientFeesAmount = newPatientFeesAmount
    }

    fun getReturnPatientFeesAmount(): String? {
        return returnPatientFeesAmount
    }

    fun setReturnPatientFeesAmount(returnPatientFeesAmount: String?) {
        this.returnPatientFeesAmount = returnPatientFeesAmount
    }

    fun getReportPatientFeesAmount(): String? {
        return reportPatientFeesAmount
    }

    fun setReportPatientFeesAmount(reportPatientFeesAmount: String?) {
        this.reportPatientFeesAmount = reportPatientFeesAmount
    }
}