package com.netizen.medicom.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class TokenList {

    @SerializedName("tokenInfoID")
    @Expose
    private var tokenInfoID: Int? = null

    @SerializedName("customTokenID")
    @Expose
    private var customTokenID: String? = null

    @SerializedName("tokenDetails")
    @Expose
    private var tokenDetails: String? = null

    @SerializedName("attachmentName")
    @Expose
    private var attachmentName: String? = null

    @SerializedName("attachmentPath")
    @Expose
    private var attachmentPath: String? = null

    @SerializedName("createDate")
    @Expose
    private var createDate: String? = null

    @SerializedName("tokenContact")
    @Expose
    private var tokenContact: String? = null

    @SerializedName("tokenStatus")
    @Expose
    private var tokenStatus: Int? = null

    @SerializedName("ratingDate")
    @Expose
    private var ratingDate: Any? = null

    @SerializedName("ratingMark")
    @Expose
    private var ratingMark: Int? = null

    @SerializedName("ratingMessage")
    @Expose
    private var ratingMessage: Any? = null

    @SerializedName("solveDate")
    @Expose
    private var solveDate: String? = null

    @SerializedName("solveMessage")
    @Expose
    private var solveMessage: Any? = null

    @SerializedName("tokenTypeInfoDTO")
    @Expose
    private var tokenTypeInfoDTO: TokenTypeInfoDTO? = null

    @SerializedName("attachContent")
    @Expose
    private var attachContent: Any? = null

    @SerializedName("attachSaveOrEditable")
    @Expose
    private var attachSaveOrEditable: Boolean? = null


    fun getTokenInfoID(): Int? {
        return tokenInfoID
    }

    fun setTokenInfoID(tokenInfoID: Int?) {
        this.tokenInfoID = tokenInfoID
    }

    fun getCustomTokenID(): String? {
        return customTokenID
    }

    fun setCustomTokenID(customTokenID: String?) {
        this.customTokenID = customTokenID
    }

    fun getTokenDetails(): String? {
        return tokenDetails
    }

    fun setTokenDetails(tokenDetails: String?) {
        this.tokenDetails = tokenDetails
    }

    fun getAttachmentName(): String? {
        return attachmentName
    }

    fun setAttachmentName(attachmentName: String?) {
        this.attachmentName = attachmentName
    }

    fun getAttachmentPath(): String? {
        return attachmentPath
    }

    fun setAttachmentPath(attachmentPath: String?) {
        this.attachmentPath = attachmentPath
    }

    fun getCreateDate(): String? {
        return createDate
    }

    fun setCreateDate(createDate: String?) {
        this.createDate = createDate
    }

    fun getTokenContact(): String? {
        return tokenContact
    }

    fun setTokenContact(tokenContact: String?) {
        this.tokenContact = tokenContact
    }

    fun getTokenStatus(): Int? {
        return tokenStatus
    }

    fun setTokenStatus(tokenStatus: Int?) {
        this.tokenStatus = tokenStatus
    }

    fun getRatingDate(): Any? {
        return ratingDate
    }

    fun setRatingDate(ratingDate: Any?) {
        this.ratingDate = ratingDate
    }

    fun getRatingMark(): Int? {
        return ratingMark
    }

    fun setRatingMark(ratingMark: Int?) {
        this.ratingMark = ratingMark
    }

    fun getRatingMessage(): Any? {
        return ratingMessage
    }

    fun setRatingMessage(ratingMessage: Any?) {
        this.ratingMessage = ratingMessage
    }

    fun getSolveDate(): String? {
        return solveDate
    }

    fun setSolveDate(solveDate: String?) {
        this.solveDate = solveDate
    }

    fun getSolveMessage(): Any? {
        return solveMessage
    }

    fun setSolveMessage(solveMessage: Any?) {
        this.solveMessage = solveMessage
    }

    fun getTokenTypeInfoDTO(): TokenTypeInfoDTO? {
        return tokenTypeInfoDTO
    }

    fun setTokenTypeInfoDTO(tokenTypeInfoDTO: TokenTypeInfoDTO?) {
        this.tokenTypeInfoDTO = tokenTypeInfoDTO
    }

    fun getAttachContent(): Any? {
        return attachContent
    }

    fun setAttachContent(attachContent: Any?) {
        this.attachContent = attachContent
    }

    fun getAttachSaveOrEditable(): Boolean? {
        return attachSaveOrEditable
    }

    fun setAttachSaveOrEditable(attachSaveOrEditable: Boolean?) {
        this.attachSaveOrEditable = attachSaveOrEditable
    }

    class TokenTypeInfoDTO {

        @SerializedName("coreCategoryID")
        @Expose
        private var coreCategoryID: Int? = null

        @SerializedName("categoryDefaultCode")
        @Expose
        private var categoryDefaultCode: String? = null

        @SerializedName("categoryName")
        @Expose
        private var categoryName: String? = null

        fun getCoreCategoryID(): Int? {
            return coreCategoryID
        }

        fun setCoreCategoryID(coreCategoryID: Int?) {
            this.coreCategoryID = coreCategoryID
        }

        fun getCategoryDefaultCode(): String? {
            return categoryDefaultCode
        }

        fun setCategoryDefaultCode(categoryDefaultCode: String?) {
            this.categoryDefaultCode = categoryDefaultCode
        }

        fun getCategoryName(): String? {
            return categoryName
        }

        fun setCategoryName(categoryName: String?) {
            this.categoryName = categoryName
        }
    }
}