package com.netizen.medicom.model

class UserLogin1() {
    private var userId: String? = null
    private var userName: String? = null
    private var userPassword: String? = null
    private var check: String? = null

    constructor(userId: String, userName: String, userPassword: String) : this() {
        this.userId = userId
        this.userName = userName
        this.userPassword = userPassword
    }

    fun getuserId(): String? {
        return userName
    }

    fun setuserId(userName: String) {
        this.userName = userName
    }

    fun getuserName(): String? {
        return userName
    }

    fun setuserName(userName: String) {
        this.userName = userName
    }

    fun getuserPassword(): String? {
        return userPassword
    }

    fun setuserPassword(userPassword: String) {
        this.userPassword = userPassword
    }

    fun getcheck(): String? {
        return check
    }

    fun setcheck(check: String) {
        this.check = check
    }
}