package com.netizen.medicom.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class OfferProductPost {

    @SerializedName("actualUnitPrice")
    @Expose
    private var actualUnitPrice: Double? = null

    @SerializedName("payableAmount")
    @Expose
    private var payableAmount: Double? = null

    @SerializedName("totalDiscount")
    @Expose
    private var totalDiscount: Double? = null

    @SerializedName("totalPrice")
    @Expose
    private var totalPrice: Double? = null

    @SerializedName("productOfferDTO")
    var productOfferDTO: ProductOfferDTO? = null

    constructor() {
    }

    constructor(
        actualUnitPrice: Double?,
        payableAmount: Double?,
        totalDiscount: Double?,
        totalPrice: Double?,
        productOfferDTO: ProductOfferDTO?
    ) {
        this.actualUnitPrice = actualUnitPrice
        this.payableAmount = payableAmount
        this.totalDiscount = totalDiscount
        this.totalPrice = totalPrice
        this.productOfferDTO = productOfferDTO
    }


    class ProductOfferDTO {
        @SerializedName("productOfferID")
        var productOfferID: Int? = 0

        constructor(productOfferID: Int?) {
            this.productOfferID = productOfferID
        }
    }
}



