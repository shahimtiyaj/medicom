package com.netizen.medicom.model

import com.google.gson.annotations.SerializedName

class ProfileInformation {
    
    @SerializedName("netiID")
     var netiID: Int? = null

    @SerializedName("customNetiID")
     var customNetiID: String? = null

    @SerializedName("fullName")
    var fullName: String? = null

    @SerializedName("gender")
     var gender: String? = null

    @SerializedName("religion")
     var religion: String? = null

    @SerializedName("dateOfBirth")
     var dateOfBirth: String? = null

    @SerializedName("basicMobile")
     var basicMobile: String? = null

    @SerializedName("basicEmail")
     var basicEmail: String? = null

    @SerializedName("imagePath")
     var imagePath: String? = null

    @SerializedName("imageName")
     var imageName: String? = null

    @SerializedName("userWalletBalance")
     var userWalletBalance: Float? = null

    @SerializedName("userReserveBalance")
     var userReserveBalance: Float? = null

    @SerializedName("smsBalance")
     var smsBalance: Float? = null

    @SerializedName("area")
     var area: String? = null
  @SerializedName("upazilla")
     var upazilla: String? = null

    @SerializedName("district")
     var district: String? = null

    @SerializedName("division")
     var division: String? = null

    @SerializedName("userStatus")
    var userStatus: String? = null


    fun getCustomNetiId(): String? {
        return customNetiID
    }

    fun setCustomNetiId(customNetiID: String) {
        this.customNetiID = customNetiID
    }

}
