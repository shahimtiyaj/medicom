package com.netizen.medicom.model

import com.google.gson.annotations.SerializedName

class ProfileImage {

    @SerializedName("fileContent")
    var fileContent: String? = null
}
