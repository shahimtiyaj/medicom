package com.netizen.medicom.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class TagPostData {

    @SerializedName("userBasicInfoDTO")
    @Expose
    private var userBasicInfoDTO: UserBasicInfoDTO? = null

    @SerializedName("userBankAccountTaggingDTO")
    @Expose
    private var userBankAccountTaggingDTO: UserBankAccountTaggingDTO? = null

    @SerializedName("taggingTypeDefaultCode")
    @Expose
    private var taggingTypeDefaultCode: String? = null

    fun getUserBasicInfoDTO(): UserBasicInfoDTO? {
        return userBasicInfoDTO
    }

    fun setUserBasicInfoDTO(userBasicInfoDTO: UserBasicInfoDTO?) {
        this.userBasicInfoDTO = userBasicInfoDTO
    }

    fun getUserBankAccountTaggingDTO(): UserBankAccountTaggingDTO? {
        return userBankAccountTaggingDTO
    }

    fun setUserBankAccountTaggingDTO(userBankAccountTaggingDTO: UserBankAccountTaggingDTO?) {
        this.userBankAccountTaggingDTO = userBankAccountTaggingDTO
    }

    fun getTaggingTypeDefaultCode(): String? {
        return taggingTypeDefaultCode
    }

    fun setTaggingTypeDefaultCode(taggingTypeDefaultCode: String?) {
        this.taggingTypeDefaultCode = taggingTypeDefaultCode
    }

    class UserBankAccountTaggingDTO {

        @SerializedName("userBankAccountInfoDTO")
        @Expose
        private var userBankAccountInfoDTO: UserBankAccountInfoDTO? = null

        fun getUserBankAccountInfoDTO(): UserBankAccountInfoDTO? {
            return userBankAccountInfoDTO
        }

        fun setUserBankAccountInfoDTO(userBankAccountInfoDTO: UserBankAccountInfoDTO?) {
            this.userBankAccountInfoDTO = userBankAccountInfoDTO
        }

        class UserBankAccountInfoDTO {

            @SerializedName("userBankAccId")
            @Expose
            private var userBankAccId: Int? = null

            fun getUserBankAccId(): Int? {
                return userBankAccId
            }

            fun setUserBankAccId(userBankAccId: Int?) {
                this.userBankAccId = userBankAccId
            }
        }
    }

    class UserBasicInfoDTO {

        @SerializedName("netiID")
        @Expose
        private var netiID: Int? = null

        fun getNetiID(): Int? {
            return netiID
        }

        fun setNetiID(netiID: Int?) {
            this.netiID = netiID
        }
    }
}