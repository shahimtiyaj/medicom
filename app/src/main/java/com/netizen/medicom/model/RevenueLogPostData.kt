package com.netizen.medicom.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class RevenueLogPostData {

    @SerializedName("startDate")
    @Expose
    private var startDate: String? = null
    @SerializedName("endDate")
    @Expose
    private var endDate: String? = null

    fun getStartDate(): String? {
        return startDate
    }

    fun setStartDate(startDate: String?) {
        this.startDate = startDate
    }

    fun getEndDate(): String? {
        return endDate
    }

    fun setEndDate(endDate: String?) {
        this.endDate = endDate
    }
}