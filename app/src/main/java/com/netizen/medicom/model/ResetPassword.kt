package com.netizen.medicom.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class ResetPassword {

    @SerializedName("userName")
    @Expose
    private var userName: String? = null

    @SerializedName("password")
    @Expose
    private var password: String? = null

    @SerializedName("newPassword")
    @Expose
    private var newPassword: String? = null

    constructor(userName: String?, password: String?, newPassword: String?) {
        this.userName = userName
        this.password = password
        this.newPassword = newPassword
    }

    constructor(password: String?, newPassword: String?) {
        this.password = password
        this.newPassword = newPassword
    }

    fun getUserName(): String? {
        return userName
    }

    fun setUserName(userName: String?) {
        this.userName = userName
    }

    fun getPassword(): String? {
        return password
    }

    fun setPassword(password: String?) {
        this.password = password
    }

    fun getNewPassword(): String? {
        return newPassword
    }

    fun setNewPassword(newPassword: String?) {
        this.newPassword = newPassword
    }
}