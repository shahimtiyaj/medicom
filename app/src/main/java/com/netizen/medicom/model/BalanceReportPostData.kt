package com.netizen.medicom.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class BalanceReportPostData {

    @SerializedName("requestStartDate")
    @Expose
    private var requestStartDate: String? = null
    @SerializedName("requestEndDate")
    @Expose
    private var requestEndDate: String? = null
    @SerializedName("requestType")
    @Expose
    private var requestType: String? = null
    @SerializedName("transactionType")
    @Expose
    private var transactionType: String? = null
    @SerializedName("status")
    @Expose
    private var status: Int? = null
    @SerializedName("limit")
    @Expose
    private var limit: Int? = null

    fun getRequestStartDate(): String? {
        return requestStartDate
    }

    fun setRequestStartDate(requestStartDate: String?) {
        this.requestStartDate = requestStartDate
    }

    fun getRequestEndDate(): String? {
        return requestEndDate
    }

    fun setRequestEndDate(requestEndDate: String?) {
        this.requestEndDate = requestEndDate
    }

    fun getRequestType(): String? {
        return requestType
    }

    fun setRequestType(requestType: String?) {
        this.requestType = requestType
    }

    fun getTransactionType(): String? {
        return transactionType
    }

    fun setTransactionType(transactionType: String?) {
        this.transactionType = transactionType
    }

    fun getStatus(): Int? {
        return status
    }

    fun setStatus(status: Int?) {
        this.status = status
    }

    fun getLimit(): Int? {
        return limit
    }

    fun setLimit(limit: Int?) {
        this.limit = limit
    }
}