package com.netizen.medicom.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class PatientEnrollResponse {

    @SerializedName("message")
    @Expose
    private var message: String? = null

    @SerializedName("msgType")
    @Expose
    private var msgType: Int? = null

    @SerializedName("item")
    @Expose
    private var item: Any? = null

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String?) {
        this.message = message
    }

    fun getMsgType(): Int? {
        return msgType
    }

    fun setMsgType(msgType: Int?) {
        this.msgType = msgType
    }

    fun getItem(): Any? {
        return item
    }

    fun setItem(item: Any?) {
        this.item = item
    }

}