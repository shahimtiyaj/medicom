package com.netizen.medicom.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class Info {

    @SerializedName("netiID")
    @Expose
    private var netiID: Int? = null

    @SerializedName("customNetiID")
    @Expose
    private var customNetiID: Int? = null

    @SerializedName("categoryName") //---------------------
    @Expose
    private var categoryName: String? = null

    @SerializedName("fullName")
    @Expose
    private var fullName: String? = null

    @SerializedName("gender")
    @Expose
    private var gender: String? = null

    @SerializedName("religion")
    @Expose
    private var religion: String? = null

    @SerializedName("dateOfBirth")
    @Expose
    private var dateOfBirth: String? = null

    @SerializedName("basicMobile")
    @Expose
    private var basicMobile: String? = null

    @SerializedName("basicEmail")
    @Expose
    private var basicEmail: String? = null

    @SerializedName("imagePath")
    @Expose
    private var imagePath: String? = null

    @SerializedName("imageName")
    @Expose
    private var imageName: String? = null

    @SerializedName("userWalletBalance")
    @Expose
    private var userWalletBalance: Double? = null

    @SerializedName("userReserveBalance")
    @Expose
    private var userReserveBalance: Double? = null

    @SerializedName("smsBalance")
    @Expose
    private var smsBalance: Double? = null

    @SerializedName("voiceBalance")
    @Expose
    private var voiceBalance: Double? = null

    @SerializedName("emailBalance")
    @Expose
    private var emailBalance: Double? = null

    @SerializedName("validationStatus")
    @Expose
    private var validationStatus: Int? = null

    @SerializedName("userEnableStatus")
    @Expose
    private var userEnableStatus: Int? = null

    @SerializedName("registrationDate")
    @Expose
    private var registrationDate: Date? = null


    fun getNetiID(): Int? {
        return netiID
    }

    fun setNetiID(netiID: Int?) {
        this.netiID = netiID
    }

    fun getCustomNetiID(): Int? {
        return customNetiID
    }

    fun setCustomNetiID(customNetiID: Int?) {
        this.customNetiID = customNetiID
    }

    fun getFullName(): String? {
        return fullName
    }

    fun setFullName(fullName: String) {
        this.fullName = fullName
    }

    fun getGender(): String? {
        return gender
    }

    fun setGender(gender: String) {
        this.gender = gender
    }

    fun getReligion(): String? {
        return religion
    }

    fun setReligion(religion: String) {
        this.religion = religion
    }

    fun getDateOfBirth(): String? {
        return dateOfBirth
    }

    fun setDateOfBirth(dateOfBirth: String) {
        this.dateOfBirth = dateOfBirth
    }

    fun getBasicMobile(): String? {
        return basicMobile
    }

    fun setBasicMobile(basicMobile: String) {
        this.basicMobile = basicMobile
    }

    fun getBasicEmail(): String? {
        return basicEmail
    }

    fun setBasicEmail(basicEmail: String) {
        this.basicEmail = basicEmail
    }

    fun getImagePath(): String? {
        return imagePath
    }

    fun setImagePath(imagePath: String) {
        this.imagePath = imagePath
    }

    fun getImageName(): String? {
        return imageName
    }

    fun setImageName(imageName: String) {
        this.imageName = imageName
    }

    fun getUserWalletBalance(): Double? {
        return userWalletBalance
    }

    fun setUserWalletBalance(userWalletBalance: Double?) {
        this.userWalletBalance = userWalletBalance
    }

    fun getUserReserveBalance(): Double? {
        return userReserveBalance
    }

    fun setUserReserveBalance(userReserveBalance: Double?) {
        this.userReserveBalance = userReserveBalance
    }

    fun getSmsBalance(): Double? {
        return smsBalance
    }

    fun setSmsBalance(smsBalance: Double?) {
        this.smsBalance = smsBalance
    }

    fun getVoiceBalance(): Double? {
        return voiceBalance
    }

    fun setVoiceBalance(voiceBalance: Double?) {
        this.voiceBalance = voiceBalance
    }

    fun getEmailBalance(): Double? {
        return emailBalance
    }

    fun setEmailBalance(emailBalance: Double?) {
        this.emailBalance = emailBalance
    }

    fun getValidationStatus(): Int? {
        return validationStatus
    }

    fun setValidationStatus(validationStatus: Int?) {
        this.validationStatus = validationStatus
    }

    fun getUserEnableStatus(): Int? {
        return userEnableStatus
    }

    fun setUserEnableStatus(userEnableStatus: Int?) {
        this.userEnableStatus = userEnableStatus
    }

    fun getRegistrationDate(): Date? {
        return registrationDate
    }

    fun setRegistrationDate(registrationDate: Date?) {
        this.registrationDate = registrationDate
    }

    fun getAddress(): String? {
        return categoryName
    }

    fun setAddress(categoryName: String) {
        this.categoryName = categoryName
    }

}
