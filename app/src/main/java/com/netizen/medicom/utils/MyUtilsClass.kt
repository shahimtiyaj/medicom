package com.netizen.medicom.utils

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.text.TextUtils
import android.text.format.DateFormat
import android.util.Patterns
import android.view.View
import android.view.WindowManager
import es.dmoral.toasty.Toasty
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

class MyUtilsClass {

    companion object {

        fun getDateFormat(date: String): String {
            val dateFormatprev = SimpleDateFormat("yyyy-mm-dd")
            val dateFormatprevParse = dateFormatprev.parse(date)
            val dateFormat = SimpleDateFormat("dd/mm/yyyy")
            val userDateOfBirth = dateFormat.format(dateFormatprevParse!!)

            return userDateOfBirth
        }

        fun getDateFormatForSerachData(date: String): String {
            val dateFormatprev = SimpleDateFormat("dd/mm/yyyy") //dd/mm/yyyy
            val dateFormatprevParse = dateFormatprev.parse(date)
            val dateFormat = SimpleDateFormat("yyyy-mm-dd")
            val userDateOfBirth = dateFormat.format(dateFormatprevParse!!)

            return userDateOfBirth
        }

        fun getDate(time: Long?): String? {
            val cal = Calendar.getInstance(Locale.ENGLISH)

            if (time != null) {
                cal.timeInMillis = time
            }

            return DateFormat.format("dd MMM, yyyy", cal).toString()
        }

        fun showErrorToasty(context: Context, message: String) {
            Toasty.error(context, message, Toasty.LENGTH_SHORT).show()
        }

        fun showsDatePicker(context: Context, dateListener: DatePickerDialog.OnDateSetListener) {
            val calendar= Calendar.getInstance()
            val datePickerDialog = DatePickerDialog(context, dateListener,
                Calendar.getInstance().get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH))

            datePickerDialog.datePicker.maxDate = System.currentTimeMillis()
            datePickerDialog.show()
        }

        fun isValidPassword(password: String?): Boolean {
            val pattern: Pattern
            val matcher: Matcher
            val PASSWORD_PATTERN =
                "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$"
            pattern = Pattern.compile(PASSWORD_PATTERN)
            matcher = pattern.matcher(password)
            return matcher.matches()
        }

         fun isValidEmail(email: String): Boolean {
            return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email)
                .matches()
        }

        fun validCellPhone(number: String?): Boolean {
            return Patterns.PHONE.matcher(number).matches()
        }

         fun requestFocus(view: View, context: Activity) {
            if (view.requestFocus()) {
                context.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
            }
        }
    }


}