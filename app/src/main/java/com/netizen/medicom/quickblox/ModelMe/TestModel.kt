package com.netizen.medicom.quickblox.ModelMe

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class TestModel {

    @SerializedName("login")
    @Expose
    private var login: String? = null

    @SerializedName("password")
    @Expose
    private var password: String? = null

    constructor() {

    }

    constructor(login: String?, password: String?) {
        this.login = login
        this.password = password
    }

    fun getLogin(): String? {
        return login
    }

    fun setLogin(login: String?) {
        this.login = login
    }

    fun getPassword(): String? {
        return password
    }

    fun setPassword(password: String?) {
        this.password = password
    }

}