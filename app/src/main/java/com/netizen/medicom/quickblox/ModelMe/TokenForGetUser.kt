package com.netizen.medicom.quickblox.ModelMe

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class TokenForGetUser {
    @SerializedName("token")
    @Expose
    private var token: String? = null

    @SerializedName("username")
    @Expose
    private var username: String? = null

    constructor() {

    }


    constructor(token: String?, username: String?) {
        this.token = token
        this.username = username
    }

    fun getToken(): String? {
        return token
    }

    fun setToken(token: String?) {
        this.token = token
    }

    fun getUsername(): String? {
        return username
    }

    fun setUsername(username: String?) {
        this.username = username
    }
}