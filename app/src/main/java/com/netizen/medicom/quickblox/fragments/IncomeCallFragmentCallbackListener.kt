package com.netizen.medicom.quickblox.fragments


interface IncomeCallFragmentCallbackListener {

    fun onAcceptCurrentSession()

    fun onRejectCurrentSession()
}