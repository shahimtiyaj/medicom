package com.netizen.medicom.quickblox.ModelMe

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class QbUser {

    @SerializedName("current_page")
    @Expose
    private var currentPage: Int? = null

    @SerializedName("per_page")
    @Expose
    private var perPage: Int? = null

    @SerializedName("total_entries")
    @Expose
    private var totalEntries: Int? = null

    @SerializedName("items")
    @Expose
    private var items: List<Item?>? = null

    fun getCurrentPage(): Int? {
        return currentPage
    }

    fun setCurrentPage(currentPage: Int?) {
        this.currentPage = currentPage
    }

    fun getPerPage(): Int? {
        return perPage
    }

    fun setPerPage(perPage: Int?) {
        this.perPage = perPage
    }

    fun getTotalEntries(): Int? {
        return totalEntries
    }

    fun setTotalEntries(totalEntries: Int?) {
        this.totalEntries = totalEntries
    }

    fun getItems(): List<Item?>? {
        return items
    }

    fun setItems(items: List<Item?>?) {
        this.items = items
    }


    class Item {
        @SerializedName("user")
        @Expose
        var user: User? = null
    }

    class User {
        @SerializedName("id")
        @Expose
        private var id: Int? = null

        @SerializedName("full_name")
        @Expose
        private var fullName: String? = null

        @SerializedName("email")
        @Expose
        private var email: String? = null

        @SerializedName("login")
        @Expose
        private var login: String? = null

        @SerializedName("phone")
        @Expose
        private var phone: Any? = null

        @SerializedName("website")
        @Expose
        private var website: Any? = null

        @SerializedName("created_at")
        @Expose
        private var createdAt: String? = null

        @SerializedName("updated_at")
        @Expose
        private var updatedAt: String? = null

        @SerializedName("last_request_at")
        @Expose
        private var lastRequestAt: String? = null

        @SerializedName("external_user_id")
        @Expose
        private var externalUserId: Any? = null

        @SerializedName("facebook_id")
        @Expose
        private var facebookId: Any? = null

        @SerializedName("twitter_id")
        @Expose
        private var twitterId: Any? = null

        @SerializedName("blob_id")
        @Expose
        private var blobId: Any? = null

        @SerializedName("custom_data")
        @Expose
        private var customData: Any? = null

        @SerializedName("age_over16")
        @Expose
        private var ageOver16: Boolean?= null

        @SerializedName("parents_contacts")
        @Expose
        private var parentsContacts: String? = null

        @SerializedName("user_tags")
        @Expose
        private var userTags: Any? = null

        fun getId(): Int? {
            return id
        }

        fun setId(id: Int?) {
            this.id = id
        }

        fun getFullName(): String? {
            return fullName
        }

        fun setFullName(fullName: String?) {
            this.fullName = fullName
        }

        fun getEmail(): String? {
            return email
        }

        fun setEmail(email: String?) {
            this.email = email
        }

        fun getLogin(): String? {
            return login
        }

        fun setLogin(login: String?) {
            this.login = login
        }

        fun getPhone(): Any? {
            return phone
        }

        fun setPhone(phone: Any?) {
            this.phone = phone
        }

        fun getWebsite(): Any? {
            return website
        }

        fun setWebsite(website: Any?) {
            this.website = website
        }

        fun getCreatedAt(): String? {
            return createdAt
        }

        fun setCreatedAt(createdAt: String?) {
            this.createdAt = createdAt
        }

        fun getUpdatedAt(): String? {
            return updatedAt
        }

        fun setUpdatedAt(updatedAt: String?) {
            this.updatedAt = updatedAt
        }

        fun getLastRequestAt(): String? {
            return lastRequestAt
        }

        fun setLastRequestAt(lastRequestAt: String?) {
            this.lastRequestAt = lastRequestAt
        }

        fun getExternalUserId(): Any? {
            return externalUserId
        }

        fun setExternalUserId(externalUserId: Any?) {
            this.externalUserId = externalUserId
        }

        fun getFacebookId(): Any? {
            return facebookId
        }

        fun setFacebookId(facebookId: Any?) {
            this.facebookId = facebookId
        }

        fun getTwitterId(): Any? {
            return twitterId
        }

        fun setTwitterId(twitterId: Any?) {
            this.twitterId = twitterId
        }

        fun getBlobId(): Any? {
            return blobId
        }

        fun setBlobId(blobId: Any?) {
            this.blobId = blobId
        }

        fun getCustomData(): Any? {
            return customData
        }

        fun setCustomData(customData: Any?) {
            this.customData = customData
        }

        fun getAgeOver16(): Boolean? {
            return ageOver16
        }

        fun setAgeOver16(ageOver16: Boolean?) {
            this.ageOver16 = ageOver16 as Nothing?
        }

        fun getParentsContacts(): String? {
            return parentsContacts
        }

        fun setParentsContacts(parentsContacts: String?) {
            this.parentsContacts = parentsContacts
        }

        fun getUserTags(): Any? {
            return userTags
        }

        fun setUserTags(userTags: Any?) {
            this.userTags = userTags
        }

    }

}