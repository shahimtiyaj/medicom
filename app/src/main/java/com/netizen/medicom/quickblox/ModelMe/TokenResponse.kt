package com.netizen.medicom.quickblox.ModelMe

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class TokenResponse {
    @SerializedName("session")
    @Expose
    private var session: Session? = null

    fun getSession(): Session? {
        return session
    }

    fun setSession(session: Session?) {
        this.session = session
    }


    class Session {

        @SerializedName("application_id")
        @Expose
        private var applicationId: Int? = null

        @SerializedName("created_at")
        @Expose
        private var createdAt: String? = null

        @SerializedName("id")
        @Expose
        private var id: Int? = null

        @SerializedName("nonce")
        @Expose
        private var nonce: Int? = null

        @SerializedName("token")
        @Expose
        private var token: String? = null

        @SerializedName("ts")
        @Expose
        private var ts: Int? = null

        @SerializedName("updated_at")
        @Expose
        private var updatedAt: String? = null

        @SerializedName("user_id")
        @Expose
        private var userId: Int? = null

        @SerializedName("useless_id")
        @Expose
        private var uselessId: String? = null

        fun getApplicationId(): Int? {
            return applicationId
        }

        fun setApplicationId(applicationId: Int?) {
            this.applicationId = applicationId
        }

        fun getCreatedAt(): String? {
            return createdAt
        }

        fun setCreatedAt(createdAt: String?) {
            this.createdAt = createdAt
        }

        fun getId(): Int? {
            return id
        }

        fun setId(id: Int?) {
            this.id = id
        }

        fun getNonce(): Int? {
            return nonce
        }

        fun setNonce(nonce: Int?) {
            this.nonce = nonce
        }

        fun getToken(): String? {
            return token
        }

        fun setToken(token: String?) {
            this.token = token
        }

        fun getTs(): Int? {
            return ts
        }

        fun setTs(ts: Int?) {
            this.ts = ts
        }

        fun getUpdatedAt(): String? {
            return updatedAt
        }

        fun setUpdatedAt(updatedAt: String?) {
            this.updatedAt = updatedAt
        }

        fun getUserId(): Int? {
            return userId
        }

        fun setUserId(userId: Int?) {
            this.userId = userId
        }

        fun getUselessId(): String? {
            return uselessId
        }

        fun setUselessId(uselessId: String?) {
            this.uselessId = uselessId
        }


    }



}