package com.netizen.medicom.quickblox.activities

import android.app.ActivityManager
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.netizen.medicom.R
import com.netizen.medicom.quickblox.adapters.UsersAdapter
import com.netizen.medicom.quickblox.db.QbUsersDbManager
import com.netizen.medicom.quickblox.services.CallService
import com.netizen.medicom.quickblox.services.LoginService
import com.netizen.medicom.quickblox.util.loadUsersByPagedRequestBuilder
import com.netizen.medicom.quickblox.util.loadUsersBySignIn
import com.netizen.medicom.quickblox.util.signOut
import com.netizen.medicom.quickblox.utils.*
import com.netizen.medicom.view.activity.MainActivity
import com.quickblox.chat.QBChatService
import com.quickblox.core.QBEntityCallback
import com.quickblox.core.exception.QBResponseException
import com.quickblox.core.request.GenericQueryRule
import com.quickblox.core.request.QBPagedRequestBuilder
import com.quickblox.messages.services.SubscribeService
import com.quickblox.users.model.QBUser
import com.quickblox.videochat.webrtc.QBRTCClient
import com.quickblox.videochat.webrtc.QBRTCTypes

private const val PER_PAGE_SIZE_100 = 100
private const val ORDER_RULE = "order"
private const val ORDER_DESC_UPDATED = "desc date updated_at"

class OpponentsActivity : BaseActivity() {
    private val TAG = OpponentsActivity::class.java.simpleName

    private lateinit var usersRecyclerView: RecyclerView
    private lateinit var currentUser: QBUser

    private var usersAdapter: UsersAdapter? = null
    private lateinit var username: TextView

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, OpponentsActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_users)
        currentUser = SharedPrefsHelper.getQbUser()
        initDefaultActionBar()
        initUI()
        startLoginService()
    }

    override fun onResume() {
        super.onResume()
        val isIncomingCall = SharedPrefsHelper.get(EXTRA_IS_INCOMING_CALL, false)
        if (isCallServiceRunning(CallService::class.java)) {
            Log.d(TAG, "CallService is running now")
            CallActivity.start(this, isIncomingCall)
        }
        clearAppNotifications()
        loadUsers()
    }

    private fun isCallServiceRunning(serviceClass: Class<*>): Boolean {
        val manager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val services = manager.getRunningServices(Integer.MAX_VALUE)
        for (service in services) {
            if (CallService::class.java.name == service.service.className) {
                return true
            }
        }
        return false
    }

    private fun clearAppNotifications() {
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancelAll()
    }

    private fun startPermissionsActivity(checkOnlyAudio: Boolean) {
        PermissionsActivity.startForResult(this, checkOnlyAudio, PERMISSIONS)
    }

    private fun loadUsers() {
        showProgressDialog(R.string.dlg_loading_opponents)

        val rules = ArrayList<GenericQueryRule>()
        rules.add(GenericQueryRule(ORDER_RULE, ORDER_DESC_UPDATED))
        val requestBuilder = QBPagedRequestBuilder()
        requestBuilder.rules = rules
        requestBuilder.perPage = PER_PAGE_SIZE_100

        loadUsersByPagedRequestBuilder(object : QBEntityCallback<java.util.ArrayList<QBUser>> {
            override fun onSuccess(result: ArrayList<QBUser>, params: Bundle) {
                QbUsersDbManager.saveAllUsers(result, true)
                initUsersList()
                hideProgressDialog()
            }

            override fun onError(responseException: QBResponseException) {
                hideProgressDialog()
                showErrorSnackbar(
                    R.string.loading_users_error,
                    responseException,
                    View.OnClickListener { loadUsers() })
            }
        }, requestBuilder)
    }


    private fun initUI() {
        usersRecyclerView = findViewById(R.id.list_select_users)
        username = findViewById(R.id.userName)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)

        username.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val tempHrReportList = ArrayList<QBUser>()
                val currentOpponentsList = QbUsersDbManager.allUsers
                currentOpponentsList.remove(SharedPrefsHelper.getQbUser())

                currentOpponentsList.forEach { hrReport ->
                    if (hrReport.login.toString().toLowerCase().contains(s.toString(), true)) {
                        tempHrReportList.add(hrReport)
                    }
                }

                usersAdapter!!.updateUsersList(tempHrReportList)

            }
        })
    }

    private fun NetiID() {
        loadUsersBySignIn("user1", object : QBEntityCallback<QBUser?> {
            override fun onSuccess(user: QBUser?, args: Bundle) {
                username.setText(user?.fullName)
            }
            override fun onError(errors: QBResponseException) {}
        })
    }

    private fun initUsersList() {
        val currentOpponentsList = QbUsersDbManager.allUsers
        currentOpponentsList.remove(SharedPrefsHelper.getQbUser())
        if (usersAdapter == null) {
            usersAdapter = UsersAdapter(this, currentOpponentsList)
            usersAdapter!!.setSelectedItemsCountsChangedListener(object :
                UsersAdapter.SelectedItemsCountsChangedListener {
                override fun onCountSelectedItemsChanged(count: Int) {
                    updateActionBar(count)
                }
            })

            usersRecyclerView.layoutManager = LinearLayoutManager(this)
            usersRecyclerView.adapter = usersAdapter
        } else {
            usersAdapter!!.updateUsersList(currentOpponentsList)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        if (usersAdapter != null && usersAdapter!!.selectedUsers.isNotEmpty()) {
            menuInflater.inflate(R.menu.activity_selected_opponents, menu)
        } else {
            menuInflater.inflate(R.menu.activity_opponents, menu)
        }

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {

            R.id.update_opponents_list -> {
                loadUsers()
                //NetiID()
                return true
            }
            /* R.id.settings -> {
                 SettingsActivity.start(this)
                 return true
             }*/
            R.id.log_out -> {
                logout()
                return true
            }
            R.id.start_video_call -> {
                if (checkIsLoggedInChat()) {
                    startCall(true)
                }
                if (checkPermissions(PERMISSIONS)) {
                    startPermissionsActivity(false)
                }
                return true
            }
            R.id.start_audio_call -> {
                if (checkIsLoggedInChat()) {
                    startCall(false)
                }
                if (checkPermission(PERMISSIONS[1])) {
                    startPermissionsActivity(true)
                }
                return true
            }

            android.R.id.home -> {
                startActivity(Intent(this, MainActivity::class.java))
               // supportFragmentManager.beginTransaction().add(android.R.id.content, FragmentSetting()).commit()
               // supportFragmentManager.beginTransaction().addToBackStack(null)
                finish()

                return true
            }

            /* R.id.appinfo -> {
                 AppInfoActivity.start(this)
                 return true
             }*/
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun checkIsLoggedInChat(): Boolean {
        if (!QBChatService.getInstance().isLoggedIn) {
            startLoginService()
            shortToast(R.string.login_chat_retry)
            return false
        }
        return true
    }

    private fun startLoginService() {
        if (SharedPrefsHelper.hasQbUser()) {
            LoginService.start(this, SharedPrefsHelper.getQbUser())
        }
    }

    private fun startCall(isVideoCall: Boolean) {
        val usersCount = usersAdapter!!.selectedUsers.size
        if (usersCount > MAX_OPPONENTS_COUNT) {
            longToast(
                String.format(
                    getString(R.string.error_max_opponents_count),
                    MAX_OPPONENTS_COUNT
                )
            )
            return
        }

        val opponentsList = getIdsSelectedOpponents(usersAdapter!!.selectedUsers)
        val conferenceType = if (isVideoCall) {
            QBRTCTypes.QBConferenceType.QB_CONFERENCE_TYPE_VIDEO
        } else {
            QBRTCTypes.QBConferenceType.QB_CONFERENCE_TYPE_AUDIO
        }
        val qbrtcClient = QBRTCClient.getInstance(applicationContext)

        val newQbRtcSession =
            qbrtcClient.createNewSessionWithOpponents(opponentsList, conferenceType)

        WebRtcSessionManager.setCurrentSession(newQbRtcSession)

        sendPushMessage(opponentsList, currentUser.fullName)

        CallActivity.start(this, false)
    }

    private fun getIdsSelectedOpponents(selectedUsers: Collection<QBUser>): ArrayList<Int> {
        val opponentsIds = ArrayList<Int>()
        if (!selectedUsers.isEmpty()) {
            for (qbUser in selectedUsers) {
                opponentsIds.add(qbUser.id)
            }
        }
        return opponentsIds
    }

    private fun updateActionBar(countSelectedUsers: Int) {
        if (countSelectedUsers < 1) {
            initDefaultActionBar()
        } else {
            val title = if (countSelectedUsers > 1) {
                R.string.tile_many_users_selected
            } else {
                R.string.title_one_user_selected
            }
            supportActionBar?.title = getString(title, countSelectedUsers)
            supportActionBar?.subtitle = null
        }
        invalidateOptionsMenu()
    }

    private fun initDefaultActionBar() {
        val currentUserFullName = SharedPrefsHelper.getQbUser().fullName
        supportActionBar?.title = ""
        supportActionBar?.subtitle = getString(R.string.subtitle_text_logged_in_as, currentUserFullName)
    }

    private fun logout() {
        SubscribeService.unSubscribeFromPushes(this)
        LoginService.logout(this)
        removeAllUserData()
        //startLoginActivity()
        finish()
    }

    private fun removeAllUserData() {
        SharedPrefsHelper.clearAllData()
        QbUsersDbManager.clearDB()
        signOut()
    }

    private fun startLoginActivity() {
        LoginActivity.start(this)
        finish()
    }

    override fun onBackPressed() {
        super.onBackPressed(); //replaced
       // supportFragmentManager.beginTransaction().add(android.R.id.content, FragmentSetting()).commit()
        //finish()
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }



    /*  fun getToken() {

          val header = HashMap<String?, String?>()
          header["Authorization"] =
              "bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1ODg1MDEzMzEsInVzZXJfbmFtZSI6ImFybWFtdW5ATkVUSUlENjY2IiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9DUk0iLCJST0xFX1NVUFBPUlQiLCJST0xFX0RXUyIsIlJPTEVfTUFOQUdFTUVOVCIsIlJPTEVfQURNSVNJQSIsIlJPTEVfVVNFUiIsIlJPTEVfQklKT1kiLCJST0xFX0FETUlOIiwiUk9MRV9TQSIsIlJPTEVfRU1VU0VSIiwiUk9MRV9FTUFETUlOIiwiUk9MRV9QQVJUTkVSIl0sImp0aSI6ImFjNDQyZTBhLThlNGEtNGEwMS1iMDQ1LTAzNTQ4ZWE5MzdkMSIsImNsaWVudF9pZCI6ImRldmdsYW4tY2xpZW50Iiwic2NvcGUiOlsicmVhZCIsIndyaXRlIiwidHJ1c3QiXX0.9T-JLLECUrV-MaA1bPnGnZ-RK5QWJZ55it_fVNdG1F0"
          header["Content-Type"] = "application/json"

          val testModel = TestModel("aziz00747", "12345678")

          val service = Retrofit.Builder().baseUrl("http://3.211.144.191:9001/").addConverterFactory(
              GsonConverterFactory.create()
          ).build()

          val call = service.create(ApiInterface::class.java).getToken(header, testModel)

          //calling the api-----------------------------------------------------------------------
          call?.enqueue(object : Callback<TokenResponse> {
              override fun onResponse(call: Call<TokenResponse>, response: Response<TokenResponse>) {
                  Log.d("onResponse", "Token:" + response.body()?.getSession()?.getToken())
                  if (response.isSuccessful) {
                      Log.d(
                          "onResponse",
                          "Success quick blox token:" + response.body()?.getSession()?.getToken()
                      )
                      AppPreferences(applicationContext).setToken(
                          response.body()?.getSession()?.getToken()
                      )
                  } else {
                      Log.d("onResponse", "Not zoom:" + response.body())
                  }
              }

              override fun onFailure(call: Call<TokenResponse>, t: Throwable) {
                  Log.d("onFailure", t.toString())
              }
          })

      }


      // API Call from netiworld
      fun getUserListQuickBlox() {

          val header = HashMap<String?, String?>()
          header["Authorization"] =
              "bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1ODg1MDEzMzEsInVzZXJfbmFtZSI6ImFybWFtdW5ATkVUSUlENjY2IiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9DUk0iLCJST0xFX1NVUFBPUlQiLCJST0xFX0RXUyIsIlJPTEVfTUFOQUdFTUVOVCIsIlJPTEVfQURNSVNJQSIsIlJPTEVfVVNFUiIsIlJPTEVfQklKT1kiLCJST0xFX0FETUlOIiwiUk9MRV9TQSIsIlJPTEVfRU1VU0VSIiwiUk9MRV9FTUFETUlOIiwiUk9MRV9QQVJUTkVSIl0sImp0aSI6ImFjNDQyZTBhLThlNGEtNGEwMS1iMDQ1LTAzNTQ4ZWE5MzdkMSIsImNsaWVudF9pZCI6ImRldmdsYW4tY2xpZW50Iiwic2NvcGUiOlsicmVhZCIsIndyaXRlIiwidHJ1c3QiXX0.9T-JLLECUrV-MaA1bPnGnZ-RK5QWJZ55it_fVNdG1F0"
          header["Content-Type"] = "application/json"

          try {
              val testModel =
                  TokenForGetUser(AppPreferences(applicationContext).getToken(), "aziz00747")

              val service = Retrofit.Builder().baseUrl("http://3.211.144.191:9001/")
                  .addConverterFactory(GsonConverterFactory.create()).build()

              val call = service.create(ApiInterface::class.java).userListGetData(header, testModel)

              //calling the api-----------------------------------------------------------------------
              call?.enqueue(object : Callback<QBUser> {
                  override fun onResponse(
                      call: Call<QBUser>,
                      response: Response<QBUser>
                  ) {
                      Log.d("onResponse", "Quick Blox List :" + response.errorBody().toString())

                      if (response.code() == 302) {

                          val type = object : TypeToken<QBUser>() {}.type
                          val datalist: QBUser

                          datalist = Gson().fromJson(response.errorBody()!!.charStream(), type)
                          Log.d("onResponse", "Success QuickBlox Data:" + datalist)
                          //QbUsersDbManager.saveAllUsers(datalist, true)
                          //initUsersList()
                      } else {
                          Log.d("onResponse", "Fail to get Data:" + response.body())
                      }
                  }

                  override fun onFailure(call: Call<QBUser>, t: Throwable) {
                      Log.d("onFailure", t.toString())
                  }
              })

          } catch (e: NumberFormatException) {
              e.printStackTrace()
          } catch (e: NullPointerException) {
              e.printStackTrace()
          }
      }*/
}
