package com.netizen.medicom.view.fragment.patientCorner.call

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.netizen.medicom.R

/**
 * A simple [Fragment] subclass.
 */
class IncomingCallFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_incoming_call, container, false)
    }
}
