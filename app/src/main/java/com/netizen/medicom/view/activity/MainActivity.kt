package com.netizen.medicom.view.activity

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.netizen.medicom.R
import com.netizen.medicom.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

   //test git --------

    private lateinit var navController: NavController

    companion object {

        private lateinit var bottomNavigation: BottomNavigationView

        fun showBottomNavigation() {
            bottomNavigation.visibility = View.VISIBLE
        }

        fun hideBottomNavigation() {
            bottomNavigation.visibility = View.GONE
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.AppTheme)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        navController = Navigation.findNavController(this@MainActivity, R.id.navHost_fragment)

        bottomNavigation = binding.bottomNavigation
        supportActionBar?.hide()

//        initViews()
//        initObservers()
    }
}
