package com.netizen.medicom.view.fragment.doctorCorner

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.netizen.medicom.R
import com.netizen.medicom.databinding.FragmentCallDetailsHistoryBinding
import kotlinx.android.synthetic.main.call_details_history_single_medicinelist_layout.view.*

class FragmentCallDetailsHistory : Fragment() {
    private lateinit var binding: FragmentCallDetailsHistoryBinding
    private var isDropOpened: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_call_details_history, container, false)

        initViews()

        return binding.root
    }

    private fun initViews() {
        binding.callDetails.txt_medicine.setOnClickListener {
            if (!isDropOpened) {
                showhidedropDown()
            } else {
                hidedropDown()
            }
        }
    }

    private fun hidedropDown() {
        isDropOpened = false
        binding.callDetails.txt_medicine.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_drop_down_icon, 0)
        binding.callDetails.linear1.visibility = View.GONE
        binding.callDetails.linear2.visibility = View.GONE
        binding.callDetails.view0.visibility = View.GONE
        binding.callDetails.view1.visibility = View.GONE
    }

    private fun showhidedropDown() {
        isDropOpened = true
        binding.callDetails.txt_medicine.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_drop_up_icon, 0)
        binding.callDetails.linear1.visibility = View.VISIBLE
        binding.callDetails.linear2.visibility = View.VISIBLE
        binding.callDetails.view0.visibility = View.VISIBLE
        binding.callDetails.view1.visibility = View.VISIBLE
    }

}
