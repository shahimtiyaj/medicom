package com.netizen.medicom.view.fragment.doctorCorner

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.netizen.medicom.R
import com.netizen.medicom.databinding.FragmentDoctorDashboardBinding
import com.netizen.medicom.quickblox.activities.OpponentsActivity
import com.netizen.medicom.quickblox.services.LoginService
import com.netizen.medicom.quickblox.utils.SharedPrefsHelper


class FragmentDoctorDashboard : Fragment() {
    private lateinit var binding: FragmentDoctorDashboardBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_doctor_dashboard, container, false)

        initViews()

        return binding.root
    }

    private fun initViews() {

        binding.switch5.setOnToggledListener { _, isOn ->
            if (!isOn) {
                binding.available.visibility = View.GONE
            } else {
                binding.available.visibility = View.VISIBLE
            }
        }

        binding.cardDetails.setOnClickListener {
            findNavController().navigate(R.id.action_fragmentDoctorDashboard_to_fragmentCallDetailsHistory)
        }

        binding.findDoctor.setOnClickListener {
            if (SharedPrefsHelper.hasQbUser()) {
                LoginService.start(requireActivity(), SharedPrefsHelper.getQbUser())
                OpponentsActivity.start(requireActivity())
                activity?.finish()
            } else {
                //LoginActivity.start(activity!!)
                //Toasty.error(context!!, "Sorry! No Doctor Found.", Toasty.LENGTH_LONG).show()
            }
        }
    }
}
