package com.netizen.medicom.view.fragment.signIn

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.netizen.medicom.R
import com.netizen.medicom.databinding.FragmentForgotPasswordTwoBinding
import com.netizen.medicom.model.UserCheckForgotPass
import com.netizen.medicom.utils.Loaders
import com.netizen.medicom.viewModel.UserViewModel
import es.dmoral.toasty.Toasty

class FragmentForgotPasswordTwo : Fragment() {
    private lateinit var binding: FragmentForgotPasswordTwoBinding
    private var userViewModel: UserViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userViewModel = ViewModelProvider(
            requireActivity(),
            UserViewModel.SignInViewModelFactory(requireContext().applicationContext as Application)
        ).get(UserViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_forgot_password_two,
            container,
            false
        )
        binding.lifecycleOwner = this

        initViews()
        initObservers()
      //  userViewModel?.getUserInfoForgotPassword("ms448")

        return binding.root
    }

    private fun initViews() {
        binding.next.setOnClickListener {
            mobileVarify =binding.editMobileVarify.text.toString()
            if (binding.editMobileVarify.text.isNullOrEmpty()){
                binding.editMobileVarify.error=resources.getString(R.string.must_sent_mobile)
            }
            else{
                userViewModel?.getOtpForgotPassword(mobileVarify, userInformation)
            }
        }
    }

    private fun initObservers() {
        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.error.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(requireContext(), it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.error.value = null
            }
        })

        Loaders.success.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(requireContext(), it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.success.value = null
                //findNavController().popBackStack(R.id.fragmentForgotPasswordTwo, true);
                findNavController().navigate(R.id.action_fragmentForgotPasswordTwo_to_fragmentForgotPasswordThree)
            }
        })

        userViewModel?.userInfoListForgotPass?.observe(viewLifecycleOwner, Observer {
            setUserInfo(it)
        })
    }

    private fun setUserInfo(userCheckForgotPass: UserCheckForgotPass?) {
        binding.username.setText(":  "+userCheckForgotPass?.userName)
        binding.birthday.setText(":  "+userCheckForgotPass?.dateOfBirth)
        binding.mobileVarify.setText(":  "+userCheckForgotPass?.basicMobile)

       // val textWatcher = MaskTextWatcher( binding.mobileVarify, "#### ####")
       // binding.mobileVarify.addTextChangedListener(textWatcher)

        username = userCheckForgotPass?.userName.toString()

         userInformation = UserCheckForgotPass(
                userCheckForgotPass?.basicMobile,
                userCheckForgotPass?.basicEmail,
                userCheckForgotPass?.fullName,
                userCheckForgotPass?.customNetiID,
                userCheckForgotPass?.dateOfBirth,
                userCheckForgotPass?.userName,
                userCheckForgotPass?.netiID
        )
    }

    companion object {
        var username = ""
        var mobileVarify = ""
        var userInformation = UserCheckForgotPass()
    }
}
