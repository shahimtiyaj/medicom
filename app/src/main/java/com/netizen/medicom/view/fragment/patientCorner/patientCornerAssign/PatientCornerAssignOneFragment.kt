package com.netizen.medicom.view.fragment.patientCorner.patientCornerAssign

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController

import com.netizen.medicom.R
import com.netizen.medicom.databinding.FragmentPatientCornerAssignOneBinding

/**
 * A simple [Fragment] subclass.
 */
class PatientCornerAssignOneFragment : Fragment() {

    private lateinit var binding: FragmentPatientCornerAssignOneBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_patient_corner_assign_one,
            container,
            false
        )

        initViews()

        return binding.root
    }

    private fun initViews() {
        binding.buttonYes.setOnClickListener {
            findNavController().navigate(R.id.action_patientCornerAssignOneFragment_to_patientCornerAssignTwoFragment)
        }
    }
}
