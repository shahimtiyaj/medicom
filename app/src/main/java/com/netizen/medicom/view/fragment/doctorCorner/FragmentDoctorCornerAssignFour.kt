package com.netizen.medicom.view.fragment.doctorCorner

import android.app.Application
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.netizen.medicom.R
import com.netizen.medicom.databinding.FragmentDoctorCornerAssignFourBinding
import com.netizen.medicom.quickblox.DEFAULT_USER_PASSWORD
import com.netizen.medicom.quickblox.activities.ERROR_LOGIN_ALREADY_TAKEN_HTTP_STATUS
import com.netizen.medicom.quickblox.services.LoginService
import com.netizen.medicom.quickblox.util.signInUser
import com.netizen.medicom.quickblox.util.signUp
import com.netizen.medicom.quickblox.utils.EXTRA_LOGIN_ERROR_MESSAGE
import com.netizen.medicom.quickblox.utils.EXTRA_LOGIN_RESULT
import com.netizen.medicom.quickblox.utils.EXTRA_LOGIN_RESULT_CODE
import com.netizen.medicom.quickblox.utils.SharedPrefsHelper
import com.netizen.medicom.viewModel.HomeViewModel
import com.netizen.medicom.viewModel.ProfileViewModel
import com.quickblox.core.QBEntityCallback
import com.quickblox.core.exception.QBResponseException
import com.quickblox.users.QBUsers
import com.quickblox.users.model.QBUser

class FragmentDoctorCornerAssignFour : Fragment() {
    private lateinit var binding: FragmentDoctorCornerAssignFourBinding

    private var profileViewModel: ProfileViewModel? = null
    private lateinit var homeViewModel: HomeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        profileViewModel = ViewModelProvider(
            this,
            ProfileViewModel.ProfileViewModelFactory(requireContext().applicationContext as Application)
        ).get(ProfileViewModel::class.java)

        homeViewModel = ViewModelProvider(requireActivity(),
            HomeViewModel.HomeViewModelFactory(requireContext().applicationContext as Application))
            .get(HomeViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_doctor_corner_assign_four,
            container,
            false
        )

        binding.lifecycleOwner = this

        initViews()
        initObservables()

        homeViewModel.getHomePageInfo()
        profileViewModel?.getProfileInfo()

        return binding.root

    }


    private fun initViews() {
        binding.buttonSubmit.setOnClickListener {
            findNavController().navigate(R.id.action_fragmentDoctorCornerAssignFour_to_fragmentDoctorCornerAssignSuccessful)
        }
    }

    private fun initObservables() {

        profileViewModel?.fullName?.observe(viewLifecycleOwner, Observer { fullName ->
            if (!fullName.isNullOrEmpty()) {
              //  userFullName = fullName
            }
        })

        profileViewModel?.customNetiID?.observe(viewLifecycleOwner, Observer { customNetiId ->
            if (!customNetiId.isNullOrEmpty()) {
               // userLoginNetiID = customNetiId
            }
        })

        homeViewModel.homePageInfoLiveData.observe(viewLifecycleOwner, Observer {homePageInfo ->
            userLoginNetiID=homePageInfo.getBasicInfo()?.getCustomNetiID().toString()
            userFullName=homePageInfo.getBasicInfo()?.getBasicEmail().toString()

            //Quick blox sign up funtion-------------------
            val user = createUserWithEnteredData()
            signUpNewUser(user)
        })
    }

    //QUICK BLOX --------SDK--Function----------------------------

    private lateinit var user: QBUser

    companion object {
        var userFullName = ""
        var userLoginNetiID = ""
    }

    private fun createUserWithEnteredData(): QBUser {
        val qbUser = QBUser()
        val userLoginNetiID = userLoginNetiID
        val userFullName = userFullName
        qbUser.login = userLoginNetiID
        qbUser.fullName = userFullName
        qbUser.password = DEFAULT_USER_PASSWORD
        return qbUser
    }

    private fun signUpNewUser(newUser: QBUser) {
        signUp(newUser, object : QBEntityCallback<QBUser> {
            override fun onSuccess(result: QBUser, params: Bundle) {
                SharedPrefsHelper.saveQbUser(newUser)
                // loginToChat(result)
            }

            override fun onError(e: QBResponseException) {
                if (e.httpStatusCode == ERROR_LOGIN_ALREADY_TAKEN_HTTP_STATUS) {
                    signInCreatedUser(newUser)
                } else {
                    //  longToast(R.string.sign_up_error)
                }
            }
        })
    }

    private fun loginToChat(qbUser: QBUser) {
        qbUser.password = DEFAULT_USER_PASSWORD
        user = qbUser
        startLoginService(qbUser)
    }

    private fun signInCreatedUser(user: QBUser) {
        signInUser(user, object : QBEntityCallback<QBUser> {
            override fun onSuccess(result: QBUser, params: Bundle) {
                SharedPrefsHelper.saveQbUser(user)
                updateUserOnServer(user)
                //loginToChat(result)
            }

            override fun onError(responseException: QBResponseException) {
                // longToast(R.string.sign_in_error)
            }
        })
    }

    private fun updateUserOnServer(user: QBUser) {
        user.password = null
        QBUsers.updateUser(user).performAsync(object : QBEntityCallback<QBUser> {
            override fun onSuccess(updUser: QBUser?, params: Bundle?) {
                // OpponentsActivity.start(activity!!)
                // activity?.finish()
            }

            override fun onError(responseException: QBResponseException?) {
                //  longToast(R.string.update_user_error)
            }
        })
    }

    private fun startLoginService(qbUser: QBUser) {
        val tempIntent = Intent(activity, LoginService::class.java)
        val pendingIntent = activity?.createPendingResult(EXTRA_LOGIN_RESULT_CODE, tempIntent, 0)
        LoginService.start(requireActivity(), qbUser, pendingIntent)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == EXTRA_LOGIN_RESULT_CODE) {
            var isLoginSuccess = false
            data?.let {
                isLoginSuccess = it.getBooleanExtra(EXTRA_LOGIN_RESULT, false)
            }
            var errorMessage = getString(R.string.unknown_error)
            data?.let {
                errorMessage = it.getStringExtra(EXTRA_LOGIN_ERROR_MESSAGE)
            }
            if (isLoginSuccess) {
                SharedPrefsHelper.saveQbUser(user)
                signInCreatedUser(user)
            } else {
                //  longToast(getString(R.string.login_chat_login_error) + errorMessage)
            }
        }
    }
}
