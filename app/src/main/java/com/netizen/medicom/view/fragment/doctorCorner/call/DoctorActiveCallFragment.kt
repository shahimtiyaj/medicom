package com.netizen.medicom.view.fragment.doctorCorner.call

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.netizen.medicom.R

/**
 * A simple [Fragment] subclass.
 */
class DoctorActiveCallFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_doctor_active_call, container, false)
    }

}
