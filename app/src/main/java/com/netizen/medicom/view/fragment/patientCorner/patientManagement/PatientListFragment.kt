package com.netizen.medicom.view.fragment.patientCorner.patientManagement

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.netizen.medicom.R
import com.netizen.medicom.adapter.FamilyMemberAdapter
import com.netizen.medicom.databinding.FragmentPatientListBinding
import com.netizen.medicom.model.PatientFamilyMemberList
import com.netizen.medicom.viewModel.PatientViewModel

/**
 * A simple [Fragment] subclass.
 */
class PatientListFragment : Fragment() {
    private lateinit var binding: FragmentPatientListBinding
    private lateinit var patientViewModel: PatientViewModel

    private var familyMemberArrayList=ArrayList<PatientFamilyMemberList.Item>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        patientViewModel = ViewModelProvider(
            requireActivity(),
            PatientViewModel.PatientViewModelFactory(requireContext().applicationContext as Application)
        )
            .get(PatientViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_patient_list, container, false)

        initObservers()
        patientViewModel.getFamilyMemberList()

        return binding.root
    }

    private fun initObservers() {
        patientViewModel.familyMemberArrayList.observe(viewLifecycleOwner, Observer { familyMemberArrayList ->
        this.familyMemberArrayList=familyMemberArrayList as ArrayList<PatientFamilyMemberList.Item>
            setAdapter(familyMemberArrayList)
        })
    }

    private fun setAdapter(familyMemberList: ArrayList<PatientFamilyMemberList.Item>){
        binding.recyleViewFamilyMember.layoutManager=LinearLayoutManager(requireContext())
        binding.recyleViewFamilyMember.setHasFixedSize(true)
        binding.recyleViewFamilyMember.adapter=FamilyMemberAdapter(requireContext(), familyMemberList)
    }
}
