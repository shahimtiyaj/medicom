package com.netizen.medicom.view.fragment.patientCorner.doctorSearch

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil

import com.netizen.medicom.R
import com.netizen.medicom.databinding.FragmentDoctorSearchBinding

/**
 * A simple [Fragment] subclass.
 */
class DoctorSearchFragment : Fragment() {

    private lateinit var binding: FragmentDoctorSearchBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_doctor_search,
            container,
            false
        )

        initViews()

        return binding.root
    }

    private fun initViews() {
        binding.buttonNext.setOnClickListener {

        }
    }
}
