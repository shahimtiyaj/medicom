package com.netizen.medicom.view.fragment.signIn

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.findNavController
import com.netizen.medicom.R
import com.netizen.medicom.databinding.ResetPasswordSucessLayoutBinding


class PasswordChangeSuccessDialogFragment : DialogFragment() {
    private lateinit var binding: ResetPasswordSucessLayoutBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding= DataBindingUtil.inflate(inflater, R.layout.reset_password_sucess_layout, container, false)

        binding.gotoLogin.setOnClickListener {
            dismiss()
            findNavController().navigate(R.id.signInFragment)
        }

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.TransparentDialogTheme)
    }
}
