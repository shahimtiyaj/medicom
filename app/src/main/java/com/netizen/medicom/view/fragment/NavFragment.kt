package com.netizen.medicom.view.fragment

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.netizen.medicom.R
import com.netizen.medicom.databinding.FragmentNavBinding
import com.netizen.medicom.viewModel.HomeViewModel

class NavFragment : DialogFragment()  {

    private lateinit var binding: FragmentNavBinding
    private var isDashboardOpened: Boolean = false
    private var isPatientInfoOpened: Boolean = false
    private var isFindDoctorOpened: Boolean = false
    private lateinit var homeViewModel: HomeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.TransparentDialogTheme)
        homeViewModel = ViewModelProvider(requireActivity(),
            HomeViewModel.HomeViewModelFactory(requireContext().applicationContext as Application))
            .get(HomeViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_nav, container, false)

        initViews()

        return binding.root
    }

    private fun initViews() {

        binding.textViewDashboard.setOnClickListener {
            if (!isDashboardOpened) {
                showDashboardPoint()
            } else {
                hideDashboardPoint()
            }

            hidePatientInfoPoint()
            hideFindDoctorPoint()
        }

        binding.textViewPatientInfo.setOnClickListener {
            if (!isPatientInfoOpened) {
                showPatientInfoPoint()
            } else {
                hidePatientInfoPoint()
            }
            hideDashboardPoint()
            hideFindDoctorPoint()
        }

        binding.textViewFindDoctor.setOnClickListener {
            if (!isFindDoctorOpened) {
                showFindDoctorPoint()
            } else {
                hideFindDoctorPoint()
            }
            hideDashboardPoint()
            hidePatientInfoPoint()
        }

        binding.textViewMyDashboard.setOnClickListener {
            dismiss()
            homeViewModel.isPatientDashboardClicked.value = true
        }

        binding.textViewAddPatient.setOnClickListener {
            dismiss()
            homeViewModel.isAddPatientClicked.value = true
        }

        binding.textViewPatientList.setOnClickListener {
            dismiss()
            homeViewModel.isPatientListClicked.value = true
        }

        binding.textViewMyStartFinding.setOnClickListener {
            dismiss()
            homeViewModel.isStartCallClicked.value = true
        }

    }

    private fun showDashboardPoint() {
        isDashboardOpened = true
        binding.textViewDashboard.setBackgroundResource(R.drawable.menu_hover_background)
        binding.textViewDashboard.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_white_24dp, 0, R.drawable.ic_drop_up_icon, 0)
        binding.textViewMyDashboard.visibility = View.VISIBLE
        binding.textViewMyDashboard.visibility = View.VISIBLE
    }

    private fun hideDashboardPoint() {
        isDashboardOpened = false
        binding.textViewDashboard.setBackgroundResource(0)
        binding.textViewDashboard.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_white_24dp, 0, R.drawable.ic_drop_down_icon, 0)
        binding.textViewMyDashboard.visibility = View.GONE
        binding.textViewMyDashboard.visibility = View.GONE
    }

    private fun showPatientInfoPoint() {
        isPatientInfoOpened = true
        binding.textViewPatientInfo.setBackgroundResource(R.drawable.menu_hover_background)
        binding.textViewPatientInfo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_white_24dp, 0, R.drawable.ic_drop_up_icon, 0)
        binding.textViewAddPatient.visibility = View.VISIBLE
        binding.textViewPatientList.visibility = View.VISIBLE
    }

    private fun hidePatientInfoPoint() {
        isPatientInfoOpened = false
        binding.textViewPatientInfo.setBackgroundResource(0)
        binding.textViewPatientInfo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_white_24dp, 0, R.drawable.ic_drop_down_icon, 0)
        binding.textViewAddPatient.visibility = View.GONE
        binding.textViewPatientList.visibility = View.GONE
    }

    private fun showFindDoctorPoint() {
        isFindDoctorOpened = true
        binding.textViewFindDoctor.setBackgroundResource(R.drawable.menu_hover_background)
        binding.textViewFindDoctor.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_white_24dp, 0, R.drawable.ic_drop_up_icon, 0)
        binding.textViewMyStartFinding.visibility = View.VISIBLE
    }

    private fun hideFindDoctorPoint() {
        isFindDoctorOpened = false
        binding.textViewFindDoctor.setBackgroundResource(0)
        binding.textViewFindDoctor.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_white_24dp, 0, R.drawable.ic_drop_down_icon, 0)
        binding.textViewMyStartFinding.visibility = View.GONE
    }
}
