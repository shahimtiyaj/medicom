package com.netizen.medicom.view.fragment.doctorCorner

import android.app.Application
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.netizen.medicom.R
import com.netizen.medicom.databinding.FragmentDoctorCornerAssignTwoBinding
import com.netizen.medicom.utils.Loaders
import com.netizen.medicom.viewModel.DoctorViewModel
import com.netizen.medicom.viewModel.PatientViewModel
import es.dmoral.toasty.Toasty

class FragmentDoctorCornerAssignTwo : Fragment() {
    private lateinit var binding: FragmentDoctorCornerAssignTwoBinding
    private lateinit var patientViewModel: PatientViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        patientViewModel = ViewModelProvider(
            requireActivity(),
            PatientViewModel.PatientViewModelFactory(requireContext().applicationContext as Application)
        ).get(PatientViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_doctor_corner_assign_two,
            container,
            false
        )

        initViews()

        initObservers()

        return binding.root
    }

    private fun initViews() {
        binding.editTextReferenceId.addTextChangedListener(MyTextWatcher(binding.btnSearchReferenceId))

        binding.btnSearchReferenceId.setOnClickListener {
            referenceId = binding.editTextReferenceId.text.toString()
            patientViewModel.checkReferenceId(binding.editTextReferenceId.text.toString())
        }

        binding.buttonNext.setOnClickListener {
            if (binding.editTextReferenceId.text.isNullOrEmpty()) {
                Toasty.error(
                    requireContext(),
                    "Custom Neti ID can't left empty.",
                    Toasty.LENGTH_LONG
                ).show()
            } else {
                findNavController().navigate(R.id.action_fragmentDoctorCornerAssignTwo_to_fragmentDoctorCornerAssignThree)
            }
        }
    }

    private fun initObservers() {

        Loaders.error.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(requireContext(), it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.error.value = null
            }
        })

        Loaders.success.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Loaders.success.value = null
            }
        })

        patientViewModel.fullName.observe(viewLifecycleOwner, Observer { fullName ->
            if (!fullName.isNullOrEmpty()) {
                binding.etName.text = fullName
                binding.buttonNext.isEnabled = true
                binding.buttonNext.setBackgroundResource(R.drawable.rec_gradient)
            }
        })

        patientViewModel.PhoneNo.observe(viewLifecycleOwner, Observer { phoneNo ->
            if (!phoneNo.isNullOrEmpty()) {
                binding.etPhone.text = phoneNo
            }
        })

    }

    private inner class MyTextWatcher(private val view: View) : TextWatcher {

        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            binding.buttonNext.isEnabled = false
            binding.buttonNext.setBackgroundColor(Color.GRAY)// From android.graphics.Color
            binding.buttonNext.setBackgroundResource(R.drawable.disable_color)
        }

        override fun afterTextChanged(editable: Editable) {}
    }

    companion object {
        var referenceId: String = ""
    }
}
