package com.netizen.medicom.view.fragment

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.netizen.medicom.R
import com.netizen.medicom.databinding.FragmentHomeBinding
import com.netizen.medicom.quickblox.activities.OpponentsActivity
import com.netizen.medicom.quickblox.services.LoginService
import com.netizen.medicom.quickblox.utils.SharedPrefsHelper
import com.netizen.medicom.view.activity.MainActivity
import com.netizen.medicom.viewModel.HomeViewModel

/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : Fragment() {

    private lateinit var binding: FragmentHomeBinding
    private lateinit var homeViewModel: HomeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        homeViewModel = ViewModelProvider(requireActivity(),
            HomeViewModel.HomeViewModelFactory(requireContext().applicationContext as Application))
            .get(HomeViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_home,
            container,
            false
        )

        MainActivity.showBottomNavigation()
        initViews()
        initObservers()

        return binding.root
    }

    private fun initViews() {

        binding.imageViewNav.setOnClickListener {
            NavFragment().show(requireActivity().supportFragmentManager, null)
        }

        binding.layoutPatient.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_patientCornerAssignOneFragment)
        }

        binding.layoutDoctor.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_fragmentDoctorCornerAssignOne)
        }
    }

    private fun initObservers() {

        homeViewModel.isPatientDashboardClicked.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().navigate(R.id.action_homeFragment_to_patientDashboardFragment)
                homeViewModel.isPatientDashboardClicked.value = false
            }
        })
        homeViewModel.isAddPatientClicked.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().navigate(R.id.action_homeFragment_to_addPatientFragment)
                homeViewModel.isAddPatientClicked.value = false
            }
        })

        homeViewModel.isPatientListClicked.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().navigate(R.id.action_homeFragment_to_patientListFragment)
                homeViewModel.isPatientListClicked.value = false
            }
        })

        homeViewModel.isStartCallClicked.observe(viewLifecycleOwner, Observer {
            if (it) {
                if (SharedPrefsHelper.hasQbUser()) {
                    LoginService.start(requireActivity(), SharedPrefsHelper.getQbUser())
                    OpponentsActivity.start(requireActivity())
                    activity?.finish()
                } else {

                }
                homeViewModel.isStartCallClicked.value = false
            }
        })
    }
}
