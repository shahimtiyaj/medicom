package com.netizen.medicom.view.fragment.splashScreens

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.ViewPager

import com.netizen.medicom.R
import com.netizen.medicom.adapter.SplashPagerAdapter
import com.netizen.medicom.databinding.FragmentSplashMainBinding

/**
 * A simple [Fragment] subclass.
 */
class SplashMainFragment : Fragment() {

    private lateinit var binding: FragmentSplashMainBinding

    private lateinit var viewpager: ViewPager
    private lateinit var adapter: SplashPagerAdapter

    private var currentPage = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_splash_main,
            container,
            false
        )

        viewpager = binding.viewPager
        binding.tabLayoutDots.setupWithViewPager(viewpager, true)

        initViews()
        setupViewPager()

        return binding.root
    }

    private fun initViews() {
        viewpager.addOnPageChangeListener(object: ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                if (position >= 3) {
                    binding.buttonNext.text = "Get Started"
                } else {
                    binding.buttonNext.text = "Next"
                }

                currentPage = position
            }
        })

        binding.buttonNext.setOnClickListener {
            viewpager.setCurrentItem(++currentPage, true)
        }
    }

    private fun setupViewPager() {
        adapter = SplashPagerAdapter(requireActivity().supportFragmentManager)

        adapter.addFragment(SplashFindDoctorFragment())
        adapter.addFragment(SplashSelectDoctorFragment())
        adapter.addFragment(SplashVideoChatFragment())
        adapter.addFragment(SplashGetTreatmentFragment())

        viewpager.adapter = adapter
    }
}
