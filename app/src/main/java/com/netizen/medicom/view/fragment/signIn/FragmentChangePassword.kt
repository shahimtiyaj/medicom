package com.netizen.medicom.view.fragment.signIn

import android.app.Application
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.netizen.medicom.R
import com.netizen.medicom.databinding.FragmentChangePasswordBinding
import com.netizen.medicom.utils.Loaders
import com.netizen.medicom.view.fragment.ToolBarFragment
import com.netizen.medicom.viewModel.ProfileViewModel
import com.netizen.medicom.viewModel.UserViewModel
import es.dmoral.toasty.Toasty

class FragmentChangePassword : Fragment() {

    private lateinit var binding: FragmentChangePasswordBinding
    private var userViewModel: UserViewModel? = null
    private var profileViewModel: ProfileViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userViewModel = ViewModelProvider(
            requireActivity(),
            UserViewModel.SignInViewModelFactory(requireContext().applicationContext as Application)
        ).get(UserViewModel::class.java)

        profileViewModel = ViewModelProvider(
            this,
            ProfileViewModel.ProfileViewModelFactory(requireContext().applicationContext as Application)
        ).get(ProfileViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_change_password, container, false)
        binding.lifecycleOwner = this

        initViews()
        initObservers()

        binding.lifecycleOwner = this

        profileViewModel?.getProfileInfo()

        return binding.root
    }

    private fun initViews() {
        val bundle = Bundle()
        bundle.putString("title", "Change Password")
        val toolBarFragment = ToolBarFragment()
        toolBarFragment.arguments = bundle

        val fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(binding.fragmentContainer.id, toolBarFragment)
        fragmentTransaction.commit()

        binding.changePassword.setOnClickListener {
            if (binding.editTextPassword.text.isNullOrEmpty()) {
                binding.editTextPassword.error = "Current Password is Required"
            } else if (binding.editTextNewPassword.text.isNullOrEmpty()) {
                binding.editTextNewPassword.error = "New Password is Required"
            } else if (binding.editTextRePass.text.isNullOrEmpty()) {
                binding.editTextRePass.error = "Password didn't match"
            } else {
                if (binding.editTextNewPassword.text.toString() != binding.editTextRePass.text.toString()) {
                    binding.editTextRePass.error = "Password didn't Match"
                }
                else {
                    userViewModel?.getChangePassword(
                        binding.editTextPassword.text.toString(),
                        binding.editTextNewPassword.text.toString()
                    )
                }
            }
        }
    }

    private fun initObservers() {
        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.error.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(requireContext(), it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.error.value = null
            }
        })

        Loaders.success.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(requireContext(), it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.success.value = null
            }
        })

        userViewModel?.isListFound?.observe(viewLifecycleOwner, Observer {
            if (it != null && it) {
                findNavController().navigate(R.id.action_fragmentForgotPasswordOne_to_fragmentForgotPasswordTwo)
                userViewModel!!.isListFound.value = false
            }
        })

        //Getting Information-------------------------------------------------------
        profileViewModel?.fullName?.observe(viewLifecycleOwner, Observer { fullName ->
            if (!fullName.isNullOrEmpty()) {
                binding.txtProfileName.text = fullName
            }
        })

        profileViewModel?.customNetiID?.observe(viewLifecycleOwner, Observer { customNetiId ->
            if (!customNetiId.isNullOrEmpty()) {
                binding.txtNetiId.text = "Neti ID : " + customNetiId
            }
        })

        profileViewModel?.imagePath?.observe(viewLifecycleOwner, Observer { imagepath ->
            if (!imagepath.isNullOrEmpty()) {
                profileViewModel?.getProfileImage(imagepath)
            }
        })

        profileViewModel?.photoFileContent?.observe(
            viewLifecycleOwner,
            Observer { photoFileContent ->
                if (!photoFileContent.isNullOrEmpty()) {
                    val theByteArray: ByteArray? = Base64.decode(photoFileContent, Base64.DEFAULT)
                    binding.profilePicId.setImageBitmap(theByteArray?.let {
                        convertToBitmap(it)
                    })
                }
            })
    }

    private fun convertToBitmap(b: ByteArray): Bitmap {
        Log.d("ArraySize", b.size.toString())
        return BitmapFactory.decodeByteArray(b, 0, b.size)
    }
}
