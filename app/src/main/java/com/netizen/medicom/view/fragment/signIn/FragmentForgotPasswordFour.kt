package com.netizen.medicom.view.fragment.signIn

import android.app.Application
import android.os.Bundle
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.netizen.medicom.R
import com.netizen.medicom.databinding.FragmentForgotPasswordFourBinding
import com.netizen.medicom.utils.Loaders
import com.netizen.medicom.utils.AppUtilsClass
import com.netizen.medicom.view.fragment.signIn.FragmentForgotPasswordTwo.Companion.username
import com.netizen.medicom.viewModel.UserViewModel
import es.dmoral.toasty.Toasty

class FragmentForgotPasswordFour : Fragment() {

    private lateinit var binding: FragmentForgotPasswordFourBinding
    private var userViewModel: UserViewModel? = null

    private var isPasswordVisible = false
    private var isRePasswordVisible = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userViewModel = ViewModelProvider(
            this,
            UserViewModel.SignInViewModelFactory(requireContext().applicationContext as Application)
        ).get(UserViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_forgot_password_four,
            container,
            false
        )

        initViews()
        initObservers()

        return binding.root
    }

    private fun initViews() {
        binding.imageViewPasswordToggle.setOnClickListener {
            if (isPasswordVisible) {
                isPasswordVisible = false
                binding.inputUserPassword.transformationMethod = PasswordTransformationMethod()
                binding.imageViewPasswordToggle.setImageResource(R.drawable.ic_visibility_off_blue_24dp)
            } else {
                isPasswordVisible = true
                binding.inputUserPassword.transformationMethod = null
                binding.imageViewPasswordToggle.setImageResource(R.drawable.ic_visibility_blue_24dp)
            }
        }

        binding.imageViewRePasswordToggle.setOnClickListener {
            if (isRePasswordVisible) {
                isRePasswordVisible = false
                binding.inputUserRePassword.transformationMethod = PasswordTransformationMethod()
                binding.imageViewRePasswordToggle.setImageResource(R.drawable.ic_visibility_off_blue_24dp)
            } else {
                isRePasswordVisible = true
                binding.inputUserRePassword.transformationMethod = null
                binding.imageViewRePasswordToggle.setImageResource(R.drawable.ic_visibility_blue_24dp)
            }
        }

        binding.resetPassword.setOnClickListener {
            when {
                binding.inputUserPassword.text.isNullOrEmpty() -> {
                    binding.inputUserPassword.error = "Password is Required"
                }
                binding.inputUserRePassword.text.isNullOrEmpty() -> {
                    binding.inputUserRePassword.error = "Password didn't Match"
                   // Toasty.error(context!!, "Password didn't match !", Toasty.LENGTH_LONG).show()
                }
                else -> {
                    if (binding.inputUserPassword.text.toString() != binding.inputUserRePassword.text.toString()) {
                        binding.inputUserRePassword.error = "Password didn't Match"
                    }
                    else if (!AppUtilsClass.isValidPassword(binding.inputUserPassword.text.toString())) {
                        binding.inputUserRePassword.error =
                            "Please choose a stronger password. Try a mix of letters, numbers and symbols."
                    } else if (!(binding.inputUserPassword.text.toString().length in 6..16)) {
                        binding.inputUserRePassword.error = "Password length minimum 6 and maximum 16 digit"
                    }
                    else {
                        userViewModel?.getResetPassword(
                            username,
                            binding.inputUserPassword.text.toString(),
                            binding.inputUserRePassword.text.toString()
                        )
                    }
                }
            }
        }
    }

    private fun initObservers() {
        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.error.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(requireContext(), it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.error.value = null
            }
        })

        Loaders.success.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(requireContext(), it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.success.value = null
                //findNavController().popBackStack(R.id.fragmentForgotPasswordThree, true)
                //findNavController().navigate(R.id.fragmentForgotPasswordFour)
                PasswordChangeSuccessDialogFragment().show(requireActivity().supportFragmentManager, null)
            }
        })

    }


}
