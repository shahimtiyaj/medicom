package com.netizen.medicom.view.fragment.doctorCorner

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController

import com.netizen.medicom.R
import com.netizen.medicom.databinding.FragmentDoctorCornerAssignSuccessfulBinding

class FragmentDoctorCornerAssignSuccessful : Fragment() {
    private lateinit var binding: FragmentDoctorCornerAssignSuccessfulBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_doctor_corner_assign_successful, container, false)

        initViews()

        return binding.root
    }

    private fun initViews() {
        binding.buttonOk.setOnClickListener {
            findNavController().navigate(R.id.action_fragmentDoctorCornerAssignSuccessful_to_fragmentDoctorDashboard)
        }
    }
}
