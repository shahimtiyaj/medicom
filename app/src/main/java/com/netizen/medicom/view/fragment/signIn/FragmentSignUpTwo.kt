package com.netizen.medicom.view.fragment.signIn

import android.app.Application
import android.app.DatePickerDialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.netizen.medicom.R
import com.netizen.medicom.databinding.FragmentSignUpTwoBinding
import com.netizen.medicom.utils.Loaders
import com.netizen.medicom.utils.AppUtilsClass
import com.netizen.medicom.utils.AppUtilsClass.Companion.isValidEmail
import com.netizen.medicom.view.fragment.SearchSpinnerDialogFragment
import com.netizen.medicom.viewModel.UserViewModel
import es.dmoral.toasty.Toasty
import java.text.SimpleDateFormat
import java.util.*


class FragmentSignUpTwo : Fragment() {

    private lateinit var binding: FragmentSignUpTwoBinding
    private var userViewModel: UserViewModel? = null
    private var bundleArgs: FragmentSignUpTwoArgs? = null
    private var tempDistList: ArrayList<String>? = null
    private var tempAreaList: ArrayList<String>? = null
    private var religion: String? = null
    private var gender: String? = null
    private var calendar = Calendar.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userViewModel = ViewModelProvider(
            requireActivity(),
            UserViewModel.SignInViewModelFactory(requireContext().applicationContext as Application)
        ).get(UserViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sign_up_two, container, false)

        binding.lifecycleOwner = this
        initViews()
        initObservables()
        userViewModel?.getDistrictList()

        bundleArgs = arguments?.let { FragmentSignUpTwoArgs.fromBundle(it) }
        //binding.userTitle.text = bundleArgs?.username

        return binding.root
    }

    private fun initViews() {
        binding.loginHere.setOnClickListener {
            findNavController().navigate(R.id.action_fragmentSignUpTwo_to_signInFragment)
        }

        binding.mobile.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (!s.toString().startsWith("01", true)) {
                    binding.mobile.error = getString(R.string.err_msg_phone_invalid)
                }
            }
        })

        binding.signup.setOnClickListener {

            if (!isValidEmail(binding.email.text.toString()))
            {
                binding.email.error = getString(R.string.err_msg_email)
            }
            else{
                userViewModel?.onSignUpClick(
                    binding.inputUserFullName.text.toString(),
                    gender,
                    religion,
                    binding.birthday.text.toString(),
                    binding.mobile.text.toString(),
                    binding.email.text.toString(),
                    bundleArgs?.username,
                    bundleArgs?.password
                )
            }
        }

        binding.district.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("dataType", "user")
            bundle.putString("title", "district")
            bundle.putInt("spinnerType", 1)

            val searchSpinnerFragment = SearchSpinnerDialogFragment()
            searchSpinnerFragment.arguments = bundle
            searchSpinnerFragment.show(requireActivity().supportFragmentManager, null)
        }

        binding.area.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("dataType", "user")
            bundle.putString("title", "area")
            bundle.putInt("spinnerType", 2)

            val searchSpinnerFragment = SearchSpinnerDialogFragment()
            searchSpinnerFragment.arguments = bundle
            searchSpinnerFragment.show(requireActivity().supportFragmentManager, null)
        }

        binding.spinnerReligion.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {}
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    if (position != 0) {
                        religion = parent!!.getItemAtPosition(position).toString()
                    }
                }
            }

        binding.rdGroup.setOnCheckedChangeListener() { group, checkedId ->
            if (binding.rdbMale.isChecked) {
                gender = "Male"
                Log.d("Gender", gender)
            } else if (binding.rdbFemale.isChecked) {
                gender = "Female"
                Log.d("Gender", gender)
            }
            else{
                gender = "Others"
            }
        }

        binding.birthday.setOnClickListener {
            AppUtilsClass.showsDatePicker(requireContext(), dateSetListener)
        }
    }

    private fun initObservables() {

        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })
        Loaders.success?.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Loaders.success.value = null
                val bundle = Bundle()
                bundle.putString("name", binding.inputUserFullName.text.toString())
                val regFragment = RegSuccessDialogFragment()
                regFragment.arguments = bundle
                regFragment.show(requireActivity().supportFragmentManager, null)
                Loaders.success.value = null

              //  RegSuccessDialogFragment().show(activity!!.supportFragmentManager, null)
            }
        })
        Loaders.error?.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(requireContext(), it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.error?.value = null
            }
        })

        Loaders.searchValue1.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                binding.area.text = null
                binding.district.setText(it)
                userViewModel?.getCoreCategoryIdDistrict(it)
            }
        })

        Loaders.searchValue2.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                binding.area.setText(it)
                userViewModel?.getCoreCategoryIdArea(it)
            }
        })

        //Sign Up Page
        userViewModel?.tempDistrictList?.observe(viewLifecycleOwner, Observer { tempDistList ->
            if (!tempDistList.isNullOrEmpty()) {
                this.tempDistList = tempDistList
            }
        })

        userViewModel?.tempAreaList?.observe(viewLifecycleOwner, Observer { tempAreaList ->
            if (!tempAreaList.isNullOrEmpty()) {
                this.tempAreaList = tempAreaList
            }
        })

        userViewModel?.isFullnameEmpty?.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.inputUserFullName.error = "User full name can't left empty"
                userViewModel?.isFullnameEmpty!!.value = false
            }
        })

        userViewModel?.isGenderEmpty?.observe(viewLifecycleOwner, Observer {
            if (it) {
                showErrorToast("Gender can't left empty")
                userViewModel?.isGenderEmpty!!.value = false
            }
        })

        userViewModel?.isReligionEmpty?.observe(viewLifecycleOwner, Observer {
            if (it) {
                showErrorToast("Religion can't left empty")
                userViewModel?.isReligionEmpty!!.value = false
            }
        })

        userViewModel?.isBirthdayEmpty?.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.birthday.error = "Birth date can't left empty"
                showErrorToast("Birth date can't left empty")
                userViewModel?.isBirthdayEmpty!!.value = false
            }
        })
        userViewModel?.isMobileEmpty?.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.mobile.error = "Mobile number can't left empty."
                userViewModel?.isMobileEmpty!!.value = false
            }
        })

        userViewModel?.isEmailEmpty?.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.email.error = "Email can't left empty."
                userViewModel?.isEmailEmpty!!.value = false
            }
        })

        userViewModel?.isDistrictEmpty?.observe(viewLifecycleOwner, Observer {
            if (it) {
                showErrorToast("District can't left empty.")
                userViewModel?.isDistrictEmpty!!.value = false
            }
        })

        userViewModel?.isAreaEmpty?.observe(viewLifecycleOwner, Observer {
            if (it) {
                showErrorToast("Area can't left empty.")
                userViewModel?.isAreaEmpty!!.value = false
            }
        })
    }

    private val dateSetListener =
        DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            calendar.set(Calendar.YEAR, year)
            calendar.set(Calendar.MONTH, monthOfYear)
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
            val dateFormat1 = SimpleDateFormat("dd/MM/yy", Locale.US)
            binding.birthday.setText(dateFormat.format(calendar.time))
            binding.birthday.error=null
        }

    private fun showErrorToast(message: String) {
        Toasty.error(requireContext(), message, Toasty.LENGTH_LONG).show()
    }
}
