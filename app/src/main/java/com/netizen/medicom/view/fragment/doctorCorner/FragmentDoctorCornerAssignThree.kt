package com.netizen.medicom.view.fragment.doctorCorner

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.netizen.medicom.R
import com.netizen.medicom.databinding.FragmentDoctorCornerAssignThreeBinding
import com.netizen.medicom.utils.Loaders
import com.netizen.medicom.view.fragment.SearchSpinnerDialogFragment
import com.netizen.medicom.view.fragment.doctorCorner.FragmentDoctorCornerAssignTwo.Companion.referenceId
import com.netizen.medicom.viewModel.DoctorViewModel
import es.dmoral.toasty.Toasty

class FragmentDoctorCornerAssignThree : Fragment() {

    private lateinit var binding: FragmentDoctorCornerAssignThreeBinding
    private var doctorViewModel: DoctorViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        doctorViewModel = ViewModelProvider(
            requireActivity(),
            DoctorViewModel.DoctorViewModelFactory(requireContext().applicationContext as Application)
        ).get(DoctorViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_doctor_corner_assign_three,
            container,
            false
        )

        initViews()
        initObservers()

        return binding.root
    }

    private fun initViews() {

        binding.editTextDesignation.setOnClickListener {
            doctorViewModel?.getDoctorDesignation()
            val bundle = Bundle()
            bundle.putString("dataType", "doctor")
            bundle.putString("title", "designation")
            bundle.putInt("spinnerType", 1)

            val searchSpinnerFragment = SearchSpinnerDialogFragment()
            searchSpinnerFragment.arguments = bundle
            searchSpinnerFragment.show(requireActivity().supportFragmentManager, null)
        }

        binding.editTextSpeciality.setOnClickListener {
            doctorViewModel?.getSpecialist()
            val bundle = Bundle()
            bundle.putString("dataType", "doctor")
            bundle.putString("title", "specialist")
            bundle.putInt("spinnerType", 2)

            val searchSpinnerFragment = SearchSpinnerDialogFragment()
            searchSpinnerFragment.arguments = bundle
            searchSpinnerFragment.show(requireActivity().supportFragmentManager, null)
        }

        /* binding.buttonNext.setOnClickListener {
             findNavController().navigate(R.id.action_fragmentDoctorCornerAssignThree_to_fragmentDoctorCornerAssignFour)
         }*/

        binding.buttonNext.setOnClickListener {

/*            doctorViewModel?.onSignUpClick(
                "1000000665",
                "3605",
                "3615",
                "Dhaka medical",
                "1011",
                "Very sad",
                "No",
                "200",
                "300",
                "400"
            )*/

            doctorViewModel?.onSignUpClick(
                referenceId,
                "3605",
                "3615",
                binding.editTextInstitute.text.toString(),
                binding.editTextBmdcRegNo.text.toString(),
                binding.editTextDescription.text.toString(),
                binding.editTextDegree.text.toString(),
                "200",
                "300",
                "400"
            )
        }
    }

    private fun initObservers() {

        Loaders.error.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(requireContext(), it, Toasty.LENGTH_LONG).show()
                Loaders.error.value = null
            }
        })

        Loaders.success.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(requireContext(), it, Toasty.LENGTH_LONG).show()
                Loaders.success?.value = null
                findNavController().navigate(R.id.action_fragmentDoctorCornerAssignThree_to_fragmentDoctorCornerAssignFour)
            }
        })

        Loaders.searchValue1.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                binding.editTextDesignation.setText(it)
                designationCoreCategoryId = doctorViewModel?.getDoctorDesignationId(it).toString()
            }
        })

        Loaders.searchValue2.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                binding.editTextSpeciality.setText(it)
            }
        })

        doctorViewModel?.isDesignationId?.observe(viewLifecycleOwner, Observer {
            if (it) {
                Toasty.error(requireContext(), "Designation can't left empty", Toast.LENGTH_LONG)
                    .show()
                doctorViewModel?.isDesignationId!!.value = false
            }
        })

        doctorViewModel?.isSpecialistId?.observe(viewLifecycleOwner, Observer {
            if (it) {
                Toasty.error(requireContext(), "Specialist can't left empty", Toast.LENGTH_LONG)
                    .show()
                doctorViewModel?.isSpecialistId!!.value = false
            }
        })

        doctorViewModel?.isInstituteName?.observe(viewLifecycleOwner, Observer {
            if (it) {
                Toasty.error(requireContext(), "Institute name can't left empty", Toast.LENGTH_LONG)
                    .show()
                doctorViewModel?.isInstituteName!!.value = false
            }
        })

        doctorViewModel?.isBmdcNumber?.observe(viewLifecycleOwner, Observer {
            if (it) {
                Toasty.error(requireContext(), "BMDC number can't left empty", Toast.LENGTH_LONG).show()
                doctorViewModel?.isBmdcNumber!!.value = false
            }
        })

        doctorViewModel?.isDescription?.observe(viewLifecycleOwner, Observer {
            if (it) {
                Toasty.error(requireContext(), "Description can't left empty", Toast.LENGTH_LONG)
                    .show()
                doctorViewModel?.isDescription!!.value = false
            }
        })

        doctorViewModel?.isDegreeInfo?.observe(viewLifecycleOwner, Observer {
            if (it) {
                Toasty.error(requireContext(), "Degree Info can't left empty", Toast.LENGTH_LONG)
                    .show()
                doctorViewModel?.isDegreeInfo!!.value = false
            }
        })
    }

    companion object {
        var designationCoreCategoryId: String = ""
    }
}
