package com.netizen.medicom.view.fragment.doctorCorner.prescription

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil

import com.netizen.medicom.R
import com.netizen.medicom.databinding.FragmentPrescriptionHomeBinding
import com.netizen.medicom.view.fragment.ToolBarFragment

/**
 * A simple [Fragment] subclass.
 */
class PrescriptionHomeFragment : Fragment() {

    private lateinit var binding: FragmentPrescriptionHomeBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_prescription_home,
            container,
            false
        )

        initViews()

        return binding.root
    }

    private fun initViews() {
//        val bundle = Bundle()
//        bundle.putString("title", "Offer Product")
//
//        val toolBarFragment = ToolBarFragment()
//        toolBarFragment.arguments = bundle
//
//        val fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
//        fragmentTransaction.add(binding.fragmentContainer.id, toolBarFragment)
//        fragmentTransaction.commit()
        
        requireActivity().supportFragmentManager.beginTransaction().add(binding.fragmentContainer.id, ToolBarFragment()).commit()
    }
}
