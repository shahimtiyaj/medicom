package com.netizen.medicom.view.fragment.patientCorner.patientCornerAssign

import android.app.Application
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.netizen.medicom.R
import com.netizen.medicom.databinding.FragmentPatientCornerAssignTwoBinding
import com.netizen.medicom.utils.Loaders
import com.netizen.medicom.viewModel.PatientViewModel
import es.dmoral.toasty.Toasty
import androidx.navigation.fragment.findNavController

/**
 * A simple [Fragment] subclass.
 */
class PatientCornerAssignTwoFragment : Fragment() {

    private lateinit var binding: FragmentPatientCornerAssignTwoBinding
    private lateinit var patientViewModel: PatientViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        patientViewModel = ViewModelProvider(requireActivity(), PatientViewModel.PatientViewModelFactory(requireContext().applicationContext as Application)
        ).get(PatientViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_patient_corner_assign_two, container, false)

        initViews()

        initObservers()

        return binding.root
    }

    private fun initViews() {

        binding.etReferenceId.addTextChangedListener(MyTextWatcher(binding.etReferenceId))

        binding.btnSearchReferenceId.setOnClickListener {
            patientViewModel.checkReferenceId(binding.etReferenceId.text.toString())
        }

        binding.buttonSubmit.setOnClickListener {
            patientViewModel.patientRegSubmit(binding.etReferenceId.text.toString())
        }
    }

    private fun initObservers() {

        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.error.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(requireContext(), it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.error.value = null
            }
        })

        Loaders.success.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
               // Toasty.success(requireContext(), it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.success.value = null
                findNavController().navigate(R.id.action_patientCornerAssignTwoFragment_to_partnerCornerAssignSuccessfulFragment)
            }
        })

        patientViewModel.fullName.observe(viewLifecycleOwner, Observer { fullName ->
            if (!fullName.isNullOrEmpty()) {
                binding.etName.text = fullName
                binding.buttonSubmit?.isEnabled = true
                binding.buttonSubmit.setBackgroundResource(R.drawable.rec_gradient)
            }
        })

        patientViewModel.PhoneNo.observe(viewLifecycleOwner, Observer { phoneNo ->
            if (!phoneNo.isNullOrEmpty()) {
                binding.etPhone.text = phoneNo
            }
        })
    }

    private inner class MyTextWatcher(private val view: View) : TextWatcher {

        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

        }

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            binding.buttonSubmit.isEnabled = false
            binding.buttonSubmit.setBackgroundColor(Color.GRAY)// From android.graphics.Color
            binding.buttonSubmit.setBackgroundResource(R.drawable.disable_color)
        }

        override fun afterTextChanged(editable: Editable) {

        }
    }
}
