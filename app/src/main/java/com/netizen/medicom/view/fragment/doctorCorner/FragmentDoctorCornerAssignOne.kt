package com.netizen.medicom.view.fragment.doctorCorner

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.netizen.medicom.R
import com.netizen.medicom.databinding.FragmentDoctorCornerAssignOneBinding

class FragmentDoctorCornerAssignOne : Fragment() {
    private lateinit var binding: FragmentDoctorCornerAssignOneBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_doctor_corner_assign_one,
            container,
            false
        )

        initViews()

        return binding.root
    }

    private fun initViews() {
        binding.buttonYes.setOnClickListener {
            findNavController().navigate(R.id.action_fragmentDoctorCornerAssignOne_to_fragmentDoctorCornerAssignTwo)
        }
    }
}
