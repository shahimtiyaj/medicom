package com.netizen.medicom.view.fragment.signIn

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.netizen.medicom.R
import com.netizen.medicom.databinding.FragmentForgotPasswordOneBinding
import com.netizen.medicom.model.UserCheckForgotPass
import com.netizen.medicom.utils.Loaders
import com.netizen.medicom.viewModel.UserViewModel
import es.dmoral.toasty.Toasty

class FragmentForgotPasswordOne : Fragment() {
    private lateinit var binding: FragmentForgotPasswordOneBinding
    private var userViewModel: UserViewModel? = null
    private var userInfoList= UserCheckForgotPass()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userViewModel = ViewModelProvider(
            requireActivity(),
            UserViewModel.SignInViewModelFactory(requireContext().applicationContext as Application)
        ).get(UserViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_forgot_password_one, container, false)
        binding.lifecycleOwner = this

        initViews()
        initObservers()

        return binding.root
    }

    private fun initViews() {
        binding.next.setOnClickListener {
            if (binding.inputUserName.text.isNullOrEmpty()){
                binding.inputUserName.error=resources.getString(R.string.user_name_error)
            }
            else{
                userViewModel?.getUserInfoForgotPassword(binding.inputUserName.text.toString())
            }
        }

        binding.loginHere.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    private fun initObservers() {
        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.error.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(requireContext(), it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.error.value = null
            }
        })

        Loaders.success.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(requireContext(), it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.success.value = null
            }
        })

        userViewModel?.isListFound?.observe(viewLifecycleOwner, Observer {
            if (it!= null && it) {
                //findNavController().popBackStack(R.id.fragmentForgotPasswordOne, true);
                findNavController().navigate(R.id.action_fragmentForgotPasswordOne_to_fragmentForgotPasswordTwo)
                userViewModel!!.isListFound.value = false
            }
        })
    }
}
