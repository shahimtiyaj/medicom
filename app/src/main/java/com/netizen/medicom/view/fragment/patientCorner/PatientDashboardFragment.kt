package com.netizen.medicom.view.fragment.patientCorner

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.netizen.medicom.R
import com.netizen.medicom.databinding.FragmentPatientDashboardBinding

/**
 * A simple [Fragment] subclass.
 */
class PatientDashboardFragment : Fragment() {
    private lateinit var binding: FragmentPatientDashboardBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_patient_dashboard, container, false)

        initViews()

        return binding.root
    }

    private fun initViews() {
        binding.addPatient.setOnClickListener {
            findNavController().navigate(R.id.action_patientDashboardFragment_to_addPatientFragment)
        }
    }
}
