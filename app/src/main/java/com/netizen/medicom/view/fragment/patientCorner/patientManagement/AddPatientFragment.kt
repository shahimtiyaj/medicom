package com.netizen.medicom.view.fragment.patientCorner.patientManagement

import android.app.Application
import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.netizen.medicom.R
import com.netizen.medicom.databinding.FragmentAddPatientBinding
import com.netizen.medicom.utils.AppUtilsClass
import com.netizen.medicom.utils.AppUtilsClass.Companion.requestFocus
import com.netizen.medicom.utils.Loaders
import com.netizen.medicom.utils.MyUtilsClass
import com.netizen.medicom.viewModel.PatientViewModel
import es.dmoral.toasty.Toasty
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class AddPatientFragment : Fragment() {
    private lateinit var binding: FragmentAddPatientBinding
    private lateinit var patientViewModel: PatientViewModel
    private var calendar = Calendar.getInstance()
    private var gender: String? = null
    private var bloodGroup: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        patientViewModel = ViewModelProvider(requireActivity(), PatientViewModel.PatientViewModelFactory(requireContext().applicationContext as Application)
        ).get(PatientViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_patient, container, false)

        initViews()

        initObservers()

        return binding.root
    }

    private fun initViews() {

        binding.btnViewPatienList.setOnClickListener {
            findNavController().navigate(R.id.action_addPatientFragment_to_patientListFragment)
        }

        binding.addPatient.setOnClickListener {
                patientViewModel.onAddPatient(
                    binding.etRelation.text.toString(),
                    binding.etName.text.toString(),
                    binding.etPhone.text.toString(),
                    binding.etEmail.text.toString(),
                    binding.etDateOfBirth.text.toString(),
                    gender,
                    bloodGroup, "")
        }

        binding.etDateOfBirth.setOnClickListener {
            MyUtilsClass.showsDatePicker(requireContext(), dateSetListener)
        }

        binding.spinnerGender.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {}
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    if (position != 0) {
                        gender = parent!!.getItemAtPosition(position).toString()
                    }
                }
            }

        binding.spinnerBG.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {}
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    if (position != 0) {
                        bloodGroup = parent!!.getItemAtPosition(position).toString()
                    }
                }
            }
    }

    private fun initObservers() {

        Loaders.error.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(requireContext(), it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.error.value = null
            }
        })

        Loaders.success.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Loaders.success.value = null
                //findNavController().navigate(R.id.action_patientCornerAssignTwoFragment_to_partnerCornerAssignSuccessfulFragment)
            }
        })

        patientViewModel.isRelation.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.etRelation.error = "Patient relation can't left empty"
                patientViewModel.isRelation.value = false
                requestFocus(binding.etRelation, requireActivity())
            }
        })

        patientViewModel.isName.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.etName.error = "Patient name can't left empty"
                patientViewModel.isName.value = false
                requestFocus(binding.etName, requireActivity())

            }
        })

        patientViewModel.isMobile.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.etPhone.error = "Patient phone no. can't left empty"
                patientViewModel.isMobile.value = false
                requestFocus(binding.etPhone, requireActivity())

            }
        })

        patientViewModel.isEmail.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.etEmail.error = "Patient email can't left empty"
                patientViewModel.isEmail.value = false
                requestFocus(binding.etEmail, requireActivity())

            }
        })

        patientViewModel.isBirthday.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.etDateOfBirth.error = "Patient birthday can't left empty"
                patientViewModel.isBirthday.value = false
                requestFocus(binding.etDateOfBirth, requireActivity())

            }
        })

        patientViewModel.isGender.observe(viewLifecycleOwner, Observer {
            if (it) {
                showErrorToast("Patient Gender can't left empty.")
                patientViewModel.isGender.value = false
            }
        })

        patientViewModel.isBloodGroup.observe(viewLifecycleOwner, Observer {
            if (it) {
                showErrorToast("Patient Blood Group can't left empty.")
                patientViewModel.isBloodGroup.value = false
            }
        })
    }

    private val dateSetListener =
        DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            calendar.set(Calendar.YEAR, year)
            calendar.set(Calendar.MONTH, monthOfYear)
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
            binding.etDateOfBirth.setText(dateFormat.format(calendar.time))
            binding.etDateOfBirth.error=null
        }

    private fun showErrorToast(message: String) {
        Toasty.error(requireContext(), message, Toasty.LENGTH_LONG).show()
    }
}
