package com.netizen.medicom.view.fragment.signIn

import android.app.Application
import android.os.Bundle
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.netizen.medicom.R
import com.netizen.medicom.databinding.FragmentSignUpOneBinding
import com.netizen.medicom.utils.AppUtilsClass
import com.netizen.medicom.utils.AppUtilsClass.Companion.isValidEmail
import com.netizen.medicom.utils.AppUtilsClass.Companion.requestFocus
import com.netizen.medicom.viewModel.UserViewModel

class FragmentSignUpOne : Fragment() {

    private lateinit var binding: FragmentSignUpOneBinding
    private var userViewModel: UserViewModel? = null

    private var isPasswordVisible = false
    private var isRePasswordVisible = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userViewModel = ViewModelProvider(
            this,
            UserViewModel.SignInViewModelFactory(requireContext().applicationContext as Application)
        ).get(UserViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sign_up_one, container, false)

        initViews()

        return binding.root
    }

    private fun initViews() {
        binding.imageViewPasswordToggle.setOnClickListener {
            if (isPasswordVisible) {
                isPasswordVisible = false
                binding.inputUserPassword.transformationMethod = PasswordTransformationMethod()
                binding.imageViewPasswordToggle.setImageResource(R.drawable.ic_visibility_off_blue_24dp)
            } else {
                isPasswordVisible = true
                binding.inputUserPassword.transformationMethod = null
                binding.imageViewPasswordToggle.setImageResource(R.drawable.ic_visibility_blue_24dp)
            }
        }

        binding.imageViewRePasswordToggle.setOnClickListener {
            if (isRePasswordVisible) {
                isRePasswordVisible = false
                binding.inputUserRePassword.transformationMethod = PasswordTransformationMethod()
                binding.imageViewRePasswordToggle.setImageResource(R.drawable.ic_visibility_off_blue_24dp)
            } else {
                isRePasswordVisible = true
                binding.inputUserRePassword.transformationMethod = null
                binding.imageViewRePasswordToggle.setImageResource(R.drawable.ic_visibility_blue_24dp)
            }
        }

        binding.next.setOnClickListener {
            when {
                binding.inputUserName.text.isNullOrEmpty() -> {
                    binding.inputUserName.error = "Username can't left empty"
                    requestFocus(binding.inputUserName, requireActivity())
                }

                binding.inputUserPassword.text.isNullOrEmpty() -> {
                    binding.inputUserPassword.error = "Password can't left empty"
                    requestFocus(binding.inputUserPassword, requireActivity())
                }

                binding.inputUserRePassword.text.isNullOrEmpty() -> {
                    binding.inputUserRePassword.error = "Password didn't Match"
                    requestFocus(binding.inputUserRePassword, requireActivity())
                }

                else -> {

                    val validNumber = "^[+]?[0-9]{8,15}$" .toRegex()

                    if (!isValidEmail(binding.inputUserName.text.toString()))
                    {
                        binding.inputUserName.error = "Username must be phone number or email."
                        requestFocus(binding.inputUserName, requireActivity())
                    }

                    else if (binding.inputUserPassword.text.toString() != binding.inputUserRePassword.text.toString()) {
                        binding.inputUserRePassword.error = "Password didn't Match"
                        requestFocus(binding.inputUserRePassword, requireActivity())

                    } else if (!AppUtilsClass.isValidPassword(binding.inputUserPassword.text.toString())) {
                        binding.inputUserRePassword.error =
                            "Please choose a stronger password. Try a mix of letters, numbers and symbols."
                        requestFocus(binding.inputUserRePassword, requireActivity())

                    } else if (!(binding.inputUserPassword.text.toString().length in 6..16)) {
                        binding.inputUserRePassword.error = "Password length min 6 and max 16"
                        requestFocus(binding.inputUserRePassword, requireActivity())

                    } else {
                        val action =
                            FragmentSignUpOneDirections.actionFragmentSignUpOneToFragmentSignUpTwo(
                                binding.inputUserName.text.toString(),
                                binding.inputUserPassword.text.toString()
                            )
                        findNavController().navigate(action)
                    }
                }
            }
        }

        binding.loginHere.setOnClickListener {
            findNavController().popBackStack(R.id.signInFragment, true)
        }
    }
}
