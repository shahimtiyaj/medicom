package com.netizen.medicom.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.netizen.medicom.R
import com.netizen.medicom.databinding.FragmentToolBarBinding

/**
 * A simple [Fragment] subclass.
 */
class ToolBarFragment : Fragment() {

    private lateinit var binding: FragmentToolBarBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_tool_bar,
            container,
            false
        )

        binding.toolbarTitle.text = arguments?.getString("title")

        initViews()

        return binding.root
    }

    private fun initViews() {
//        binding.imageViewBack.setOnClickListener {
//            findNavController().popBackStack()
//        }
//
//        binding.textViewSetting.setOnClickListener {
//            Loaders.isToolbarSettingClicked.value = true
//        }
//
//        binding.layoutNotification.notificationBadge.setOnClickListener {
//            Loaders.isToolbarNotificationClicked.value = true
//        }
    }
}
