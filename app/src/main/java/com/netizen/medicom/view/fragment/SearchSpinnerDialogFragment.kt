package com.netizen.medicom.view.fragment

import android.app.Application
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.netizen.medicom.R
import com.netizen.medicom.adapter.SearchSpinnerAdapter
import com.netizen.medicom.databinding.FragmentSearchSpinnerBinding
import com.netizen.medicom.utils.Loaders
import com.netizen.medicom.viewModel.DoctorViewModel

/**
 * A simple [Fragment] subclass.
 */
class SearchSpinnerDialogFragment : DialogFragment() {

    private lateinit var binding: FragmentSearchSpinnerBinding

    private var doctorViewModel: DoctorViewModel? = null

    private var spinnerDataList: ArrayList<String>? = null

    private var dataType: String? = null

    private var spinnerType: Int? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search_spinner, container, false)

        binding.textViewSearch.text = "Search " + requireArguments().getString("title")

        initViewModels()
        initViews()
        initObservers()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataType = requireArguments().getString("dataType")?.toLowerCase()
        spinnerType = requireArguments().getInt("spinnerType")
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val params = dialog!!.window!!.attributes
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog!!.window!!.attributes = params
    }

    private fun initViewModels() {
        when (dataType) {
            "doctor" -> {
                doctorViewModel =
                    ViewModelProvider(requireActivity(), DoctorViewModel.DoctorViewModelFactory(requireContext().applicationContext as Application)).get(
                        DoctorViewModel::class.java
                    )
            }
        }
    }

    private fun initViews() {
        binding.textViewClose.setOnClickListener { dismiss() }
        binding.editTextSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val tempSpinnerDataList = ArrayList<String>()
                spinnerDataList?.forEach { spinnerData ->
                    if (spinnerData.toLowerCase().contains(s.toString(), true)) {
                        tempSpinnerDataList.add(spinnerData)
                    }

                    setSpinnerDataList(tempSpinnerDataList)
                }
            }
        })
    }

    private fun initObservers() {

        doctorViewModel?.tempDoctorDesignationList?.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty() && spinnerType == 1) {
                spinnerDataList = it
                setSpinnerDataList(it)
            }
        })

        doctorViewModel?.tempSpecialistList?.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty() && spinnerType == 2) {
                spinnerDataList = it
                setSpinnerDataList(it)
            }
        })
        Loaders.searchValue1.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Loaders.searchValue1.postValue(null)
                dismiss()
            }
        })

        Loaders.searchValue2.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Loaders.searchValue2.postValue(null)
                dismiss()
            }
        })
    }

    private fun setSpinnerDataList(tempList: List<String>) {
        binding.recyclerViewSearchSpinnerData.layoutManager = LinearLayoutManager(context)
        binding.recyclerViewSearchSpinnerData.setHasFixedSize(true)
        binding.recyclerViewSearchSpinnerData.adapter = SearchSpinnerAdapter(requireContext(), tempList, spinnerType)
    }
}
