package com.netizen.medicom.repository

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.netizen.medicom.apiService.ApiClient
import com.netizen.medicom.apiService.ApiInterface
import com.netizen.medicom.model.*
import com.netizen.medicom.utils.AppPreferences
import com.netizen.medicom.utils.Loaders
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class PatientRepository(application: Application) {

    private val appPreferences = AppPreferences(application)

    var fullName = MutableLiveData<String>()
    var phoneNo = MutableLiveData<String>()
    val patientFamilyMemberArrayList = MutableLiveData<List<PatientFamilyMemberList.Item?>>()
    var isPatientFamilyMemberFound = MutableLiveData<Boolean>()

    fun patientRegistration(referenceId: String) {

        Loaders.isLoading0.value = true

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${appPreferences.getToken()}"

        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.patientRegistrationSubmit(
            "bearer " + appPreferences.getToken(), referenceId)

        //calling the api---------------------------------------------------------------
        call?.enqueue(object : Callback<PatientEnrollResponse> {
            override fun onResponse(call: Call<PatientEnrollResponse>, response: Response<PatientEnrollResponse>) {
                try {

                    Log.d("onResponse", "Patient Registration Success:" + response.body())

                    when {
                        response.code() == 201 -> {
                            Loaders.success.postValue("Patient Registration Successfully Completed.")
                        }

                        response.code() == 409 ->{
                            Loaders.error.postValue("User Already Exists!")
                            Loaders.isLoading0.value = false
                        }
                        else -> {
                            Loaders.error.postValue("Patient Registration Failed !")
                            Loaders.isLoading0.value = false
                        }
                    }
                    Loaders.isLoading0.value = false
                }

                catch (e: Exception){
                    e.printStackTrace()
                    Loaders.isLoading0.value = false
                    Loaders.error.postValue("Something went wrong! Please try again.")
                }
            }

            override fun onFailure(call: Call<PatientEnrollResponse>, t: Throwable) {
                Log.d("onFailure", t.toString())
                Loaders.isLoading0.value = false
                Loaders.error.postValue("Something went wrong! Please try again.")
            }
        })
    }

    fun getPersonInfoForPatient(customNetiID: String) {

        Loaders.isLoading0.value = true

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${appPreferences.getToken()}"
        Log.d("Custom Neti Id: ", customNetiID)

        val service = ApiClient.getClient?.create(ApiInterface::class.java)

        val userCall = service?.getDataSearchByNetiID(header, customNetiID)

        userCall?.enqueue(object : Callback<String> {

            override fun onResponse(call: Call<String>, response: Response<String>) {

                try {
                    Log.d("onResponse", "Main Wallet Transfer response :" + response.body().toString())

                    if (response.code() == 302) {
                        val walletTransfer = response.errorBody()?.source()?.buffer()?.readUtf8()
                        val jsonObj = JSONObject(walletTransfer!!)
                        fullName.postValue(jsonObj.getString("fullName"))
                        phoneNo.postValue(jsonObj.getString("basicMobile"))
                    }

                    else {
                        Loaders.isLoading0.value = false
                        Loaders.error.value = "Opps ! Something Wrong"
                    }
                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    e.printStackTrace()
                    Loaders.isLoading0.value = false
                    Loaders.error.value = "Something went wrong! Please try again."
                }
            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                Log.d("onFailure", t.toString())
                Loaders.isLoading0.value = false
                Loaders.error.value = "Couldn't get reference ID! Please try again."
            }
        })
    }

    fun addPatientFamilyMember(addPatient: AddPatientFamilyMember) {

        Loaders.isLoading0.value = true

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${appPreferences.getToken()}"

        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.addPatientSubmit(
            "bearer " + appPreferences.getToken(), addPatient)

        //calling the api---------------------------------------------------------------
        call?.enqueue(object : Callback<PatientEnrollResponse> {
            override fun onResponse(call: Call<PatientEnrollResponse>, response: Response<PatientEnrollResponse>) {
                try {
                    Log.d("onResponse", "Add Patient Success:" + response.body())

                    when {
                        response.code() == 201 -> {
                            Loaders.success.postValue("Add Patient Successfully.")
                        }

                        response.code() == 409 ->{
                            Loaders.error.postValue("User Already Exists!")
                            Loaders.isLoading0.value = false
                        }

                        else -> {
                            Loaders.error.postValue("Add Patient Failed !")
                            Loaders.isLoading0.value = false
                        }
                    }
                    Loaders.isLoading0.value = false
                }

                catch (e: Exception){
                    e.printStackTrace()
                    Loaders.isLoading0.value = false
                    Loaders.error.postValue("Something went wrong! Please try again.")
                }
            }

            override fun onFailure(call: Call<PatientEnrollResponse>, t: Throwable) {
                Log.d("onFailure", t.toString())
                Loaders.isLoading0.value = false
                Loaders.error.postValue("Something went wrong! Please try again.")
            }
        })
    }

    //Add Family member list reports-------------------------------------------------------

    fun getPatientFamilyMemberData() {

        Loaders.isLoading0.value = true

        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.getPatientFamilyMemberList("bearer " + appPreferences.getToken())
        val  emptyList= ArrayList<PatientFamilyMemberList.Item?>()

        //calling the api
        call?.enqueue(object : Callback<PatientFamilyMemberList> {
            override fun onResponse(
                call: Call<PatientFamilyMemberList>,
                response: Response<PatientFamilyMemberList>
            ) {
                try {
                    Log.d("onResponse", "Balance Deposit:" + response.body())

                    if (response.code() == 200) {
                        Log.d("onResponse", "Token:" + response.body())
                        patientFamilyMemberArrayList.postValue(response.body()?.getItem())
                        isPatientFamilyMemberFound.value = true
                    }
                    else{
                        isPatientFamilyMemberFound.value = false
                        patientFamilyMemberArrayList.postValue(emptyList)
                        Loaders.apiError.value ="Sorry ! No Data Found."
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    isPatientFamilyMemberFound.value = false
                    patientFamilyMemberArrayList.postValue(emptyList)
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Something went wrong! Please try again."
                }

                Loaders.isLoading0.value = false
            }

            override fun onFailure(call: Call<PatientFamilyMemberList>, t: Throwable) {
                Log.d("onFailure", t.toString())
                patientFamilyMemberArrayList.postValue(emptyList)
                isPatientFamilyMemberFound.value = false
                Loaders.isLoading0.value = false
                Loaders.apiError.value ="Balance Statement Get Data Fail"
            }
        })
    }

}