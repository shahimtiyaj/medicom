package com.netizen.medicom.repository

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.netizen.medicom.apiService.ApiClient
import com.netizen.medicom.apiService.ApiInterface
import com.netizen.medicom.model.*
import com.netizen.medicom.utils.AppPreferences
import com.netizen.medicom.utils.JWTUtils
import com.netizen.medicom.utils.Loaders
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.HashMap

class UserRepository(val application: Application) {
    private val appPreferences = AppPreferences(application)

    val isLoggedIn = MutableLiveData<Boolean>()
    var accessToken = MutableLiveData<String>()
    val districtList = MutableLiveData<List<DistrictModel>>()
    val areaList = MutableLiveData<List<AreaModel>>()
    val userInfoListForgotPass = MutableLiveData<UserCheckForgotPass>()
    var isListFound = MutableLiveData<Boolean>()

     fun loginAuthentication(loginUser: LoginUser) {

        Loaders.isLoading0.value = true

        val params = HashMap<String?, String?>()
        params["username"] = "test"
        params["password"] = "test"
        params["grant_type"] = "password"

        val header = HashMap<String?, String?>()
        header["Authorization"] = "Basic ZGV2Z2xhbi1jbGllbnQ6ZGV2Z2xhbi1zZWNyZXQ="
        header["Content-Type"] = "application/x-www-form-urlencoded"
        header["NZUSER"] = "${loginUser.getStrUser()}:${loginUser.getStrPassword()}:password"

        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.postData(params, header)

        //calling the api
        call?.enqueue(object : Callback<PostResponse> {
            override fun onResponse(call: Call<PostResponse>, response: Response<PostResponse>) {
                try {
                    Log.d("onResponse", "Access Token:" + response.body()?.getToken())

                    accessToken.postValue(response.body()?.getToken().toString())

                    when {
                        response.code() == 200 -> {
                            isLoggedIn.value = true

                            val separateBody = response.body()?.getToken()?.split("\\.".toRegex())?.dropLastWhile { it.isEmpty() }?.toTypedArray()

                            val header = separateBody?.get(0)?.let { JWTUtils.getJson(it) }
                            val Body = separateBody?.get(1)?.let { JWTUtils.getJson(it) }

                            val jsonObject = JSONObject(Body)
                            val name = jsonObject.getString("user_name")
                            val tokens = StringTokenizer(name, "@")
                            val username = tokens.nextToken()
                            val natiid = tokens.nextToken()

                            Log.d("JSON OBJECT HEADER", "Header Data: $header")
                            Log.d("JSON OBJECT BODY", "Body Data: $Body")
                            Log.d("User Name", "Name & ID: $name")
                            Log.d("User Name", "Name : $username")
                            Log.d("Neti ID", "ID : $natiid")
                        }

                        response.code()>=500 -> {
                            Loaders.error.postValue("Server down. Please try again later.")
                            Loaders.isLoading0.value = false
                            isLoggedIn.value = false
                        }

                        else -> {
                            Loaders.error.postValue("Invalid username or password!")
                           // Loaders.error?.value = response.message().toString()
                            Loaders.isLoading0.value = false
                            isLoggedIn.value = false
                        }
                    }
                    Loaders.isLoading0.value = false
                }

                catch (e: Exception){
                    e.printStackTrace()
                    Loaders.isLoading0.value = false
                    isLoggedIn.value = false
                    Loaders.error.postValue("Something went wrong! Please try again.")
                }
            }

            override fun onFailure(call: Call<PostResponse>, t: Throwable) {
                Log.d("onFailure", t.toString())
                Loaders.isLoading0.value = false
                isLoggedIn.value = false
                Loaders.error.postValue("Something went wrong! Please try again.")
            }

        })
    }

    fun registrationUser(registrationPost: RegistrationPost) {

        Loaders.isLoading0.value = true

       // val header = HashMap<String?, String?>()
       // header["Content-Type"] = "application/x-www-form-urlencoded"
        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.registrationSubmit(registrationPost)

        //calling the api
        call?.enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                try {
                    Log.d("onResponse", "Registration Success:" + response.body())
                    when {
                        response.code() == 201 -> {
                            Loaders.success.postValue("User Registration Successfully Completed.")
                        }
                        response.code() == 409 ->{
                            Loaders.error.postValue("User Already Exists!")
                            Loaders.isLoading0.value = false
                        }

                        else -> {
                            Loaders.error.postValue("Registration Failed !")
                            Loaders.isLoading0.value = false
                        }
                    }
                    Loaders.isLoading0.value = false
                }

                catch (e: Exception){
                    e.printStackTrace()
                    Loaders.isLoading0.value = false
                    Loaders.error.postValue("Something went wrong! Please try again.")
                }
            }

            override fun onFailure(call: Call<Void>, t: Throwable) {
                Log.d("onFailure", t.toString())
                Loaders.isLoading0.value = false
                Loaders.error.postValue("Something went wrong! Please try again.")
            }
        })
    }


    fun getDistrictList() {
        Loaders.isLoading2.value = true

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiService.getDistrict()

        call.enqueue(object : Callback<List<DistrictModel>> {

            override fun onFailure(call: Call<List<DistrictModel>>, t: Throwable) {
                Loaders.isLoading2.value = false
                Loaders.error.value = "Couldn't get district list! Please try again"
            }

            override fun onResponse(
                call: Call<List<DistrictModel>>,
                response: Response<List<DistrictModel>>
            ) {
                try {
                    if (response.code() == 302) {
                        val type = object : TypeToken<List<DistrictModel>>() {}.type
                        districtList.value = Gson().fromJson(response.errorBody()!!.charStream(), type)
                        Log.e("District", districtList.value?.size.toString())
                    } else {
                        Loaders.error.value = response.message().toString()
                    }

                    Loaders.isLoading2.value = false
                } catch (e: Exception) {
                    Loaders.isLoading2.value = false
                    Loaders.error.value = "Something went wrong! Please try again"
                }
            }

        })
    }

    fun getAreaList(coreCategoryId: String) {
        Loaders.isLoading3.value = true

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiService.getArea(coreCategoryId)

        call.enqueue(object : Callback<List<AreaModel>> {
            override fun onFailure(call: Call<List<AreaModel>>, t: Throwable) {
                Loaders.isLoading3.value = false
                Loaders.error.value = "Couldn't get area list! Please try again"
            }

            override fun onResponse(
                call: Call<List<AreaModel>>,
                response: Response<List<AreaModel>>
            ) {
                try {
                    if (response.code() == 302) {
                        val type = object : TypeToken<List<AreaModel>>() {}.type
                        areaList.value = Gson().fromJson(response.errorBody()!!.charStream(), type)
                        Log.e("Area", areaList.value?.size.toString())
                    } else {
                        Loaders.error.value = response.message().toString()
                    }

                    Loaders.isLoading3.value = false
                } catch (e: Exception) {
                    Loaders.isLoading3.value = false
                    Loaders.error.value = "Something went wrong! Please try again"
                }
            }
        })
    }

    fun getUserInfoForgotPassword(userName: String) {

        Loaders.isLoading0.value = true

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiService.userCheckForgotPaasword(userName)

        call.enqueue(object : Callback<UserCheckForgotPass> {
            override fun onFailure(call: Call<UserCheckForgotPass>, t: Throwable) {
                Loaders.isLoading0.value = false
                isListFound.value = false
                Loaders.error.value = "Couldn't get user info list! Please try again"
            }

            override fun onResponse(
                call: Call<UserCheckForgotPass>,
                response: Response<UserCheckForgotPass>
            ) {
                try {
                    if (response.code() == 302) {
                        val type = object : TypeToken<UserCheckForgotPass>() {}.type
                        userInfoListForgotPass.value = Gson().fromJson(response.errorBody()!!.charStream(), type)
                        isListFound.value = true
                    }
                    if (response.code()==404){
                        Loaders.error.value = "User Basic Info not found!"
                        isListFound.value = false
                    }

                    else {
                        Loaders.error.value = response.message().toString()
                        isListFound.value = false

                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    isListFound.value = false
                    Loaders.error.value = "Something went wrong! Please try again"
                }
            }
        })
    }

    //Get OTP after checking mobile number
    fun otpGetForgotPassword(userContactNo: String, userCheckForgotPass: UserCheckForgotPass?) {

        Loaders.isLoading0.value = true

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiService.forgotPasswordOTP(userContactNo, userCheckForgotPass)

        call.enqueue(object : Callback<Void> {
            override fun onFailure(call: Call<Void>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.error.value = "Couldn't mobile number! Please try again"
            }

            override fun onResponse(
                call: Call<Void>,
                response: Response<Void>
            ) {
                try {
                    if (response.isSuccessful) {
                        Loaders.success.value ="6 Digit OTP code has sent"
                    } else {
                        Loaders.error.value = "Invalid Mobile Number!"
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.error.value = "Something went wrong! Please try again"
                }
            }
        })
    }

    fun otpVerifyGetForgotPassword(code: String) {

        Loaders.isLoading0.value = true

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiService.forgotPasswordOTPVarify(code)

        call.enqueue(object : Callback<Any> {
            override fun onFailure(call: Call<Any>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.error.value = "Couldn't verify OTP! Please try again"
            }

            override fun onResponse(
                call: Call<Any>,
                response: Response<Any>
            ) {
                try {
                    if (response.isSuccessful) {
                        Loaders.apiSuccessforOTPVarify.value ="OTP Verified Successfully."
                    } else {
                        Loaders.apiErrorOTPInvalid.value = "Invalid OTP!"
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.error.value = "Something went wrong! Please try again"
                }
            }
        })
    }

    fun resetPassword(resetPassword: ResetPassword?) {

        Loaders.isLoading0.value = true

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiService.resetPasswordAfterForgot(resetPassword)

        call.enqueue(object : Callback<Void> {
            override fun onFailure(call: Call<Void>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.error.value = "Couldn't reset password! Please try again"
            }

            override fun onResponse(
                call: Call<Void>,
                response: Response<Void>
            ) {
                try {
                    if (response.isSuccessful) {
                        Loaders.success.value ="Password Reset Successfully."
                    } else {
                        Loaders.error.value = "Password didn't reset"
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.error.value = "Something went wrong! Please try again"
                }
            }
        })
    }

    fun changePassword(resetPassword: ResetPassword?) {

        Loaders.isLoading0.value = true
        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${appPreferences.getToken()}"
        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiService.changePassword(header, resetPassword)

        call.enqueue(object : Callback<Void> {
            override fun onFailure(call: Call<Void>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.error.value = "Couldn't change password! Please try again"
            }

            override fun onResponse(
                call: Call<Void>,
                response: Response<Void>
            ) {
                try {
                    if (response.isSuccessful) {
                        Loaders.success.value ="Password Change Successfully."
                    } else {
                        Loaders.error.value = "Password didn't change"
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.error.value = "Something went wrong! Please try again"
                }
            }
        })
    }
}