package com.netizen.medicom.repository

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.netizen.medicom.apiService.ApiClient
import com.netizen.medicom.apiService.ApiInterface
import com.netizen.medicom.model.DoctorDesignation
import com.netizen.medicom.model.DoctorEnroll
import com.netizen.medicom.utils.AppPreferences
import com.netizen.medicom.utils.Loaders
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.List
import kotlin.collections.set

class DoctorRepository(application: Application) {

    private val appPreferences = AppPreferences(application)
    val doctorDesignationArrayList = MutableLiveData<List<DoctorDesignation>>()
    var isDoctorDesignationFound = MutableLiveData<Boolean>()

    fun doctorRegistration(doctorEnroll: DoctorEnroll) {

        Loaders.isLoading0.value = true
        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${appPreferences.getToken()}"

        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.doctorRegistrationSubmit(header, doctorEnroll)

        //calling the api---------------------------------------------------------------
        call?.enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                try {

                    Log.d("onResponse", "Registration Success:" + response.body())

                    when {
                        response.code() == 201 -> {
                            Loaders.success.postValue("Doctor Registration Successfully Completed.")
                        }

                        response.code() == 409 ->{
                            Loaders.error.postValue("User Already Exists!")
                            Loaders.isLoading0.value = false
                        }
                        else -> {
                            Loaders.error.postValue("Doctor Registration Failed !")
                            Loaders.isLoading0.value = false
                        }
                    }
                    Loaders.isLoading0.value = false
                }

                catch (e: Exception){
                    e.printStackTrace()
                    Loaders.isLoading0.value = false
                    Loaders.error.postValue("Something went wrong! Please try again.")
                }
            }

            override fun onFailure(call: Call<Void>, t: Throwable) {
                Log.d("onFailure", t.toString())
                Loaders.isLoading0.value = false
                Loaders.error.postValue("Something went wrong! Please try again.")
            }
        })
    }

    //Add Family member list reports-------------------------------------------------------

    fun getDoctorDesignation(typeDefaultCode: String, typeStatus: String ) {

        Loaders.isLoading0.value = true

        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.getDoctorDesignation("bearer " + appPreferences.getToken(),
            typeDefaultCode, typeStatus)
        val  emptyList= ArrayList<DoctorDesignation>()

        //calling the api
        call?.enqueue(object : Callback<List<DoctorDesignation>> {
            override fun onResponse(
                call: Call<List<DoctorDesignation>>,
                response: Response<List<DoctorDesignation>>
            ) {
                try {
                    Log.d("onResponse", "Doctor Designation:" + response.body())

                    if (response.code() == 302) {
                        val type = object : TypeToken<List<DoctorDesignation>>() {}.type
                        val errorResponse: List<DoctorDesignation>? = Gson().fromJson(response.errorBody()!!.charStream(), type)
                        doctorDesignationArrayList.postValue(errorResponse)
                        isDoctorDesignationFound.value = true
                    }
                    else{
                        isDoctorDesignationFound.value = false
                        doctorDesignationArrayList.postValue(emptyList)
                        Loaders.apiError.value ="Sorry ! No Data Found."
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    isDoctorDesignationFound.value = false
                    doctorDesignationArrayList.postValue(emptyList)
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Something went wrong! Please try again."
                }

                Loaders.isLoading0.value = false
            }

            override fun onFailure(call: Call<List<DoctorDesignation>>, t: Throwable) {
                Log.d("onFailure", t.toString())
                doctorDesignationArrayList.postValue(emptyList)
                isDoctorDesignationFound.value = false
                Loaders.isLoading0.value = false
                Loaders.apiError.value ="Doctor Designation Get Data Fail"
            }
        })
    }
}