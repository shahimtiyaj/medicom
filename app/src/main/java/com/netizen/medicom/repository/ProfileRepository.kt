package com.netizen.medicom.repository

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.netizen.medicom.apiService.ApiClient
import com.netizen.medicom.apiService.ApiInterface
import com.netizen.medicom.model.ProfileImage
import com.netizen.medicom.model.ProfileInformation
import com.netizen.medicom.utils.AppPreferences
import com.netizen.medicom.utils.Loaders
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProfileRepository(val application: Application) {

    private val appPreferences = AppPreferences(application)
    var customNetiID = MutableLiveData<String>()
    var fullName = MutableLiveData<String>()
    var basicMobile = MutableLiveData<String>()
    var basicEmail = MutableLiveData<String>()
    var userStatus = MutableLiveData<String>()
    var village = MutableLiveData<String>()
    var upozilla = MutableLiveData<String>()
    var district = MutableLiveData<String>()
    var division = MutableLiveData<String>()
    var imagePath = MutableLiveData<String>()
    var photoFileContent = MutableLiveData<String>()
    var gender = MutableLiveData<String>()
    var religion = MutableLiveData<String>()
    var dateOfBirth = MutableLiveData<String>()
    var age = MutableLiveData<String>()

    fun getUserProfileInfoData() {
        Loaders.isLoading0.value = true

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${appPreferences.getToken()}"
        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val userCall = service?.getUserProfileInfo(header)

        userCall?.enqueue(object : Callback<ProfileInformation> {

            override fun onResponse(
                call: Call<ProfileInformation>,
                response: Response<ProfileInformation>
            ) {
                try {
//                    Log.d("onResponse", "Main :" + response.body().toString())

                    if (response.isSuccessful) {
                        fullName.postValue(response.body()?.fullName)
                        customNetiID.postValue(response.body()?.getCustomNetiId())
                        basicMobile.postValue(response.body()?.basicMobile)
                        basicEmail.postValue(response.body()?.basicEmail)
                        userStatus.postValue(response.body()?.userStatus)
                        village.postValue(response.body()?.area)
                        upozilla.postValue(response.body()?.upazilla)
                        district.postValue(response.body()?.district)
                        division.postValue(response.body()?.division)
                        imagePath.postValue(response.body()?.imagePath)
                    } else {
                        Loaders.apiError.value = "Couldn't load profile information!"
                       //  gender.postValue(response.body()?.gender)
                       //  religion.postValue(response.body()?.religion)
                       //  dateOfBirth.postValue(response.body()?.dateOfBirth)
                        // age.postValue(response.body()?.ag)
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    e.printStackTrace()
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't load profile information!"
                }
            }

            override fun onFailure(call: Call<ProfileInformation>, t: Throwable) {
//                Log.d("onFailure", t.toString())
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't load profile information!"
            }
        })
    }

     fun getProfilePhoto(imagePath: String) {
         Loaders.isLoading0.value = true

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${appPreferences.getToken()}"
        val service = ApiClient.getClient?.create(ApiInterface::class.java)

        val call = service?.getProfile(header, imagePath)

        //calling the api
        call?.enqueue(object : Callback<ProfileImage> {
            override fun onResponse(call: Call<ProfileImage>, response: Response<ProfileImage>) {
                try {
                    if (response.code() == 200) {
                        photoFileContent.postValue(response.body()?.fileContent)
                    } else {
                        Loaders.apiError.value = "Couldn't load profile picture!"
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't load profile picture!"
                }
                Log.d("onResponse", "Main Response:" + response.body())

                if (response.code() == 200) {
                    Log.d("onResponse", "Profile Image:" + response.body()?.fileContent)
                    photoFileContent.postValue(response.body()?.fileContent)
                    Log.d("onResponse", "Photo:" + response.body()?.fileContent)
                }
            }

            override fun onFailure(call: Call<ProfileImage>, t: Throwable) {
//                Log.d("onFailure", t.toString())
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't load profile picture!"
            }
        })
    }

}