package com.netizen.medicom.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0013\n\u0002\u0010\u0002\n\u0002\b\u0013\u0018\u00002\u00020\u0001:\u0001>B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0019\u001a\u0004\u0018\u00010\u0001J\r\u0010\u001a\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u001bJ\b\u0010\u001c\u001a\u0004\u0018\u00010\bJ\b\u0010\u001d\u001a\u0004\u0018\u00010\bJ\b\u0010\u001e\u001a\u0004\u0018\u00010\bJ\b\u0010\u001f\u001a\u0004\u0018\u00010\bJ\b\u0010 \u001a\u0004\u0018\u00010\u0001J\r\u0010!\u001a\u0004\u0018\u00010\u000e\u00a2\u0006\u0002\u0010\"J\b\u0010#\u001a\u0004\u0018\u00010\u0001J\b\u0010$\u001a\u0004\u0018\u00010\bJ\b\u0010%\u001a\u0004\u0018\u00010\u0001J\b\u0010&\u001a\u0004\u0018\u00010\bJ\b\u0010\'\u001a\u0004\u0018\u00010\bJ\r\u0010(\u001a\u0004\u0018\u00010\u000e\u00a2\u0006\u0002\u0010\"J\r\u0010)\u001a\u0004\u0018\u00010\u000e\u00a2\u0006\u0002\u0010\"J\b\u0010*\u001a\u0004\u0018\u00010\u0018J\u0010\u0010+\u001a\u00020,2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0001J\u0015\u0010-\u001a\u00020,2\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010.J\u0010\u0010/\u001a\u00020,2\b\u0010\u0007\u001a\u0004\u0018\u00010\bJ\u0010\u00100\u001a\u00020,2\b\u0010\t\u001a\u0004\u0018\u00010\bJ\u0010\u00101\u001a\u00020,2\b\u0010\n\u001a\u0004\u0018\u00010\bJ\u0010\u00102\u001a\u00020,2\b\u0010\u000b\u001a\u0004\u0018\u00010\bJ\u0010\u00103\u001a\u00020,2\b\u0010\f\u001a\u0004\u0018\u00010\u0001J\u0015\u00104\u001a\u00020,2\b\u0010\r\u001a\u0004\u0018\u00010\u000e\u00a2\u0006\u0002\u00105J\u0010\u00106\u001a\u00020,2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001J\u0010\u00107\u001a\u00020,2\b\u0010\u0011\u001a\u0004\u0018\u00010\bJ\u0010\u00108\u001a\u00020,2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001J\u0010\u00109\u001a\u00020,2\b\u0010\u0013\u001a\u0004\u0018\u00010\bJ\u0010\u0010:\u001a\u00020,2\b\u0010\u0014\u001a\u0004\u0018\u00010\bJ\u0015\u0010;\u001a\u00020,2\b\u0010\u0015\u001a\u0004\u0018\u00010\u000e\u00a2\u0006\u0002\u00105J\u0015\u0010<\u001a\u00020,2\b\u0010\u0016\u001a\u0004\u0018\u00010\u000e\u00a2\u0006\u0002\u00105J\u0010\u0010=\u001a\u00020,2\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\r\u001a\u0004\u0018\u00010\u000e8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u000fR\u0014\u0010\u0010\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0015\u001a\u0004\u0018\u00010\u000e8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u000fR\u0016\u0010\u0016\u001a\u0004\u0018\u00010\u000e8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u000fR\u0014\u0010\u0017\u001a\u0004\u0018\u00010\u00188\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006?"}, d2 = {"Lcom/netizen/medicom/model/TokenList;", "", "()V", "attachContent", "attachSaveOrEditable", "", "Ljava/lang/Boolean;", "attachmentName", "", "attachmentPath", "createDate", "customTokenID", "ratingDate", "ratingMark", "", "Ljava/lang/Integer;", "ratingMessage", "solveDate", "solveMessage", "tokenContact", "tokenDetails", "tokenInfoID", "tokenStatus", "tokenTypeInfoDTO", "Lcom/netizen/medicom/model/TokenList$TokenTypeInfoDTO;", "getAttachContent", "getAttachSaveOrEditable", "()Ljava/lang/Boolean;", "getAttachmentName", "getAttachmentPath", "getCreateDate", "getCustomTokenID", "getRatingDate", "getRatingMark", "()Ljava/lang/Integer;", "getRatingMessage", "getSolveDate", "getSolveMessage", "getTokenContact", "getTokenDetails", "getTokenInfoID", "getTokenStatus", "getTokenTypeInfoDTO", "setAttachContent", "", "setAttachSaveOrEditable", "(Ljava/lang/Boolean;)V", "setAttachmentName", "setAttachmentPath", "setCreateDate", "setCustomTokenID", "setRatingDate", "setRatingMark", "(Ljava/lang/Integer;)V", "setRatingMessage", "setSolveDate", "setSolveMessage", "setTokenContact", "setTokenDetails", "setTokenInfoID", "setTokenStatus", "setTokenTypeInfoDTO", "TokenTypeInfoDTO", "app_debug"})
public final class TokenList {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "tokenInfoID")
    private java.lang.Integer tokenInfoID;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "customTokenID")
    private java.lang.String customTokenID;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "tokenDetails")
    private java.lang.String tokenDetails;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "attachmentName")
    private java.lang.String attachmentName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "attachmentPath")
    private java.lang.String attachmentPath;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "createDate")
    private java.lang.String createDate;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "tokenContact")
    private java.lang.String tokenContact;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "tokenStatus")
    private java.lang.Integer tokenStatus;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "ratingDate")
    private java.lang.Object ratingDate;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "ratingMark")
    private java.lang.Integer ratingMark;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "ratingMessage")
    private java.lang.Object ratingMessage;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "solveDate")
    private java.lang.String solveDate;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "solveMessage")
    private java.lang.Object solveMessage;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "tokenTypeInfoDTO")
    private com.netizen.medicom.model.TokenList.TokenTypeInfoDTO tokenTypeInfoDTO;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "attachContent")
    private java.lang.Object attachContent;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "attachSaveOrEditable")
    private java.lang.Boolean attachSaveOrEditable;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getTokenInfoID() {
        return null;
    }
    
    public final void setTokenInfoID(@org.jetbrains.annotations.Nullable()
    java.lang.Integer tokenInfoID) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCustomTokenID() {
        return null;
    }
    
    public final void setCustomTokenID(@org.jetbrains.annotations.Nullable()
    java.lang.String customTokenID) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTokenDetails() {
        return null;
    }
    
    public final void setTokenDetails(@org.jetbrains.annotations.Nullable()
    java.lang.String tokenDetails) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAttachmentName() {
        return null;
    }
    
    public final void setAttachmentName(@org.jetbrains.annotations.Nullable()
    java.lang.String attachmentName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAttachmentPath() {
        return null;
    }
    
    public final void setAttachmentPath(@org.jetbrains.annotations.Nullable()
    java.lang.String attachmentPath) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCreateDate() {
        return null;
    }
    
    public final void setCreateDate(@org.jetbrains.annotations.Nullable()
    java.lang.String createDate) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTokenContact() {
        return null;
    }
    
    public final void setTokenContact(@org.jetbrains.annotations.Nullable()
    java.lang.String tokenContact) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getTokenStatus() {
        return null;
    }
    
    public final void setTokenStatus(@org.jetbrains.annotations.Nullable()
    java.lang.Integer tokenStatus) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getRatingDate() {
        return null;
    }
    
    public final void setRatingDate(@org.jetbrains.annotations.Nullable()
    java.lang.Object ratingDate) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getRatingMark() {
        return null;
    }
    
    public final void setRatingMark(@org.jetbrains.annotations.Nullable()
    java.lang.Integer ratingMark) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getRatingMessage() {
        return null;
    }
    
    public final void setRatingMessage(@org.jetbrains.annotations.Nullable()
    java.lang.Object ratingMessage) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSolveDate() {
        return null;
    }
    
    public final void setSolveDate(@org.jetbrains.annotations.Nullable()
    java.lang.String solveDate) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getSolveMessage() {
        return null;
    }
    
    public final void setSolveMessage(@org.jetbrains.annotations.Nullable()
    java.lang.Object solveMessage) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.medicom.model.TokenList.TokenTypeInfoDTO getTokenTypeInfoDTO() {
        return null;
    }
    
    public final void setTokenTypeInfoDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.medicom.model.TokenList.TokenTypeInfoDTO tokenTypeInfoDTO) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getAttachContent() {
        return null;
    }
    
    public final void setAttachContent(@org.jetbrains.annotations.Nullable()
    java.lang.Object attachContent) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean getAttachSaveOrEditable() {
        return null;
    }
    
    public final void setAttachSaveOrEditable(@org.jetbrains.annotations.Nullable()
    java.lang.Boolean attachSaveOrEditable) {
    }
    
    public TokenList() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\t\u001a\u0004\u0018\u00010\u0004J\b\u0010\n\u001a\u0004\u0018\u00010\u0004J\r\u0010\u000b\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\fJ\u0010\u0010\r\u001a\u00020\u000e2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u000f\u001a\u00020\u000e2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0015\u0010\u0010\u001a\u00020\u000e2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0011R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\b\u00a8\u0006\u0012"}, d2 = {"Lcom/netizen/medicom/model/TokenList$TokenTypeInfoDTO;", "", "()V", "categoryDefaultCode", "", "categoryName", "coreCategoryID", "", "Ljava/lang/Integer;", "getCategoryDefaultCode", "getCategoryName", "getCoreCategoryID", "()Ljava/lang/Integer;", "setCategoryDefaultCode", "", "setCategoryName", "setCoreCategoryID", "(Ljava/lang/Integer;)V", "app_debug"})
    public static final class TokenTypeInfoDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "coreCategoryID")
        private java.lang.Integer coreCategoryID;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "categoryDefaultCode")
        private java.lang.String categoryDefaultCode;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "categoryName")
        private java.lang.String categoryName;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getCoreCategoryID() {
            return null;
        }
        
        public final void setCoreCategoryID(@org.jetbrains.annotations.Nullable()
        java.lang.Integer coreCategoryID) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCategoryDefaultCode() {
            return null;
        }
        
        public final void setCategoryDefaultCode(@org.jetbrains.annotations.Nullable()
        java.lang.String categoryDefaultCode) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCategoryName() {
            return null;
        }
        
        public final void setCategoryName(@org.jetbrains.annotations.Nullable()
        java.lang.String categoryName) {
        }
        
        public TokenTypeInfoDTO() {
            super();
        }
    }
}