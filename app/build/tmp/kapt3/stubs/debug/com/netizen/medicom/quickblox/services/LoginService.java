package com.netizen.medicom.quickblox.services;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0010\u0018\u0000 +2\u00020\u0001:\u0001+B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u000f\u001a\u00020\u0010H\u0002J\b\u0010\u0011\u001a\u00020\u0010H\u0002J\b\u0010\u0012\u001a\u00020\u0010H\u0002J\b\u0010\u0013\u001a\u00020\u0010H\u0002J\b\u0010\u0014\u001a\u00020\u0015H\u0002J\u0010\u0010\u0016\u001a\u00020\u00102\u0006\u0010\u0017\u001a\u00020\nH\u0002J\b\u0010\u0018\u001a\u00020\u0010H\u0002J\u0012\u0010\u0019\u001a\u0004\u0018\u00010\u001a2\u0006\u0010\u001b\u001a\u00020\u001cH\u0016J\b\u0010\u001d\u001a\u00020\u0010H\u0016J\b\u0010\u001e\u001a\u00020\u0010H\u0016J\"\u0010\u001f\u001a\u00020\b2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001c2\u0006\u0010 \u001a\u00020\b2\u0006\u0010!\u001a\u00020\bH\u0016J\u0010\u0010\"\u001a\u00020\u00102\u0006\u0010#\u001a\u00020\u001cH\u0016J\u0012\u0010$\u001a\u00020\u00102\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0002J\u001a\u0010%\u001a\u00020\u00102\u0006\u0010&\u001a\u00020\u00152\b\u0010\'\u001a\u0004\u0018\u00010\u0004H\u0002J\b\u0010(\u001a\u00020\u0010H\u0002J\b\u0010)\u001a\u00020\u0010H\u0002J\b\u0010*\u001a\u00020\u0010H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006,"}, d2 = {"Lcom/netizen/medicom/quickblox/services/LoginService;", "Landroid/app/Service;", "()V", "TAG", "", "chatService", "Lcom/quickblox/chat/QBChatService;", "currentCommand", "", "currentUser", "Lcom/quickblox/users/model/QBUser;", "pendingIntent", "Landroid/app/PendingIntent;", "rtcClient", "Lcom/quickblox/videochat/webrtc/QBRTCClient;", "createChatService", "", "destroyRtcClientAndChat", "initPingListener", "initQBRTCClient", "isCallServiceRunning", "", "loginToChat", "qbUser", "logout", "onBind", "Landroid/os/IBinder;", "intent", "Landroid/content/Intent;", "onCreate", "onDestroy", "onStartCommand", "flags", "startId", "onTaskRemoved", "rootIntent", "parseIntentExtras", "sendResultToActivity", "isSuccess", "errorMessage", "startActionsOnSuccessLogin", "startLoginToChat", "startSuitableActions", "Companion", "app_debug"})
public final class LoginService extends android.app.Service {
    private final java.lang.String TAG = null;
    private com.quickblox.chat.QBChatService chatService;
    private com.quickblox.videochat.webrtc.QBRTCClient rtcClient;
    private android.app.PendingIntent pendingIntent;
    private int currentCommand = 0;
    private com.quickblox.users.model.QBUser currentUser;
    public static final com.netizen.medicom.quickblox.services.LoginService.Companion Companion = null;
    
    @java.lang.Override()
    public void onCreate() {
    }
    
    @java.lang.Override()
    public int onStartCommand(@org.jetbrains.annotations.Nullable()
    android.content.Intent intent, int flags, int startId) {
        return 0;
    }
    
    private final void parseIntentExtras(android.content.Intent intent) {
    }
    
    private final void startSuitableActions() {
    }
    
    private final void createChatService() {
    }
    
    private final void startLoginToChat() {
    }
    
    private final void loginToChat(com.quickblox.users.model.QBUser qbUser) {
    }
    
    private final void startActionsOnSuccessLogin() {
    }
    
    private final void initPingListener() {
    }
    
    private final void initQBRTCClient() {
    }
    
    private final void sendResultToActivity(boolean isSuccess, java.lang.String errorMessage) {
    }
    
    private final void logout() {
    }
    
    private final void destroyRtcClientAndChat() {
    }
    
    @java.lang.Override()
    public void onDestroy() {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.os.IBinder onBind(@org.jetbrains.annotations.NotNull()
    android.content.Intent intent) {
        return null;
    }
    
    @java.lang.Override()
    public void onTaskRemoved(@org.jetbrains.annotations.NotNull()
    android.content.Intent rootIntent) {
    }
    
    private final boolean isCallServiceRunning() {
        return false;
    }
    
    public LoginService() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\"\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\t2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000bJ\u000e\u0010\f\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\r"}, d2 = {"Lcom/netizen/medicom/quickblox/services/LoginService$Companion;", "", "()V", "logout", "", "context", "Landroid/content/Context;", "start", "qbUser", "Lcom/quickblox/users/model/QBUser;", "pendingIntent", "Landroid/app/PendingIntent;", "stop", "app_debug"})
    public static final class Companion {
        
        public final void start(@org.jetbrains.annotations.NotNull()
        android.content.Context context, @org.jetbrains.annotations.NotNull()
        com.quickblox.users.model.QBUser qbUser, @org.jetbrains.annotations.Nullable()
        android.app.PendingIntent pendingIntent) {
        }
        
        public final void stop(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
        }
        
        public final void logout(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
        }
        
        private Companion() {
            super();
        }
    }
}