package com.netizen.medicom.viewModel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\r\u0018\u00002\u00020\u0001:\u0001YB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0016\u0010@\u001a\u00020A2\u0006\u0010B\u001a\u00020\u00072\u0006\u0010C\u001a\u00020\u0007J\u000e\u0010\u0012\u001a\u00020A2\u0006\u0010D\u001a\u00020\u0007J\u000e\u0010\u0017\u001a\u00020A2\u0006\u0010D\u001a\u00020\u0007J\u0006\u0010\u001b\u001a\u00020AJ\u000e\u0010E\u001a\u00020A2\u0006\u0010F\u001a\u00020\u0007J\u0018\u0010G\u001a\u00020A2\u0006\u0010H\u001a\u00020\u00072\b\u0010I\u001a\u0004\u0018\u00010;J\u001e\u0010J\u001a\u00020A2\u0006\u0010K\u001a\u00020\u00072\u0006\u0010B\u001a\u00020\u00072\u0006\u0010C\u001a\u00020\u0007J&\u0010L\u001a\u0012\u0012\u0004\u0012\u00020\u000704j\b\u0012\u0004\u0012\u00020\u0007`M2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\rH\u0002J&\u0010N\u001a\u0012\u0012\u0004\u0012\u00020\u000704j\b\u0012\u0004\u0012\u00020\u0007`M2\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u001a0\rH\u0002J\u000e\u0010O\u001a\u00020A2\u0006\u0010K\u001a\u00020\u0007J\u0016\u0010P\u001a\u00020A2\u0006\u0010K\u001a\u00020\u00072\u0006\u0010B\u001a\u00020\u0007JV\u0010Q\u001a\u00020A2\b\u0010R\u001a\u0004\u0018\u00010\u00072\b\u0010S\u001a\u0004\u0018\u00010\u00072\b\u0010T\u001a\u0004\u0018\u00010\u00072\b\u0010U\u001a\u0004\u0018\u00010\u00072\b\u0010V\u001a\u0004\u0018\u00010\u00072\b\u0010W\u001a\u0004\u0018\u00010\u00072\b\u0010K\u001a\u0004\u0018\u00010\u00072\b\u0010X\u001a\u0004\u0018\u00010\u0007R \u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR&\u0010\f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\r0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\t\"\u0004\b\u0010\u0010\u000bR\u001c\u0010\u0011\u001a\u0004\u0018\u00010\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u001c\u0010\u0016\u001a\u0004\u0018\u00010\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0013\"\u0004\b\u0018\u0010\u0015R&\u0010\u0019\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u001a0\r0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001b\u0010\t\"\u0004\b\u001c\u0010\u000bR \u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u001e0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\t\"\u0004\b\u001f\u0010\u000bR \u0010 \u001a\b\u0012\u0004\u0012\u00020\u001e0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b \u0010\t\"\u0004\b!\u0010\u000bR \u0010\"\u001a\b\u0012\u0004\u0012\u00020\u001e0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010\t\"\u0004\b#\u0010\u000bR \u0010$\u001a\b\u0012\u0004\u0012\u00020\u001e0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b$\u0010\t\"\u0004\b%\u0010\u000bR \u0010&\u001a\b\u0012\u0004\u0012\u00020\u001e0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b&\u0010\t\"\u0004\b\'\u0010\u000bR \u0010(\u001a\b\u0012\u0004\u0012\u00020\u001e0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010\t\"\u0004\b)\u0010\u000bR\u0017\u0010*\u001a\b\u0012\u0004\u0012\u00020\u001e0\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b*\u0010\tR\u0017\u0010+\u001a\b\u0012\u0004\u0012\u00020\u001e0\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b+\u0010\tR \u0010,\u001a\b\u0012\u0004\u0012\u00020\u001e0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b,\u0010\t\"\u0004\b-\u0010\u000bR\u0017\u0010.\u001a\b\u0012\u0004\u0012\u00020\u001e0\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b.\u0010\tR \u0010/\u001a\b\u0012\u0004\u0012\u00020\u001e0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b/\u0010\t\"\u0004\b0\u0010\u000bR\u0017\u00101\u001a\b\u0012\u0004\u0012\u00020\u001e0\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b1\u0010\tR+\u00102\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0007 5*\n\u0012\u0004\u0012\u00020\u0007\u0018\u0001040403\u00a2\u0006\b\n\u0000\u001a\u0004\b6\u00107R+\u00108\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0007 5*\n\u0012\u0004\u0012\u00020\u0007\u0018\u0001040403\u00a2\u0006\b\n\u0000\u001a\u0004\b9\u00107R \u0010:\u001a\b\u0012\u0004\u0012\u00020;0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b<\u0010\t\"\u0004\b=\u0010\u000bR\u000e\u0010>\u001a\u00020?X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006Z"}, d2 = {"Lcom/netizen/medicom/viewModel/UserViewModel;", "Landroidx/lifecycle/AndroidViewModel;", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "accessToken", "Landroidx/lifecycle/MutableLiveData;", "", "getAccessToken", "()Landroidx/lifecycle/MutableLiveData;", "setAccessToken", "(Landroidx/lifecycle/MutableLiveData;)V", "areaList", "", "Lcom/netizen/medicom/model/AreaModel;", "getAreaList", "setAreaList", "coreCategoryIdArea", "getCoreCategoryIdArea", "()Ljava/lang/String;", "setCoreCategoryIdArea", "(Ljava/lang/String;)V", "coreCategoryIdDistrict", "getCoreCategoryIdDistrict", "setCoreCategoryIdDistrict", "districtList", "Lcom/netizen/medicom/model/DistrictModel;", "getDistrictList", "setDistrictList", "isAreaEmpty", "", "setAreaEmpty", "isBirthdayEmpty", "setBirthdayEmpty", "isDistrictEmpty", "setDistrictEmpty", "isEmailEmpty", "setEmailEmpty", "isFullnameEmpty", "setFullnameEmpty", "isGenderEmpty", "setGenderEmpty", "isListFound", "isLoggedIn", "isMobileEmpty", "setMobileEmpty", "isPasswordEmpty", "isReligionEmpty", "setReligionEmpty", "isUserNameEmpty", "tempAreaList", "Landroidx/lifecycle/LiveData;", "Ljava/util/ArrayList;", "kotlin.jvm.PlatformType", "getTempAreaList", "()Landroidx/lifecycle/LiveData;", "tempDistrictList", "getTempDistrictList", "userInfoListForgotPass", "Lcom/netizen/medicom/model/UserCheckForgotPass;", "getUserInfoListForgotPass", "setUserInfoListForgotPass", "userRepository", "Lcom/netizen/medicom/repository/UserRepository;", "getChangePassword", "", "password", "newPassword", "selectItemName", "getOTPVerifyGetForgotPassword", "otpCode", "getOtpForgotPassword", "userContactNo", "userInformation", "getResetPassword", "userName", "getTempArea", "Lkotlin/collections/ArrayList;", "getTempDistrict", "getUserInfoForgotPassword", "onLogInClick", "onSignUpClick", "fullName", "gender", "religion", "dateOfBirth", "basicMobile", "basicEmail", "userPassword", "SignInViewModelFactory", "app_debug"})
public final class UserViewModel extends androidx.lifecycle.AndroidViewModel {
    private final com.netizen.medicom.repository.UserRepository userRepository = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isUserNameEmpty = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isPasswordEmpty = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isLoggedIn = null;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> accessToken;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.medicom.model.DistrictModel>> districtList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.medicom.model.AreaModel>> areaList;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String coreCategoryIdDistrict;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String coreCategoryIdArea;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isFullnameEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isGenderEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isReligionEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isBirthdayEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isMobileEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isEmailEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDistrictEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isAreaEmpty;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isListFound = null;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<com.netizen.medicom.model.UserCheckForgotPass> userInfoListForgotPass;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> tempDistrictList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> tempAreaList = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isUserNameEmpty() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isPasswordEmpty() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isLoggedIn() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getAccessToken() {
        return null;
    }
    
    public final void setAccessToken(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.medicom.model.DistrictModel>> getDistrictList() {
        return null;
    }
    
    public final void setDistrictList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.medicom.model.DistrictModel>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.medicom.model.AreaModel>> getAreaList() {
        return null;
    }
    
    public final void setAreaList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.medicom.model.AreaModel>> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCoreCategoryIdDistrict() {
        return null;
    }
    
    public final void setCoreCategoryIdDistrict(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCoreCategoryIdArea() {
        return null;
    }
    
    public final void setCoreCategoryIdArea(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isFullnameEmpty() {
        return null;
    }
    
    public final void setFullnameEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isGenderEmpty() {
        return null;
    }
    
    public final void setGenderEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isReligionEmpty() {
        return null;
    }
    
    public final void setReligionEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isBirthdayEmpty() {
        return null;
    }
    
    public final void setBirthdayEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isMobileEmpty() {
        return null;
    }
    
    public final void setMobileEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isEmailEmpty() {
        return null;
    }
    
    public final void setEmailEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDistrictEmpty() {
        return null;
    }
    
    public final void setDistrictEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isAreaEmpty() {
        return null;
    }
    
    public final void setAreaEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isListFound() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.netizen.medicom.model.UserCheckForgotPass> getUserInfoListForgotPass() {
        return null;
    }
    
    public final void setUserInfoListForgotPass(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<com.netizen.medicom.model.UserCheckForgotPass> p0) {
    }
    
    public final void getUserInfoForgotPassword(@org.jetbrains.annotations.NotNull()
    java.lang.String userName) {
    }
    
    public final void getOtpForgotPassword(@org.jetbrains.annotations.NotNull()
    java.lang.String userContactNo, @org.jetbrains.annotations.Nullable()
    com.netizen.medicom.model.UserCheckForgotPass userInformation) {
    }
    
    public final void getOTPVerifyGetForgotPassword(@org.jetbrains.annotations.NotNull()
    java.lang.String otpCode) {
    }
    
    public final void getResetPassword(@org.jetbrains.annotations.NotNull()
    java.lang.String userName, @org.jetbrains.annotations.NotNull()
    java.lang.String password, @org.jetbrains.annotations.NotNull()
    java.lang.String newPassword) {
    }
    
    public final void getChangePassword(@org.jetbrains.annotations.NotNull()
    java.lang.String password, @org.jetbrains.annotations.NotNull()
    java.lang.String newPassword) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> getTempDistrictList() {
        return null;
    }
    
    private final java.util.ArrayList<java.lang.String> getTempDistrict(java.util.List<com.netizen.medicom.model.DistrictModel> districtList) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> getTempAreaList() {
        return null;
    }
    
    private final java.util.ArrayList<java.lang.String> getTempArea(java.util.List<com.netizen.medicom.model.AreaModel> areaList) {
        return null;
    }
    
    public final void getDistrictList() {
    }
    
    public final void getCoreCategoryIdDistrict(@org.jetbrains.annotations.NotNull()
    java.lang.String selectItemName) {
    }
    
    public final void getCoreCategoryIdArea(@org.jetbrains.annotations.NotNull()
    java.lang.String selectItemName) {
    }
    
    public final void onLogInClick(@org.jetbrains.annotations.NotNull()
    java.lang.String userName, @org.jetbrains.annotations.NotNull()
    java.lang.String password) {
    }
    
    public final void onSignUpClick(@org.jetbrains.annotations.Nullable()
    java.lang.String fullName, @org.jetbrains.annotations.Nullable()
    java.lang.String gender, @org.jetbrains.annotations.Nullable()
    java.lang.String religion, @org.jetbrains.annotations.Nullable()
    java.lang.String dateOfBirth, @org.jetbrains.annotations.Nullable()
    java.lang.String basicMobile, @org.jetbrains.annotations.Nullable()
    java.lang.String basicEmail, @org.jetbrains.annotations.Nullable()
    java.lang.String userName, @org.jetbrains.annotations.Nullable()
    java.lang.String userPassword) {
    }
    
    public UserViewModel(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        super(null);
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\'\u0010\u0007\u001a\u0002H\b\"\n\b\u0000\u0010\b*\u0004\u0018\u00010\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\b0\u000bH\u0016\u00a2\u0006\u0002\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\r"}, d2 = {"Lcom/netizen/medicom/viewModel/UserViewModel$SignInViewModelFactory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "getApplication", "()Landroid/app/Application;", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "app_debug"})
    public static final class SignInViewModelFactory implements androidx.lifecycle.ViewModelProvider.Factory {
        @org.jetbrains.annotations.NotNull()
        private final android.app.Application application = null;
        
        @java.lang.Override()
        public <T extends androidx.lifecycle.ViewModel>T create(@org.jetbrains.annotations.NotNull()
        java.lang.Class<T> modelClass) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.app.Application getApplication() {
            return null;
        }
        
        public SignInViewModelFactory(@org.jetbrains.annotations.NotNull()
        android.app.Application application) {
            super();
        }
    }
}