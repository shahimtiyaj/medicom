package com.netizen.medicom.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0013\n\u0002\u0010\u0002\n\u0002\b\f\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\r\u0010\u0012\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0013J\r\u0010\u0014\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0013J\b\u0010\u0015\u001a\u0004\u0018\u00010\bJ\b\u0010\u0016\u001a\u0004\u0018\u00010\bJ\r\u0010\u0017\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010\u0018J\r\u0010\u0019\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010\u0018J\r\u0010\u001a\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010\u0018J\r\u0010\u001b\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010\u0018J\b\u0010\u001c\u001a\u0004\u0018\u00010\bJ\r\u0010\u001d\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010\u0018J\u0015\u0010\u001e\u001a\u00020\u001f2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010 J\u0015\u0010!\u001a\u00020\u001f2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010 J\u000e\u0010\"\u001a\u00020\u001f2\u0006\u0010\u0007\u001a\u00020\bJ\u000e\u0010#\u001a\u00020\u001f2\u0006\u0010\t\u001a\u00020\bJ\u0015\u0010$\u001a\u00020\u001f2\b\u0010\n\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010%J\u0015\u0010&\u001a\u00020\u001f2\b\u0010\r\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010%J\u0015\u0010\'\u001a\u00020\u001f2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010%J\u0015\u0010(\u001a\u00020\u001f2\b\u0010\u000f\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010%J\u000e\u0010)\u001a\u00020\u001f2\u0006\u0010\u0010\u001a\u00020\bJ\u0015\u0010*\u001a\u00020\u001f2\b\u0010\u0011\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010%R\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\n\u001a\u0004\u0018\u00010\u000b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\fR\u0016\u0010\r\u001a\u0004\u0018\u00010\u000b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\fR\u0016\u0010\u000e\u001a\u0004\u0018\u00010\u000b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\fR\u0016\u0010\u000f\u001a\u0004\u0018\u00010\u000b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\fR\u0014\u0010\u0010\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0011\u001a\u0004\u0018\u00010\u000b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\f\u00a8\u0006+"}, d2 = {"Lcom/netizen/medicom/model/OfferProduct;", "", "()V", "actualPrice", "", "Ljava/lang/Float;", "discountPercent", "offerCode", "", "offerNote", "offerQuantity", "", "Ljava/lang/Long;", "offerStartDate", "offerUseableTime", "offerUsed", "productName", "productOfferID", "getActualPrice", "()Ljava/lang/Float;", "getDiscountPercent", "getOfferCode", "getOfferNote", "getOfferQuantity", "()Ljava/lang/Long;", "getOfferStartDate", "getOfferUseableTime", "getOfferUsed", "getProductName", "getProductOfferID", "setActualPrice", "", "(Ljava/lang/Float;)V", "setDiscountPercent", "setOfferCode", "setOfferNote", "setOfferQuantity", "(Ljava/lang/Long;)V", "setOfferStartDate", "setOfferUseableTime", "setOfferUsed", "setProductName", "setProductOfferID", "app_debug"})
public final class OfferProduct {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "productOfferID")
    private java.lang.Long productOfferID;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "offerCode")
    private java.lang.String offerCode;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "actualPrice")
    private java.lang.Float actualPrice;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "discountPercent")
    private java.lang.Float discountPercent;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "offerStartDate")
    private java.lang.Long offerStartDate;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "offerQuantity")
    private java.lang.Long offerQuantity;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "offerUseableTime")
    private java.lang.Long offerUseableTime;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "offerNote")
    private java.lang.String offerNote;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "offerUsed")
    private java.lang.Long offerUsed;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "productName")
    private java.lang.String productName;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getProductOfferID() {
        return null;
    }
    
    public final void setProductOfferID(@org.jetbrains.annotations.Nullable()
    java.lang.Long productOfferID) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getOfferCode() {
        return null;
    }
    
    public final void setOfferCode(@org.jetbrains.annotations.NotNull()
    java.lang.String offerCode) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Float getActualPrice() {
        return null;
    }
    
    public final void setActualPrice(@org.jetbrains.annotations.Nullable()
    java.lang.Float actualPrice) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Float getDiscountPercent() {
        return null;
    }
    
    public final void setDiscountPercent(@org.jetbrains.annotations.Nullable()
    java.lang.Float discountPercent) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getOfferStartDate() {
        return null;
    }
    
    public final void setOfferStartDate(@org.jetbrains.annotations.Nullable()
    java.lang.Long offerStartDate) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getOfferQuantity() {
        return null;
    }
    
    public final void setOfferQuantity(@org.jetbrains.annotations.Nullable()
    java.lang.Long offerQuantity) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getOfferUseableTime() {
        return null;
    }
    
    public final void setOfferUseableTime(@org.jetbrains.annotations.Nullable()
    java.lang.Long offerUseableTime) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getOfferNote() {
        return null;
    }
    
    public final void setOfferNote(@org.jetbrains.annotations.NotNull()
    java.lang.String offerNote) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getOfferUsed() {
        return null;
    }
    
    public final void setOfferUsed(@org.jetbrains.annotations.Nullable()
    java.lang.Long offerUsed) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getProductName() {
        return null;
    }
    
    public final void setProductName(@org.jetbrains.annotations.NotNull()
    java.lang.String productName) {
    }
    
    public OfferProduct() {
        super();
    }
}