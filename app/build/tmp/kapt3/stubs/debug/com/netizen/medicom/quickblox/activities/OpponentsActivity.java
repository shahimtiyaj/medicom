package com.netizen.medicom.quickblox.activities;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\f\u0018\u0000 42\u00020\u0001:\u00014B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\r\u001a\u00020\u000eH\u0002J\b\u0010\u000f\u001a\u00020\u0010H\u0002J\b\u0010\u0011\u001a\u00020\u000eH\u0002J&\u0010\u0012\u001a\u0012\u0012\u0004\u0012\u00020\u00140\u0013j\b\u0012\u0004\u0012\u00020\u0014`\u00152\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00060\u0017H\u0002J\b\u0010\u0018\u001a\u00020\u000eH\u0002J\b\u0010\u0019\u001a\u00020\u000eH\u0002J\b\u0010\u001a\u001a\u00020\u000eH\u0002J\u0014\u0010\u001b\u001a\u00020\u00102\n\u0010\u001c\u001a\u0006\u0012\u0002\b\u00030\u001dH\u0002J\b\u0010\u001e\u001a\u00020\u000eH\u0002J\b\u0010\u001f\u001a\u00020\u000eH\u0002J\b\u0010 \u001a\u00020\u000eH\u0016J\u0012\u0010!\u001a\u00020\u000e2\b\u0010\"\u001a\u0004\u0018\u00010#H\u0014J\u0010\u0010$\u001a\u00020\u00102\u0006\u0010%\u001a\u00020&H\u0016J\u0010\u0010\'\u001a\u00020\u00102\u0006\u0010(\u001a\u00020)H\u0016J\b\u0010*\u001a\u00020\u000eH\u0014J\b\u0010+\u001a\u00020\u000eH\u0002J\u0010\u0010,\u001a\u00020\u000e2\u0006\u0010-\u001a\u00020\u0010H\u0002J\b\u0010.\u001a\u00020\u000eH\u0002J\b\u0010/\u001a\u00020\u000eH\u0002J\u0010\u00100\u001a\u00020\u000e2\u0006\u00101\u001a\u00020\u0010H\u0002J\u0010\u00102\u001a\u00020\u000e2\u0006\u00103\u001a\u00020\u0014H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u00065"}, d2 = {"Lcom/netizen/medicom/quickblox/activities/OpponentsActivity;", "Lcom/netizen/medicom/quickblox/activities/BaseActivity;", "()V", "TAG", "", "currentUser", "Lcom/quickblox/users/model/QBUser;", "username", "Landroid/widget/TextView;", "usersAdapter", "Lcom/netizen/medicom/quickblox/adapters/UsersAdapter;", "usersRecyclerView", "Landroidx/recyclerview/widget/RecyclerView;", "NetiID", "", "checkIsLoggedInChat", "", "clearAppNotifications", "getIdsSelectedOpponents", "Ljava/util/ArrayList;", "", "Lkotlin/collections/ArrayList;", "selectedUsers", "", "initDefaultActionBar", "initUI", "initUsersList", "isCallServiceRunning", "serviceClass", "Ljava/lang/Class;", "loadUsers", "logout", "onBackPressed", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateOptionsMenu", "menu", "Landroid/view/Menu;", "onOptionsItemSelected", "item", "Landroid/view/MenuItem;", "onResume", "removeAllUserData", "startCall", "isVideoCall", "startLoginActivity", "startLoginService", "startPermissionsActivity", "checkOnlyAudio", "updateActionBar", "countSelectedUsers", "Companion", "app_debug"})
public final class OpponentsActivity extends com.netizen.medicom.quickblox.activities.BaseActivity {
    private final java.lang.String TAG = null;
    private androidx.recyclerview.widget.RecyclerView usersRecyclerView;
    private com.quickblox.users.model.QBUser currentUser;
    private com.netizen.medicom.quickblox.adapters.UsersAdapter usersAdapter;
    private android.widget.TextView username;
    public static final com.netizen.medicom.quickblox.activities.OpponentsActivity.Companion Companion = null;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    private final boolean isCallServiceRunning(java.lang.Class<?> serviceClass) {
        return false;
    }
    
    private final void clearAppNotifications() {
    }
    
    private final void startPermissionsActivity(boolean checkOnlyAudio) {
    }
    
    private final void loadUsers() {
    }
    
    private final void initUI() {
    }
    
    private final void NetiID() {
    }
    
    private final void initUsersList() {
    }
    
    @java.lang.Override()
    public boolean onCreateOptionsMenu(@org.jetbrains.annotations.NotNull()
    android.view.Menu menu) {
        return false;
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    private final boolean checkIsLoggedInChat() {
        return false;
    }
    
    private final void startLoginService() {
    }
    
    private final void startCall(boolean isVideoCall) {
    }
    
    private final java.util.ArrayList<java.lang.Integer> getIdsSelectedOpponents(java.util.Collection<? extends com.quickblox.users.model.QBUser> selectedUsers) {
        return null;
    }
    
    private final void updateActionBar(int countSelectedUsers) {
    }
    
    private final void initDefaultActionBar() {
    }
    
    private final void logout() {
    }
    
    private final void removeAllUserData() {
    }
    
    private final void startLoginActivity() {
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    public OpponentsActivity() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/netizen/medicom/quickblox/activities/OpponentsActivity$Companion;", "", "()V", "start", "", "context", "Landroid/content/Context;", "app_debug"})
    public static final class Companion {
        
        public final void start(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
        }
        
        private Companion() {
            super();
        }
    }
}