package com.netizen.medicom.quickblox.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\n\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"}, d2 = {"QB_USER_FULL_NAME", "", "QB_USER_ID", "QB_USER_LOGIN", "QB_USER_PASSWORD", "QB_USER_TAGS", "SHARED_PREFS_NAME", "app_debug"})
public final class SharedPrefsHelperKt {
    private static final java.lang.String SHARED_PREFS_NAME = "qb";
    private static final java.lang.String QB_USER_ID = "qb_user_id";
    private static final java.lang.String QB_USER_LOGIN = "qb_user_login";
    private static final java.lang.String QB_USER_PASSWORD = "qb_user_password";
    private static final java.lang.String QB_USER_FULL_NAME = "qb_user_full_name";
    private static final java.lang.String QB_USER_TAGS = "qb_user_tags";
}