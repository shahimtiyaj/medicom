package com.netizen.medicom.quickblox.fragments;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u00aa\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004J\b\u0010 \u001a\u00020!H\u0002J\u0017\u0010\"\u001a\u00020#2\b\u0010$\u001a\u0004\u0018\u00010\u0016H\u0002\u00a2\u0006\u0002\u0010%J\u0010\u0010&\u001a\u00020!2\u0006\u0010\'\u001a\u00020(H\u0002J\u0010\u0010)\u001a\u00020*2\u0006\u0010+\u001a\u00020\u0016H\u0002J(\u0010,\u001a\b\u0012\u0004\u0012\u00020#0-2\f\u0010.\u001a\b\u0012\u0004\u0012\u00020#0-2\f\u0010/\u001a\b\u0012\u0004\u0012\u00020\u00160\u0015J\b\u00100\u001a\u00020\bH\u0002J\b\u00101\u001a\u00020!H\u0002J\b\u00102\u001a\u00020!H\u0002J\b\u00103\u001a\u00020!H\u0002J\u0010\u00104\u001a\u00020!2\u0006\u00105\u001a\u000206H\u0002J\u0014\u00107\u001a\u00020\b2\f\u00108\u001a\b\u0012\u0004\u0012\u00020#0-J\u0010\u00109\u001a\u00020!2\u0006\u0010:\u001a\u00020;H\u0016J\u0010\u0010<\u001a\u00020!2\u0006\u0010=\u001a\u000206H\u0016J\u0012\u0010>\u001a\u00020!2\b\u0010?\u001a\u0004\u0018\u00010@H\u0016J&\u0010A\u001a\u0004\u0018\u0001062\u0006\u0010B\u001a\u00020C2\b\u0010D\u001a\u0004\u0018\u00010E2\b\u0010?\u001a\u0004\u0018\u00010@H\u0016J\b\u0010F\u001a\u00020!H\u0016J\b\u0010G\u001a\u00020!H\u0016J\b\u0010H\u001a\u00020!H\u0002J\u0010\u0010I\u001a\u00020!2\u0006\u0010\r\u001a\u00020\u000eH\u0002J\b\u0010J\u001a\u00020!H\u0002J\b\u0010K\u001a\u00020!H\u0002J\b\u0010L\u001a\u00020!H\u0002J\u0006\u0010M\u001a\u00020!R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0014\u001a\n\u0012\u0004\u0012\u00020\u0016\u0018\u00010\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001aX\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001e\u001a\u0004\u0018\u00010\u001fX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006N"}, d2 = {"Lcom/netizen/medicom/quickblox/fragments/IncomeCallFragment;", "Landroidx/fragment/app/Fragment;", "Ljava/io/Serializable;", "Landroid/view/View$OnClickListener;", "()V", "CLICK_DELAY", "", "TAG", "", "alsoOnCallText", "Landroid/widget/TextView;", "callTypeTextView", "callerNameTextView", "conferenceType", "Lcom/quickblox/videochat/webrtc/QBRTCTypes$QBConferenceType;", "currentSession", "Lcom/quickblox/videochat/webrtc/QBRTCSession;", "incomeCallFragmentCallbackListener", "Lcom/netizen/medicom/quickblox/fragments/IncomeCallFragmentCallbackListener;", "lastClickTime", "opponentsIds", "", "", "progressUserName", "Landroid/widget/ProgressBar;", "rejectButton", "Landroid/widget/ImageButton;", "ringtonePlayer", "Lcom/netizen/medicom/quickblox/utils/RingtonePlayer;", "takeButton", "vibrator", "Landroid/os/Vibrator;", "accept", "", "createStubUserById", "Lcom/quickblox/users/model/QBUser;", "userId", "(Ljava/lang/Integer;)Lcom/quickblox/users/model/QBUser;", "enableButtons", "enable", "", "getBackgroundForCallerAvatar", "Landroid/graphics/drawable/Drawable;", "callerId", "getListAllUsersFromIds", "Ljava/util/ArrayList;", "existedUsers", "allIds", "getOtherIncUsersNames", "hideToolBar", "initButtonsListener", "initFields", "initUI", "view", "Landroid/view/View;", "makeStringFromUsersFullNames", "allUsers", "onAttach", "activity", "Landroid/app/Activity;", "onClick", "v", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onStart", "onStop", "reject", "setDisplayedTypeCall", "setVisibilityAlsoOnCallTextView", "startCallNotification", "stopCallNotification", "updateUserFromServer", "app_debug"})
public final class IncomeCallFragment extends androidx.fragment.app.Fragment implements java.io.Serializable, android.view.View.OnClickListener {
    private final java.lang.String TAG = null;
    private final long CLICK_DELAY = 0L;
    private android.widget.TextView callTypeTextView;
    private android.widget.ImageButton rejectButton;
    private android.widget.ImageButton takeButton;
    private android.widget.TextView alsoOnCallText;
    private android.widget.ProgressBar progressUserName;
    private android.widget.TextView callerNameTextView;
    private java.util.List<java.lang.Integer> opponentsIds;
    private android.os.Vibrator vibrator;
    private com.quickblox.videochat.webrtc.QBRTCTypes.QBConferenceType conferenceType;
    private long lastClickTime = 0L;
    private com.netizen.medicom.quickblox.utils.RingtonePlayer ringtonePlayer;
    private com.netizen.medicom.quickblox.fragments.IncomeCallFragmentCallbackListener incomeCallFragmentCallbackListener;
    private com.quickblox.videochat.webrtc.QBRTCSession currentSession;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    public void onAttach(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity) {
    }
    
    @java.lang.Override()
    public void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    private final void initFields() {
    }
    
    private final void hideToolBar() {
    }
    
    @java.lang.Override()
    public void onStart() {
    }
    
    private final void initButtonsListener() {
    }
    
    private final void initUI(android.view.View view) {
    }
    
    public final void updateUserFromServer() {
    }
    
    private final void setVisibilityAlsoOnCallTextView() {
    }
    
    private final android.graphics.drawable.Drawable getBackgroundForCallerAvatar(int callerId) {
        return null;
    }
    
    private final void startCallNotification() {
    }
    
    private final void stopCallNotification() {
    }
    
    private final java.lang.String getOtherIncUsersNames() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String makeStringFromUsersFullNames(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.quickblox.users.model.QBUser> allUsers) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.quickblox.users.model.QBUser> getListAllUsersFromIds(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.quickblox.users.model.QBUser> existedUsers, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.Integer> allIds) {
        return null;
    }
    
    private final com.quickblox.users.model.QBUser createStubUserById(java.lang.Integer userId) {
        return null;
    }
    
    private final void setDisplayedTypeCall(com.quickblox.videochat.webrtc.QBRTCTypes.QBConferenceType conferenceType) {
    }
    
    @java.lang.Override()
    public void onStop() {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.NotNull()
    android.view.View v) {
    }
    
    private final void accept() {
    }
    
    private final void reject() {
    }
    
    private final void enableButtons(boolean enable) {
    }
    
    public IncomeCallFragment() {
        super();
    }
}