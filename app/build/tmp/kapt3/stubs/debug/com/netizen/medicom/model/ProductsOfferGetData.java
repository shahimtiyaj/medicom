package com.netizen.medicom.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0006\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0014\n\u0002\u0010\u0002\n\u0002\b\u000b\u0018\u00002\u00020\u0001B\u0007\b\u0016\u00a2\u0006\u0002\u0010\u0002BW\b\u0016\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\b\u001a\u0004\u0018\u00010\t\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u000b\u0012\b\u0010\f\u001a\u0004\u0018\u00010\u000b\u0012\b\u0010\r\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010\u000eJ\r\u0010\u0016\u001a\u0004\u0018\u00010\u0010\u00a2\u0006\u0002\u0010\u0017J\b\u0010\u0018\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0019\u001a\u0004\u0018\u00010\u0004J\r\u0010\u001a\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010\u001bJ\b\u0010\u001c\u001a\u0004\u0018\u00010\u0004J\r\u0010\u001d\u001a\u0004\u0018\u00010\u0010\u00a2\u0006\u0002\u0010\u0017J\r\u0010\u001e\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\u001fJ\b\u0010 \u001a\u0004\u0018\u00010\u0004J\b\u0010!\u001a\u0004\u0018\u00010\u0004J\r\u0010\"\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010\u001bJ\r\u0010#\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010\u001bJ\u000e\u0010$\u001a\u00020%2\u0006\u0010\u000f\u001a\u00020\u0010J\u000e\u0010&\u001a\u00020%2\u0006\u0010\u0007\u001a\u00020\u0004J\u000e\u0010\'\u001a\u00020%2\u0006\u0010\u0003\u001a\u00020\u0004J\u000e\u0010(\u001a\u00020%2\u0006\u0010\r\u001a\u00020\u000bJ\u000e\u0010)\u001a\u00020%2\u0006\u0010\u0006\u001a\u00020\u0004J\u000e\u0010*\u001a\u00020%2\u0006\u0010\u0013\u001a\u00020\u0010J\u000e\u0010+\u001a\u00020%2\u0006\u0010\b\u001a\u00020\tJ\u000e\u0010,\u001a\u00020%2\u0006\u0010\u0005\u001a\u00020\u0004J\u000e\u0010-\u001a\u00020%2\u0006\u0010\u0015\u001a\u00020\u0004J\u000e\u0010.\u001a\u00020%2\u0006\u0010\f\u001a\u00020\u000bJ\u000e\u0010/\u001a\u00020%2\u0006\u0010\n\u001a\u00020\u000bR\u0016\u0010\u000f\u001a\u0004\u0018\u00010\u00108\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0011R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\r\u001a\u0004\u0018\u00010\u000b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0012R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0013\u001a\u0004\u0018\u00010\u00108\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0011R\u0016\u0010\b\u001a\u0004\u0018\u00010\t8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0014R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0015\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\f\u001a\u0004\u0018\u00010\u000b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0012R\u0016\u0010\n\u001a\u0004\u0018\u00010\u000b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0012\u00a8\u00060"}, d2 = {"Lcom/netizen/medicom/model/ProductsOfferGetData;", "", "()V", "offerUseDate", "", "productType", "productName", "offerCode", "productQuantity", "", "totalPrice", "", "totalDiscount", "payableAmount", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;)V", "genCodeStatus", "", "Ljava/lang/Integer;", "Ljava/lang/Double;", "productPurchaseLogID", "Ljava/lang/Long;", "purchasePoint", "getGenCodeStatus", "()Ljava/lang/Integer;", "getOfferCode", "getOfferUseDate", "getPayableAmount", "()Ljava/lang/Double;", "getProductName", "getProductPurchaseLogID", "getProductQuantity", "()Ljava/lang/Long;", "getProductType", "getPurchasePoint", "getTotalDiscount", "getTotalPrice", "setGenCodeStatus", "", "setOfferCode", "setOfferUseDate", "setPayableAmount", "setProductName", "setProductPurchaseLogID", "setProductQuantity", "setProductType", "setPurchasePoint", "setTotalDiscount", "setTotalPrice", "app_debug"})
public final class ProductsOfferGetData {
    @com.google.gson.annotations.SerializedName(value = "offerUseDate")
    private java.lang.String offerUseDate;
    @com.google.gson.annotations.SerializedName(value = "productType")
    private java.lang.String productType;
    @com.google.gson.annotations.SerializedName(value = "productName")
    private java.lang.String productName;
    @com.google.gson.annotations.SerializedName(value = "offerCode")
    private java.lang.String offerCode;
    @com.google.gson.annotations.SerializedName(value = "productQuantity")
    private java.lang.Long productQuantity;
    @com.google.gson.annotations.SerializedName(value = "totalPrice")
    private java.lang.Double totalPrice;
    @com.google.gson.annotations.SerializedName(value = "totalDiscount")
    private java.lang.Double totalDiscount;
    @com.google.gson.annotations.SerializedName(value = "payableAmount")
    private java.lang.Double payableAmount;
    @com.google.gson.annotations.SerializedName(value = "productPurchaseLogID")
    private java.lang.Integer productPurchaseLogID;
    @com.google.gson.annotations.SerializedName(value = "genCodeStatus")
    private java.lang.Integer genCodeStatus;
    @com.google.gson.annotations.SerializedName(value = "purchasePoint")
    private java.lang.String purchasePoint;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getOfferUseDate() {
        return null;
    }
    
    public final void setOfferUseDate(@org.jetbrains.annotations.NotNull()
    java.lang.String offerUseDate) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getProductType() {
        return null;
    }
    
    public final void setProductType(@org.jetbrains.annotations.NotNull()
    java.lang.String productType) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getProductName() {
        return null;
    }
    
    public final void setProductName(@org.jetbrains.annotations.NotNull()
    java.lang.String productName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getOfferCode() {
        return null;
    }
    
    public final void setOfferCode(@org.jetbrains.annotations.NotNull()
    java.lang.String offerCode) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getProductQuantity() {
        return null;
    }
    
    public final void setProductQuantity(long productQuantity) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Double getTotalPrice() {
        return null;
    }
    
    public final void setTotalPrice(double totalPrice) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Double getTotalDiscount() {
        return null;
    }
    
    public final void setTotalDiscount(double totalDiscount) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Double getPayableAmount() {
        return null;
    }
    
    public final void setPayableAmount(double payableAmount) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getProductPurchaseLogID() {
        return null;
    }
    
    public final void setProductPurchaseLogID(int productPurchaseLogID) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getGenCodeStatus() {
        return null;
    }
    
    public final void setGenCodeStatus(int genCodeStatus) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPurchasePoint() {
        return null;
    }
    
    public final void setPurchasePoint(@org.jetbrains.annotations.NotNull()
    java.lang.String purchasePoint) {
    }
    
    public ProductsOfferGetData() {
        super();
    }
    
    public ProductsOfferGetData(@org.jetbrains.annotations.Nullable()
    java.lang.String offerUseDate, @org.jetbrains.annotations.Nullable()
    java.lang.String productType, @org.jetbrains.annotations.Nullable()
    java.lang.String productName, @org.jetbrains.annotations.Nullable()
    java.lang.String offerCode, @org.jetbrains.annotations.Nullable()
    java.lang.Long productQuantity, @org.jetbrains.annotations.Nullable()
    java.lang.Double totalPrice, @org.jetbrains.annotations.Nullable()
    java.lang.Double totalDiscount, @org.jetbrains.annotations.Nullable()
    java.lang.Double payableAmount) {
        super();
    }
}