package com.netizen.medicom.quickblox.db;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\r\u001a\u00020\u000eJ\b\u0010\u000f\u001a\u00020\u0010H\u0002J\u0017\u0010\u0011\u001a\u0004\u0018\u00010\t2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013\u00a2\u0006\u0002\u0010\u0014J$\u0010\u0015\u001a\u0012\u0012\u0004\u0012\u00020\t0\bj\b\u0012\u0004\u0012\u00020\t`\n2\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00130\u0017J&\u0010\u0018\u001a\u00020\u000e2\u0016\u0010\u0007\u001a\u0012\u0012\u0004\u0012\u00020\t0\bj\b\u0012\u0004\u0012\u00020\t`\n2\u0006\u0010\u0019\u001a\u00020\u001aJ\u000e\u0010\u001b\u001a\u00020\u000e2\u0006\u0010\u001c\u001a\u00020\tR\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R!\u0010\u0007\u001a\u0012\u0012\u0004\u0012\u00020\t0\bj\b\u0012\u0004\u0012\u00020\t`\n8F\u00a2\u0006\u0006\u001a\u0004\b\u000b\u0010\f\u00a8\u0006\u001d"}, d2 = {"Lcom/netizen/medicom/quickblox/db/QbUsersDbManager;", "", "()V", "TAG", "", "getTAG", "()Ljava/lang/String;", "allUsers", "Ljava/util/ArrayList;", "Lcom/quickblox/users/model/QBUser;", "Lkotlin/collections/ArrayList;", "getAllUsers", "()Ljava/util/ArrayList;", "clearDB", "", "getDb", "Landroid/database/sqlite/SQLiteDatabase;", "getUserById", "userId", "", "(Ljava/lang/Integer;)Lcom/quickblox/users/model/QBUser;", "getUsersByIds", "usersIds", "", "saveAllUsers", "needRemoveOldData", "", "saveUser", "qbUser", "app_debug"})
public final class QbUsersDbManager {
    @org.jetbrains.annotations.NotNull()
    private static final java.lang.String TAG = null;
    public static final com.netizen.medicom.quickblox.db.QbUsersDbManager INSTANCE = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getTAG() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.quickblox.users.model.QBUser> getAllUsers() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.quickblox.users.model.QBUser getUserById(@org.jetbrains.annotations.Nullable()
    java.lang.Integer userId) {
        return null;
    }
    
    public final void saveAllUsers(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.quickblox.users.model.QBUser> allUsers, boolean needRemoveOldData) {
    }
    
    public final void saveUser(@org.jetbrains.annotations.NotNull()
    com.quickblox.users.model.QBUser qbUser) {
    }
    
    public final void clearDB() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.quickblox.users.model.QBUser> getUsersByIds(@org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.Integer> usersIds) {
        return null;
    }
    
    private final android.database.sqlite.SQLiteDatabase getDb() {
        return null;
    }
    
    private QbUsersDbManager() {
        super();
    }
}