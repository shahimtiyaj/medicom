package com.netizen.medicom.quickblox.adapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0002 !B\u001b\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\u0002\u0010\bJ\b\u0010\u0012\u001a\u00020\u0013H\u0016J\u0018\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00022\u0006\u0010\u0017\u001a\u00020\u0013H\u0016J\u0018\u0010\u0018\u001a\u00020\u00022\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0013H\u0016J\u000e\u0010\u001c\u001a\u00020\u00152\u0006\u0010\r\u001a\u00020\u000eJ\u0010\u0010\u001d\u001a\u00020\u00152\u0006\u0010\u001e\u001a\u00020\u0007H\u0002J\u0014\u0010\u001f\u001a\u00020\u00152\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006R\u0014\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00070\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00070\u00068F\u00a2\u0006\u0006\u001a\u0004\b\u0010\u0010\u0011R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""}, d2 = {"Lcom/netizen/medicom/quickblox/adapters/UsersAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/netizen/medicom/quickblox/adapters/UsersAdapter$ViewHolder;", "context", "Landroid/content/Context;", "usersList", "", "Lcom/quickblox/users/model/QBUser;", "(Landroid/content/Context;Ljava/util/List;)V", "_selectedUsers", "", "getContext", "()Landroid/content/Context;", "selectedItemsCountsChangedListener", "Lcom/netizen/medicom/quickblox/adapters/UsersAdapter$SelectedItemsCountsChangedListener;", "selectedUsers", "getSelectedUsers", "()Ljava/util/List;", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "setSelectedItemsCountsChangedListener", "toggleSelection", "qbUser", "updateUsersList", "SelectedItemsCountsChangedListener", "ViewHolder", "app_debug"})
public final class UsersAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.netizen.medicom.quickblox.adapters.UsersAdapter.ViewHolder> {
    private final java.util.List<com.quickblox.users.model.QBUser> _selectedUsers = null;
    private com.netizen.medicom.quickblox.adapters.UsersAdapter.SelectedItemsCountsChangedListener selectedItemsCountsChangedListener;
    @org.jetbrains.annotations.NotNull()
    private final android.content.Context context = null;
    private java.util.List<? extends com.quickblox.users.model.QBUser> usersList;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.quickblox.users.model.QBUser> getSelectedUsers() {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.netizen.medicom.quickblox.adapters.UsersAdapter.ViewHolder holder, int position) {
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    public final void updateUsersList(@org.jetbrains.annotations.NotNull()
    java.util.List<? extends com.quickblox.users.model.QBUser> usersList) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.netizen.medicom.quickblox.adapters.UsersAdapter.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    private final void toggleSelection(com.quickblox.users.model.QBUser qbUser) {
    }
    
    public final void setSelectedItemsCountsChangedListener(@org.jetbrains.annotations.NotNull()
    com.netizen.medicom.quickblox.adapters.UsersAdapter.SelectedItemsCountsChangedListener selectedItemsCountsChangedListener) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getContext() {
        return null;
    }
    
    public UsersAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.util.List<? extends com.quickblox.users.model.QBUser> usersList) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0019\u0010\u0005\u001a\n \u0007*\u0004\u0018\u00010\u00060\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0019\u0010\n\u001a\n \u0007*\u0004\u0018\u00010\u000b0\u000b\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0019\u0010\u000e\u001a\n \u0007*\u0004\u0018\u00010\u000f0\u000f\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011\u00a8\u0006\u0012"}, d2 = {"Lcom/netizen/medicom/quickblox/adapters/UsersAdapter$ViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "view", "Landroid/view/View;", "(Landroid/view/View;)V", "opponentIcon", "Landroid/widget/ImageView;", "kotlin.jvm.PlatformType", "getOpponentIcon", "()Landroid/widget/ImageView;", "opponentName", "Landroid/widget/TextView;", "getOpponentName", "()Landroid/widget/TextView;", "rootLayout", "Landroid/widget/LinearLayout;", "getRootLayout", "()Landroid/widget/LinearLayout;", "app_debug"})
    public static final class ViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        private final android.widget.ImageView opponentIcon = null;
        private final android.widget.TextView opponentName = null;
        private final android.widget.LinearLayout rootLayout = null;
        
        public final android.widget.ImageView getOpponentIcon() {
            return null;
        }
        
        public final android.widget.TextView getOpponentName() {
            return null;
        }
        
        public final android.widget.LinearLayout getRootLayout() {
            return null;
        }
        
        public ViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View view) {
            super(null);
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lcom/netizen/medicom/quickblox/adapters/UsersAdapter$SelectedItemsCountsChangedListener;", "", "onCountSelectedItemsChanged", "", "count", "", "app_debug"})
    public static abstract interface SelectedItemsCountsChangedListener {
        
        public abstract void onCountSelectedItemsChanged(int count);
    }
}