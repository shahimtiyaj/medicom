package com.netizen.medicom.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014J\u0016\u0010\u0015\u001a\u00020\u00122\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0017R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001d\u0010\u0007\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR \u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000e0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\f\"\u0004\b\u000f\u0010\u0010\u00a8\u0006\u0019"}, d2 = {"Lcom/netizen/medicom/repository/DoctorRepository;", "", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "appPreferences", "Lcom/netizen/medicom/utils/AppPreferences;", "doctorDesignationArrayList", "Landroidx/lifecycle/MutableLiveData;", "", "Lcom/netizen/medicom/model/DoctorDesignation;", "getDoctorDesignationArrayList", "()Landroidx/lifecycle/MutableLiveData;", "isDoctorDesignationFound", "", "setDoctorDesignationFound", "(Landroidx/lifecycle/MutableLiveData;)V", "doctorRegistration", "", "doctorEnroll", "Lcom/netizen/medicom/model/DoctorEnroll;", "getDoctorDesignation", "typeDefaultCode", "", "typeStatus", "app_debug"})
public final class DoctorRepository {
    private final com.netizen.medicom.utils.AppPreferences appPreferences = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.medicom.model.DoctorDesignation>> doctorDesignationArrayList = null;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDoctorDesignationFound;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.medicom.model.DoctorDesignation>> getDoctorDesignationArrayList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDoctorDesignationFound() {
        return null;
    }
    
    public final void setDoctorDesignationFound(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    public final void doctorRegistration(@org.jetbrains.annotations.NotNull()
    com.netizen.medicom.model.DoctorEnroll doctorEnroll) {
    }
    
    public final void getDoctorDesignation(@org.jetbrains.annotations.NotNull()
    java.lang.String typeDefaultCode, @org.jetbrains.annotations.NotNull()
    java.lang.String typeStatus) {
    }
    
    public DoctorRepository(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        super();
    }
}