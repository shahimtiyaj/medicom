package com.netizen.medicom.quickblox.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u0007\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\tJ\u001c\u0010\n\u001a\u0002H\u000b\"\u0004\b\u0000\u0010\u000b2\u0006\u0010\b\u001a\u00020\tH\u0086\u0002\u00a2\u0006\u0002\u0010\fJ$\u0010\n\u001a\u0002H\u000b\"\u0004\b\u0000\u0010\u000b2\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\r\u001a\u0002H\u000bH\u0086\u0002\u00a2\u0006\u0002\u0010\u000eJ\u0006\u0010\u000f\u001a\u00020\u0010J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\b\u001a\u00020\tH\u0002J\u0006\u0010\u0013\u001a\u00020\u0012J\u0018\u0010\u0014\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\t2\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001J\u000e\u0010\u0016\u001a\u00020\u00062\u0006\u0010\u0017\u001a\u00020\u0010R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"}, d2 = {"Lcom/netizen/medicom/quickblox/utils/SharedPrefsHelper;", "", "()V", "sharedPreferences", "Landroid/content/SharedPreferences;", "clearAllData", "", "delete", "key", "", "get", "T", "(Ljava/lang/String;)Ljava/lang/Object;", "defValue", "(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;", "getQbUser", "Lcom/quickblox/users/model/QBUser;", "has", "", "hasQbUser", "save", "value", "saveQbUser", "qbUser", "app_debug"})
public final class SharedPrefsHelper {
    private static android.content.SharedPreferences sharedPreferences;
    public static final com.netizen.medicom.quickblox.utils.SharedPrefsHelper INSTANCE = null;
    
    public final void delete(@org.jetbrains.annotations.NotNull()
    java.lang.String key) {
    }
    
    public final void save(@org.jetbrains.annotations.NotNull()
    java.lang.String key, @org.jetbrains.annotations.Nullable()
    java.lang.Object value) {
    }
    
    public final void saveQbUser(@org.jetbrains.annotations.NotNull()
    com.quickblox.users.model.QBUser qbUser) {
    }
    
    public final void clearAllData() {
    }
    
    public final boolean hasQbUser() {
        return false;
    }
    
    @kotlin.Suppress(names = {"UNCHECKED_CAST"})
    public final <T extends java.lang.Object>T get(@org.jetbrains.annotations.NotNull()
    java.lang.String key) {
        return null;
    }
    
    @kotlin.Suppress(names = {"UNCHECKED_CAST"})
    public final <T extends java.lang.Object>T get(@org.jetbrains.annotations.NotNull()
    java.lang.String key, T defValue) {
        return null;
    }
    
    private final boolean has(java.lang.String key) {
        return false;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.quickblox.users.model.QBUser getQbUser() {
        return null;
    }
    
    private SharedPrefsHelper() {
        super();
    }
}