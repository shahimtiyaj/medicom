package com.netizen.medicom.quickblox.ModelMe;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001\bB\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0006\u001a\u00020\u00072\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lcom/netizen/medicom/quickblox/ModelMe/TokenResponse;", "", "()V", "session", "Lcom/netizen/medicom/quickblox/ModelMe/TokenResponse$Session;", "getSession", "setSession", "", "Session", "app_debug"})
public final class TokenResponse {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "session")
    private com.netizen.medicom.quickblox.ModelMe.TokenResponse.Session session;
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.medicom.quickblox.ModelMe.TokenResponse.Session getSession() {
        return null;
    }
    
    public final void setSession(@org.jetbrains.annotations.Nullable()
    com.netizen.medicom.quickblox.ModelMe.TokenResponse.Session session) {
    }
    
    public TokenResponse() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0012\n\u0002\u0010\u0002\n\u0002\b\n\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\r\u0010\u000f\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0010J\b\u0010\u0011\u001a\u0004\u0018\u00010\u0007J\r\u0010\u0012\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0010J\r\u0010\u0013\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0010J\b\u0010\u0014\u001a\u0004\u0018\u00010\u0007J\r\u0010\u0015\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0010J\b\u0010\u0016\u001a\u0004\u0018\u00010\u0007J\b\u0010\u0017\u001a\u0004\u0018\u00010\u0007J\r\u0010\u0018\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0010J\u0015\u0010\u0019\u001a\u00020\u001a2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u001bJ\u0010\u0010\u001c\u001a\u00020\u001a2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007J\u0015\u0010\u001d\u001a\u00020\u001a2\b\u0010\b\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u001bJ\u0015\u0010\u001e\u001a\u00020\u001a2\b\u0010\t\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u001bJ\u0010\u0010\u001f\u001a\u00020\u001a2\b\u0010\n\u001a\u0004\u0018\u00010\u0007J\u0015\u0010 \u001a\u00020\u001a2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u001bJ\u0010\u0010!\u001a\u00020\u001a2\b\u0010\f\u001a\u0004\u0018\u00010\u0007J\u0010\u0010\"\u001a\u00020\u001a2\b\u0010\r\u001a\u0004\u0018\u00010\u0007J\u0015\u0010#\u001a\u00020\u001a2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u001bR\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u0016\u0010\t\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u0014\u0010\n\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u0014\u0010\f\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000e\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005\u00a8\u0006$"}, d2 = {"Lcom/netizen/medicom/quickblox/ModelMe/TokenResponse$Session;", "", "()V", "applicationId", "", "Ljava/lang/Integer;", "createdAt", "", "id", "nonce", "token", "ts", "updatedAt", "uselessId", "userId", "getApplicationId", "()Ljava/lang/Integer;", "getCreatedAt", "getId", "getNonce", "getToken", "getTs", "getUpdatedAt", "getUselessId", "getUserId", "setApplicationId", "", "(Ljava/lang/Integer;)V", "setCreatedAt", "setId", "setNonce", "setToken", "setTs", "setUpdatedAt", "setUselessId", "setUserId", "app_debug"})
    public static final class Session {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "application_id")
        private java.lang.Integer applicationId;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "created_at")
        private java.lang.String createdAt;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "id")
        private java.lang.Integer id;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "nonce")
        private java.lang.Integer nonce;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "token")
        private java.lang.String token;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "ts")
        private java.lang.Integer ts;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "updated_at")
        private java.lang.String updatedAt;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "user_id")
        private java.lang.Integer userId;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "useless_id")
        private java.lang.String uselessId;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getApplicationId() {
            return null;
        }
        
        public final void setApplicationId(@org.jetbrains.annotations.Nullable()
        java.lang.Integer applicationId) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCreatedAt() {
            return null;
        }
        
        public final void setCreatedAt(@org.jetbrains.annotations.Nullable()
        java.lang.String createdAt) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getId() {
            return null;
        }
        
        public final void setId(@org.jetbrains.annotations.Nullable()
        java.lang.Integer id) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getNonce() {
            return null;
        }
        
        public final void setNonce(@org.jetbrains.annotations.Nullable()
        java.lang.Integer nonce) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getToken() {
            return null;
        }
        
        public final void setToken(@org.jetbrains.annotations.Nullable()
        java.lang.String token) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getTs() {
            return null;
        }
        
        public final void setTs(@org.jetbrains.annotations.Nullable()
        java.lang.Integer ts) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getUpdatedAt() {
            return null;
        }
        
        public final void setUpdatedAt(@org.jetbrains.annotations.Nullable()
        java.lang.String updatedAt) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getUserId() {
            return null;
        }
        
        public final void setUserId(@org.jetbrains.annotations.Nullable()
        java.lang.Integer userId) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getUselessId() {
            return null;
        }
        
        public final void setUselessId(@org.jetbrains.annotations.Nullable()
        java.lang.String uselessId) {
        }
        
        public Session() {
            super();
        }
    }
}