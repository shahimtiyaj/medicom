package com.netizen.medicom.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u0002\n\u0002\b\t\u0018\u00002\u00020\u0001:\u0001\u001fB\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u000e\u001a\u0004\u0018\u00010\u0004J\r\u0010\u000f\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0010J\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001J\b\u0010\u0012\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0013\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0014\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0015\u001a\u0004\u0018\u00010\rJ\u0010\u0010\u0016\u001a\u00020\u00172\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0015\u0010\u0018\u001a\u00020\u00172\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0019J\u0010\u0010\u001a\u001a\u00020\u00172\b\u0010\b\u001a\u0004\u0018\u00010\u0001J\u0010\u0010\u001b\u001a\u00020\u00172\b\u0010\t\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u001c\u001a\u00020\u00172\b\u0010\n\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u001d\u001a\u00020\u00172\b\u0010\u000b\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u001e\u001a\u00020\u00172\b\u0010\f\u001a\u0004\u0018\u00010\rR\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0014\u0010\b\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\u0004\u0018\u00010\r8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "}, d2 = {"Lcom/netizen/medicom/model/TokenSubmitPostData;", "", "()V", "attachContent", "", "attachSaveOrEditable", "", "Ljava/lang/Boolean;", "attachmentName", "tokenContact", "tokenDetails", "tokenSource", "tokenTypeInfoDTO", "Lcom/netizen/medicom/model/TokenSubmitPostData$TokenTypeInfoDTO;", "getAttachContent", "getAttachSaveOrEditable", "()Ljava/lang/Boolean;", "getAttachmentName", "getTokenContact", "getTokenDetails", "getTokenSource", "getTokenTypeInfoDTO", "setAttachContent", "", "setAttachSaveOrEditable", "(Ljava/lang/Boolean;)V", "setAttachmentName", "setTokenContact", "setTokenDetails", "setTokenSource", "setTokenTypeInfoDTO", "TokenTypeInfoDTO", "app_debug"})
public final class TokenSubmitPostData {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "attachSaveOrEditable")
    private java.lang.Boolean attachSaveOrEditable;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "tokenSource")
    private java.lang.String tokenSource;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "tokenDetails")
    private java.lang.String tokenDetails;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "tokenContact")
    private java.lang.String tokenContact;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "tokenTypeInfoDTO")
    private com.netizen.medicom.model.TokenSubmitPostData.TokenTypeInfoDTO tokenTypeInfoDTO;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "attachContent")
    private java.lang.String attachContent;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "attachmentName")
    private java.lang.Object attachmentName;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean getAttachSaveOrEditable() {
        return null;
    }
    
    public final void setAttachSaveOrEditable(@org.jetbrains.annotations.Nullable()
    java.lang.Boolean attachSaveOrEditable) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTokenSource() {
        return null;
    }
    
    public final void setTokenSource(@org.jetbrains.annotations.Nullable()
    java.lang.String tokenSource) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTokenDetails() {
        return null;
    }
    
    public final void setTokenDetails(@org.jetbrains.annotations.Nullable()
    java.lang.String tokenDetails) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTokenContact() {
        return null;
    }
    
    public final void setTokenContact(@org.jetbrains.annotations.Nullable()
    java.lang.String tokenContact) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.medicom.model.TokenSubmitPostData.TokenTypeInfoDTO getTokenTypeInfoDTO() {
        return null;
    }
    
    public final void setTokenTypeInfoDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.medicom.model.TokenSubmitPostData.TokenTypeInfoDTO tokenTypeInfoDTO) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAttachContent() {
        return null;
    }
    
    public final void setAttachContent(@org.jetbrains.annotations.Nullable()
    java.lang.String attachContent) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getAttachmentName() {
        return null;
    }
    
    public final void setAttachmentName(@org.jetbrains.annotations.Nullable()
    java.lang.Object attachmentName) {
    }
    
    public TokenSubmitPostData() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\r\u0010\u0006\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0007J\u0015\u0010\b\u001a\u00020\t2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\nR\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005\u00a8\u0006\u000b"}, d2 = {"Lcom/netizen/medicom/model/TokenSubmitPostData$TokenTypeInfoDTO;", "", "()V", "coreCategoryID", "", "Ljava/lang/Integer;", "getCoreCategoryID", "()Ljava/lang/Integer;", "setCoreCategoryID", "", "(Ljava/lang/Integer;)V", "app_debug"})
    public static final class TokenTypeInfoDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "coreCategoryID")
        private java.lang.Integer coreCategoryID;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getCoreCategoryID() {
            return null;
        }
        
        public final void setCoreCategoryID(@org.jetbrains.annotations.Nullable()
        java.lang.Integer coreCategoryID) {
        }
        
        public TokenTypeInfoDTO() {
            super();
        }
    }
}