package com.netizen.medicom.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u000b\n\u0002\u0010\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\r\u001a\u0004\u0018\u00010\u0004J\b\u0010\u000e\u001a\u0004\u0018\u00010\u0004J\b\u0010\u000f\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0010\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0011\u001a\u0004\u0018\u00010\u0004J\r\u0010\u0012\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0002\u0010\u0013J\b\u0010\u0014\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0015\u001a\u00020\u00162\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0017\u001a\u00020\u00162\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0018\u001a\u00020\u00162\b\u0010\u0006\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0019\u001a\u00020\u00162\b\u0010\u0007\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u001a\u001a\u00020\u00162\b\u0010\b\u001a\u0004\u0018\u00010\u0004J\u0015\u0010\u001b\u001a\u00020\u00162\b\u0010\t\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0002\u0010\u001cJ\u0010\u0010\u001d\u001a\u00020\u00162\b\u0010\f\u001a\u0004\u0018\u00010\u0004R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\t\u001a\u0004\u0018\u00010\n8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u000bR\u0014\u0010\f\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"}, d2 = {"Lcom/netizen/medicom/model/RevenueLogGetData;", "", "()V", "amount", "", "date", "grade", "gradePercent", "productName", "purchaseId", "", "Ljava/lang/Integer;", "revenueType", "getAmount", "getDate", "getGrade", "getGradePercent", "getProductName", "getPurchaseId", "()Ljava/lang/Integer;", "getRevenueType", "setAmount", "", "setDate", "setGrade", "setGradePercent", "setProductName", "setPurchaseId", "(Ljava/lang/Integer;)V", "setRevenueType", "app_debug"})
public final class RevenueLogGetData {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "date")
    private java.lang.String date;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "amount")
    private java.lang.String amount;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "grade")
    private java.lang.String grade;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "purchase_id")
    private java.lang.Integer purchaseId;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "revenue_type")
    private java.lang.String revenueType;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "grade_percent")
    private java.lang.String gradePercent;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "productName")
    private java.lang.String productName;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDate() {
        return null;
    }
    
    public final void setDate(@org.jetbrains.annotations.Nullable()
    java.lang.String date) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAmount() {
        return null;
    }
    
    public final void setAmount(@org.jetbrains.annotations.Nullable()
    java.lang.String amount) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getGrade() {
        return null;
    }
    
    public final void setGrade(@org.jetbrains.annotations.Nullable()
    java.lang.String grade) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getPurchaseId() {
        return null;
    }
    
    public final void setPurchaseId(@org.jetbrains.annotations.Nullable()
    java.lang.Integer purchaseId) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getRevenueType() {
        return null;
    }
    
    public final void setRevenueType(@org.jetbrains.annotations.Nullable()
    java.lang.String revenueType) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getGradePercent() {
        return null;
    }
    
    public final void setGradePercent(@org.jetbrains.annotations.Nullable()
    java.lang.String gradePercent) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getProductName() {
        return null;
    }
    
    public final void setProductName(@org.jetbrains.annotations.Nullable()
    java.lang.String productName) {
    }
    
    public RevenueLogGetData() {
        super();
    }
}