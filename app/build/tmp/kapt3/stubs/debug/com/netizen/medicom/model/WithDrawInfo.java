package com.netizen.medicom.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001:\u0001\u0011B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\b\u001a\u0004\u0018\u00010\u0001J\b\u0010\t\u001a\u0004\u0018\u00010\u0001J\b\u0010\n\u001a\u0004\u0018\u00010\u0001J\b\u0010\u000b\u001a\u0004\u0018\u00010\u0007J\u0010\u0010\f\u001a\u00020\r2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0001J\u0010\u0010\u000e\u001a\u00020\r2\b\u0010\u0004\u001a\u0004\u0018\u00010\u0001J\u0010\u0010\u000f\u001a\u00020\r2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0001J\u0010\u0010\u0010\u001a\u00020\r2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"}, d2 = {"Lcom/netizen/medicom/model/WithDrawInfo;", "", "()V", "taggingDate", "taggingID", "taggingTypeCoreCategoryInfoDTO", "userBankAccountInfoDTO", "Lcom/netizen/medicom/model/WithDrawInfo$UserBankAccountInfoDTO;", "getTaggingDate", "getTaggingID", "getTaggingTypeCoreCategoryInfoDTO", "getUserBankAccountInfoDTO", "setTaggingDate", "", "setTaggingID", "setTaggingTypeCoreCategoryInfoDTO", "setUserBankAccountInfoDTO", "UserBankAccountInfoDTO", "app_debug"})
public final class WithDrawInfo {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "taggingID")
    private java.lang.Object taggingID;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "taggingDate")
    private java.lang.Object taggingDate;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "taggingTypeCoreCategoryInfoDTO")
    private java.lang.Object taggingTypeCoreCategoryInfoDTO;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "userBankAccountInfoDTO")
    private com.netizen.medicom.model.WithDrawInfo.UserBankAccountInfoDTO userBankAccountInfoDTO;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getTaggingID() {
        return null;
    }
    
    public final void setTaggingID(@org.jetbrains.annotations.Nullable()
    java.lang.Object taggingID) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getTaggingDate() {
        return null;
    }
    
    public final void setTaggingDate(@org.jetbrains.annotations.Nullable()
    java.lang.Object taggingDate) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getTaggingTypeCoreCategoryInfoDTO() {
        return null;
    }
    
    public final void setTaggingTypeCoreCategoryInfoDTO(@org.jetbrains.annotations.Nullable()
    java.lang.Object taggingTypeCoreCategoryInfoDTO) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.medicom.model.WithDrawInfo.UserBankAccountInfoDTO getUserBankAccountInfoDTO() {
        return null;
    }
    
    public final void setUserBankAccountInfoDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.medicom.model.WithDrawInfo.UserBankAccountInfoDTO userBankAccountInfoDTO) {
    }
    
    public WithDrawInfo() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b \n\u0002\u0010\u0002\n\u0002\b\u0016\u0018\u00002\u00020\u0001:\u0001GB\u0005\u00a2\u0006\u0002\u0010\u0002J\r\u0010\u001c\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u001dJ\b\u0010\u001e\u001a\u0004\u0018\u00010\u0007J\b\u0010\u001f\u001a\u0004\u0018\u00010\u0007J\b\u0010 \u001a\u0004\u0018\u00010\u0007J\b\u0010!\u001a\u0004\u0018\u00010\u0001J\r\u0010\"\u001a\u0004\u0018\u00010\f\u00a2\u0006\u0002\u0010#J\b\u0010$\u001a\u0004\u0018\u00010\u0007J\b\u0010%\u001a\u0004\u0018\u00010\u0007J\b\u0010&\u001a\u0004\u0018\u00010\u0011J\b\u0010\'\u001a\u0004\u0018\u00010\u0001J\b\u0010(\u001a\u0004\u0018\u00010\u0007J\b\u0010)\u001a\u0004\u0018\u00010\u0007J\b\u0010*\u001a\u0004\u0018\u00010\u0007J\b\u0010+\u001a\u0004\u0018\u00010\u0001J\r\u0010,\u001a\u0004\u0018\u00010\f\u00a2\u0006\u0002\u0010#J\b\u0010-\u001a\u0004\u0018\u00010\u0001J\b\u0010.\u001a\u0004\u0018\u00010\u0001J\r\u0010/\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u001dJ\b\u00100\u001a\u0004\u0018\u00010\u0001J\u0015\u00101\u001a\u0002022\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u00103J\u0010\u00104\u001a\u0002022\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007J\u0010\u00105\u001a\u0002022\b\u0010\b\u001a\u0004\u0018\u00010\u0007J\u0010\u00106\u001a\u0002022\b\u0010\t\u001a\u0004\u0018\u00010\u0007J\u0010\u00107\u001a\u0002022\b\u0010\n\u001a\u0004\u0018\u00010\u0001J\u0015\u00108\u001a\u0002022\b\u0010\u000b\u001a\u0004\u0018\u00010\f\u00a2\u0006\u0002\u00109J\u0010\u0010:\u001a\u0002022\b\u0010\u000e\u001a\u0004\u0018\u00010\u0007J\u0010\u0010;\u001a\u0002022\b\u0010\u000f\u001a\u0004\u0018\u00010\u0007J\u0010\u0010<\u001a\u0002022\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011J\u0010\u0010=\u001a\u0002022\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001J\u0010\u0010>\u001a\u0002022\b\u0010\u0013\u001a\u0004\u0018\u00010\u0007J\u0010\u0010?\u001a\u0002022\b\u0010\u0014\u001a\u0004\u0018\u00010\u0007J\u0010\u0010@\u001a\u0002022\b\u0010\u0015\u001a\u0004\u0018\u00010\u0007J\u0010\u0010A\u001a\u0002022\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001J\u0015\u0010B\u001a\u0002022\b\u0010\u0017\u001a\u0004\u0018\u00010\f\u00a2\u0006\u0002\u00109J\u0010\u0010C\u001a\u0002022\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001J\u0010\u0010D\u001a\u0002022\b\u0010\u0019\u001a\u0004\u0018\u00010\u0001J\u0015\u0010E\u001a\u0002022\b\u0010\u001a\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u00103J\u0010\u0010F\u001a\u0002022\b\u0010\u001b\u001a\u0004\u0018\u00010\u0001R\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000b\u001a\u0004\u0018\u00010\f8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\rR\u0014\u0010\u000e\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u0004\u0018\u00010\u00118\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0015\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0016\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0017\u001a\u0004\u0018\u00010\f8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\rR\u0014\u0010\u0018\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0019\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u001a\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u0014\u0010\u001b\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006H"}, d2 = {"Lcom/netizen/medicom/model/WithDrawInfo$UserBankAccountInfoDTO;", "", "()V", "accEnableStatus", "", "Ljava/lang/Integer;", "bankAccHolderName", "", "bankAccNumber", "bankNote", "chequeSlipContent", "chequeSlipEditable", "", "Ljava/lang/Boolean;", "chequeSlipName", "chequeSlipPath", "coreBankBranchInfoDTO", "Lcom/netizen/medicom/model/WithDrawInfo$UserBankAccountInfoDTO$CoreBankBranchInfoDTO;", "coreCategoryInfoDTO", "lastDateExecuted", "lastIpExecuted", "lastUserExecuted", "othersAttachmentContent", "othersAttachmentEditable", "othersAttachmentName", "othersAttachmentPath", "userBankAccId", "userBasicInfoDTO", "getAccEnableStatus", "()Ljava/lang/Integer;", "getBankAccHolderName", "getBankAccNumber", "getBankNote", "getChequeSlipContent", "getChequeSlipEditable", "()Ljava/lang/Boolean;", "getChequeSlipName", "getChequeSlipPath", "getCoreBankBranchInfoDTO", "getCoreCategoryInfoDTO", "getLastDateExecuted", "getLastIpExecuted", "getLastUserExecuted", "getOthersAttachmentContent", "getOthersAttachmentEditable", "getOthersAttachmentName", "getOthersAttachmentPath", "getUserBankAccId", "getUserBasicInfoDTO", "setAccEnableStatus", "", "(Ljava/lang/Integer;)V", "setBankAccHolderName", "setBankAccNumber", "setBankNote", "setChequeSlipContent", "setChequeSlipEditable", "(Ljava/lang/Boolean;)V", "setChequeSlipName", "setChequeSlipPath", "setCoreBankBranchInfoDTO", "setCoreCategoryInfoDTO", "setLastDateExecuted", "setLastIpExecuted", "setLastUserExecuted", "setOthersAttachmentContent", "setOthersAttachmentEditable", "setOthersAttachmentName", "setOthersAttachmentPath", "setUserBankAccId", "setUserBasicInfoDTO", "CoreBankBranchInfoDTO", "app_debug"})
    public static final class UserBankAccountInfoDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "userBankAccId")
        private java.lang.Integer userBankAccId;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "bankAccNumber")
        private java.lang.String bankAccNumber;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "bankAccHolderName")
        private java.lang.String bankAccHolderName;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "bankNote")
        private java.lang.String bankNote;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "accEnableStatus")
        private java.lang.Integer accEnableStatus;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "chequeSlipName")
        private java.lang.String chequeSlipName;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "chequeSlipPath")
        private java.lang.String chequeSlipPath;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "othersAttachmentName")
        private java.lang.Object othersAttachmentName;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "othersAttachmentPath")
        private java.lang.Object othersAttachmentPath;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "chequeSlipEditable")
        private java.lang.Boolean chequeSlipEditable;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "othersAttachmentEditable")
        private java.lang.Boolean othersAttachmentEditable;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "chequeSlipContent")
        private java.lang.Object chequeSlipContent;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "othersAttachmentContent")
        private java.lang.Object othersAttachmentContent;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "coreCategoryInfoDTO")
        private java.lang.Object coreCategoryInfoDTO;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "userBasicInfoDTO")
        private java.lang.Object userBasicInfoDTO;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "coreBankBranchInfoDTO")
        private com.netizen.medicom.model.WithDrawInfo.UserBankAccountInfoDTO.CoreBankBranchInfoDTO coreBankBranchInfoDTO;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "lastUserExecuted")
        private java.lang.String lastUserExecuted;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "lastIpExecuted")
        private java.lang.String lastIpExecuted;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "lastDateExecuted")
        private java.lang.String lastDateExecuted;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getUserBankAccId() {
            return null;
        }
        
        public final void setUserBankAccId(@org.jetbrains.annotations.Nullable()
        java.lang.Integer userBankAccId) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getBankAccNumber() {
            return null;
        }
        
        public final void setBankAccNumber(@org.jetbrains.annotations.Nullable()
        java.lang.String bankAccNumber) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getBankAccHolderName() {
            return null;
        }
        
        public final void setBankAccHolderName(@org.jetbrains.annotations.Nullable()
        java.lang.String bankAccHolderName) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getBankNote() {
            return null;
        }
        
        public final void setBankNote(@org.jetbrains.annotations.Nullable()
        java.lang.String bankNote) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getAccEnableStatus() {
            return null;
        }
        
        public final void setAccEnableStatus(@org.jetbrains.annotations.Nullable()
        java.lang.Integer accEnableStatus) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getChequeSlipName() {
            return null;
        }
        
        public final void setChequeSlipName(@org.jetbrains.annotations.Nullable()
        java.lang.String chequeSlipName) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getChequeSlipPath() {
            return null;
        }
        
        public final void setChequeSlipPath(@org.jetbrains.annotations.Nullable()
        java.lang.String chequeSlipPath) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Object getOthersAttachmentName() {
            return null;
        }
        
        public final void setOthersAttachmentName(@org.jetbrains.annotations.Nullable()
        java.lang.Object othersAttachmentName) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Object getOthersAttachmentPath() {
            return null;
        }
        
        public final void setOthersAttachmentPath(@org.jetbrains.annotations.Nullable()
        java.lang.Object othersAttachmentPath) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Boolean getChequeSlipEditable() {
            return null;
        }
        
        public final void setChequeSlipEditable(@org.jetbrains.annotations.Nullable()
        java.lang.Boolean chequeSlipEditable) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Boolean getOthersAttachmentEditable() {
            return null;
        }
        
        public final void setOthersAttachmentEditable(@org.jetbrains.annotations.Nullable()
        java.lang.Boolean othersAttachmentEditable) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Object getChequeSlipContent() {
            return null;
        }
        
        public final void setChequeSlipContent(@org.jetbrains.annotations.Nullable()
        java.lang.Object chequeSlipContent) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Object getOthersAttachmentContent() {
            return null;
        }
        
        public final void setOthersAttachmentContent(@org.jetbrains.annotations.Nullable()
        java.lang.Object othersAttachmentContent) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Object getCoreCategoryInfoDTO() {
            return null;
        }
        
        public final void setCoreCategoryInfoDTO(@org.jetbrains.annotations.Nullable()
        java.lang.Object coreCategoryInfoDTO) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Object getUserBasicInfoDTO() {
            return null;
        }
        
        public final void setUserBasicInfoDTO(@org.jetbrains.annotations.Nullable()
        java.lang.Object userBasicInfoDTO) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final com.netizen.medicom.model.WithDrawInfo.UserBankAccountInfoDTO.CoreBankBranchInfoDTO getCoreBankBranchInfoDTO() {
            return null;
        }
        
        public final void setCoreBankBranchInfoDTO(@org.jetbrains.annotations.Nullable()
        com.netizen.medicom.model.WithDrawInfo.UserBankAccountInfoDTO.CoreBankBranchInfoDTO coreBankBranchInfoDTO) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getLastUserExecuted() {
            return null;
        }
        
        public final void setLastUserExecuted(@org.jetbrains.annotations.Nullable()
        java.lang.String lastUserExecuted) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getLastIpExecuted() {
            return null;
        }
        
        public final void setLastIpExecuted(@org.jetbrains.annotations.Nullable()
        java.lang.String lastIpExecuted) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getLastDateExecuted() {
            return null;
        }
        
        public final void setLastDateExecuted(@org.jetbrains.annotations.Nullable()
        java.lang.String lastDateExecuted) {
        }
        
        public UserBankAccountInfoDTO() {
            super();
        }
        
        @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0013\n\u0002\u0010\u0002\n\u0002\b\f\u0018\u00002\u00020\u0001:\u0001(B\u0005\u00a2\u0006\u0002\u0010\u0002J\r\u0010\u0011\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0012J\b\u0010\u0013\u001a\u0004\u0018\u00010\u0007J\b\u0010\u0014\u001a\u0004\u0018\u00010\tJ\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001J\b\u0010\u0016\u001a\u0004\u0018\u00010\u0007J\r\u0010\u0017\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0012J\b\u0010\u0018\u001a\u0004\u0018\u00010\u0007J\b\u0010\u0019\u001a\u0004\u0018\u00010\u0007J\b\u0010\u001a\u001a\u0004\u0018\u00010\u0007J\b\u0010\u001b\u001a\u0004\u0018\u00010\u0007J\u0015\u0010\u001c\u001a\u00020\u001d2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u001eJ\u0010\u0010\u001f\u001a\u00020\u001d2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007J\u0010\u0010 \u001a\u00020\u001d2\b\u0010\b\u001a\u0004\u0018\u00010\tJ\u0010\u0010!\u001a\u00020\u001d2\b\u0010\n\u001a\u0004\u0018\u00010\u0001J\u0010\u0010\"\u001a\u00020\u001d2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0007J\u0015\u0010#\u001a\u00020\u001d2\b\u0010\f\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u001eJ\u0010\u0010$\u001a\u00020\u001d2\b\u0010\r\u001a\u0004\u0018\u00010\u0007J\u0010\u0010%\u001a\u00020\u001d2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0007J\u0010\u0010&\u001a\u00020\u001d2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0007J\u0010\u0010\'\u001a\u00020\u001d2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0007R\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\u0004\u0018\u00010\t8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\f\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u0014\u0010\r\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006)"}, d2 = {"Lcom/netizen/medicom/model/WithDrawInfo$UserBankAccountInfoDTO$CoreBankBranchInfoDTO;", "", "()V", "branchID", "", "Ljava/lang/Integer;", "branchName", "", "coreBankInfoDTO", "Lcom/netizen/medicom/model/WithDrawInfo$UserBankAccountInfoDTO$CoreBankBranchInfoDTO$CoreBankInfoDTO;", "coreDistrictInfoDTO", "detailsNote", "enableStatus", "lastDateExecuted", "lastIpExecuted", "lastUserExecuted", "routingNumber", "getBranchID", "()Ljava/lang/Integer;", "getBranchName", "getCoreBankInfoDTO", "getCoreDistrictInfoDTO", "getDetailsNote", "getEnableStatus", "getLastDateExecuted", "getLastIpExecuted", "getLastUserExecuted", "getRoutingNumber", "setBranchID", "", "(Ljava/lang/Integer;)V", "setBranchName", "setCoreBankInfoDTO", "setCoreDistrictInfoDTO", "setDetailsNote", "setEnableStatus", "setLastDateExecuted", "setLastIpExecuted", "setLastUserExecuted", "setRoutingNumber", "CoreBankInfoDTO", "app_debug"})
        public static final class CoreBankBranchInfoDTO {
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "branchID")
            private java.lang.Integer branchID;
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "branchName")
            private java.lang.String branchName;
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "routingNumber")
            private java.lang.String routingNumber;
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "detailsNote")
            private java.lang.String detailsNote;
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "enableStatus")
            private java.lang.Integer enableStatus;
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "coreBankInfoDTO")
            private com.netizen.medicom.model.WithDrawInfo.UserBankAccountInfoDTO.CoreBankBranchInfoDTO.CoreBankInfoDTO coreBankInfoDTO;
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "coreDistrictInfoDTO")
            private java.lang.Object coreDistrictInfoDTO;
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "lastUserExecuted")
            private java.lang.String lastUserExecuted;
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "lastIpExecuted")
            private java.lang.String lastIpExecuted;
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "lastDateExecuted")
            private java.lang.String lastDateExecuted;
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.Integer getBranchID() {
                return null;
            }
            
            public final void setBranchID(@org.jetbrains.annotations.Nullable()
            java.lang.Integer branchID) {
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.String getBranchName() {
                return null;
            }
            
            public final void setBranchName(@org.jetbrains.annotations.Nullable()
            java.lang.String branchName) {
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.String getRoutingNumber() {
                return null;
            }
            
            public final void setRoutingNumber(@org.jetbrains.annotations.Nullable()
            java.lang.String routingNumber) {
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.String getDetailsNote() {
                return null;
            }
            
            public final void setDetailsNote(@org.jetbrains.annotations.Nullable()
            java.lang.String detailsNote) {
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.Integer getEnableStatus() {
                return null;
            }
            
            public final void setEnableStatus(@org.jetbrains.annotations.Nullable()
            java.lang.Integer enableStatus) {
            }
            
            @org.jetbrains.annotations.Nullable()
            public final com.netizen.medicom.model.WithDrawInfo.UserBankAccountInfoDTO.CoreBankBranchInfoDTO.CoreBankInfoDTO getCoreBankInfoDTO() {
                return null;
            }
            
            public final void setCoreBankInfoDTO(@org.jetbrains.annotations.Nullable()
            com.netizen.medicom.model.WithDrawInfo.UserBankAccountInfoDTO.CoreBankBranchInfoDTO.CoreBankInfoDTO coreBankInfoDTO) {
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.Object getCoreDistrictInfoDTO() {
                return null;
            }
            
            public final void setCoreDistrictInfoDTO(@org.jetbrains.annotations.Nullable()
            java.lang.Object coreDistrictInfoDTO) {
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.String getLastUserExecuted() {
                return null;
            }
            
            public final void setLastUserExecuted(@org.jetbrains.annotations.Nullable()
            java.lang.String lastUserExecuted) {
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.String getLastIpExecuted() {
                return null;
            }
            
            public final void setLastIpExecuted(@org.jetbrains.annotations.Nullable()
            java.lang.String lastIpExecuted) {
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.String getLastDateExecuted() {
                return null;
            }
            
            public final void setLastDateExecuted(@org.jetbrains.annotations.Nullable()
            java.lang.String lastDateExecuted) {
            }
            
            public CoreBankBranchInfoDTO() {
                super();
            }
            
            @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u001b\n\u0002\u0010\u0002\n\u0002\b\u000e\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0013\u001a\u0004\u0018\u00010\u0004J\r\u0010\u0014\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0015J\b\u0010\u0016\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0017\u001a\u0004\u0018\u00010\u0004J\r\u0010\u0018\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0015J\r\u0010\u0019\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0015J\b\u0010\u001a\u001a\u0004\u0018\u00010\u0004J\b\u0010\u001b\u001a\u0004\u0018\u00010\u0004J\b\u0010\u001c\u001a\u0004\u0018\u00010\u0004J\b\u0010\u001d\u001a\u0004\u0018\u00010\u0001J\r\u0010\u001e\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0015J\b\u0010\u001f\u001a\u0004\u0018\u00010\u0001J\r\u0010 \u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0015J\u0010\u0010!\u001a\u00020\"2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0015\u0010#\u001a\u00020\"2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010$J\u0010\u0010%\u001a\u00020\"2\b\u0010\b\u001a\u0004\u0018\u00010\u0004J\u0010\u0010&\u001a\u00020\"2\b\u0010\t\u001a\u0004\u0018\u00010\u0004J\u0015\u0010\'\u001a\u00020\"2\b\u0010\n\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010$J\u0015\u0010(\u001a\u00020\"2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010$J\u0010\u0010)\u001a\u00020\"2\b\u0010\f\u001a\u0004\u0018\u00010\u0004J\u0010\u0010*\u001a\u00020\"2\b\u0010\r\u001a\u0004\u0018\u00010\u0004J\u0010\u0010+\u001a\u00020\"2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0004J\u0010\u0010,\u001a\u00020\"2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0001J\u0015\u0010-\u001a\u00020\"2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010$J\u0010\u0010.\u001a\u00020\"2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001J\u0015\u0010/\u001a\u00020\"2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010$R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0014\u0010\b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\n\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0016\u0010\u000b\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0014\u0010\f\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0010\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0014\u0010\u0011\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0012\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007\u00a8\u00060"}, d2 = {"Lcom/netizen/medicom/model/WithDrawInfo$UserBankAccountInfoDTO$CoreBankBranchInfoDTO$CoreBankInfoDTO;", "", "()V", "categoryDefaultCode", "", "categoryEnableStatus", "", "Ljava/lang/Integer;", "categoryName", "categoryNote", "categorySerial", "coreCategoryID", "lastDateExecuted", "lastIpExecuted", "lastUserExecuted", "parentCoreCategoryInfoDTO", "parentStatus", "parentTypeInfoDTO", "typeStatus", "getCategoryDefaultCode", "getCategoryEnableStatus", "()Ljava/lang/Integer;", "getCategoryName", "getCategoryNote", "getCategorySerial", "getCoreCategoryID", "getLastDateExecuted", "getLastIpExecuted", "getLastUserExecuted", "getParentCoreCategoryInfoDTO", "getParentStatus", "getParentTypeInfoDTO", "getTypeStatus", "setCategoryDefaultCode", "", "setCategoryEnableStatus", "(Ljava/lang/Integer;)V", "setCategoryName", "setCategoryNote", "setCategorySerial", "setCoreCategoryID", "setLastDateExecuted", "setLastIpExecuted", "setLastUserExecuted", "setParentCoreCategoryInfoDTO", "setParentStatus", "setParentTypeInfoDTO", "setTypeStatus", "app_debug"})
            public static final class CoreBankInfoDTO {
                @com.google.gson.annotations.Expose()
                @com.google.gson.annotations.SerializedName(value = "coreCategoryID")
                private java.lang.Integer coreCategoryID;
                @com.google.gson.annotations.Expose()
                @com.google.gson.annotations.SerializedName(value = "categoryDefaultCode")
                private java.lang.String categoryDefaultCode;
                @com.google.gson.annotations.Expose()
                @com.google.gson.annotations.SerializedName(value = "categoryName")
                private java.lang.String categoryName;
                @com.google.gson.annotations.Expose()
                @com.google.gson.annotations.SerializedName(value = "categoryNote")
                private java.lang.String categoryNote;
                @com.google.gson.annotations.Expose()
                @com.google.gson.annotations.SerializedName(value = "categoryEnableStatus")
                private java.lang.Integer categoryEnableStatus;
                @com.google.gson.annotations.Expose()
                @com.google.gson.annotations.SerializedName(value = "categorySerial")
                private java.lang.Integer categorySerial;
                @com.google.gson.annotations.Expose()
                @com.google.gson.annotations.SerializedName(value = "typeStatus")
                private java.lang.Integer typeStatus;
                @com.google.gson.annotations.Expose()
                @com.google.gson.annotations.SerializedName(value = "parentStatus")
                private java.lang.Integer parentStatus;
                @com.google.gson.annotations.Expose()
                @com.google.gson.annotations.SerializedName(value = "parentTypeInfoDTO")
                private java.lang.Object parentTypeInfoDTO;
                @com.google.gson.annotations.Expose()
                @com.google.gson.annotations.SerializedName(value = "parentCoreCategoryInfoDTO")
                private java.lang.Object parentCoreCategoryInfoDTO;
                @com.google.gson.annotations.Expose()
                @com.google.gson.annotations.SerializedName(value = "lastUserExecuted")
                private java.lang.String lastUserExecuted;
                @com.google.gson.annotations.Expose()
                @com.google.gson.annotations.SerializedName(value = "lastIpExecuted")
                private java.lang.String lastIpExecuted;
                @com.google.gson.annotations.Expose()
                @com.google.gson.annotations.SerializedName(value = "lastDateExecuted")
                private java.lang.String lastDateExecuted;
                
                @org.jetbrains.annotations.Nullable()
                public final java.lang.Integer getCoreCategoryID() {
                    return null;
                }
                
                public final void setCoreCategoryID(@org.jetbrains.annotations.Nullable()
                java.lang.Integer coreCategoryID) {
                }
                
                @org.jetbrains.annotations.Nullable()
                public final java.lang.String getCategoryDefaultCode() {
                    return null;
                }
                
                public final void setCategoryDefaultCode(@org.jetbrains.annotations.Nullable()
                java.lang.String categoryDefaultCode) {
                }
                
                @org.jetbrains.annotations.Nullable()
                public final java.lang.String getCategoryName() {
                    return null;
                }
                
                public final void setCategoryName(@org.jetbrains.annotations.Nullable()
                java.lang.String categoryName) {
                }
                
                @org.jetbrains.annotations.Nullable()
                public final java.lang.String getCategoryNote() {
                    return null;
                }
                
                public final void setCategoryNote(@org.jetbrains.annotations.Nullable()
                java.lang.String categoryNote) {
                }
                
                @org.jetbrains.annotations.Nullable()
                public final java.lang.Integer getCategoryEnableStatus() {
                    return null;
                }
                
                public final void setCategoryEnableStatus(@org.jetbrains.annotations.Nullable()
                java.lang.Integer categoryEnableStatus) {
                }
                
                @org.jetbrains.annotations.Nullable()
                public final java.lang.Integer getCategorySerial() {
                    return null;
                }
                
                public final void setCategorySerial(@org.jetbrains.annotations.Nullable()
                java.lang.Integer categorySerial) {
                }
                
                @org.jetbrains.annotations.Nullable()
                public final java.lang.Integer getTypeStatus() {
                    return null;
                }
                
                public final void setTypeStatus(@org.jetbrains.annotations.Nullable()
                java.lang.Integer typeStatus) {
                }
                
                @org.jetbrains.annotations.Nullable()
                public final java.lang.Integer getParentStatus() {
                    return null;
                }
                
                public final void setParentStatus(@org.jetbrains.annotations.Nullable()
                java.lang.Integer parentStatus) {
                }
                
                @org.jetbrains.annotations.Nullable()
                public final java.lang.Object getParentTypeInfoDTO() {
                    return null;
                }
                
                public final void setParentTypeInfoDTO(@org.jetbrains.annotations.Nullable()
                java.lang.Object parentTypeInfoDTO) {
                }
                
                @org.jetbrains.annotations.Nullable()
                public final java.lang.Object getParentCoreCategoryInfoDTO() {
                    return null;
                }
                
                public final void setParentCoreCategoryInfoDTO(@org.jetbrains.annotations.Nullable()
                java.lang.Object parentCoreCategoryInfoDTO) {
                }
                
                @org.jetbrains.annotations.Nullable()
                public final java.lang.String getLastUserExecuted() {
                    return null;
                }
                
                public final void setLastUserExecuted(@org.jetbrains.annotations.Nullable()
                java.lang.String lastUserExecuted) {
                }
                
                @org.jetbrains.annotations.Nullable()
                public final java.lang.String getLastIpExecuted() {
                    return null;
                }
                
                public final void setLastIpExecuted(@org.jetbrains.annotations.Nullable()
                java.lang.String lastIpExecuted) {
                }
                
                @org.jetbrains.annotations.Nullable()
                public final java.lang.String getLastDateExecuted() {
                    return null;
                }
                
                public final void setLastDateExecuted(@org.jetbrains.annotations.Nullable()
                java.lang.String lastDateExecuted) {
                }
                
                public CoreBankInfoDTO() {
                    super();
                }
            }
        }
    }
}