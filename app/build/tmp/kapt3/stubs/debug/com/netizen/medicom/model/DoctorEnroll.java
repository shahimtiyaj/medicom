package com.netizen.medicom.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0015\n\u0002\u0010\u0002\n\u0002\b\n\u0018\u00002\u00020\u0001B\u0007\b\u0016\u00a2\u0006\u0002\u0010\u0002Bk\b\u0016\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\f\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\r\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u000eJ\b\u0010\u000f\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0010\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0011\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0012\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0013\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0014\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0015\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0016\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0017\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0018\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0019\u001a\u00020\u001a2\b\u0010\b\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u001b\u001a\u00020\u001a2\b\u0010\n\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u001c\u001a\u00020\u001a2\b\u0010\t\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u001d\u001a\u00020\u001a2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u001e\u001a\u00020\u001a2\b\u0010\u0007\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u001f\u001a\u00020\u001a2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0004J\u0010\u0010 \u001a\u00020\u001a2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u0010!\u001a\u00020\u001a2\b\u0010\r\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\"\u001a\u00020\u001a2\b\u0010\f\u001a\u0004\u0018\u00010\u0004J\u0010\u0010#\u001a\u00020\u001a2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0004R\u0014\u0010\b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006$"}, d2 = {"Lcom/netizen/medicom/model/DoctorEnroll;", "", "()V", "parterReferenceId", "", "designationId", "specialistId", "instituteName", "bmdcNumber", "description", "degreeInfo", "newPatientFeesAmount", "returnPatientFeesAmount", "reportPatientFeesAmount", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getBmdcNumber", "getDegreeInfo", "getDescription", "getDesignationId", "getInstituteName", "getNewPatientFeesAmount", "getParterReferenceId", "getReportPatientFeesAmount", "getReturnPatientFeesAmount", "getSpecialistId", "setBmdcNumber", "", "setDegreeInfo", "setDescription", "setDesignationId", "setInstituteName", "setNewPatientFeesAmount", "setParterReferenceId", "setReportPatientFeesAmount", "setReturnPatientFeesAmount", "setSpecialistId", "app_debug"})
public final class DoctorEnroll {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "parterReferenceId")
    private java.lang.String parterReferenceId;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "designationId")
    private java.lang.String designationId;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "specialistId")
    private java.lang.String specialistId;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "instituteName")
    private java.lang.String instituteName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "bmdcNumber")
    private java.lang.String bmdcNumber;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "description")
    private java.lang.String description;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "degreeInfo")
    private java.lang.String degreeInfo;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "newPatientFeesAmount")
    private java.lang.String newPatientFeesAmount;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "returnPatientFeesAmount")
    private java.lang.String returnPatientFeesAmount;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "reportPatientFeesAmount")
    private java.lang.String reportPatientFeesAmount;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getParterReferenceId() {
        return null;
    }
    
    public final void setParterReferenceId(@org.jetbrains.annotations.Nullable()
    java.lang.String parterReferenceId) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDesignationId() {
        return null;
    }
    
    public final void setDesignationId(@org.jetbrains.annotations.Nullable()
    java.lang.String designationId) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSpecialistId() {
        return null;
    }
    
    public final void setSpecialistId(@org.jetbrains.annotations.Nullable()
    java.lang.String specialistId) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getInstituteName() {
        return null;
    }
    
    public final void setInstituteName(@org.jetbrains.annotations.Nullable()
    java.lang.String instituteName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getBmdcNumber() {
        return null;
    }
    
    public final void setBmdcNumber(@org.jetbrains.annotations.Nullable()
    java.lang.String bmdcNumber) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDescription() {
        return null;
    }
    
    public final void setDescription(@org.jetbrains.annotations.Nullable()
    java.lang.String description) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDegreeInfo() {
        return null;
    }
    
    public final void setDegreeInfo(@org.jetbrains.annotations.Nullable()
    java.lang.String degreeInfo) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getNewPatientFeesAmount() {
        return null;
    }
    
    public final void setNewPatientFeesAmount(@org.jetbrains.annotations.Nullable()
    java.lang.String newPatientFeesAmount) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getReturnPatientFeesAmount() {
        return null;
    }
    
    public final void setReturnPatientFeesAmount(@org.jetbrains.annotations.Nullable()
    java.lang.String returnPatientFeesAmount) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getReportPatientFeesAmount() {
        return null;
    }
    
    public final void setReportPatientFeesAmount(@org.jetbrains.annotations.Nullable()
    java.lang.String reportPatientFeesAmount) {
    }
    
    public DoctorEnroll() {
        super();
    }
    
    public DoctorEnroll(@org.jetbrains.annotations.Nullable()
    java.lang.String parterReferenceId, @org.jetbrains.annotations.Nullable()
    java.lang.String designationId, @org.jetbrains.annotations.Nullable()
    java.lang.String specialistId, @org.jetbrains.annotations.Nullable()
    java.lang.String instituteName, @org.jetbrains.annotations.Nullable()
    java.lang.String bmdcNumber, @org.jetbrains.annotations.Nullable()
    java.lang.String description, @org.jetbrains.annotations.Nullable()
    java.lang.String degreeInfo, @org.jetbrains.annotations.Nullable()
    java.lang.String newPatientFeesAmount, @org.jetbrains.annotations.Nullable()
    java.lang.String returnPatientFeesAmount, @org.jetbrains.annotations.Nullable()
    java.lang.String reportPatientFeesAmount) {
        super();
    }
}