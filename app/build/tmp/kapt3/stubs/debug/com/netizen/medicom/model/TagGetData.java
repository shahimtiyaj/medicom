package com.netizen.medicom.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001:\u0002\f\rB\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0007\u001a\u0004\u0018\u00010\u0004J\b\u0010\b\u001a\u0004\u0018\u00010\u0006J\u0010\u0010\t\u001a\u00020\n2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u000b\u001a\u00020\n2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"}, d2 = {"Lcom/netizen/medicom/model/TagGetData;", "", "()V", "taggingTypeCoreCategoryInfoDTO", "Lcom/netizen/medicom/model/TagGetData$TaggingTypeCoreCategoryInfoDTO;", "userBankAccountInfoDTO", "Lcom/netizen/medicom/model/TagGetData$UserBankAccountInfoDTO;", "getTaggingTypeCoreCategoryInfoDTO", "getUserBankAccountInfoDTO", "setTaggingTypeCoreCategoryInfoDTO", "", "setUserBankAccountInfoDTO", "TaggingTypeCoreCategoryInfoDTO", "UserBankAccountInfoDTO", "app_debug"})
public final class TagGetData {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "taggingTypeCoreCategoryInfoDTO")
    private com.netizen.medicom.model.TagGetData.TaggingTypeCoreCategoryInfoDTO taggingTypeCoreCategoryInfoDTO;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "userBankAccountInfoDTO")
    private com.netizen.medicom.model.TagGetData.UserBankAccountInfoDTO userBankAccountInfoDTO;
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.medicom.model.TagGetData.TaggingTypeCoreCategoryInfoDTO getTaggingTypeCoreCategoryInfoDTO() {
        return null;
    }
    
    public final void setTaggingTypeCoreCategoryInfoDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.medicom.model.TagGetData.TaggingTypeCoreCategoryInfoDTO taggingTypeCoreCategoryInfoDTO) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.medicom.model.TagGetData.UserBankAccountInfoDTO getUserBankAccountInfoDTO() {
        return null;
    }
    
    public final void setUserBankAccountInfoDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.medicom.model.TagGetData.UserBankAccountInfoDTO userBankAccountInfoDTO) {
    }
    
    public TagGetData() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\t\u001a\u0004\u0018\u00010\u0004J\b\u0010\n\u001a\u0004\u0018\u00010\u0004J\r\u0010\u000b\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\fJ\u0010\u0010\r\u001a\u00020\u000e2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u000f\u001a\u00020\u000e2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0015\u0010\u0010\u001a\u00020\u000e2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0011R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\b\u00a8\u0006\u0012"}, d2 = {"Lcom/netizen/medicom/model/TagGetData$TaggingTypeCoreCategoryInfoDTO;", "", "()V", "categoryDefaultCode", "", "categoryName", "coreCategoryID", "", "Ljava/lang/Integer;", "getCategoryDefaultCode", "getCategoryName", "getCoreCategoryID", "()Ljava/lang/Integer;", "setCategoryDefaultCode", "", "setCategoryName", "setCoreCategoryID", "(Ljava/lang/Integer;)V", "app_debug"})
    public static final class TaggingTypeCoreCategoryInfoDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "coreCategoryID")
        private java.lang.Integer coreCategoryID;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "categoryDefaultCode")
        private java.lang.String categoryDefaultCode;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "categoryName")
        private java.lang.String categoryName;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getCoreCategoryID() {
            return null;
        }
        
        public final void setCoreCategoryID(@org.jetbrains.annotations.Nullable()
        java.lang.Integer coreCategoryID) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCategoryDefaultCode() {
            return null;
        }
        
        public final void setCategoryDefaultCode(@org.jetbrains.annotations.Nullable()
        java.lang.String categoryDefaultCode) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCategoryName() {
            return null;
        }
        
        public final void setCategoryName(@org.jetbrains.annotations.Nullable()
        java.lang.String categoryName) {
        }
        
        public TaggingTypeCoreCategoryInfoDTO() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\t\u001a\u0004\u0018\u00010\u0004J\b\u0010\n\u001a\u0004\u0018\u00010\u0004J\r\u0010\u000b\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\fJ\u0010\u0010\r\u001a\u00020\u000e2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u000f\u001a\u00020\u000e2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0015\u0010\u0010\u001a\u00020\u000e2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0011R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\b\u00a8\u0006\u0012"}, d2 = {"Lcom/netizen/medicom/model/TagGetData$UserBankAccountInfoDTO;", "", "()V", "bankAccHolderName", "", "bankAccNumber", "userBankAccId", "", "Ljava/lang/Integer;", "getBankAccHolderName", "getBankAccNumber", "getUserBankAccId", "()Ljava/lang/Integer;", "setBankAccHolderName", "", "setBankAccNumber", "setUserBankAccId", "(Ljava/lang/Integer;)V", "app_debug"})
    public static final class UserBankAccountInfoDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "userBankAccId")
        private java.lang.Integer userBankAccId;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "bankAccNumber")
        private java.lang.String bankAccNumber;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "bankAccHolderName")
        private java.lang.String bankAccHolderName;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getUserBankAccId() {
            return null;
        }
        
        public final void setUserBankAccId(@org.jetbrains.annotations.Nullable()
        java.lang.Integer userBankAccId) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getBankAccNumber() {
            return null;
        }
        
        public final void setBankAccNumber(@org.jetbrains.annotations.Nullable()
        java.lang.String bankAccNumber) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getBankAccHolderName() {
            return null;
        }
        
        public final void setBankAccHolderName(@org.jetbrains.annotations.Nullable()
        java.lang.String bankAccHolderName) {
        }
        
        public UserBankAccountInfoDTO() {
            super();
        }
    }
}