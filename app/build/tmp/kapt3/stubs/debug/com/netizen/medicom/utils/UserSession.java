package com.netizen.medicom.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u000f\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u000f\u001a\u00020\u0010J\b\u0010\u0011\u001a\u0004\u0018\u00010\u0006J\b\u0010\u0012\u001a\u0004\u0018\u00010\u0006J\u0006\u0010\u0013\u001a\u00020\u0014J\u0006\u0010\u0015\u001a\u00020\u0010J\u0006\u0010\u0016\u001a\u00020\u0010J\u0010\u0010\u0017\u001a\u00020\u00102\b\u0010\u0018\u001a\u0004\u0018\u00010\u0006J\u0010\u0010\u0019\u001a\u00020\u00102\b\u0010\u001a\u001a\u0004\u0018\u00010\u0006R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u0004\u0018\u00010\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"}, d2 = {"Lcom/netizen/medicom/utils/UserSession;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "IS_USER_LOGIN", "", "KEY_USER_NAME", "KEY_USER_PASS", "PREFERENCE_NAME", "editor", "Landroid/content/SharedPreferences$Editor;", "mContext", "mPreferences", "Landroid/content/SharedPreferences;", "createUserLogInSession", "", "getUserName", "getUserPass", "isUserLoggedIn", "", "logOutUser", "removeUserLogInSession", "setUserCredentials", "userName", "setUserPassCredentials", "userPass", "app_debug"})
public final class UserSession {
    private final java.lang.String PREFERENCE_NAME = "user_session";
    private final java.lang.String IS_USER_LOGIN = "is_user_logged_in";
    private final java.lang.String KEY_USER_NAME = "user_name";
    private final java.lang.String KEY_USER_PASS = "user_pass";
    private android.content.Context mContext;
    private android.content.SharedPreferences mPreferences;
    private android.content.SharedPreferences.Editor editor;
    
    public final void setUserCredentials(@org.jetbrains.annotations.Nullable()
    java.lang.String userName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getUserName() {
        return null;
    }
    
    public final void setUserPassCredentials(@org.jetbrains.annotations.Nullable()
    java.lang.String userPass) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getUserPass() {
        return null;
    }
    
    public final void removeUserLogInSession() {
    }
    
    public final void createUserLogInSession() {
    }
    
    public final boolean isUserLoggedIn() {
        return false;
    }
    
    /**
     * Clear session details
     */
    public final void logOutUser() {
    }
    
    public UserSession(@org.jetbrains.annotations.Nullable()
    android.content.Context context) {
        super();
    }
}