package com.netizen.medicom.quickblox.util;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u000e\n\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0002"}, d2 = {"PING_ALARM_ACTION", "", "app_debug"})
public final class ChatPingAlarmManagerKt {
    private static final java.lang.String PING_ALARM_ACTION = "com.quickblox.chat.ping.ACTION";
}