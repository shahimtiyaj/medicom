package com.netizen.medicom.quickblox.fragments;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0090\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0000\n\u0002\u0010\u000e\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\b&\u0018\u0000 ]2\u00020\u00012\u00020\u0002:\u0001]B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010/\u001a\u0002002\u0006\u00101\u001a\u00020\u0019H\u0014J\b\u00102\u001a\u000200H\u0002J\b\u00103\u001a\u000200H$J\b\u00104\u001a\u000200H$J\b\u00105\u001a\u000200H$J\u0017\u00106\u001a\u00020\u00112\b\u00107\u001a\u0004\u0018\u000108H\u0002\u00a2\u0006\u0002\u00109J\u0010\u0010:\u001a\u0004\u0018\u00010;2\u0006\u00107\u001a\u000208J*\u0010<\u001a\b\u0012\u0004\u0012\u00020\u00110\u001e2\f\u0010=\u001a\b\u0012\u0004\u0012\u00020\u00110\u001e2\f\u0010>\u001a\b\u0012\u0004\u0012\u0002080?H\u0002J\u0016\u0010@\u001a\u00020A2\f\u0010B\u001a\b\u0012\u0004\u0012\u00020\u00110\u001eH\u0002J\b\u0010C\u001a\u000200H\u0002J\b\u0010D\u001a\u000200H\u0002J\b\u0010E\u001a\u000200H\u0014J\b\u0010F\u001a\u000200H\u0014J\b\u0010G\u001a\u000200H\u0002J\u0012\u0010H\u001a\u0002002\b\u0010I\u001a\u0004\u0018\u00010$H\u0014J\u0010\u0010J\u001a\u0002002\u0006\u0010K\u001a\u00020LH\u0016J\b\u0010M\u001a\u000200H\u0016J\b\u0010N\u001a\u000200H\u0016J\u0012\u0010O\u001a\u0002002\b\u0010P\u001a\u0004\u0018\u00010QH\u0016J&\u0010R\u001a\u0004\u0018\u00010$2\u0006\u0010S\u001a\u00020T2\b\u0010U\u001a\u0004\u0018\u00010V2\b\u0010P\u001a\u0004\u0018\u00010QH\u0016J\b\u0010W\u001a\u000200H\u0016J\u0016\u0010X\u001a\u0002002\f\u0010Y\u001a\b\u0012\u0004\u0012\u00020\u00110\u001eH\u0016J\b\u0010Z\u001a\u000200H\u0016J\b\u0010[\u001a\u000200H\u0002J\b\u0010\\\u001a\u000200H\u0002R\u001a\u0010\u0004\u001a\u00020\u0005X\u0084.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR\u001c\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR\u001a\u0010\u0010\u001a\u00020\u0011X\u0084.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0019X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082.\u00a2\u0006\u0002\n\u0000R \u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00110\u001eX\u0084.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010 \"\u0004\b!\u0010\"R\u001a\u0010#\u001a\u00020$X\u0084.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b%\u0010&\"\u0004\b\'\u0010(R\u001a\u0010)\u001a\u00020\u0005X\u0084.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b*\u0010\u0007\"\u0004\b+\u0010\tR\u001a\u0010,\u001a\u00020\u0005X\u0084.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b-\u0010\u0007\"\u0004\b.\u0010\t\u00a8\u0006^"}, d2 = {"Lcom/netizen/medicom/quickblox/fragments/BaseConversationFragment;", "Lcom/netizen/medicom/quickblox/fragments/BaseToolBarFragment;", "Lcom/netizen/medicom/quickblox/activities/CallActivity$CurrentCallStateCallback;", "()V", "allOpponentsTextView", "Landroid/widget/TextView;", "getAllOpponentsTextView", "()Landroid/widget/TextView;", "setAllOpponentsTextView", "(Landroid/widget/TextView;)V", "conversationFragmentCallback", "Lcom/netizen/medicom/quickblox/fragments/ConversationFragmentCallback;", "getConversationFragmentCallback", "()Lcom/netizen/medicom/quickblox/fragments/ConversationFragmentCallback;", "setConversationFragmentCallback", "(Lcom/netizen/medicom/quickblox/fragments/ConversationFragmentCallback;)V", "currentUser", "Lcom/quickblox/users/model/QBUser;", "getCurrentUser", "()Lcom/quickblox/users/model/QBUser;", "setCurrentUser", "(Lcom/quickblox/users/model/QBUser;)V", "handUpVideoCall", "Landroid/widget/ImageButton;", "isIncomingCall", "", "isStarted", "micToggleVideoCall", "Landroid/widget/ToggleButton;", "opponents", "Ljava/util/ArrayList;", "getOpponents", "()Ljava/util/ArrayList;", "setOpponents", "(Ljava/util/ArrayList;)V", "outgoingOpponentsRelativeLayout", "Landroid/view/View;", "getOutgoingOpponentsRelativeLayout", "()Landroid/view/View;", "setOutgoingOpponentsRelativeLayout", "(Landroid/view/View;)V", "ringingTextView", "getRingingTextView", "setRingingTextView", "timerCallText", "getTimerCallText", "setTimerCallText", "actionButtonsEnabled", "", "inability", "clearButtonsState", "configureActionBar", "configureOutgoingScreen", "configureToolbar", "createStubUserById", "userId", "", "(Ljava/lang/Integer;)Lcom/quickblox/users/model/QBUser;", "getConnectionState", "Lcom/quickblox/videochat/webrtc/QBRTCTypes$QBRTCConnectionState;", "getListAllUsersFromIds", "existedUsers", "allIds", "", "getUserNamesFromUsersFullNames", "", "allUsers", "hideOutgoingScreen", "initActionBar", "initButtonsListener", "initFields", "initOpponentsList", "initViews", "view", "onAttach", "activity", "Landroid/app/Activity;", "onCallStarted", "onCallStopped", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onDestroy", "onOpponentsListUpdated", "newUsers", "onStart", "prepareAndShowOutgoingScreen", "startTimer", "Companion", "app_debug"})
public abstract class BaseConversationFragment extends com.netizen.medicom.quickblox.fragments.BaseToolBarFragment implements com.netizen.medicom.quickblox.activities.CallActivity.CurrentCallStateCallback {
    private boolean isIncomingCall = false;
    @org.jetbrains.annotations.NotNull()
    protected android.widget.TextView timerCallText;
    @org.jetbrains.annotations.Nullable()
    private com.netizen.medicom.quickblox.fragments.ConversationFragmentCallback conversationFragmentCallback;
    @org.jetbrains.annotations.NotNull()
    protected com.quickblox.users.model.QBUser currentUser;
    @org.jetbrains.annotations.NotNull()
    protected java.util.ArrayList<com.quickblox.users.model.QBUser> opponents;
    private boolean isStarted = false;
    private android.widget.ToggleButton micToggleVideoCall;
    private android.widget.ImageButton handUpVideoCall;
    @org.jetbrains.annotations.NotNull()
    protected android.view.View outgoingOpponentsRelativeLayout;
    @org.jetbrains.annotations.NotNull()
    protected android.widget.TextView allOpponentsTextView;
    @org.jetbrains.annotations.NotNull()
    protected android.widget.TextView ringingTextView;
    public static final com.netizen.medicom.quickblox.fragments.BaseConversationFragment.Companion Companion = null;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    protected final android.widget.TextView getTimerCallText() {
        return null;
    }
    
    protected final void setTimerCallText(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    protected final com.netizen.medicom.quickblox.fragments.ConversationFragmentCallback getConversationFragmentCallback() {
        return null;
    }
    
    protected final void setConversationFragmentCallback(@org.jetbrains.annotations.Nullable()
    com.netizen.medicom.quickblox.fragments.ConversationFragmentCallback p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final com.quickblox.users.model.QBUser getCurrentUser() {
        return null;
    }
    
    protected final void setCurrentUser(@org.jetbrains.annotations.NotNull()
    com.quickblox.users.model.QBUser p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final java.util.ArrayList<com.quickblox.users.model.QBUser> getOpponents() {
        return null;
    }
    
    protected final void setOpponents(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.quickblox.users.model.QBUser> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final android.view.View getOutgoingOpponentsRelativeLayout() {
        return null;
    }
    
    protected final void setOutgoingOpponentsRelativeLayout(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final android.widget.TextView getAllOpponentsTextView() {
        return null;
    }
    
    protected final void setAllOpponentsTextView(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final android.widget.TextView getRingingTextView() {
        return null;
    }
    
    protected final void setRingingTextView(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @java.lang.Override()
    public void onAttach(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity) {
    }
    
    @java.lang.Override()
    public void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    private final void initActionBar() {
    }
    
    protected abstract void configureActionBar();
    
    protected abstract void configureToolbar();
    
    private final void prepareAndShowOutgoingScreen() {
    }
    
    private final java.lang.String getUserNamesFromUsersFullNames(java.util.ArrayList<com.quickblox.users.model.QBUser> allUsers) {
        return null;
    }
    
    protected abstract void configureOutgoingScreen();
    
    protected void initFields() {
    }
    
    @java.lang.Override()
    public void onStart() {
    }
    
    @java.lang.Override()
    public void onDestroy() {
    }
    
    protected void initViews(@org.jetbrains.annotations.Nullable()
    android.view.View view) {
    }
    
    protected void initButtonsListener() {
    }
    
    private final void clearButtonsState() {
    }
    
    protected void actionButtonsEnabled(boolean inability) {
    }
    
    private final void startTimer() {
    }
    
    private final void hideOutgoingScreen() {
    }
    
    @java.lang.Override()
    public void onCallStarted() {
    }
    
    @java.lang.Override()
    public void onCallStopped() {
    }
    
    @java.lang.Override()
    public void onOpponentsListUpdated(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.quickblox.users.model.QBUser> newUsers) {
    }
    
    private final void initOpponentsList() {
    }
    
    private final java.util.ArrayList<com.quickblox.users.model.QBUser> getListAllUsersFromIds(java.util.ArrayList<com.quickblox.users.model.QBUser> existedUsers, java.util.List<java.lang.Integer> allIds) {
        return null;
    }
    
    private final com.quickblox.users.model.QBUser createStubUserById(java.lang.Integer userId) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.quickblox.videochat.webrtc.QBRTCTypes.QBRTCConnectionState getConnectionState(int userId) {
        return null;
    }
    
    public BaseConversationFragment() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0007\u00a8\u0006\b"}, d2 = {"Lcom/netizen/medicom/quickblox/fragments/BaseConversationFragment$Companion;", "", "()V", "newInstance", "Lcom/netizen/medicom/quickblox/fragments/BaseConversationFragment;", "baseConversationFragment", "isIncomingCall", "", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final com.netizen.medicom.quickblox.fragments.BaseConversationFragment newInstance(@org.jetbrains.annotations.NotNull()
        com.netizen.medicom.quickblox.fragments.BaseConversationFragment baseConversationFragment, boolean isIncomingCall) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}