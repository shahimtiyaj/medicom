package com.netizen.medicom.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0010\u0002\n\u0002\b\u000e\u0018\u00002\u00020\u0001:\u0002,-B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0013\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0014\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0015\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0016\u001a\u0004\u0018\u00010\u0004J\r\u0010\u0017\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\u0018J\b\u0010\u0019\u001a\u0004\u0018\u00010\u0004J\b\u0010\u001a\u001a\u0004\u0018\u00010\rJ\b\u0010\u001b\u001a\u0004\u0018\u00010\u000fJ\b\u0010\u001c\u001a\u0004\u0018\u00010\u0004J\r\u0010\u001d\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\u0018J\b\u0010\u001e\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u001f\u001a\u00020 2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u0010!\u001a\u00020 2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\"\u001a\u00020 2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0004J\u0010\u0010#\u001a\u00020 2\b\u0010\u0007\u001a\u0004\u0018\u00010\u0004J\u0015\u0010$\u001a\u00020 2\b\u0010\b\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010%J\u0010\u0010&\u001a\u00020 2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\'\u001a\u00020 2\b\u0010\f\u001a\u0004\u0018\u00010\rJ\u0010\u0010(\u001a\u00020 2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fJ\u0010\u0010)\u001a\u00020 2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0004J\u0015\u0010*\u001a\u00020 2\b\u0010\u0011\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010%J\u0010\u0010+\u001a\u00020 2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0004R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\b\u001a\u0004\u0018\u00010\t8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\nR\u0014\u0010\u000b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\u0004\u0018\u00010\r8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u0004\u0018\u00010\u000f8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0011\u001a\u0004\u0018\u00010\t8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\nR\u0014\u0010\u0012\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006."}, d2 = {"Lcom/netizen/medicom/model/BankAccountPostData;", "", "()V", "bankAccHolderName", "", "bankAccNumber", "bankNote", "chequeSlipContent", "chequeSlipEditable", "", "Ljava/lang/Boolean;", "chequeSlipName", "coreBankBranchInfoDTO", "Lcom/netizen/medicom/model/BankAccountPostData$CoreBankBranchInfoDTO;", "coreCategoryInfoDTO", "Lcom/netizen/medicom/model/BankAccountPostData$CoreCategoryInfoDTO;", "othersAttachmentContent", "othersAttachmentEditable", "othersAttachmentName", "getBankAccHolderName", "getBankAccNumber", "getBankNote", "getChequeSlipContent", "getChequeSlipEditable", "()Ljava/lang/Boolean;", "getChequeSlipName", "getCoreBankBranchInfoDTO", "getCoreCategoryInfoDTO", "getOthersAttachmentContent", "getOthersAttachmentEditable", "getOthersAttachmentName", "setBankAccHolderName", "", "setBankAccNumber", "setBankNote", "setChequeSlipContent", "setChequeSlipEditable", "(Ljava/lang/Boolean;)V", "setChequeSlipName", "setCoreBankBranchInfoDTO", "setCoreCategoryInfoDTO", "setOthersAttachmentContent", "setOthersAttachmentEditable", "setOthersAttachmentName", "CoreBankBranchInfoDTO", "CoreCategoryInfoDTO", "app_debug"})
public final class BankAccountPostData {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "coreCategoryInfoDTO")
    private com.netizen.medicom.model.BankAccountPostData.CoreCategoryInfoDTO coreCategoryInfoDTO;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "bankAccNumber")
    private java.lang.String bankAccNumber;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "bankAccHolderName")
    private java.lang.String bankAccHolderName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "bankNote")
    private java.lang.String bankNote;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "chequeSlipEditable")
    private java.lang.Boolean chequeSlipEditable;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "chequeSlipName")
    private java.lang.String chequeSlipName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "chequeSlipContent")
    private java.lang.String chequeSlipContent;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "othersAttachmentEditable")
    private java.lang.Boolean othersAttachmentEditable;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "othersAttachmentName")
    private java.lang.String othersAttachmentName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "othersAttachmentContent")
    private java.lang.String othersAttachmentContent;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "coreBankBranchInfoDTO")
    private com.netizen.medicom.model.BankAccountPostData.CoreBankBranchInfoDTO coreBankBranchInfoDTO;
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.medicom.model.BankAccountPostData.CoreCategoryInfoDTO getCoreCategoryInfoDTO() {
        return null;
    }
    
    public final void setCoreCategoryInfoDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.medicom.model.BankAccountPostData.CoreCategoryInfoDTO coreCategoryInfoDTO) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getBankAccNumber() {
        return null;
    }
    
    public final void setBankAccNumber(@org.jetbrains.annotations.Nullable()
    java.lang.String bankAccNumber) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getBankAccHolderName() {
        return null;
    }
    
    public final void setBankAccHolderName(@org.jetbrains.annotations.Nullable()
    java.lang.String bankAccHolderName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getBankNote() {
        return null;
    }
    
    public final void setBankNote(@org.jetbrains.annotations.Nullable()
    java.lang.String bankNote) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean getChequeSlipEditable() {
        return null;
    }
    
    public final void setChequeSlipEditable(@org.jetbrains.annotations.Nullable()
    java.lang.Boolean chequeSlipEditable) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getChequeSlipName() {
        return null;
    }
    
    public final void setChequeSlipName(@org.jetbrains.annotations.Nullable()
    java.lang.String chequeSlipName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getChequeSlipContent() {
        return null;
    }
    
    public final void setChequeSlipContent(@org.jetbrains.annotations.Nullable()
    java.lang.String chequeSlipContent) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean getOthersAttachmentEditable() {
        return null;
    }
    
    public final void setOthersAttachmentEditable(@org.jetbrains.annotations.Nullable()
    java.lang.Boolean othersAttachmentEditable) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getOthersAttachmentName() {
        return null;
    }
    
    public final void setOthersAttachmentName(@org.jetbrains.annotations.Nullable()
    java.lang.String othersAttachmentName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getOthersAttachmentContent() {
        return null;
    }
    
    public final void setOthersAttachmentContent(@org.jetbrains.annotations.Nullable()
    java.lang.String othersAttachmentContent) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.medicom.model.BankAccountPostData.CoreBankBranchInfoDTO getCoreBankBranchInfoDTO() {
        return null;
    }
    
    public final void setCoreBankBranchInfoDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.medicom.model.BankAccountPostData.CoreBankBranchInfoDTO coreBankBranchInfoDTO) {
    }
    
    public BankAccountPostData() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\r\u0010\n\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0006J\u0015\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\bR\"\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\t\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\u000e"}, d2 = {"Lcom/netizen/medicom/model/BankAccountPostData$CoreBankBranchInfoDTO;", "", "()V", "branchID", "", "getBranchID", "()Ljava/lang/Integer;", "setBranchID", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "getBranchId", "setBranchId", "", "branchId", "app_debug"})
    public static final class CoreBankBranchInfoDTO {
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "branchID")
        private java.lang.Integer branchID;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getBranchID() {
            return null;
        }
        
        public final void setBranchID(@org.jetbrains.annotations.Nullable()
        java.lang.Integer p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getBranchId() {
            return null;
        }
        
        public final void setBranchId(@org.jetbrains.annotations.Nullable()
        java.lang.Integer branchId) {
        }
        
        public CoreBankBranchInfoDTO() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\r\u0010\n\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0006J\u0015\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\bR\"\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\t\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\u000e"}, d2 = {"Lcom/netizen/medicom/model/BankAccountPostData$CoreCategoryInfoDTO;", "", "()V", "coreCategoryID", "", "getCoreCategoryID", "()Ljava/lang/Integer;", "setCoreCategoryID", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "getCoreCategoryId", "setCoreCategoryId", "", "coreCategoryId", "app_debug"})
    public static final class CoreCategoryInfoDTO {
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "coreCategoryID")
        private java.lang.Integer coreCategoryID;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getCoreCategoryID() {
            return null;
        }
        
        public final void setCoreCategoryID(@org.jetbrains.annotations.Nullable()
        java.lang.Integer p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getCoreCategoryId() {
            return null;
        }
        
        public final void setCoreCategoryId(@org.jetbrains.annotations.Nullable()
        java.lang.Integer coreCategoryId) {
        }
        
        public CoreCategoryInfoDTO() {
            super();
        }
    }
}