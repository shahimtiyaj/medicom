package com.netizen.medicom.quickblox.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0007\u001a\u0004\u0018\u00010\u0006J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0006H\u0016J\u0012\u0010\u000b\u001a\u00020\t2\b\u0010\n\u001a\u0004\u0018\u00010\u0006H\u0016J\u0010\u0010\f\u001a\u00020\t2\b\u0010\r\u001a\u0004\u0018\u00010\u0006R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"}, d2 = {"Lcom/netizen/medicom/quickblox/utils/WebRtcSessionManager;", "Lcom/quickblox/videochat/webrtc/callbacks/QBRTCClientSessionCallbacksImpl;", "()V", "TAG", "", "currentSession", "Lcom/quickblox/videochat/webrtc/QBRTCSession;", "getCurrentSession", "onReceiveNewSession", "", "session", "onSessionClosed", "setCurrentSession", "qbCurrentSession", "app_debug"})
public final class WebRtcSessionManager extends com.quickblox.videochat.webrtc.callbacks.QBRTCClientSessionCallbacksImpl {
    private static final java.lang.String TAG = null;
    private static com.quickblox.videochat.webrtc.QBRTCSession currentSession;
    public static final com.netizen.medicom.quickblox.utils.WebRtcSessionManager INSTANCE = null;
    
    @org.jetbrains.annotations.Nullable()
    public final com.quickblox.videochat.webrtc.QBRTCSession getCurrentSession() {
        return null;
    }
    
    public final void setCurrentSession(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.QBRTCSession qbCurrentSession) {
    }
    
    @java.lang.Override()
    public void onReceiveNewSession(@org.jetbrains.annotations.NotNull()
    com.quickblox.videochat.webrtc.QBRTCSession session) {
    }
    
    @java.lang.Override()
    public void onSessionClosed(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.QBRTCSession session) {
    }
    
    private WebRtcSessionManager() {
        super();
    }
}