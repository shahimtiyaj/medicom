package com.netizen.medicom.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\n\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u001f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0006B\u0005\u00a2\u0006\u0002\u0010\u0007J\b\u0010\t\u001a\u0004\u0018\u00010\u0003J\b\u0010\n\u001a\u0004\u0018\u00010\u0003J\b\u0010\u000b\u001a\u0004\u0018\u00010\u0003J\b\u0010\f\u001a\u0004\u0018\u00010\u0003J\u000e\u0010\r\u001a\u00020\u000e2\u0006\u0010\b\u001a\u00020\u0003J\u000e\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0004\u001a\u00020\u0003J\u000e\u0010\u0010\u001a\u00020\u000e2\u0006\u0010\u0004\u001a\u00020\u0003J\u000e\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u0005\u001a\u00020\u0003R\u0010\u0010\b\u001a\u0004\u0018\u00010\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"}, d2 = {"Lcom/netizen/medicom/model/UserLogin1;", "", "userId", "", "userName", "userPassword", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "()V", "check", "getcheck", "getuserId", "getuserName", "getuserPassword", "setcheck", "", "setuserId", "setuserName", "setuserPassword", "app_debug"})
public final class UserLogin1 {
    private java.lang.String userId;
    private java.lang.String userName;
    private java.lang.String userPassword;
    private java.lang.String check;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getuserId() {
        return null;
    }
    
    public final void setuserId(@org.jetbrains.annotations.NotNull()
    java.lang.String userName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getuserName() {
        return null;
    }
    
    public final void setuserName(@org.jetbrains.annotations.NotNull()
    java.lang.String userName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getuserPassword() {
        return null;
    }
    
    public final void setuserPassword(@org.jetbrains.annotations.NotNull()
    java.lang.String userPassword) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getcheck() {
        return null;
    }
    
    public final void setcheck(@org.jetbrains.annotations.NotNull()
    java.lang.String check) {
    }
    
    public UserLogin1() {
        super();
    }
    
    public UserLogin1(@org.jetbrains.annotations.NotNull()
    java.lang.String userId, @org.jetbrains.annotations.NotNull()
    java.lang.String userName, @org.jetbrains.annotations.NotNull()
    java.lang.String userPassword) {
        super();
    }
}