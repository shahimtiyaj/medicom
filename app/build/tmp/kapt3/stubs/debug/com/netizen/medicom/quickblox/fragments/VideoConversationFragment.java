package com.netizen.medicom.quickblox.fragments;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u00e0\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\n\n\u0002\u0010\u0007\n\u0002\b\u0016\n\u0002\u0010$\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u001e\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u00012\u00020\u00022\b\u0012\u0004\u0012\u00020\u00040\u00032\b\u0012\u0004\u0012\u00020\u00040\u00052\u00020\u00062\u00020\u0007:\u0004\u009e\u0001\u009f\u0001B\u0005\u00a2\u0006\u0002\u0010\bJ\u0010\u00100\u001a\u0002012\u0006\u00102\u001a\u00020\u000eH\u0014J\b\u00103\u001a\u000201H\u0002J\b\u00104\u001a\u000201H\u0014J\b\u00105\u001a\u000201H\u0014J\b\u00106\u001a\u000201H\u0014J\b\u00107\u001a\u00020\u0013H\u0002J \u00108\u001a\u00020\u00132\u0006\u00109\u001a\u00020\u00132\u0006\u0010:\u001a\u00020\u00132\u0006\u0010;\u001a\u00020<H\u0002J \u0010=\u001a\u0002012\u0006\u0010>\u001a\u00020 2\u0006\u0010?\u001a\u00020\u001e2\u0006\u0010@\u001a\u00020\u000eH\u0002J \u0010=\u001a\u0002012\u0006\u0010A\u001a\u00020\u00132\u0006\u0010>\u001a\u00020 2\u0006\u0010?\u001a\u00020\u001eH\u0002J\u0019\u0010B\u001a\u0004\u0018\u00010%2\b\u0010C\u001a\u0004\u0018\u00010\u0013H\u0002\u00a2\u0006\u0002\u0010DJ\r\u0010E\u001a\u00020\u0013H\u0010\u00a2\u0006\u0002\bFJ\u0012\u0010G\u001a\u0004\u0018\u00010%2\u0006\u0010C\u001a\u00020\u0013H\u0002J\b\u0010H\u001a\u000201H\u0014J\b\u0010I\u001a\u000201H\u0002J\b\u0010J\u001a\u000201H\u0014J\u0012\u0010K\u001a\u0002012\b\u0010L\u001a\u0004\u0018\u00010+H\u0015J\u0018\u0010M\u001a\u0002012\u0006\u0010N\u001a\u00020%2\u0006\u0010O\u001a\u00020\u0013H\u0016J5\u0010P\u001a\u0002012\u0006\u0010Q\u001a\u00020\u00042\b\u0010A\u001a\u0004\u0018\u00010\u00132\u0014\u0010R\u001a\u0010\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\n\u0018\u00010SH\u0016\u00a2\u0006\u0002\u0010TJ5\u0010U\u001a\u0002012\u0006\u0010Q\u001a\u00020\u00042\b\u0010A\u001a\u0004\u0018\u00010\u00132\u0014\u0010R\u001a\u0010\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\n\u0018\u00010SH\u0016\u00a2\u0006\u0002\u0010TJ\b\u0010V\u001a\u000201H\u0016J\u0010\u0010W\u001a\u0002012\u0006\u0010X\u001a\u00020\nH\u0016J\u001a\u0010Y\u001a\u0002012\b\u0010Z\u001a\u0004\u0018\u00010\u00042\u0006\u0010A\u001a\u00020\u0013H\u0016J\u001f\u0010[\u001a\u0002012\u0006\u0010Z\u001a\u00020\u00042\b\u0010A\u001a\u0004\u0018\u00010\u0013H\u0016\u00a2\u0006\u0002\u0010\\J\u0012\u0010]\u001a\u0002012\b\u0010^\u001a\u0004\u0018\u00010_H\u0016J\u0018\u0010`\u001a\u0002012\u0006\u0010a\u001a\u00020)2\u0006\u0010b\u001a\u00020cH\u0016J&\u0010d\u001a\u0004\u0018\u00010+2\u0006\u0010b\u001a\u00020e2\b\u0010f\u001a\u0004\u0018\u00010g2\b\u0010^\u001a\u0004\u0018\u00010_H\u0016J\b\u0010h\u001a\u000201H\u0016J\u001f\u0010i\u001a\u0002012\u0006\u0010Z\u001a\u00020\u00042\b\u0010j\u001a\u0004\u0018\u00010\u0013H\u0016\u00a2\u0006\u0002\u0010\\J\u0010\u0010k\u001a\u0002012\u0006\u0010O\u001a\u00020\u0013H\u0016J\u001a\u0010l\u001a\u0002012\b\u0010Z\u001a\u0004\u0018\u00010\u00042\u0006\u0010?\u001a\u00020\u001eH\u0016J\u0016\u0010m\u001a\u0002012\f\u0010n\u001a\b\u0012\u0004\u0012\u00020\u00110oH\u0016J\u0010\u0010p\u001a\u00020\u000e2\u0006\u0010q\u001a\u00020rH\u0016J\b\u0010s\u001a\u000201H\u0016J5\u0010t\u001a\u0002012\u0006\u0010Q\u001a\u00020\u00042\b\u0010A\u001a\u0004\u0018\u00010\u00132\u0014\u0010R\u001a\u0010\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\n\u0018\u00010SH\u0016\u00a2\u0006\u0002\u0010TJ)\u0010u\u001a\u0002012\b\u0010Q\u001a\u0004\u0018\u00010\u00042\u0006\u0010?\u001a\u00020\u001e2\b\u0010C\u001a\u0004\u0018\u00010\u0013H\u0016\u00a2\u0006\u0002\u0010vJ\b\u0010w\u001a\u000201H\u0016J\u0010\u0010x\u001a\u0002012\u0006\u0010Q\u001a\u00020\u0004H\u0016J\b\u0010y\u001a\u000201H\u0016J\u0018\u0010z\u001a\u0002012\u0006\u0010Z\u001a\u00020\u00042\u0006\u0010{\u001a\u00020|H\u0016J\u0018\u0010}\u001a\u0002012\u0006\u0010Q\u001a\u00020\u00042\u0006\u0010A\u001a\u00020\u0013H\u0016J\b\u0010~\u001a\u000201H\u0002J\b\u0010\u007f\u001a\u000201H\u0002J\t\u0010\u0080\u0001\u001a\u000201H\u0002J\t\u0010\u0081\u0001\u001a\u000201H\u0002J\u0011\u0010\u0082\u0001\u001a\u0002012\u0006\u0010O\u001a\u00020\u0013H\u0002J\t\u0010\u0083\u0001\u001a\u000201H\u0002J\u0017\u0010\u0084\u0001\u001a\u0002012\f\u0010n\u001a\b\u0012\u0004\u0012\u00020\u00110oH\u0002J\t\u0010\u0085\u0001\u001a\u000201H\u0002J\u0019\u0010\u0086\u0001\u001a\u0002012\b\u0010A\u001a\u0004\u0018\u00010\u0013H\u0002\u00a2\u0006\u0003\u0010\u0087\u0001J\t\u0010\u0088\u0001\u001a\u000201H\u0002J\u0011\u0010\u0089\u0001\u001a\u0002012\u0006\u0010:\u001a\u00020\u0013H\u0002J\u0011\u0010\u008a\u0001\u001a\u0002012\u0006\u0010A\u001a\u00020\u0013H\u0002J\t\u0010\u008b\u0001\u001a\u000201H\u0002J\u0019\u0010\u008c\u0001\u001a\u0002012\u0006\u0010C\u001a\u00020\u00132\u0006\u0010?\u001a\u00020\u001eH\u0002J\"\u0010\u008d\u0001\u001a\u0002012\b\u0010A\u001a\u0004\u0018\u00010\u00132\u0007\u0010\u008e\u0001\u001a\u00020\nH\u0002\u00a2\u0006\u0003\u0010\u008f\u0001J\t\u0010\u0090\u0001\u001a\u000201H\u0002J\u0011\u0010\u0091\u0001\u001a\u0002012\u0006\u0010A\u001a\u00020\u0013H\u0002J\u0013\u0010\u0092\u0001\u001a\u0002012\b\u0010q\u001a\u0004\u0018\u00010rH\u0002J\u0012\u0010\u0093\u0001\u001a\u0002012\u0007\u0010\u0094\u0001\u001a\u00020\u000eH\u0002J\t\u0010\u0095\u0001\u001a\u000201H\u0002J\u0017\u0010\u0096\u0001\u001a\u0002012\f\u0010n\u001a\b\u0012\u0004\u0012\u00020\u00110oH\u0002J\u001a\u0010\u0097\u0001\u001a\u0002012\u0006\u0010A\u001a\u00020\u00132\u0007\u0010\u0098\u0001\u001a\u00020\nH\u0002J\u0011\u0010\u0099\u0001\u001a\u0002012\u0006\u0010q\u001a\u00020rH\u0002J\u001b\u0010\u009a\u0001\u001a\u0002012\u0007\u0010>\u001a\u00030\u009b\u00012\u0007\u0010\u009c\u0001\u001a\u00020\u000eH\u0002J\u0011\u0010\u009d\u0001\u001a\u0002012\u0006\u0010O\u001a\u00020\u0013H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001d\u001a\u0004\u0018\u00010\u001eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020 X\u0082.\u00a2\u0006\u0002\n\u0000R\u0012\u0010!\u001a\u00060\"R\u00020\u0000X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010#\u001a\b\u0012\u0004\u0012\u00020%0$X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\'X\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010(\u001a\u0004\u0018\u00010)X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010*\u001a\u0004\u0018\u00010+X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010,\u001a\u00020-X\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010.\u001a\u0004\u0018\u00010 X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010/\u001a\u00020\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u00a0\u0001"}, d2 = {"Lcom/netizen/medicom/quickblox/fragments/VideoConversationFragment;", "Lcom/netizen/medicom/quickblox/fragments/BaseConversationFragment;", "Ljava/io/Serializable;", "Lcom/quickblox/videochat/webrtc/callbacks/QBRTCClientVideoTracksCallbacks;", "Lcom/quickblox/videochat/webrtc/QBRTCSession;", "Lcom/quickblox/videochat/webrtc/callbacks/QBRTCSessionStateCallback;", "Lcom/quickblox/videochat/webrtc/callbacks/QBRTCSessionEventsCallback;", "Lcom/netizen/medicom/quickblox/adapters/OpponentsFromCallAdapter$OnAdapterEventListener;", "()V", "TAG", "", "actionVideoButtonsLayout", "Landroid/widget/LinearLayout;", "allCallbacksInit", "", "allOpponents", "", "Lcom/quickblox/users/model/QBUser;", "amountOpponents", "", "cameraToggle", "Landroid/widget/ToggleButton;", "connectionEstablished", "connectionStatusLocal", "Landroid/widget/TextView;", "isCurrentCameraFront", "isLocalVideoFullScreen", "isPeerToPeerCall", "isRemoteShown", "localVideoTrack", "Lcom/quickblox/videochat/webrtc/view/QBRTCVideoTrack;", "localVideoView", "Lcom/quickblox/videochat/webrtc/view/QBRTCSurfaceView;", "localViewOnClickListener", "Lcom/netizen/medicom/quickblox/fragments/VideoConversationFragment$LocalViewOnClickListener;", "opponentViewHolders", "Landroid/util/SparseArray;", "Lcom/netizen/medicom/quickblox/adapters/OpponentsFromCallAdapter$ViewHolder;", "opponentsAdapter", "Lcom/netizen/medicom/quickblox/adapters/OpponentsFromCallAdapter;", "optionsMenu", "Landroid/view/Menu;", "parentView", "Landroid/view/View;", "recyclerView", "Landroidx/recyclerview/widget/RecyclerView;", "remoteFullScreenVideoView", "userIDFullScreen", "actionButtonsEnabled", "", "inability", "addListeners", "configureActionBar", "configureOutgoingScreen", "configureToolbar", "defineColumnsCount", "defineSize", "measuredWidth", "columnsCount", "padding", "", "fillVideoView", "videoView", "videoTrack", "remoteRenderer", "userId", "findHolder", "userID", "(Ljava/lang/Integer;)Lcom/netizen/medicom/quickblox/adapters/OpponentsFromCallAdapter$ViewHolder;", "getFragmentLayout", "getFragmentLayout$app_debug", "getViewHolderForOpponent", "initButtonsListener", "initCorrectSizeForLocalView", "initFields", "initViews", "view", "onBindLastViewHolder", "holder", "position", "onCallAcceptByUser", "session", "userInfo", "", "(Lcom/quickblox/videochat/webrtc/QBRTCSession;Ljava/lang/Integer;Ljava/util/Map;)V", "onCallRejectByUser", "onCallStopped", "onCallTimeUpdate", "time", "onConnectedToUser", "qbrtcSession", "onConnectionClosedForUser", "(Lcom/quickblox/videochat/webrtc/QBRTCSession;Ljava/lang/Integer;)V", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateOptionsMenu", "menu", "inflater", "Landroid/view/MenuInflater;", "onCreateView", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onDetach", "onDisconnectedFromUser", "integer", "onItemClick", "onLocalVideoTrackReceive", "onOpponentsListUpdated", "newUsers", "Ljava/util/ArrayList;", "onOptionsItemSelected", "item", "Landroid/view/MenuItem;", "onPause", "onReceiveHangUpFromUser", "onRemoteVideoTrackReceive", "(Lcom/quickblox/videochat/webrtc/QBRTCSession;Lcom/quickblox/videochat/webrtc/view/QBRTCVideoTrack;Ljava/lang/Integer;)V", "onResume", "onSessionClosed", "onStart", "onStateChanged", "qbrtcSessionState", "Lcom/quickblox/videochat/webrtc/BaseSession$QBRTCSessionState;", "onUserNotAnswer", "releaseOpponentsViews", "releaseViewHolders", "releaseViews", "removeListeners", "replaceUsersInAdapter", "restoreSession", "runUpdateUsersNames", "setAnotherUserToFullScreen", "setBackgroundOpponentView", "(Ljava/lang/Integer;)V", "setDuringCallActionBar", "setGrid", "setProgressBarForOpponentGone", "setRecyclerViewVisibleState", "setRemoteViewMultiCall", "setStatusForOpponent", "status", "(Ljava/lang/Integer;Ljava/lang/String;)V", "startScreenSharing", "swapUsersFullscreenToPreview", "switchCamera", "toggleCamera", "isNeedEnableCam", "toggleCameraInternal", "updateAllOpponentsList", "updateNameForOpponent", "newUserName", "updateSwitchCameraIcon", "updateVideoView", "Lorg/webrtc/SurfaceViewRenderer;", "mirror", "updateViewHolders", "DividerItemDecoration", "LocalViewOnClickListener", "app_debug"})
public final class VideoConversationFragment extends com.netizen.medicom.quickblox.fragments.BaseConversationFragment implements java.io.Serializable, com.quickblox.videochat.webrtc.callbacks.QBRTCClientVideoTracksCallbacks<com.quickblox.videochat.webrtc.QBRTCSession>, com.quickblox.videochat.webrtc.callbacks.QBRTCSessionStateCallback<com.quickblox.videochat.webrtc.QBRTCSession>, com.quickblox.videochat.webrtc.callbacks.QBRTCSessionEventsCallback, com.netizen.medicom.quickblox.adapters.OpponentsFromCallAdapter.OnAdapterEventListener {
    private final java.lang.String TAG = null;
    private android.widget.ToggleButton cameraToggle;
    private android.view.View parentView;
    private android.widget.LinearLayout actionVideoButtonsLayout;
    private android.widget.TextView connectionStatusLocal;
    private androidx.recyclerview.widget.RecyclerView recyclerView;
    private com.quickblox.videochat.webrtc.view.QBRTCSurfaceView localVideoView;
    private com.quickblox.videochat.webrtc.view.QBRTCSurfaceView remoteFullScreenVideoView;
    private android.util.SparseArray<com.netizen.medicom.quickblox.adapters.OpponentsFromCallAdapter.ViewHolder> opponentViewHolders;
    private com.netizen.medicom.quickblox.adapters.OpponentsFromCallAdapter opponentsAdapter;
    private java.util.List<com.quickblox.users.model.QBUser> allOpponents;
    private com.netizen.medicom.quickblox.fragments.VideoConversationFragment.LocalViewOnClickListener localViewOnClickListener;
    private boolean isPeerToPeerCall = false;
    private com.quickblox.videochat.webrtc.view.QBRTCVideoTrack localVideoTrack;
    private android.view.Menu optionsMenu;
    private boolean isRemoteShown = false;
    private int amountOpponents = 0;
    private int userIDFullScreen = 0;
    private boolean connectionEstablished = false;
    private boolean allCallbacksInit = false;
    private boolean isCurrentCameraFront = false;
    private boolean isLocalVideoFullScreen = false;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    protected void configureOutgoingScreen() {
    }
    
    @java.lang.Override()
    protected void configureActionBar() {
    }
    
    @java.lang.Override()
    protected void configureToolbar() {
    }
    
    @java.lang.Override()
    public int getFragmentLayout$app_debug() {
        return 0;
    }
    
    @java.lang.Override()
    protected void initFields() {
    }
    
    private final void setDuringCallActionBar() {
    }
    
    private final void addListeners() {
    }
    
    private final void removeListeners() {
    }
    
    @java.lang.Override()
    protected void actionButtonsEnabled(boolean inability) {
    }
    
    @java.lang.Override()
    public void onStart() {
    }
    
    @java.lang.Override()
    public void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @android.annotation.SuppressLint(value = {"WrongConstant"})
    @java.lang.Override()
    protected void initViews(@org.jetbrains.annotations.Nullable()
    android.view.View view) {
    }
    
    private final void restoreSession() {
    }
    
    private final void initCorrectSizeForLocalView() {
    }
    
    private final void setGrid(int columnsCount) {
    }
    
    private final int defineSize(int measuredWidth, int columnsCount, float padding) {
        return 0;
    }
    
    private final int defineColumnsCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onResume() {
    }
    
    @java.lang.Override()
    public void onPause() {
    }
    
    @java.lang.Override()
    public void onDetach() {
    }
    
    private final void releaseViewHolders() {
    }
    
    private final void releaseViews() {
    }
    
    @java.lang.Override()
    public void onCallStopped() {
    }
    
    @java.lang.Override()
    protected void initButtonsListener() {
    }
    
    private final void switchCamera(android.view.MenuItem item) {
    }
    
    private final void updateSwitchCameraIcon(android.view.MenuItem item) {
    }
    
    private final void toggleCameraInternal() {
    }
    
    private final void toggleCamera(boolean isNeedEnableCam) {
    }
    
    @java.lang.Override()
    public void onLocalVideoTrackReceive(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.QBRTCSession qbrtcSession, @org.jetbrains.annotations.NotNull()
    com.quickblox.videochat.webrtc.view.QBRTCVideoTrack videoTrack) {
    }
    
    @java.lang.Override()
    public void onRemoteVideoTrackReceive(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.QBRTCSession session, @org.jetbrains.annotations.NotNull()
    com.quickblox.videochat.webrtc.view.QBRTCVideoTrack videoTrack, @org.jetbrains.annotations.Nullable()
    java.lang.Integer userID) {
    }
    
    @java.lang.Override()
    public void onBindLastViewHolder(@org.jetbrains.annotations.NotNull()
    com.netizen.medicom.quickblox.adapters.OpponentsFromCallAdapter.ViewHolder holder, int position) {
    }
    
    @java.lang.Override()
    public void onItemClick(int position) {
    }
    
    private final void replaceUsersInAdapter(int position) {
    }
    
    private final void updateViewHolders(int position) {
    }
    
    private final void swapUsersFullscreenToPreview(int userId) {
    }
    
    private final void setRemoteViewMultiCall(int userID, com.quickblox.videochat.webrtc.view.QBRTCVideoTrack videoTrack) {
    }
    
    private final void setRecyclerViewVisibleState() {
    }
    
    private final com.netizen.medicom.quickblox.adapters.OpponentsFromCallAdapter.ViewHolder getViewHolderForOpponent(int userID) {
        return null;
    }
    
    private final com.netizen.medicom.quickblox.adapters.OpponentsFromCallAdapter.ViewHolder findHolder(java.lang.Integer userID) {
        return null;
    }
    
    private final void releaseOpponentsViews() {
    }
    
    /**
     * @param userId set userId if it from fullscreen videoTrack
     */
    private final void fillVideoView(com.quickblox.videochat.webrtc.view.QBRTCSurfaceView videoView, com.quickblox.videochat.webrtc.view.QBRTCVideoTrack videoTrack, boolean remoteRenderer) {
    }
    
    private final void updateVideoView(org.webrtc.SurfaceViewRenderer videoView, boolean mirror) {
    }
    
    /**
     * @param userId set userId if it from fullscreen videoTrack
     */
    private final void fillVideoView(int userId, com.quickblox.videochat.webrtc.view.QBRTCSurfaceView videoView, com.quickblox.videochat.webrtc.view.QBRTCVideoTrack videoTrack) {
    }
    
    private final void setStatusForOpponent(java.lang.Integer userId, java.lang.String status) {
    }
    
    private final void updateNameForOpponent(int userId, java.lang.String newUserName) {
    }
    
    private final void setProgressBarForOpponentGone(int userId) {
    }
    
    private final void setBackgroundOpponentView(java.lang.Integer userId) {
    }
    
    @java.lang.Override()
    public void onStateChanged(@org.jetbrains.annotations.NotNull()
    com.quickblox.videochat.webrtc.QBRTCSession qbrtcSession, @org.jetbrains.annotations.NotNull()
    com.quickblox.videochat.webrtc.BaseSession.QBRTCSessionState qbrtcSessionState) {
    }
    
    @java.lang.Override()
    public void onConnectedToUser(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.QBRTCSession qbrtcSession, int userId) {
    }
    
    @java.lang.Override()
    public void onConnectionClosedForUser(@org.jetbrains.annotations.NotNull()
    com.quickblox.videochat.webrtc.QBRTCSession qbrtcSession, @org.jetbrains.annotations.Nullable()
    java.lang.Integer userId) {
    }
    
    @java.lang.Override()
    public void onDisconnectedFromUser(@org.jetbrains.annotations.NotNull()
    com.quickblox.videochat.webrtc.QBRTCSession qbrtcSession, @org.jetbrains.annotations.Nullable()
    java.lang.Integer integer) {
    }
    
    @java.lang.Override()
    public void onUserNotAnswer(@org.jetbrains.annotations.NotNull()
    com.quickblox.videochat.webrtc.QBRTCSession session, int userId) {
    }
    
    @java.lang.Override()
    public void onCallRejectByUser(@org.jetbrains.annotations.NotNull()
    com.quickblox.videochat.webrtc.QBRTCSession session, @org.jetbrains.annotations.Nullable()
    java.lang.Integer userId, @org.jetbrains.annotations.Nullable()
    java.util.Map<java.lang.String, java.lang.String> userInfo) {
    }
    
    @java.lang.Override()
    public void onCallAcceptByUser(@org.jetbrains.annotations.NotNull()
    com.quickblox.videochat.webrtc.QBRTCSession session, @org.jetbrains.annotations.Nullable()
    java.lang.Integer userId, @org.jetbrains.annotations.Nullable()
    java.util.Map<java.lang.String, java.lang.String> userInfo) {
    }
    
    @java.lang.Override()
    public void onReceiveHangUpFromUser(@org.jetbrains.annotations.NotNull()
    com.quickblox.videochat.webrtc.QBRTCSession session, @org.jetbrains.annotations.Nullable()
    java.lang.Integer userId, @org.jetbrains.annotations.Nullable()
    java.util.Map<java.lang.String, java.lang.String> userInfo) {
    }
    
    @java.lang.Override()
    public void onSessionClosed(@org.jetbrains.annotations.NotNull()
    com.quickblox.videochat.webrtc.QBRTCSession session) {
    }
    
    private final void setAnotherUserToFullScreen() {
    }
    
    @java.lang.Override()
    public void onCreateOptionsMenu(@org.jetbrains.annotations.NotNull()
    android.view.Menu menu, @org.jetbrains.annotations.NotNull()
    android.view.MenuInflater inflater) {
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    private final void startScreenSharing() {
    }
    
    @java.lang.Override()
    public void onOpponentsListUpdated(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.quickblox.users.model.QBUser> newUsers) {
    }
    
    @java.lang.Override()
    public void onCallTimeUpdate(@org.jetbrains.annotations.NotNull()
    java.lang.String time) {
    }
    
    private final void updateAllOpponentsList(java.util.ArrayList<com.quickblox.users.model.QBUser> newUsers) {
    }
    
    private final void runUpdateUsersNames(java.util.ArrayList<com.quickblox.users.model.QBUser> newUsers) {
    }
    
    public VideoConversationFragment() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0080\u0004\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J(\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016R\u000e\u0010\u0007\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"}, d2 = {"Lcom/netizen/medicom/quickblox/fragments/VideoConversationFragment$DividerItemDecoration;", "Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;", "context", "Landroid/content/Context;", "dimensionDivider", "", "(Lcom/netizen/medicom/quickblox/fragments/VideoConversationFragment;Landroid/content/Context;I)V", "space", "getItemOffsets", "", "outRect", "Landroid/graphics/Rect;", "view", "Landroid/view/View;", "parent", "Landroidx/recyclerview/widget/RecyclerView;", "state", "Landroidx/recyclerview/widget/RecyclerView$State;", "app_debug"})
    public final class DividerItemDecoration extends androidx.recyclerview.widget.RecyclerView.ItemDecoration {
        private final int space = 0;
        
        @java.lang.Override()
        public void getItemOffsets(@org.jetbrains.annotations.NotNull()
        android.graphics.Rect outRect, @org.jetbrains.annotations.NotNull()
        android.view.View view, @org.jetbrains.annotations.NotNull()
        androidx.recyclerview.widget.RecyclerView parent, @org.jetbrains.annotations.NotNull()
        androidx.recyclerview.widget.RecyclerView.State state) {
        }
        
        public DividerItemDecoration(@org.jetbrains.annotations.NotNull()
        android.content.Context context, @androidx.annotation.DimenRes()
        int dimensionDivider) {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0080\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0005\u001a\u00020\u0006H\u0002J\u0010\u0010\u0007\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\tH\u0016J\b\u0010\n\u001a\u00020\u0006H\u0002J\b\u0010\u000b\u001a\u00020\u0006H\u0002J\b\u0010\f\u001a\u00020\u0006H\u0002J\b\u0010\r\u001a\u00020\u0006H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"}, d2 = {"Lcom/netizen/medicom/quickblox/fragments/VideoConversationFragment$LocalViewOnClickListener;", "Landroid/view/View$OnClickListener;", "(Lcom/netizen/medicom/quickblox/fragments/VideoConversationFragment;)V", "lastFullScreenClickTime", "", "hideToolBarAndButtons", "", "onClick", "v", "Landroid/view/View;", "setFullScreenOnOff", "shiftBottomListOpponents", "shiftMarginListOpponents", "showToolBarAndButtons", "app_debug"})
    public final class LocalViewOnClickListener implements android.view.View.OnClickListener {
        private long lastFullScreenClickTime = 0L;
        
        @java.lang.Override()
        public void onClick(@org.jetbrains.annotations.NotNull()
        android.view.View v) {
        }
        
        private final void setFullScreenOnOff() {
        }
        
        private final void hideToolBarAndButtons() {
        }
        
        private final void showToolBarAndButtons() {
        }
        
        private final void shiftBottomListOpponents() {
        }
        
        private final void shiftMarginListOpponents() {
        }
        
        public LocalViewOnClickListener() {
            super();
        }
    }
}