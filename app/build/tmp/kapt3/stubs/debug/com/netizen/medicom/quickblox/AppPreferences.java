package com.netizen.medicom.quickblox;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\t\u0018\u00002\u00020\u0001B\u000f\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004J\b\u0010\u0011\u001a\u0004\u0018\u00010\u0006J\b\u0010\u0012\u001a\u0004\u0018\u00010\u0006J\b\u0010\u0013\u001a\u0004\u0018\u00010\u0006J\b\u0010\u0014\u001a\u0004\u0018\u00010\u0006J\u0006\u0010\u0015\u001a\u00020\u0016J\u0006\u0010\u0017\u001a\u00020\u0018J\u000e\u0010\u0019\u001a\u00020\u00182\u0006\u0010\u001a\u001a\u00020\u0006J\u000e\u0010\u001b\u001a\u00020\u00182\u0006\u0010\u001c\u001a\u00020\u0006J\u0010\u0010\u001d\u001a\u00020\u00182\b\u0010\u001e\u001a\u0004\u0018\u00010\u0006J\u0010\u0010\u001f\u001a\u00020\u00182\b\u0010 \u001a\u0004\u0018\u00010\u0006R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"}, d2 = {"Lcom/netizen/medicom/quickblox/AppPreferences;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "KEY_IS_SPLASH_SHOWN", "", "KEY_MESSAGE_TYPE", "KEY_PURCHASE_POINT", "KEY_ROOM_ID", "KEY_TOKEN", "PREFERENCE_NAME", "editor", "Landroid/content/SharedPreferences$Editor;", "mContext", "mPreferences", "Landroid/content/SharedPreferences;", "getMessageType", "getPurchasePoint", "getRoomId", "getToken", "isSplashShown", "", "setIsSplashShown", "", "setMessageType", "messageName", "setPurchasePoint", "purchasePointName", "setRoomId", "roomId", "setToken", "token", "app_debug"})
public final class AppPreferences {
    private final java.lang.String PREFERENCE_NAME = "app_preferences";
    private final java.lang.String KEY_TOKEN = "access_token";
    private final java.lang.String KEY_IS_SPLASH_SHOWN = "is_splash_shown";
    private android.content.Context mContext;
    private android.content.SharedPreferences mPreferences;
    private android.content.SharedPreferences.Editor editor;
    private final java.lang.String KEY_PURCHASE_POINT = "purchase_point";
    private final java.lang.String KEY_MESSAGE_TYPE = "message_type";
    private final java.lang.String KEY_ROOM_ID = "room_id";
    
    public final void setToken(@org.jetbrains.annotations.Nullable()
    java.lang.String token) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getToken() {
        return null;
    }
    
    public final void setIsSplashShown() {
    }
    
    public final boolean isSplashShown() {
        return false;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPurchasePoint() {
        return null;
    }
    
    public final void setPurchasePoint(@org.jetbrains.annotations.NotNull()
    java.lang.String purchasePointName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMessageType() {
        return null;
    }
    
    public final void setMessageType(@org.jetbrains.annotations.NotNull()
    java.lang.String messageName) {
    }
    
    public final void setRoomId(@org.jetbrains.annotations.Nullable()
    java.lang.String roomId) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getRoomId() {
        return null;
    }
    
    public AppPreferences(@org.jetbrains.annotations.Nullable()
    android.content.Context context) {
        super();
    }
}