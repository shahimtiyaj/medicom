package com.netizen.medicom.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R\u001b\u0010\u0003\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0004\u00a2\u0006\n\n\u0002\u0010\b\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\t"}, d2 = {"Lcom/netizen/medicom/model/JSONResponse;", "", "()V", "android", "", "Lcom/netizen/medicom/model/BalanceTransferGetData;", "getAndroid", "()[Lcom/netizen/medicom/model/BalanceTransferGetData;", "[Lcom/netizen/medicom/model/BalanceTransferGetData;", "app_debug"})
public final class JSONResponse {
    @org.jetbrains.annotations.Nullable()
    private final com.netizen.medicom.model.BalanceTransferGetData[] android = null;
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.medicom.model.BalanceTransferGetData[] getAndroid() {
        return null;
    }
    
    public JSONResponse() {
        super();
    }
}