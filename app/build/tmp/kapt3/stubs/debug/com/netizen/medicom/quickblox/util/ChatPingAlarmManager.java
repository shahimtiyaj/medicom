package com.netizen.medicom.quickblox.util;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0011J\u000e\u0010\u0015\u001a\u00020\u00132\u0006\u0010\u000b\u001a\u00020\rJ\u0006\u0010\u0016\u001a\u00020\u0013R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"}, d2 = {"Lcom/netizen/medicom/quickblox/util/ChatPingAlarmManager;", "", "()V", "PING_INTERVAL", "", "TAG", "", "alarmBroadcastReceiver", "Landroid/content/BroadcastReceiver;", "alarmManager", "Landroid/app/AlarmManager;", "context", "Ljava/lang/ref/WeakReference;", "Landroid/content/Context;", "pendingIntent", "Landroid/app/PendingIntent;", "pingFailedListener", "Lorg/jivesoftware/smackx/ping/PingFailedListener;", "addPingListener", "", "listener", "onCreate", "onDestroy", "app_debug"})
public final class ChatPingAlarmManager {
    private static final java.lang.String TAG = null;
    private static final long PING_INTERVAL = 0L;
    private static android.app.PendingIntent pendingIntent;
    private static android.app.AlarmManager alarmManager;
    private static java.lang.ref.WeakReference<android.content.Context> context;
    private static org.jivesoftware.smackx.ping.PingFailedListener pingFailedListener;
    private static final android.content.BroadcastReceiver alarmBroadcastReceiver = null;
    public static final com.netizen.medicom.quickblox.util.ChatPingAlarmManager INSTANCE = null;
    
    /**
     * Register a pending intent with the AlarmManager to be broadcasted every
     * half hour and register the alarm broadcast receiver to receive this
     * intent. The receiver will check all known questions if a ping is
     * Necessary when invoked by the alarm intent.
     *
     * @param context
     */
    public final void onCreate(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    /**
     * Unregister the alarm broadcast receiver and cancel the alarm.
     */
    public final void onDestroy() {
    }
    
    public final void addPingListener(@org.jetbrains.annotations.NotNull()
    org.jivesoftware.smackx.ping.PingFailedListener listener) {
    }
    
    private ChatPingAlarmManager() {
        super();
    }
}