package com.netizen.medicom.quickblox.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u001a\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\u001a\u0010\u0010\u0003\u001a\u00020\u00012\b\b\u0001\u0010\u0004\u001a\u00020\u0001\u001a\u0010\u0010\u0005\u001a\u00020\u00012\b\b\u0001\u0010\u0006\u001a\u00020\u0001\u001a\u000e\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0004\u001a\u00020\u0001\u001a\u0010\u0010\t\u001a\u00020\b2\b\b\u0001\u0010\n\u001a\u00020\u0001\u001a\u0010\u0010\u000b\u001a\u00020\u00012\b\b\u0001\u0010\f\u001a\u00020\u0001\u001a\u0010\u0010\r\u001a\u00020\b2\b\b\u0001\u0010\u000e\u001a\u00020\u0001\u001a\u0010\u0010\u000f\u001a\u00020\u00102\b\b\u0001\u0010\u0011\u001a\u00020\u0001\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"}, d2 = {"RANDOM_COLOR_END_RANGE", "", "RANDOM_COLOR_START_RANGE", "getCircleColor", "colorPosition", "getColor", "colorId", "getColorCircleDrawable", "Landroid/graphics/drawable/Drawable;", "getColoredCircleDrawable", "color", "getDimen", "dimenId", "getDrawable", "drawableId", "getString", "", "stringId", "app_debug"})
public final class UiUtilsKt {
    private static final int RANDOM_COLOR_START_RANGE = 0;
    private static final int RANDOM_COLOR_END_RANGE = 9;
    
    @org.jetbrains.annotations.NotNull()
    public static final android.graphics.drawable.Drawable getColorCircleDrawable(int colorPosition) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final android.graphics.drawable.Drawable getColoredCircleDrawable(@androidx.annotation.ColorInt()
    int color) {
        return null;
    }
    
    public static final int getCircleColor(@androidx.annotation.IntRange(from = 0L, to = 9L)
    int colorPosition) {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getString(@androidx.annotation.StringRes()
    int stringId) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final android.graphics.drawable.Drawable getDrawable(@androidx.annotation.DrawableRes()
    int drawableId) {
        return null;
    }
    
    public static final int getColor(@androidx.annotation.ColorRes()
    int colorId) {
        return 0;
    }
    
    public static final int getDimen(@androidx.annotation.DimenRes()
    int dimenId) {
        return 0;
    }
}