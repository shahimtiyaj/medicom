package com.netizen.medicom.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"}, d2 = {"Lcom/netizen/medicom/utils/MyUtilsClass;", "", "()V", "Companion", "app_debug"})
public final class MyUtilsClass {
    public static final com.netizen.medicom.utils.MyUtilsClass.Companion Companion = null;
    
    public MyUtilsClass() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0017\u0010\u0003\u001a\u0004\u0018\u00010\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0007J\u000e\u0010\b\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\u0004J\u000e\u0010\n\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\u0004J\u000e\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u0004J\u0010\u0010\u000e\u001a\u00020\f2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0004J\u0016\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015J\u0016\u0010\u0016\u001a\u00020\u00112\u0006\u0010\u0014\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0004J\u0016\u0010\u0019\u001a\u00020\u00112\u0006\u0010\u0014\u001a\u00020\u00172\u0006\u0010\u001a\u001a\u00020\u001bJ\u0010\u0010\u001c\u001a\u00020\f2\b\u0010\u001d\u001a\u0004\u0018\u00010\u0004\u00a8\u0006\u001e"}, d2 = {"Lcom/netizen/medicom/utils/MyUtilsClass$Companion;", "", "()V", "getDate", "", "time", "", "(Ljava/lang/Long;)Ljava/lang/String;", "getDateFormat", "date", "getDateFormatForSerachData", "isValidEmail", "", "email", "isValidPassword", "password", "requestFocus", "", "view", "Landroid/view/View;", "context", "Landroid/app/Activity;", "showErrorToasty", "Landroid/content/Context;", "message", "showsDatePicker", "dateListener", "Landroid/app/DatePickerDialog$OnDateSetListener;", "validCellPhone", "number", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getDateFormat(@org.jetbrains.annotations.NotNull()
        java.lang.String date) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getDateFormatForSerachData(@org.jetbrains.annotations.NotNull()
        java.lang.String date) {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getDate(@org.jetbrains.annotations.Nullable()
        java.lang.Long time) {
            return null;
        }
        
        public final void showErrorToasty(@org.jetbrains.annotations.NotNull()
        android.content.Context context, @org.jetbrains.annotations.NotNull()
        java.lang.String message) {
        }
        
        public final void showsDatePicker(@org.jetbrains.annotations.NotNull()
        android.content.Context context, @org.jetbrains.annotations.NotNull()
        android.app.DatePickerDialog.OnDateSetListener dateListener) {
        }
        
        public final boolean isValidPassword(@org.jetbrains.annotations.Nullable()
        java.lang.String password) {
            return false;
        }
        
        public final boolean isValidEmail(@org.jetbrains.annotations.NotNull()
        java.lang.String email) {
            return false;
        }
        
        public final boolean validCellPhone(@org.jetbrains.annotations.Nullable()
        java.lang.String number) {
            return false;
        }
        
        public final void requestFocus(@org.jetbrains.annotations.NotNull()
        android.view.View view, @org.jetbrains.annotations.NotNull()
        android.app.Activity context) {
        }
        
        private Companion() {
            super();
        }
    }
}