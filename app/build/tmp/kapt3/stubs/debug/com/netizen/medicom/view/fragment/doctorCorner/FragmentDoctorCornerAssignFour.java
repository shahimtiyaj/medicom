package com.netizen.medicom.view.fragment.doctorCorner;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 %2\u00020\u0001:\u0001%B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u000b\u001a\u00020\nH\u0002J\b\u0010\f\u001a\u00020\rH\u0002J\b\u0010\u000e\u001a\u00020\rH\u0002J\u0010\u0010\u000f\u001a\u00020\r2\u0006\u0010\u0010\u001a\u00020\nH\u0002J\"\u0010\u0011\u001a\u00020\r2\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00132\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0016J\u0012\u0010\u0017\u001a\u00020\r2\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u0016J&\u0010\u001a\u001a\u0004\u0018\u00010\u001b2\u0006\u0010\u001c\u001a\u00020\u001d2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001f2\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u0016J\u0010\u0010 \u001a\u00020\r2\u0006\u0010\t\u001a\u00020\nH\u0002J\u0010\u0010!\u001a\u00020\r2\u0006\u0010\"\u001a\u00020\nH\u0002J\u0010\u0010#\u001a\u00020\r2\u0006\u0010\u0010\u001a\u00020\nH\u0002J\u0010\u0010$\u001a\u00020\r2\u0006\u0010\t\u001a\u00020\nH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006&"}, d2 = {"Lcom/netizen/medicom/view/fragment/doctorCorner/FragmentDoctorCornerAssignFour;", "Landroidx/fragment/app/Fragment;", "()V", "binding", "Lcom/netizen/medicom/databinding/FragmentDoctorCornerAssignFourBinding;", "homeViewModel", "Lcom/netizen/medicom/viewModel/HomeViewModel;", "profileViewModel", "Lcom/netizen/medicom/viewModel/ProfileViewModel;", "user", "Lcom/quickblox/users/model/QBUser;", "createUserWithEnteredData", "initObservables", "", "initViews", "loginToChat", "qbUser", "onActivityResult", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "signInCreatedUser", "signUpNewUser", "newUser", "startLoginService", "updateUserOnServer", "Companion", "app_debug"})
public final class FragmentDoctorCornerAssignFour extends androidx.fragment.app.Fragment {
    private com.netizen.medicom.databinding.FragmentDoctorCornerAssignFourBinding binding;
    private com.netizen.medicom.viewModel.ProfileViewModel profileViewModel;
    private com.netizen.medicom.viewModel.HomeViewModel homeViewModel;
    private com.quickblox.users.model.QBUser user;
    @org.jetbrains.annotations.NotNull()
    private static java.lang.String userFullName = "";
    @org.jetbrains.annotations.NotNull()
    private static java.lang.String userLoginNetiID = "";
    public static final com.netizen.medicom.view.fragment.doctorCorner.FragmentDoctorCornerAssignFour.Companion Companion = null;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    public void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    private final void initViews() {
    }
    
    private final void initObservables() {
    }
    
    private final com.quickblox.users.model.QBUser createUserWithEnteredData() {
        return null;
    }
    
    private final void signUpNewUser(com.quickblox.users.model.QBUser newUser) {
    }
    
    private final void loginToChat(com.quickblox.users.model.QBUser qbUser) {
    }
    
    private final void signInCreatedUser(com.quickblox.users.model.QBUser user) {
    }
    
    private final void updateUserOnServer(com.quickblox.users.model.QBUser user) {
    }
    
    private final void startLoginService(com.quickblox.users.model.QBUser qbUser) {
    }
    
    @java.lang.Override()
    public void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    public FragmentDoctorCornerAssignFour() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u0006\"\u0004\b\u000b\u0010\b\u00a8\u0006\f"}, d2 = {"Lcom/netizen/medicom/view/fragment/doctorCorner/FragmentDoctorCornerAssignFour$Companion;", "", "()V", "userFullName", "", "getUserFullName", "()Ljava/lang/String;", "setUserFullName", "(Ljava/lang/String;)V", "userLoginNetiID", "getUserLoginNetiID", "setUserLoginNetiID", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getUserFullName() {
            return null;
        }
        
        public final void setUserFullName(@org.jetbrains.annotations.NotNull()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getUserLoginNetiID() {
            return null;
        }
        
        public final void setUserLoginNetiID(@org.jetbrains.annotations.NotNull()
        java.lang.String p0) {
        }
        
        private Companion() {
            super();
        }
    }
}