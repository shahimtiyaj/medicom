package com.netizen.medicom.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0004J\u0014\u0010\u0007\u001a\u00020\b2\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004R\u0016\u0010\u0003\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lcom/netizen/medicom/model/MobileBankList;", "", "()V", "moblieBankAccountInfos", "", "Lcom/netizen/medicom/model/BankAccountInfo;", "getBankLIst", "setBankList", "", "app_debug"})
public final class MobileBankList {
    private java.util.List<com.netizen.medicom.model.BankAccountInfo> moblieBankAccountInfos;
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.netizen.medicom.model.BankAccountInfo> getBankLIst() {
        return null;
    }
    
    public final void setBankList(@org.jetbrains.annotations.NotNull()
    java.util.List<com.netizen.medicom.model.BankAccountInfo> moblieBankAccountInfos) {
    }
    
    public MobileBankList() {
        super();
    }
}