package com.netizen.medicom.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001:\u0001\u000fB\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\b\u001a\u0004\u0018\u00010\u0004J\b\u0010\t\u001a\u0004\u0018\u00010\u0004J\b\u0010\n\u001a\u0004\u0018\u00010\u0007J\u0010\u0010\u000b\u001a\u00020\f2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\r\u001a\u00020\f2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u000e\u001a\u00020\f2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"}, d2 = {"Lcom/netizen/medicom/model/WithdrawPostData;", "", "()V", "requestNote", "", "requestedAmount", "userBankAccountInfoDTO", "Lcom/netizen/medicom/model/WithdrawPostData$UserBankAccountInfoDTO;", "getRequestNote", "getRequestedAmount", "getUserBankAccountInfoDTO", "setRequestNote", "", "setRequestedAmount", "setUserBankAccountInfoDTO", "UserBankAccountInfoDTO", "app_debug"})
public final class WithdrawPostData {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "requestedAmount")
    private java.lang.String requestedAmount;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "requestNote")
    private java.lang.String requestNote;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "userBankAccountInfoDTO")
    private com.netizen.medicom.model.WithdrawPostData.UserBankAccountInfoDTO userBankAccountInfoDTO;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getRequestedAmount() {
        return null;
    }
    
    public final void setRequestedAmount(@org.jetbrains.annotations.Nullable()
    java.lang.String requestedAmount) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getRequestNote() {
        return null;
    }
    
    public final void setRequestNote(@org.jetbrains.annotations.Nullable()
    java.lang.String requestNote) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.medicom.model.WithdrawPostData.UserBankAccountInfoDTO getUserBankAccountInfoDTO() {
        return null;
    }
    
    public final void setUserBankAccountInfoDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.medicom.model.WithdrawPostData.UserBankAccountInfoDTO userBankAccountInfoDTO) {
    }
    
    public WithdrawPostData() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\r\u0010\u0006\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0007J\u0015\u0010\b\u001a\u00020\t2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\nR\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005\u00a8\u0006\u000b"}, d2 = {"Lcom/netizen/medicom/model/WithdrawPostData$UserBankAccountInfoDTO;", "", "()V", "userBankAccId", "", "Ljava/lang/Integer;", "getUserBankAccId", "()Ljava/lang/Integer;", "setUserBankAccId", "", "(Ljava/lang/Integer;)V", "app_debug"})
    public static final class UserBankAccountInfoDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "userBankAccId")
        private java.lang.Integer userBankAccId;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getUserBankAccId() {
            return null;
        }
        
        public final void setUserBankAccId(@org.jetbrains.annotations.Nullable()
        java.lang.Integer userBankAccId) {
        }
        
        public UserBankAccountInfoDTO() {
            super();
        }
    }
}