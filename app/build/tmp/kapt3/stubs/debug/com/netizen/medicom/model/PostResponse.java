package com.netizen.medicom.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0015\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0018\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0019\u001a\u00020\u001a2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004R \u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR \u0010\t\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u0006\"\u0004\b\u000b\u0010\bR \u0010\f\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u0006\"\u0004\b\u000e\u0010\bR \u0010\u000f\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0006\"\u0004\b\u0011\u0010\bR \u0010\u0012\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0006\"\u0004\b\u0014\u0010\bR \u0010\u0015\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0006\"\u0004\b\u0017\u0010\b\u00a8\u0006\u001b"}, d2 = {"Lcom/netizen/medicom/model/PostResponse;", "", "()V", "access_token", "", "getAccess_token", "()Ljava/lang/String;", "setAccess_token", "(Ljava/lang/String;)V", "expires_in", "getExpires_in", "setExpires_in", "jti", "getJti", "setJti", "refresh_token", "getRefresh_token", "setRefresh_token", "scope", "getScope", "setScope", "token_type", "getToken_type", "setToken_type", "getToken", "setToken", "", "app_debug"})
public final class PostResponse {
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "access_token")
    private java.lang.String access_token = "";
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "token_type")
    private java.lang.String token_type = "";
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "refresh_token")
    private java.lang.String refresh_token = "";
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "expires_in")
    private java.lang.String expires_in = "";
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "scope")
    private java.lang.String scope = "";
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "jti")
    private java.lang.String jti = "";
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAccess_token() {
        return null;
    }
    
    public final void setAccess_token(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getToken_type() {
        return null;
    }
    
    public final void setToken_type(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getRefresh_token() {
        return null;
    }
    
    public final void setRefresh_token(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getExpires_in() {
        return null;
    }
    
    public final void setExpires_in(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getScope() {
        return null;
    }
    
    public final void setScope(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getJti() {
        return null;
    }
    
    public final void setJti(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getToken() {
        return null;
    }
    
    public final void setToken(@org.jetbrains.annotations.Nullable()
    java.lang.String access_token) {
    }
    
    public PostResponse() {
        super();
    }
}