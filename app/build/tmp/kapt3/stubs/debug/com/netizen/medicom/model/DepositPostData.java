package com.netizen.medicom.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0012\n\u0002\u0010\u0002\n\u0002\b\u000b\u0018\u00002\u00020\u0001:\u0001\'B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0011\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0012\u001a\u0004\u0018\u00010\u0004J\r\u0010\u0013\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0014J\b\u0010\u0015\u001a\u0004\u0018\u00010\nJ\b\u0010\u0016\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0017\u001a\u0004\u0018\u00010\u0001J\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001J\b\u0010\u0019\u001a\u0004\u0018\u00010\u0004J\b\u0010\u001a\u001a\u0004\u0018\u00010\u0004J\b\u0010\u001b\u001a\u0004\u0018\u00010\u0004J\u000e\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u0003\u001a\u00020\u0004J\u000e\u0010\u001e\u001a\u00020\u001d2\u0006\u0010\u0005\u001a\u00020\u0004J\u000e\u0010\u001f\u001a\u00020\u001d2\u0006\u0010\u0006\u001a\u00020\u0007J\u000e\u0010 \u001a\u00020\u001d2\u0006\u0010\t\u001a\u00020\nJ\u000e\u0010!\u001a\u00020\u001d2\u0006\u0010\u000b\u001a\u00020\u0004J\u000e\u0010\"\u001a\u00020\u001d2\u0006\u0010\f\u001a\u00020\u0001J\u000e\u0010#\u001a\u00020\u001d2\u0006\u0010\r\u001a\u00020\u0001J\u000e\u0010$\u001a\u00020\u001d2\u0006\u0010\u000e\u001a\u00020\u0004J\u000e\u0010%\u001a\u00020\u001d2\u0006\u0010\u000f\u001a\u00020\u0004J\u000e\u0010&\u001a\u00020\u001d2\u0006\u0010\u0010\u001a\u00020\u0004R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\bR\u0014\u0010\t\u001a\u0004\u0018\u00010\n8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006("}, d2 = {"Lcom/netizen/medicom/model/DepositPostData;", "", "()V", "attachFileContent", "", "attachFileName", "attachFileSaveOrEditable", "", "Ljava/lang/Boolean;", "coreBankAccountInfoDTO", "Lcom/netizen/medicom/model/DepositPostData$CoreBankAccountInfoDTO;", "fromWhere", "paymentType", "requestNote", "requestedAmount", "transactionDate", "transactionNumber", "getAttachFileContent", "getAttachFileName", "getAttachFileSaveOrEditable", "()Ljava/lang/Boolean;", "getCoreBankAccountInfoDTO", "getFromWhere", "getPaymentType", "getRequestNote", "getRequestedAmount", "getTransactionDate", "getTransactionNumber", "setAttachFileContent", "", "setAttachFileName", "setAttachFileSaveOrEditable", "setCoreBankAccountInfoDTO", "setFromWhere", "setPaymentType", "setRequestNote", "setRequestedAmount", "setTransactionDate", "setTransactionNumber", "CoreBankAccountInfoDTO", "app_debug"})
public final class DepositPostData {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "attachFileContent")
    private java.lang.String attachFileContent;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "attachFileName")
    private java.lang.String attachFileName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "attachFileSaveOrEditable")
    private java.lang.Boolean attachFileSaveOrEditable;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "coreBankAccountInfoDTO")
    private com.netizen.medicom.model.DepositPostData.CoreBankAccountInfoDTO coreBankAccountInfoDTO;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "requestedAmount")
    private java.lang.String requestedAmount;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "requestNote")
    private java.lang.Object requestNote;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "fromWhere")
    private java.lang.String fromWhere;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "transactionNumber")
    private java.lang.String transactionNumber;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "transactionDate")
    private java.lang.String transactionDate;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "paymentType")
    private java.lang.Object paymentType;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAttachFileContent() {
        return null;
    }
    
    public final void setAttachFileContent(@org.jetbrains.annotations.NotNull()
    java.lang.String attachFileContent) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAttachFileName() {
        return null;
    }
    
    public final void setAttachFileName(@org.jetbrains.annotations.NotNull()
    java.lang.String attachFileName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean getAttachFileSaveOrEditable() {
        return null;
    }
    
    public final void setAttachFileSaveOrEditable(boolean attachFileSaveOrEditable) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.medicom.model.DepositPostData.CoreBankAccountInfoDTO getCoreBankAccountInfoDTO() {
        return null;
    }
    
    public final void setCoreBankAccountInfoDTO(@org.jetbrains.annotations.NotNull()
    com.netizen.medicom.model.DepositPostData.CoreBankAccountInfoDTO coreBankAccountInfoDTO) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getRequestedAmount() {
        return null;
    }
    
    public final void setRequestedAmount(@org.jetbrains.annotations.NotNull()
    java.lang.String requestedAmount) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getRequestNote() {
        return null;
    }
    
    public final void setRequestNote(@org.jetbrains.annotations.NotNull()
    java.lang.Object requestNote) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getFromWhere() {
        return null;
    }
    
    public final void setFromWhere(@org.jetbrains.annotations.NotNull()
    java.lang.String fromWhere) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTransactionNumber() {
        return null;
    }
    
    public final void setTransactionNumber(@org.jetbrains.annotations.NotNull()
    java.lang.String transactionNumber) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTransactionDate() {
        return null;
    }
    
    public final void setTransactionDate(@org.jetbrains.annotations.NotNull()
    java.lang.String transactionDate) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getPaymentType() {
        return null;
    }
    
    public final void setPaymentType(@org.jetbrains.annotations.NotNull()
    java.lang.Object paymentType) {
    }
    
    public DepositPostData() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\r\u0010\u0006\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0007J\u0015\u0010\b\u001a\u00020\t2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\nR\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005\u00a8\u0006\u000b"}, d2 = {"Lcom/netizen/medicom/model/DepositPostData$CoreBankAccountInfoDTO;", "", "()V", "coreBankAccId", "", "Ljava/lang/Integer;", "getCoreBankAccId", "()Ljava/lang/Integer;", "setCoreBankAccId", "", "(Ljava/lang/Integer;)V", "app_debug"})
    public static final class CoreBankAccountInfoDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "coreBankAccId")
        private java.lang.Integer coreBankAccId;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getCoreBankAccId() {
            return null;
        }
        
        public final void setCoreBankAccId(@org.jetbrains.annotations.Nullable()
        java.lang.Integer coreBankAccId) {
        }
        
        public CoreBankAccountInfoDTO() {
            super();
        }
    }
}