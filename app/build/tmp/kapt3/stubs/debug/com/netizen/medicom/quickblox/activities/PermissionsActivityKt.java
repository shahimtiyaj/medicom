package com.netizen.medicom.quickblox.activities;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"CHECK_ONLY_AUDIO", "", "EXTRA_PERMISSIONS", "PERMISSION_CODE", "", "app_debug"})
public final class PermissionsActivityKt {
    private static final int PERMISSION_CODE = 0;
    private static final java.lang.String EXTRA_PERMISSIONS = "extra_permissions";
    private static final java.lang.String CHECK_ONLY_AUDIO = "check_only_audio";
}