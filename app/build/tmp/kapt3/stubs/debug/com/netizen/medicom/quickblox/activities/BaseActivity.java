package com.netizen.medicom.quickblox.activities;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b&\u0018\u00002\u00020\u0001:\u0001\u001cB\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0004J\u001b\u0010\t\u001a\u00020\u00062\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\b0\u000bH\u0004\u00a2\u0006\u0002\u0010\fJ\r\u0010\r\u001a\u00020\u000eH\u0000\u00a2\u0006\u0002\b\u000fJ\b\u0010\u0010\u001a\u00020\u000eH\u0014J&\u0010\u0011\u001a\u00020\u000e2\b\b\u0001\u0010\u0012\u001a\u00020\u00132\n\u0010\u0014\u001a\u00060\u0015j\u0002`\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0004J\u0017\u0010\u0019\u001a\u00020\u000e2\b\b\u0001\u0010\u001a\u001a\u00020\u0013H\u0000\u00a2\u0006\u0002\b\u001bR\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"}, d2 = {"Lcom/netizen/medicom/quickblox/activities/BaseActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "()V", "progressDialog", "Landroid/app/ProgressDialog;", "checkPermission", "", "permission", "", "checkPermissions", "permissions", "", "([Ljava/lang/String;)Z", "hideProgressDialog", "", "hideProgressDialog$app_debug", "onPause", "showErrorSnackbar", "resId", "", "e", "Ljava/lang/Exception;", "Lkotlin/Exception;", "clickListener", "Landroid/view/View$OnClickListener;", "showProgressDialog", "messageId", "showProgressDialog$app_debug", "KeyEventListener", "app_debug"})
public abstract class BaseActivity extends androidx.appcompat.app.AppCompatActivity {
    private android.app.ProgressDialog progressDialog;
    private java.util.HashMap _$_findViewCache;
    
    public final void showProgressDialog$app_debug(@androidx.annotation.StringRes()
    int messageId) {
    }
    
    @java.lang.Override()
    protected void onPause() {
    }
    
    public final void hideProgressDialog$app_debug() {
    }
    
    protected final void showErrorSnackbar(@androidx.annotation.StringRes()
    int resId, @org.jetbrains.annotations.NotNull()
    java.lang.Exception e, @org.jetbrains.annotations.NotNull()
    android.view.View.OnClickListener clickListener) {
    }
    
    protected final boolean checkPermissions(@org.jetbrains.annotations.NotNull()
    java.lang.String[] permissions) {
        return false;
    }
    
    protected final boolean checkPermission(@org.jetbrains.annotations.NotNull()
    java.lang.String permission) {
        return false;
    }
    
    public BaseActivity() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J$\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0007\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\nH\u0016\u00a8\u0006\u000b"}, d2 = {"Lcom/netizen/medicom/quickblox/activities/BaseActivity$KeyEventListener;", "Landroid/content/DialogInterface$OnKeyListener;", "(Lcom/netizen/medicom/quickblox/activities/BaseActivity;)V", "onKey", "", "dialog", "Landroid/content/DialogInterface;", "keyCode", "", "keyEvent", "Landroid/view/KeyEvent;", "app_debug"})
    public final class KeyEventListener implements android.content.DialogInterface.OnKeyListener {
        
        @java.lang.Override()
        public boolean onKey(@org.jetbrains.annotations.Nullable()
        android.content.DialogInterface dialog, int keyCode, @org.jetbrains.annotations.Nullable()
        android.view.KeyEvent keyEvent) {
            return false;
        }
        
        public KeyEventListener() {
            super();
        }
    }
}