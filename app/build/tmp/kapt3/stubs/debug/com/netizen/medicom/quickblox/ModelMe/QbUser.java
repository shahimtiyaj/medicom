package com.netizen.medicom.quickblox.ModelMe;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0002\u0016\u0017B\u0005\u00a2\u0006\u0002\u0010\u0002J\r\u0010\u000b\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\fJ\u0010\u0010\r\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\b\u0018\u00010\u0007J\r\u0010\u000e\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\fJ\r\u0010\u000f\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\fJ\u0015\u0010\u0010\u001a\u00020\u00112\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0012J\u0018\u0010\u0013\u001a\u00020\u00112\u0010\u0010\u0006\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\b\u0018\u00010\u0007J\u0015\u0010\u0014\u001a\u00020\u00112\b\u0010\t\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0012J\u0015\u0010\u0015\u001a\u00020\u00112\b\u0010\n\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0012R\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u001c\u0010\u0006\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\b\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\t\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u0016\u0010\n\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005\u00a8\u0006\u0018"}, d2 = {"Lcom/netizen/medicom/quickblox/ModelMe/QbUser;", "", "()V", "currentPage", "", "Ljava/lang/Integer;", "items", "", "Lcom/netizen/medicom/quickblox/ModelMe/QbUser$Item;", "perPage", "totalEntries", "getCurrentPage", "()Ljava/lang/Integer;", "getItems", "getPerPage", "getTotalEntries", "setCurrentPage", "", "(Ljava/lang/Integer;)V", "setItems", "setPerPage", "setTotalEntries", "Item", "User", "app_debug"})
public final class QbUser {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "current_page")
    private java.lang.Integer currentPage;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "per_page")
    private java.lang.Integer perPage;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "total_entries")
    private java.lang.Integer totalEntries;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "items")
    private java.util.List<com.netizen.medicom.quickblox.ModelMe.QbUser.Item> items;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getCurrentPage() {
        return null;
    }
    
    public final void setCurrentPage(@org.jetbrains.annotations.Nullable()
    java.lang.Integer currentPage) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getPerPage() {
        return null;
    }
    
    public final void setPerPage(@org.jetbrains.annotations.Nullable()
    java.lang.Integer perPage) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getTotalEntries() {
        return null;
    }
    
    public final void setTotalEntries(@org.jetbrains.annotations.Nullable()
    java.lang.Integer totalEntries) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.netizen.medicom.quickblox.ModelMe.QbUser.Item> getItems() {
        return null;
    }
    
    public final void setItems(@org.jetbrains.annotations.Nullable()
    java.util.List<com.netizen.medicom.quickblox.ModelMe.QbUser.Item> items) {
    }
    
    public QbUser() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R \u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\t"}, d2 = {"Lcom/netizen/medicom/quickblox/ModelMe/QbUser$Item;", "", "()V", "user", "Lcom/netizen/medicom/quickblox/ModelMe/QbUser$User;", "getUser", "()Lcom/netizen/medicom/quickblox/ModelMe/QbUser$User;", "setUser", "(Lcom/netizen/medicom/quickblox/ModelMe/QbUser$User;)V", "app_debug"})
    public static final class Item {
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "user")
        private com.netizen.medicom.quickblox.ModelMe.QbUser.User user;
        
        @org.jetbrains.annotations.Nullable()
        public final com.netizen.medicom.quickblox.ModelMe.QbUser.User getUser() {
            return null;
        }
        
        public final void setUser(@org.jetbrains.annotations.Nullable()
        com.netizen.medicom.quickblox.ModelMe.QbUser.User p0) {
        }
        
        public Item() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u001d\n\u0002\u0010\u0002\n\u0002\b\u0013\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\r\u0010\u0019\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u001aJ\b\u0010\u001b\u001a\u0004\u0018\u00010\u0001J\b\u0010\u001c\u001a\u0004\u0018\u00010\bJ\b\u0010\u001d\u001a\u0004\u0018\u00010\u0001J\b\u0010\u001e\u001a\u0004\u0018\u00010\bJ\b\u0010\u001f\u001a\u0004\u0018\u00010\u0001J\b\u0010 \u001a\u0004\u0018\u00010\u0001J\b\u0010!\u001a\u0004\u0018\u00010\bJ\r\u0010\"\u001a\u0004\u0018\u00010\u000f\u00a2\u0006\u0002\u0010#J\b\u0010$\u001a\u0004\u0018\u00010\bJ\b\u0010%\u001a\u0004\u0018\u00010\bJ\b\u0010&\u001a\u0004\u0018\u00010\bJ\b\u0010\'\u001a\u0004\u0018\u00010\u0001J\b\u0010(\u001a\u0004\u0018\u00010\u0001J\b\u0010)\u001a\u0004\u0018\u00010\bJ\b\u0010*\u001a\u0004\u0018\u00010\u0001J\b\u0010+\u001a\u0004\u0018\u00010\u0001J\u0015\u0010,\u001a\u00020-2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010.J\u0010\u0010/\u001a\u00020-2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0001J\u0010\u00100\u001a\u00020-2\b\u0010\u0007\u001a\u0004\u0018\u00010\bJ\u0010\u00101\u001a\u00020-2\b\u0010\t\u001a\u0004\u0018\u00010\u0001J\u0010\u00102\u001a\u00020-2\b\u0010\n\u001a\u0004\u0018\u00010\bJ\u0010\u00103\u001a\u00020-2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0001J\u0010\u00104\u001a\u00020-2\b\u0010\f\u001a\u0004\u0018\u00010\u0001J\u0010\u00105\u001a\u00020-2\b\u0010\r\u001a\u0004\u0018\u00010\bJ\u0015\u00106\u001a\u00020-2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000f\u00a2\u0006\u0002\u00107J\u0010\u00108\u001a\u00020-2\b\u0010\u0011\u001a\u0004\u0018\u00010\bJ\u0010\u00109\u001a\u00020-2\b\u0010\u0012\u001a\u0004\u0018\u00010\bJ\u0010\u0010:\u001a\u00020-2\b\u0010\u0013\u001a\u0004\u0018\u00010\bJ\u0010\u0010;\u001a\u00020-2\b\u0010\u0014\u001a\u0004\u0018\u00010\u0001J\u0010\u0010<\u001a\u00020-2\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001J\u0010\u0010=\u001a\u00020-2\b\u0010\u0016\u001a\u0004\u0018\u00010\bJ\u0010\u0010>\u001a\u00020-2\b\u0010\u0017\u001a\u0004\u0018\u00010\u0001J\u0010\u0010?\u001a\u00020-2\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001R\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000e\u001a\u0004\u0018\u00010\u000f8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0010R\u0014\u0010\u0011\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0015\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0016\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0017\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0018\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006@"}, d2 = {"Lcom/netizen/medicom/quickblox/ModelMe/QbUser$User;", "", "()V", "ageOver16", "", "Ljava/lang/Boolean;", "blobId", "createdAt", "", "customData", "email", "externalUserId", "facebookId", "fullName", "id", "", "Ljava/lang/Integer;", "lastRequestAt", "login", "parentsContacts", "phone", "twitterId", "updatedAt", "userTags", "website", "getAgeOver16", "()Ljava/lang/Boolean;", "getBlobId", "getCreatedAt", "getCustomData", "getEmail", "getExternalUserId", "getFacebookId", "getFullName", "getId", "()Ljava/lang/Integer;", "getLastRequestAt", "getLogin", "getParentsContacts", "getPhone", "getTwitterId", "getUpdatedAt", "getUserTags", "getWebsite", "setAgeOver16", "", "(Ljava/lang/Boolean;)V", "setBlobId", "setCreatedAt", "setCustomData", "setEmail", "setExternalUserId", "setFacebookId", "setFullName", "setId", "(Ljava/lang/Integer;)V", "setLastRequestAt", "setLogin", "setParentsContacts", "setPhone", "setTwitterId", "setUpdatedAt", "setUserTags", "setWebsite", "app_debug"})
    public static final class User {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "id")
        private java.lang.Integer id;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "full_name")
        private java.lang.String fullName;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "email")
        private java.lang.String email;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "login")
        private java.lang.String login;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "phone")
        private java.lang.Object phone;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "website")
        private java.lang.Object website;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "created_at")
        private java.lang.String createdAt;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "updated_at")
        private java.lang.String updatedAt;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "last_request_at")
        private java.lang.String lastRequestAt;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "external_user_id")
        private java.lang.Object externalUserId;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "facebook_id")
        private java.lang.Object facebookId;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "twitter_id")
        private java.lang.Object twitterId;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "blob_id")
        private java.lang.Object blobId;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "custom_data")
        private java.lang.Object customData;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "age_over16")
        private java.lang.Boolean ageOver16;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "parents_contacts")
        private java.lang.String parentsContacts;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "user_tags")
        private java.lang.Object userTags;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getId() {
            return null;
        }
        
        public final void setId(@org.jetbrains.annotations.Nullable()
        java.lang.Integer id) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getFullName() {
            return null;
        }
        
        public final void setFullName(@org.jetbrains.annotations.Nullable()
        java.lang.String fullName) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getEmail() {
            return null;
        }
        
        public final void setEmail(@org.jetbrains.annotations.Nullable()
        java.lang.String email) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getLogin() {
            return null;
        }
        
        public final void setLogin(@org.jetbrains.annotations.Nullable()
        java.lang.String login) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Object getPhone() {
            return null;
        }
        
        public final void setPhone(@org.jetbrains.annotations.Nullable()
        java.lang.Object phone) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Object getWebsite() {
            return null;
        }
        
        public final void setWebsite(@org.jetbrains.annotations.Nullable()
        java.lang.Object website) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCreatedAt() {
            return null;
        }
        
        public final void setCreatedAt(@org.jetbrains.annotations.Nullable()
        java.lang.String createdAt) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getUpdatedAt() {
            return null;
        }
        
        public final void setUpdatedAt(@org.jetbrains.annotations.Nullable()
        java.lang.String updatedAt) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getLastRequestAt() {
            return null;
        }
        
        public final void setLastRequestAt(@org.jetbrains.annotations.Nullable()
        java.lang.String lastRequestAt) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Object getExternalUserId() {
            return null;
        }
        
        public final void setExternalUserId(@org.jetbrains.annotations.Nullable()
        java.lang.Object externalUserId) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Object getFacebookId() {
            return null;
        }
        
        public final void setFacebookId(@org.jetbrains.annotations.Nullable()
        java.lang.Object facebookId) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Object getTwitterId() {
            return null;
        }
        
        public final void setTwitterId(@org.jetbrains.annotations.Nullable()
        java.lang.Object twitterId) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Object getBlobId() {
            return null;
        }
        
        public final void setBlobId(@org.jetbrains.annotations.Nullable()
        java.lang.Object blobId) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Object getCustomData() {
            return null;
        }
        
        public final void setCustomData(@org.jetbrains.annotations.Nullable()
        java.lang.Object customData) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Boolean getAgeOver16() {
            return null;
        }
        
        public final void setAgeOver16(@org.jetbrains.annotations.Nullable()
        java.lang.Boolean ageOver16) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getParentsContacts() {
            return null;
        }
        
        public final void setParentsContacts(@org.jetbrains.annotations.Nullable()
        java.lang.String parentsContacts) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Object getUserTags() {
            return null;
        }
        
        public final void setUserTags(@org.jetbrains.annotations.Nullable()
        java.lang.Object userTags) {
        }
        
        public User() {
            super();
        }
    }
}