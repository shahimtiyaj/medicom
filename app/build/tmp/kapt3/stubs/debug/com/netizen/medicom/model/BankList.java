package com.netizen.medicom.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\'\n\u0002\u0010\u0002\n\u0002\b\u0015\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u001a\u001a\u0004\u0018\u00010\u0004J\b\u0010\u001b\u001a\u0004\u0018\u00010\u0004J\b\u0010\u001c\u001a\u0004\u0018\u00010\u0004J\r\u0010\u001d\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010\u001eJ\r\u0010\u001f\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010\u001eJ\b\u0010 \u001a\u0004\u0018\u00010\u0004J\b\u0010!\u001a\u0004\u0018\u00010\u0004J\b\u0010\"\u001a\u0004\u0018\u00010\u0004J\r\u0010#\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010\u001eJ\b\u0010$\u001a\u0004\u0018\u00010\u0004J\r\u0010%\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010\u001eJ\b\u0010&\u001a\u0004\u0018\u00010\u0004J\r\u0010\'\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010\u001eJ\b\u0010(\u001a\u0004\u0018\u00010\u0004J\b\u0010)\u001a\u0004\u0018\u00010\u0004J\b\u0010*\u001a\u0004\u0018\u00010\u0004J\b\u0010+\u001a\u0004\u0018\u00010\u0001J\b\u0010,\u001a\u0004\u0018\u00010\u0001J\b\u0010-\u001a\u0004\u0018\u00010\u0001J\r\u0010.\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010\u001eJ\u0010\u0010/\u001a\u0002002\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u00101\u001a\u0002002\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0010\u00102\u001a\u0002002\b\u0010\u0006\u001a\u0004\u0018\u00010\u0004J\u0015\u00103\u001a\u0002002\b\u0010\u0007\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u00104J\u0015\u00105\u001a\u0002002\b\u0010\n\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u00104J\u0010\u00106\u001a\u0002002\b\u0010\u000b\u001a\u0004\u0018\u00010\u0004J\u0010\u00107\u001a\u0002002\b\u0010\f\u001a\u0004\u0018\u00010\u0004J\u0010\u00108\u001a\u0002002\b\u0010\r\u001a\u0004\u0018\u00010\u0004J\u0015\u00109\u001a\u0002002\b\u0010\u000e\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u00104J\u0010\u0010:\u001a\u0002002\b\u0010\u000f\u001a\u0004\u0018\u00010\u0004J\u0015\u0010;\u001a\u0002002\b\u0010\u0010\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u00104J\u0010\u0010<\u001a\u0002002\b\u0010\u0011\u001a\u0004\u0018\u00010\u0004J\u0015\u0010=\u001a\u0002002\b\u0010\u0012\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u00104J\u0010\u0010>\u001a\u0002002\b\u0010\u0013\u001a\u0004\u0018\u00010\u0004J\u0010\u0010?\u001a\u0002002\b\u0010\u0014\u001a\u0004\u0018\u00010\u0004J\u0010\u0010@\u001a\u0002002\b\u0010\u0015\u001a\u0004\u0018\u00010\u0004J\u0010\u0010A\u001a\u0002002\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001J\u0010\u0010B\u001a\u0002002\b\u0010\u0017\u001a\u0004\u0018\u00010\u0001J\u0010\u0010C\u001a\u0002002\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001J\u0015\u0010D\u001a\u0002002\b\u0010\u0019\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u00104R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\tR\u0016\u0010\n\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\tR\u0014\u0010\u000b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000e\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\tR\u0014\u0010\u000f\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0010\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\tR\u0014\u0010\u0011\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0012\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\tR\u0014\u0010\u0013\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0015\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0016\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0017\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0018\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0019\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\t\u00a8\u0006E"}, d2 = {"Lcom/netizen/medicom/model/BankList;", "", "()V", "accNumber", "", "accountDetail", "bank", "bankID", "", "Ljava/lang/Integer;", "branchID", "branchName", "chequeSlipName", "chequeSlipPath", "districtID", "districtName", "enableStatus", "holderName", "netiID", "othersAttachmentName", "othersAttachmentPath", "routingNumber", "taggingDate", "taggingID", "taggingTypeDefaultCodes", "userBankAccountID", "getAccNumber", "getAccountDetail", "getBank", "getBankID", "()Ljava/lang/Integer;", "getBranchID", "getBranchName", "getChequeSlipName", "getChequeSlipPath", "getDistrictID", "getDistrictName", "getEnableStatus", "getHolderName", "getNetiID", "getOthersAttachmentName", "getOthersAttachmentPath", "getRoutingNumber", "getTaggingDate", "getTaggingID", "getTaggingTypeDefaultCodes", "getUserBankAccountID", "setAccNumber", "", "setAccountDetail", "setBank", "setBankID", "(Ljava/lang/Integer;)V", "setBranchID", "setBranchName", "setChequeSlipName", "setChequeSlipPath", "setDistrictID", "setDistrictName", "setEnableStatus", "setHolderName", "setNetiID", "setOthersAttachmentName", "setOthersAttachmentPath", "setRoutingNumber", "setTaggingDate", "setTaggingID", "setTaggingTypeDefaultCodes", "setUserBankAccountID", "app_debug"})
public final class BankList {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "bank")
    private java.lang.String bank;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "bankID")
    private java.lang.Integer bankID;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "accNumber")
    private java.lang.String accNumber;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "holderName")
    private java.lang.String holderName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "accountDetail")
    private java.lang.String accountDetail;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "branchID")
    private java.lang.Integer branchID;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "branchName")
    private java.lang.String branchName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "districtID")
    private java.lang.Integer districtID;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "districtName")
    private java.lang.String districtName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "routingNumber")
    private java.lang.String routingNumber;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "chequeSlipPath")
    private java.lang.String chequeSlipPath;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "chequeSlipName")
    private java.lang.String chequeSlipName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "othersAttachmentName")
    private java.lang.String othersAttachmentName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "othersAttachmentPath")
    private java.lang.String othersAttachmentPath;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "enableStatus")
    private java.lang.Integer enableStatus;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "taggingID")
    private java.lang.Object taggingID;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "taggingDate")
    private java.lang.Object taggingDate;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "userBankAccountID")
    private java.lang.Integer userBankAccountID;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "netiID")
    private java.lang.Integer netiID;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "taggingTypeDefaultCodes")
    private java.lang.Object taggingTypeDefaultCodes;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getBank() {
        return null;
    }
    
    public final void setBank(@org.jetbrains.annotations.Nullable()
    java.lang.String bank) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getBankID() {
        return null;
    }
    
    public final void setBankID(@org.jetbrains.annotations.Nullable()
    java.lang.Integer bankID) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAccNumber() {
        return null;
    }
    
    public final void setAccNumber(@org.jetbrains.annotations.Nullable()
    java.lang.String accNumber) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getHolderName() {
        return null;
    }
    
    public final void setHolderName(@org.jetbrains.annotations.Nullable()
    java.lang.String holderName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAccountDetail() {
        return null;
    }
    
    public final void setAccountDetail(@org.jetbrains.annotations.Nullable()
    java.lang.String accountDetail) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getBranchID() {
        return null;
    }
    
    public final void setBranchID(@org.jetbrains.annotations.Nullable()
    java.lang.Integer branchID) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getBranchName() {
        return null;
    }
    
    public final void setBranchName(@org.jetbrains.annotations.Nullable()
    java.lang.String branchName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getDistrictID() {
        return null;
    }
    
    public final void setDistrictID(@org.jetbrains.annotations.Nullable()
    java.lang.Integer districtID) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDistrictName() {
        return null;
    }
    
    public final void setDistrictName(@org.jetbrains.annotations.Nullable()
    java.lang.String districtName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getRoutingNumber() {
        return null;
    }
    
    public final void setRoutingNumber(@org.jetbrains.annotations.Nullable()
    java.lang.String routingNumber) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getChequeSlipPath() {
        return null;
    }
    
    public final void setChequeSlipPath(@org.jetbrains.annotations.Nullable()
    java.lang.String chequeSlipPath) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getChequeSlipName() {
        return null;
    }
    
    public final void setChequeSlipName(@org.jetbrains.annotations.Nullable()
    java.lang.String chequeSlipName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getOthersAttachmentName() {
        return null;
    }
    
    public final void setOthersAttachmentName(@org.jetbrains.annotations.Nullable()
    java.lang.String othersAttachmentName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getOthersAttachmentPath() {
        return null;
    }
    
    public final void setOthersAttachmentPath(@org.jetbrains.annotations.Nullable()
    java.lang.String othersAttachmentPath) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getEnableStatus() {
        return null;
    }
    
    public final void setEnableStatus(@org.jetbrains.annotations.Nullable()
    java.lang.Integer enableStatus) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getTaggingID() {
        return null;
    }
    
    public final void setTaggingID(@org.jetbrains.annotations.Nullable()
    java.lang.Object taggingID) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getTaggingDate() {
        return null;
    }
    
    public final void setTaggingDate(@org.jetbrains.annotations.Nullable()
    java.lang.Object taggingDate) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getUserBankAccountID() {
        return null;
    }
    
    public final void setUserBankAccountID(@org.jetbrains.annotations.Nullable()
    java.lang.Integer userBankAccountID) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getNetiID() {
        return null;
    }
    
    public final void setNetiID(@org.jetbrains.annotations.Nullable()
    java.lang.Integer netiID) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getTaggingTypeDefaultCodes() {
        return null;
    }
    
    public final void setTaggingTypeDefaultCodes(@org.jetbrains.annotations.Nullable()
    java.lang.Object taggingTypeDefaultCodes) {
    }
    
    public BankList() {
        super();
    }
}