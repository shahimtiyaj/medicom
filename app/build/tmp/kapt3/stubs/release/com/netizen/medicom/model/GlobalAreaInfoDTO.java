package com.netizen.medicom.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u001b\n\u0002\u0010\u0002\n\u0002\b\u000f\u0018\u00002\u00020\u0001:\u00010B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0013\u001a\u0004\u0018\u00010\u0004J\r\u0010\u0014\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0015J\b\u0010\u0016\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0017\u001a\u0004\u0018\u00010\u0001J\r\u0010\u0018\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0015J\r\u0010\u0019\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0015J\r\u0010\u001a\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0015J\b\u0010\u001b\u001a\u0004\u0018\u00010\u0004J\b\u0010\u001c\u001a\u0004\u0018\u00010\u0004J\b\u0010\u001d\u001a\u0004\u0018\u00010\u0001J\r\u0010\u001e\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0015J\b\u0010\u001f\u001a\u0004\u0018\u00010\u0001J\r\u0010 \u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0015J\u000e\u0010!\u001a\u00020\"2\u0006\u0010\u0003\u001a\u00020\u0004J\u0015\u0010#\u001a\u00020\"2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010$J\u000e\u0010%\u001a\u00020\"2\u0006\u0010\b\u001a\u00020\u0004J\u000e\u0010&\u001a\u00020\"2\u0006\u0010\t\u001a\u00020\u0001J\u0015\u0010\'\u001a\u00020\"2\b\u0010\n\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010$J\u0015\u0010(\u001a\u00020\"2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010$J\u0015\u0010)\u001a\u00020\"2\b\u0010\f\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010$J\u000e\u0010*\u001a\u00020\"2\u0006\u0010\r\u001a\u00020\u0004J\u000e\u0010+\u001a\u00020\"2\u0006\u0010\u000e\u001a\u00020\u0004J\u000e\u0010,\u001a\u00020\"2\u0006\u0010\u000f\u001a\u00020\u0001J\u0015\u0010-\u001a\u00020\"2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010$J\u000e\u0010.\u001a\u00020\"2\u0006\u0010\u0011\u001a\u00020\u0001J\u0015\u0010/\u001a\u00020\"2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010$R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0014\u0010\b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\n\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0016\u0010\u000b\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0016\u0010\f\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0014\u0010\r\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0010\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0014\u0010\u0011\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0012\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007\u00a8\u00061"}, d2 = {"Lcom/netizen/medicom/model/GlobalAreaInfoDTO;", "", "()V", "categoryDefaultCode", "", "categoryEnableStatus", "", "Ljava/lang/Integer;", "categoryName", "categoryNote", "categorySerial", "coreCategoryID", "lastDateExecuted", "lastIpExecuted", "lastUserExecuted", "parentCoreCategoryInfoDTO", "parentStatus", "parentTypeInfoDTO", "typeStatus", "getCategoryDefaultCode", "getCategoryEnableStatus", "()Ljava/lang/Integer;", "getCategoryName", "getCategoryNote", "getCategorySerial", "getCoreCategoryID", "getLastDateExecuted", "getLastIpExecuted", "getLastUserExecuted", "getParentCoreCategoryInfoDTO", "getParentStatus", "getParentTypeInfoDTO", "getTypeStatus", "setCategoryDefaultCode", "", "setCategoryEnableStatus", "(Ljava/lang/Integer;)V", "setCategoryName", "setCategoryNote", "setCategorySerial", "setCoreCategoryID", "setLastDateExecuted", "setLastIpExecuted", "setLastUserExecuted", "setParentCoreCategoryInfoDTO", "setParentStatus", "setParentTypeInfoDTO", "setTypeStatus", "Info", "app_release"})
public final class GlobalAreaInfoDTO {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "coreCategoryID")
    private java.lang.Integer coreCategoryID;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "categoryDefaultCode")
    private java.lang.String categoryDefaultCode;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "categoryName")
    private java.lang.String categoryName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "categoryNote")
    private java.lang.Object categoryNote;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "categoryEnableStatus")
    private java.lang.Integer categoryEnableStatus;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "categorySerial")
    private java.lang.Integer categorySerial;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "typeStatus")
    private java.lang.Integer typeStatus;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "parentStatus")
    private java.lang.Integer parentStatus;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "parentTypeInfoDTO")
    private java.lang.Object parentTypeInfoDTO;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "parentCoreCategoryInfoDTO")
    private java.lang.Object parentCoreCategoryInfoDTO;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "lastUserExecuted")
    private java.lang.String lastUserExecuted;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "lastIpExecuted")
    private java.lang.String lastIpExecuted;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "lastDateExecuted")
    private java.lang.Integer lastDateExecuted;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getCoreCategoryID() {
        return null;
    }
    
    public final void setCoreCategoryID(@org.jetbrains.annotations.Nullable()
    java.lang.Integer coreCategoryID) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCategoryDefaultCode() {
        return null;
    }
    
    public final void setCategoryDefaultCode(@org.jetbrains.annotations.NotNull()
    java.lang.String categoryDefaultCode) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCategoryName() {
        return null;
    }
    
    public final void setCategoryName(@org.jetbrains.annotations.NotNull()
    java.lang.String categoryName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getCategoryNote() {
        return null;
    }
    
    public final void setCategoryNote(@org.jetbrains.annotations.NotNull()
    java.lang.Object categoryNote) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getCategoryEnableStatus() {
        return null;
    }
    
    public final void setCategoryEnableStatus(@org.jetbrains.annotations.Nullable()
    java.lang.Integer categoryEnableStatus) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getCategorySerial() {
        return null;
    }
    
    public final void setCategorySerial(@org.jetbrains.annotations.Nullable()
    java.lang.Integer categorySerial) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getTypeStatus() {
        return null;
    }
    
    public final void setTypeStatus(@org.jetbrains.annotations.Nullable()
    java.lang.Integer typeStatus) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getParentStatus() {
        return null;
    }
    
    public final void setParentStatus(@org.jetbrains.annotations.Nullable()
    java.lang.Integer parentStatus) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getParentTypeInfoDTO() {
        return null;
    }
    
    public final void setParentTypeInfoDTO(@org.jetbrains.annotations.NotNull()
    java.lang.Object parentTypeInfoDTO) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getParentCoreCategoryInfoDTO() {
        return null;
    }
    
    public final void setParentCoreCategoryInfoDTO(@org.jetbrains.annotations.NotNull()
    java.lang.Object parentCoreCategoryInfoDTO) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getLastUserExecuted() {
        return null;
    }
    
    public final void setLastUserExecuted(@org.jetbrains.annotations.NotNull()
    java.lang.String lastUserExecuted) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getLastIpExecuted() {
        return null;
    }
    
    public final void setLastIpExecuted(@org.jetbrains.annotations.NotNull()
    java.lang.String lastIpExecuted) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getLastDateExecuted() {
        return null;
    }
    
    public final void setLastDateExecuted(@org.jetbrains.annotations.Nullable()
    java.lang.Integer lastDateExecuted) {
    }
    
    public GlobalAreaInfoDTO() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b-\n\u0002\u0010\u0002\n\u0002\b\u001d\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010%\u001a\u0004\u0018\u00010\u0004J\b\u0010&\u001a\u0004\u0018\u00010\u0004J\r\u0010\'\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010(J\b\u0010)\u001a\u0004\u0018\u00010\u0004J\r\u0010*\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010+J\b\u0010,\u001a\u0004\u0018\u00010\u0004J\b\u0010-\u001a\u0004\u0018\u00010\u0004J\b\u0010.\u001a\u0004\u0018\u00010\u0010J\b\u0010/\u001a\u0004\u0018\u00010\u0001J\b\u00100\u001a\u0004\u0018\u00010\u0004J\b\u00101\u001a\u0004\u0018\u00010\u0004J\r\u00102\u001a\u0004\u0018\u00010\u0015\u00a2\u0006\u0002\u00103J\r\u00104\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010(J\b\u00105\u001a\u0004\u0018\u00010\u0004J\b\u00106\u001a\u0004\u0018\u00010\u0004J\r\u00107\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010(J\r\u00108\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010(J\b\u00109\u001a\u0004\u0018\u00010\u0004J\r\u0010:\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010+J\r\u0010;\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010(J\b\u0010<\u001a\u0004\u0018\u00010\u0001J\b\u0010=\u001a\u0004\u0018\u00010\u0001J\r\u0010>\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010+J\r\u0010?\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010+J\r\u0010@\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010(J\r\u0010A\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010+J\u000e\u0010B\u001a\u00020C2\u0006\u0010\u0003\u001a\u00020\u0004J\u000e\u0010D\u001a\u00020C2\u0006\u0010\u0005\u001a\u00020\u0004J\u0015\u0010E\u001a\u00020C2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010FJ\u000e\u0010G\u001a\u00020C2\u0006\u0010\t\u001a\u00020\u0004J\u0015\u0010H\u001a\u00020C2\b\u0010\n\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010IJ\u000e\u0010J\u001a\u00020C2\u0006\u0010\r\u001a\u00020\u0004J\u000e\u0010K\u001a\u00020C2\u0006\u0010\u000e\u001a\u00020\u0004J\u000e\u0010L\u001a\u00020C2\u0006\u0010\u000f\u001a\u00020\u0010J\u000e\u0010M\u001a\u00020C2\u0006\u0010\u0011\u001a\u00020\u0001J\u000e\u0010N\u001a\u00020C2\u0006\u0010\u0012\u001a\u00020\u0004J\u000e\u0010O\u001a\u00020C2\u0006\u0010\u0013\u001a\u00020\u0004J\u0015\u0010P\u001a\u00020C2\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015\u00a2\u0006\u0002\u0010QJ\u0015\u0010R\u001a\u00020C2\b\u0010\u0017\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010FJ\u000e\u0010S\u001a\u00020C2\u0006\u0010\u0018\u001a\u00020\u0004J\u000e\u0010T\u001a\u00020C2\u0006\u0010\u0019\u001a\u00020\u0004J\u0015\u0010U\u001a\u00020C2\b\u0010\u001a\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010FJ\u0015\u0010V\u001a\u00020C2\b\u0010\u001b\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010FJ\u000e\u0010W\u001a\u00020C2\u0006\u0010\u001c\u001a\u00020\u0004J\u0015\u0010X\u001a\u00020C2\b\u0010\u001d\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010IJ\u0015\u0010Y\u001a\u00020C2\b\u0010\u001e\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010FJ\u000e\u0010Z\u001a\u00020C2\u0006\u0010\u001f\u001a\u00020\u0001J\u000e\u0010[\u001a\u00020C2\u0006\u0010 \u001a\u00020\u0001J\u0015\u0010\\\u001a\u00020C2\b\u0010!\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010IJ\u0015\u0010]\u001a\u00020C2\b\u0010\"\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010IJ\u0015\u0010^\u001a\u00020C2\b\u0010#\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010FJ\u0015\u0010_\u001a\u00020C2\b\u0010$\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010IR\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\bR\u0014\u0010\t\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\n\u001a\u0004\u0018\u00010\u000b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\fR\u0014\u0010\r\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0004\u0018\u00010\u00108\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0014\u001a\u0004\u0018\u00010\u00158\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0016R\u0016\u0010\u0017\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\bR\u0014\u0010\u0018\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0019\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u001a\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\bR\u0016\u0010\u001b\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\bR\u0014\u0010\u001c\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u001d\u001a\u0004\u0018\u00010\u000b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\fR\u0016\u0010\u001e\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\bR\u0014\u0010\u001f\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010 \u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010!\u001a\u0004\u0018\u00010\u000b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\fR\u0016\u0010\"\u001a\u0004\u0018\u00010\u000b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\fR\u0016\u0010#\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\bR\u0016\u0010$\u001a\u0004\u0018\u00010\u000b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\f\u00a8\u0006`"}, d2 = {"Lcom/netizen/medicom/model/GlobalAreaInfoDTO$Info;", "", "()V", "basicEmail", "", "basicMobile", "customNetiID", "", "Ljava/lang/Integer;", "dateOfBirth", "emailBalance", "", "Ljava/lang/Float;", "fullName", "gender", "globalAreaInfoDTO", "Lcom/netizen/medicom/model/GlobalAreaInfoDTO;", "imageContent", "imageName", "imagePath", "imageSaveOrEditable", "", "Ljava/lang/Boolean;", "lastDateExecuted", "lastIpExecuted", "lastUserExecuted", "netiID", "registrationDate", "religion", "smsBalance", "userEnableStatus", "userName", "userPassword", "userReserveBalance", "userWalletBalance", "validationStatus", "voiceBalance", "getBasicEmail", "getBasicMobile", "getCustomNetiID", "()Ljava/lang/Integer;", "getDateOfBirth", "getEmailBalance", "()Ljava/lang/Float;", "getFullName", "getGender", "getGlobalAreaInfoDTO", "getImageContent", "getImageName", "getImagePath", "getImageSaveOrEditable", "()Ljava/lang/Boolean;", "getLastDateExecuted", "getLastIpExecuted", "getLastUserExecuted", "getNetiID", "getRegistrationDate", "getReligion", "getSmsBalance", "getUserEnableStatus", "getUserName", "getUserPassword", "getUserReserveBalance", "getUserWalletBalance", "getValidationStatus", "getVoiceBalance", "setBasicEmail", "", "setBasicMobile", "setCustomNetiID", "(Ljava/lang/Integer;)V", "setDateOfBirth", "setEmailBalance", "(Ljava/lang/Float;)V", "setFullName", "setGender", "setGlobalAreaInfoDTO", "setImageContent", "setImageName", "setImagePath", "setImageSaveOrEditable", "(Ljava/lang/Boolean;)V", "setLastDateExecuted", "setLastIpExecuted", "setLastUserExecuted", "setNetiID", "setRegistrationDate", "setReligion", "setSmsBalance", "setUserEnableStatus", "setUserName", "setUserPassword", "setUserReserveBalance", "setUserWalletBalance", "setValidationStatus", "setVoiceBalance", "app_release"})
    public static final class Info {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "netiID")
        private java.lang.Integer netiID;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "customNetiID")
        private java.lang.Integer customNetiID;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "fullName")
        private java.lang.String fullName;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "gender")
        private java.lang.String gender;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "religion")
        private java.lang.String religion;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "dateOfBirth")
        private java.lang.String dateOfBirth;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "basicMobile")
        private java.lang.String basicMobile;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "basicEmail")
        private java.lang.String basicEmail;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "imagePath")
        private java.lang.String imagePath;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "imageName")
        private java.lang.String imageName;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "userWalletBalance")
        private java.lang.Float userWalletBalance;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "userReserveBalance")
        private java.lang.Float userReserveBalance;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "smsBalance")
        private java.lang.Float smsBalance;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "voiceBalance")
        private java.lang.Float voiceBalance;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "emailBalance")
        private java.lang.Float emailBalance;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "validationStatus")
        private java.lang.Integer validationStatus;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "userEnableStatus")
        private java.lang.Integer userEnableStatus;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "registrationDate")
        private java.lang.Integer registrationDate;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "userName")
        private java.lang.Object userName;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "userPassword")
        private java.lang.Object userPassword;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "imageContent")
        private java.lang.Object imageContent;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "imageSaveOrEditable")
        private java.lang.Boolean imageSaveOrEditable;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "globalAreaInfoDTO")
        private com.netizen.medicom.model.GlobalAreaInfoDTO globalAreaInfoDTO;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "lastUserExecuted")
        private java.lang.String lastUserExecuted;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "lastIpExecuted")
        private java.lang.String lastIpExecuted;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "lastDateExecuted")
        private java.lang.Integer lastDateExecuted;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getNetiID() {
            return null;
        }
        
        public final void setNetiID(@org.jetbrains.annotations.Nullable()
        java.lang.Integer netiID) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getCustomNetiID() {
            return null;
        }
        
        public final void setCustomNetiID(@org.jetbrains.annotations.Nullable()
        java.lang.Integer customNetiID) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getFullName() {
            return null;
        }
        
        public final void setFullName(@org.jetbrains.annotations.NotNull()
        java.lang.String fullName) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getGender() {
            return null;
        }
        
        public final void setGender(@org.jetbrains.annotations.NotNull()
        java.lang.String gender) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getReligion() {
            return null;
        }
        
        public final void setReligion(@org.jetbrains.annotations.NotNull()
        java.lang.String religion) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getDateOfBirth() {
            return null;
        }
        
        public final void setDateOfBirth(@org.jetbrains.annotations.NotNull()
        java.lang.String dateOfBirth) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getBasicMobile() {
            return null;
        }
        
        public final void setBasicMobile(@org.jetbrains.annotations.NotNull()
        java.lang.String basicMobile) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getBasicEmail() {
            return null;
        }
        
        public final void setBasicEmail(@org.jetbrains.annotations.NotNull()
        java.lang.String basicEmail) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getImagePath() {
            return null;
        }
        
        public final void setImagePath(@org.jetbrains.annotations.NotNull()
        java.lang.String imagePath) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getImageName() {
            return null;
        }
        
        public final void setImageName(@org.jetbrains.annotations.NotNull()
        java.lang.String imageName) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Float getUserWalletBalance() {
            return null;
        }
        
        public final void setUserWalletBalance(@org.jetbrains.annotations.Nullable()
        java.lang.Float userWalletBalance) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Float getUserReserveBalance() {
            return null;
        }
        
        public final void setUserReserveBalance(@org.jetbrains.annotations.Nullable()
        java.lang.Float userReserveBalance) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Float getSmsBalance() {
            return null;
        }
        
        public final void setSmsBalance(@org.jetbrains.annotations.Nullable()
        java.lang.Float smsBalance) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Float getVoiceBalance() {
            return null;
        }
        
        public final void setVoiceBalance(@org.jetbrains.annotations.Nullable()
        java.lang.Float voiceBalance) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Float getEmailBalance() {
            return null;
        }
        
        public final void setEmailBalance(@org.jetbrains.annotations.Nullable()
        java.lang.Float emailBalance) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getValidationStatus() {
            return null;
        }
        
        public final void setValidationStatus(@org.jetbrains.annotations.Nullable()
        java.lang.Integer validationStatus) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getUserEnableStatus() {
            return null;
        }
        
        public final void setUserEnableStatus(@org.jetbrains.annotations.Nullable()
        java.lang.Integer userEnableStatus) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getRegistrationDate() {
            return null;
        }
        
        public final void setRegistrationDate(@org.jetbrains.annotations.Nullable()
        java.lang.Integer registrationDate) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Object getUserName() {
            return null;
        }
        
        public final void setUserName(@org.jetbrains.annotations.NotNull()
        java.lang.Object userName) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Object getUserPassword() {
            return null;
        }
        
        public final void setUserPassword(@org.jetbrains.annotations.NotNull()
        java.lang.Object userPassword) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Object getImageContent() {
            return null;
        }
        
        public final void setImageContent(@org.jetbrains.annotations.NotNull()
        java.lang.Object imageContent) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Boolean getImageSaveOrEditable() {
            return null;
        }
        
        public final void setImageSaveOrEditable(@org.jetbrains.annotations.Nullable()
        java.lang.Boolean imageSaveOrEditable) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final com.netizen.medicom.model.GlobalAreaInfoDTO getGlobalAreaInfoDTO() {
            return null;
        }
        
        public final void setGlobalAreaInfoDTO(@org.jetbrains.annotations.NotNull()
        com.netizen.medicom.model.GlobalAreaInfoDTO globalAreaInfoDTO) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getLastUserExecuted() {
            return null;
        }
        
        public final void setLastUserExecuted(@org.jetbrains.annotations.NotNull()
        java.lang.String lastUserExecuted) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getLastIpExecuted() {
            return null;
        }
        
        public final void setLastIpExecuted(@org.jetbrains.annotations.NotNull()
        java.lang.String lastIpExecuted) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getLastDateExecuted() {
            return null;
        }
        
        public final void setLastDateExecuted(@org.jetbrains.annotations.Nullable()
        java.lang.Integer lastDateExecuted) {
        }
        
        public Info() {
            super();
        }
    }
}