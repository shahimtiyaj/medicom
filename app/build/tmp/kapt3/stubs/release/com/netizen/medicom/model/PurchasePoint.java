package com.netizen.medicom.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0007\b\u0016\u00a2\u0006\u0002\u0010\u0002B\u0011\b\u0016\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0005B%\b\u0016\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0007\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\tJ\r\u0010\u000b\u001a\u0004\u0018\u00010\f\u00a2\u0006\u0002\u0010\rJ\b\u0010\u000e\u001a\u0004\u0018\u00010\u0004J\r\u0010\u000f\u001a\u0004\u0018\u00010\f\u00a2\u0006\u0002\u0010\rJ\u0015\u0010\u0010\u001a\u00020\u00112\b\u0010\b\u001a\u0004\u0018\u00010\f\u00a2\u0006\u0002\u0010\u0012J\u000e\u0010\u0013\u001a\u00020\u00112\u0006\u0010\u0003\u001a\u00020\u0004J\u0015\u0010\u0014\u001a\u00020\u00112\b\u0010\u0006\u001a\u0004\u0018\u00010\f\u00a2\u0006\u0002\u0010\u0012R\u0016\u0010\b\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\nR\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\n\u00a8\u0006\u0015"}, d2 = {"Lcom/netizen/medicom/model/PurchasePoint;", "", "()V", "coreRoleName", "", "(Ljava/lang/String;)V", "userRoleAssignID", "", "coreRoleID", "(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;)V", "Ljava/lang/Integer;", "getCoreRoleID", "", "()Ljava/lang/Long;", "getCoreRoleName", "getUserRoleAssignID", "setCoreRoleID", "", "(Ljava/lang/Long;)V", "setCoreRoleName", "setUserRoleAssignID", "app_release"})
public final class PurchasePoint {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "userRoleAssignID")
    private java.lang.Integer userRoleAssignID;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "coreRoleID")
    private java.lang.Integer coreRoleID;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "coreRoleName")
    private java.lang.String coreRoleName;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getUserRoleAssignID() {
        return null;
    }
    
    public final void setUserRoleAssignID(@org.jetbrains.annotations.Nullable()
    java.lang.Long userRoleAssignID) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getCoreRoleID() {
        return null;
    }
    
    public final void setCoreRoleID(@org.jetbrains.annotations.Nullable()
    java.lang.Long coreRoleID) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCoreRoleName() {
        return null;
    }
    
    public final void setCoreRoleName(@org.jetbrains.annotations.NotNull()
    java.lang.String coreRoleName) {
    }
    
    public PurchasePoint() {
        super();
    }
    
    public PurchasePoint(@org.jetbrains.annotations.Nullable()
    java.lang.String coreRoleName) {
        super();
    }
    
    public PurchasePoint(@org.jetbrains.annotations.Nullable()
    java.lang.Integer userRoleAssignID, @org.jetbrains.annotations.Nullable()
    java.lang.Integer coreRoleID, @org.jetbrains.annotations.Nullable()
    java.lang.String coreRoleName) {
        super();
    }
}