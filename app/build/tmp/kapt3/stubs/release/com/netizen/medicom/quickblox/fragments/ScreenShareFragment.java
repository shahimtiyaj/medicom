package com.netizen.medicom.quickblox.fragments;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 #2\u00020\u0001:\u0004#$%&B\u0005\u00a2\u0006\u0002\u0010\u0002J\r\u0010\t\u001a\u00020\nH\u0010\u00a2\u0006\u0002\b\u000bJ\u0010\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\u0018\u0010\u0010\u001a\u00020\r2\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J&\u0010\u0015\u001a\u0004\u0018\u00010\u00162\u0006\u0010\u0013\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u0016J\b\u0010\u001c\u001a\u00020\rH\u0016J\u0010\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 H\u0016J\b\u0010!\u001a\u00020\rH\u0016J\b\u0010\"\u001a\u00020\rH\u0016R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\'"}, d2 = {"Lcom/netizen/medicom/quickblox/fragments/ScreenShareFragment;", "Lcom/netizen/medicom/quickblox/fragments/BaseToolBarFragment;", "()V", "TAG", "", "currentCallStateCallback", "Lcom/netizen/medicom/quickblox/activities/CallActivity$CurrentCallStateCallback;", "onSharingEvents", "Lcom/netizen/medicom/quickblox/fragments/ScreenShareFragment$OnSharingEvents;", "getFragmentLayout", "", "getFragmentLayout$app_release", "onAttach", "", "context", "Landroid/content/Context;", "onCreateOptionsMenu", "menu", "Landroid/view/Menu;", "inflater", "Landroid/view/MenuInflater;", "onCreateView", "Landroid/view/View;", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onDetach", "onOptionsItemSelected", "", "item", "Landroid/view/MenuItem;", "onPause", "onResume", "Companion", "CurrentCallStateCallbackImpl", "ImagesAdapter", "OnSharingEvents", "app_release"})
public final class ScreenShareFragment extends com.netizen.medicom.quickblox.fragments.BaseToolBarFragment {
    private final java.lang.String TAG = null;
    private com.netizen.medicom.quickblox.fragments.ScreenShareFragment.OnSharingEvents onSharingEvents;
    private com.netizen.medicom.quickblox.activities.CallActivity.CurrentCallStateCallback currentCallStateCallback;
    public static final com.netizen.medicom.quickblox.fragments.ScreenShareFragment.Companion Companion = null;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    public int getFragmentLayout$app_release() {
        return 0;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onResume() {
    }
    
    @java.lang.Override()
    public void onCreateOptionsMenu(@org.jetbrains.annotations.NotNull()
    android.view.Menu menu, @org.jetbrains.annotations.NotNull()
    android.view.MenuInflater inflater) {
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    @java.lang.Override()
    public void onAttach(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    @java.lang.Override()
    public void onDetach() {
    }
    
    @java.lang.Override()
    public void onPause() {
    }
    
    public ScreenShareFragment() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0015\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\b\u0010\u0007\u001a\u00020\bH\u0016J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\bH\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\f"}, d2 = {"Lcom/netizen/medicom/quickblox/fragments/ScreenShareFragment$ImagesAdapter;", "Landroidx/fragment/app/FragmentPagerAdapter;", "fm", "Landroidx/fragment/app/FragmentManager;", "(Landroidx/fragment/app/FragmentManager;)V", "images", "", "getCount", "", "getItem", "Landroidx/fragment/app/Fragment;", "position", "app_release"})
    public static final class ImagesAdapter extends androidx.fragment.app.FragmentPagerAdapter {
        private final int[] images = {2131231075, 2131231071, 2131230904, 2131231070};
        
        @java.lang.Override()
        public int getCount() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public androidx.fragment.app.Fragment getItem(int position) {
            return null;
        }
        
        public ImagesAdapter(@org.jetbrains.annotations.NotNull()
        androidx.fragment.app.FragmentManager fm) {
            super(null);
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0082\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016J\b\u0010\u0005\u001a\u00020\u0004H\u0016J\u0010\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\bH\u0016J \u0010\t\u001a\u00020\u00042\u0016\u0010\n\u001a\u0012\u0012\u0004\u0012\u00020\f0\u000bj\b\u0012\u0004\u0012\u00020\f`\rH\u0016\u00a8\u0006\u000e"}, d2 = {"Lcom/netizen/medicom/quickblox/fragments/ScreenShareFragment$CurrentCallStateCallbackImpl;", "Lcom/netizen/medicom/quickblox/activities/CallActivity$CurrentCallStateCallback;", "(Lcom/netizen/medicom/quickblox/fragments/ScreenShareFragment;)V", "onCallStarted", "", "onCallStopped", "onCallTimeUpdate", "time", "", "onOpponentsListUpdated", "newUsers", "Ljava/util/ArrayList;", "Lcom/quickblox/users/model/QBUser;", "Lkotlin/collections/ArrayList;", "app_release"})
    final class CurrentCallStateCallbackImpl implements com.netizen.medicom.quickblox.activities.CallActivity.CurrentCallStateCallback {
        
        @java.lang.Override()
        public void onCallStarted() {
        }
        
        @java.lang.Override()
        public void onCallStopped() {
        }
        
        @java.lang.Override()
        public void onOpponentsListUpdated(@org.jetbrains.annotations.NotNull()
        java.util.ArrayList<com.quickblox.users.model.QBUser> newUsers) {
        }
        
        @java.lang.Override()
        public void onCallTimeUpdate(@org.jetbrains.annotations.NotNull()
        java.lang.String time) {
        }
        
        public CurrentCallStateCallbackImpl() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&\u00a8\u0006\u0004"}, d2 = {"Lcom/netizen/medicom/quickblox/fragments/ScreenShareFragment$OnSharingEvents;", "", "onStopPreview", "", "app_release"})
    public static abstract interface OnSharingEvents {
        
        public abstract void onStopPreview();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0005"}, d2 = {"Lcom/netizen/medicom/quickblox/fragments/ScreenShareFragment$Companion;", "", "()V", "newInstance", "Lcom/netizen/medicom/quickblox/fragments/ScreenShareFragment;", "app_release"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final com.netizen.medicom.quickblox.fragments.ScreenShareFragment newInstance() {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}