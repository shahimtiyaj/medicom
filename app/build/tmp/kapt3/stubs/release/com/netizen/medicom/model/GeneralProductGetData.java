package com.netizen.medicom.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u0006\n\u0002\b\u000e\n\u0002\u0010\u0002\n\u0002\b\n\u0018\u00002\u00020\u0001:\u0001*B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0014\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0015\u001a\u0004\u0018\u00010\u0004J\r\u0010\u0016\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0017J\b\u0010\u0018\u001a\u0004\u0018\u00010\nJ\b\u0010\u0019\u001a\u0004\u0018\u00010\u0004J\r\u0010\u001a\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0017J\b\u0010\u001b\u001a\u0004\u0018\u00010\u0004J\r\u0010\u001c\u001a\u0004\u0018\u00010\u000f\u00a2\u0006\u0002\u0010\u001dJ\r\u0010\u001e\u001a\u0004\u0018\u00010\u0012\u00a2\u0006\u0002\u0010\u001fJ\u000e\u0010 \u001a\u00020!2\u0006\u0010\u0003\u001a\u00020\u0004J\u000e\u0010\"\u001a\u00020!2\u0006\u0010\u0005\u001a\u00020\u0004J\u000e\u0010#\u001a\u00020!2\u0006\u0010\u0006\u001a\u00020\u0007J\u0010\u0010$\u001a\u00020!2\b\u0010\t\u001a\u0004\u0018\u00010\nJ\u000e\u0010%\u001a\u00020!2\u0006\u0010\u000b\u001a\u00020\u0004J\u000e\u0010&\u001a\u00020!2\u0006\u0010\f\u001a\u00020\u0007J\u000e\u0010\'\u001a\u00020!2\u0006\u0010\r\u001a\u00020\u0004J\u000e\u0010(\u001a\u00020!2\u0006\u0010\u000e\u001a\u00020\u000fJ\u000e\u0010)\u001a\u00020!2\u0006\u0010\u0011\u001a\u00020\u0012R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\bR\u0014\u0010\t\u001a\u0004\u0018\u00010\n8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\f\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\bR\u0014\u0010\r\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000e\u001a\u0004\u0018\u00010\u000f8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0010R\u0016\u0010\u0011\u001a\u0004\u0018\u00010\u00128\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0013\u00a8\u0006+"}, d2 = {"Lcom/netizen/medicom/model/GeneralProductGetData;", "", "()V", "categoryName", "", "coreRoleNote", "genCodeStatus", "", "Ljava/lang/Integer;", "productInfoDTO", "Lcom/netizen/medicom/model/GeneralProductGetData$ProductInfoDTO;", "productName", "productPurchaseID", "purchaseDate", "purchaseQuantity", "", "Ljava/lang/Long;", "totalAmount", "", "Ljava/lang/Double;", "getCategoryName", "getCoreRoleNote", "getGenCodeStatus", "()Ljava/lang/Integer;", "getProductInfoDTO", "getProductName", "getProductPurchaseID", "getPurchaseDate", "getPurchaseQuantity", "()Ljava/lang/Long;", "getTotalAmount", "()Ljava/lang/Double;", "setCategoryName", "", "setCoreRoleNote", "setGenCodeStatus", "setProductInfoDTO", "setProductName", "setProductPurchaseID", "setPurchaseDate", "setPurchaseQuantity", "setTotalAmount", "ProductInfoDTO", "app_release"})
public final class GeneralProductGetData {
    @com.google.gson.annotations.SerializedName(value = "purchaseDate")
    private java.lang.String purchaseDate;
    @com.google.gson.annotations.SerializedName(value = "coreRoleNote")
    private java.lang.String coreRoleNote;
    @com.google.gson.annotations.SerializedName(value = "categoryName")
    private java.lang.String categoryName;
    @com.google.gson.annotations.SerializedName(value = "productName")
    private java.lang.String productName;
    @com.google.gson.annotations.SerializedName(value = "purchaseQuantity")
    private java.lang.Long purchaseQuantity;
    @com.google.gson.annotations.SerializedName(value = "totalAmount")
    private java.lang.Double totalAmount;
    @com.google.gson.annotations.SerializedName(value = "productPurchaseID")
    private java.lang.Integer productPurchaseID;
    @com.google.gson.annotations.SerializedName(value = "genCodeStatus")
    private java.lang.Integer genCodeStatus;
    @com.google.gson.annotations.SerializedName(value = "productInfoDTO")
    private com.netizen.medicom.model.GeneralProductGetData.ProductInfoDTO productInfoDTO;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPurchaseDate() {
        return null;
    }
    
    public final void setPurchaseDate(@org.jetbrains.annotations.NotNull()
    java.lang.String purchaseDate) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCoreRoleNote() {
        return null;
    }
    
    public final void setCoreRoleNote(@org.jetbrains.annotations.NotNull()
    java.lang.String coreRoleNote) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCategoryName() {
        return null;
    }
    
    public final void setCategoryName(@org.jetbrains.annotations.NotNull()
    java.lang.String categoryName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getProductName() {
        return null;
    }
    
    public final void setProductName(@org.jetbrains.annotations.NotNull()
    java.lang.String productName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getPurchaseQuantity() {
        return null;
    }
    
    public final void setPurchaseQuantity(long purchaseQuantity) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Double getTotalAmount() {
        return null;
    }
    
    public final void setTotalAmount(double totalAmount) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getProductPurchaseID() {
        return null;
    }
    
    public final void setProductPurchaseID(int productPurchaseID) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getGenCodeStatus() {
        return null;
    }
    
    public final void setGenCodeStatus(int genCodeStatus) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.medicom.model.GeneralProductGetData.ProductInfoDTO getProductInfoDTO() {
        return null;
    }
    
    public final void setProductInfoDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.medicom.model.GeneralProductGetData.ProductInfoDTO productInfoDTO) {
    }
    
    public GeneralProductGetData() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0006\u001a\u00020\u00072\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lcom/netizen/medicom/model/GeneralProductGetData$ProductInfoDTO;", "", "()V", "productName", "", "getProductName", "setProductName", "", "app_release"})
    public static final class ProductInfoDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "productName")
        private java.lang.String productName;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getProductName() {
            return null;
        }
        
        public final void setProductName(@org.jetbrains.annotations.Nullable()
        java.lang.String productName) {
        }
        
        public ProductInfoDTO() {
            super();
        }
    }
}