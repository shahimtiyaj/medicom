package com.netizen.medicom.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001:\u0001\u0012B\u0005\u00a2\u0006\u0002\u0010\u0002J\r\u0010\t\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\nJ\b\u0010\u000b\u001a\u0004\u0018\u00010\u0007J\r\u0010\f\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\nJ\u0015\u0010\r\u001a\u00020\u000e2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u000e2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007J\u0015\u0010\u0011\u001a\u00020\u000e2\b\u0010\b\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u000fR\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005\u00a8\u0006\u0013"}, d2 = {"Lcom/netizen/medicom/model/Products;", "", "()V", "assignEnableStatus", "", "Ljava/lang/Integer;", "productInfoDTO", "Lcom/netizen/medicom/model/Products$ProductInfoDTO;", "productRoleAssignID", "getAssignEnableStatus", "()Ljava/lang/Integer;", "getProductInfoDTO", "getProductRoleAssignID", "setAssignEnableStatus", "", "(Ljava/lang/Integer;)V", "setProductInfoDTO", "setProductRoleAssignID", "ProductInfoDTO", "app_release"})
public final class Products {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "productRoleAssignID")
    private java.lang.Integer productRoleAssignID;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "assignEnableStatus")
    private java.lang.Integer assignEnableStatus;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "productInfoDTO")
    private com.netizen.medicom.model.Products.ProductInfoDTO productInfoDTO;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getProductRoleAssignID() {
        return null;
    }
    
    public final void setProductRoleAssignID(@org.jetbrains.annotations.Nullable()
    java.lang.Integer productRoleAssignID) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getAssignEnableStatus() {
        return null;
    }
    
    public final void setAssignEnableStatus(@org.jetbrains.annotations.Nullable()
    java.lang.Integer assignEnableStatus) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.medicom.model.Products.ProductInfoDTO getProductInfoDTO() {
        return null;
    }
    
    public final void setProductInfoDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.medicom.model.Products.ProductInfoDTO productInfoDTO) {
    }
    
    public Products() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0006\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\r\u0010\f\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\rJ\r\u0010\u000e\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u000fJ\b\u0010\u0010\u001a\u0004\u0018\u00010\nJ\r\u0010\u0011\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\rJ\u0015\u0010\u0012\u001a\u00020\u00132\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0014J\u0015\u0010\u0015\u001a\u00020\u00132\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0016J\u000e\u0010\u0017\u001a\u00020\u00132\u0006\u0010\t\u001a\u00020\nJ\u0015\u0010\u0018\u001a\u00020\u00132\b\u0010\u000b\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0014R\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\bR\u0014\u0010\t\u001a\u0004\u0018\u00010\n8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005\u00a8\u0006\u0019"}, d2 = {"Lcom/netizen/medicom/model/Products$ProductInfoDTO;", "", "()V", "percentVat", "", "Ljava/lang/Double;", "productID", "", "Ljava/lang/Integer;", "productName", "", "salesPrice", "getPercentVat", "()Ljava/lang/Double;", "getProductID", "()Ljava/lang/Integer;", "getProductName", "getSalesPrice", "setPercentVat", "", "(Ljava/lang/Double;)V", "setProductID", "(Ljava/lang/Integer;)V", "setProductName", "setSalesPrice", "app_release"})
    public static final class ProductInfoDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "productID")
        private java.lang.Integer productID;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "productName")
        private java.lang.String productName;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "salesPrice")
        private java.lang.Double salesPrice;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "percentVat")
        private java.lang.Double percentVat;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getProductID() {
            return null;
        }
        
        public final void setProductID(@org.jetbrains.annotations.Nullable()
        java.lang.Integer productID) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getProductName() {
            return null;
        }
        
        public final void setProductName(@org.jetbrains.annotations.NotNull()
        java.lang.String productName) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Double getSalesPrice() {
            return null;
        }
        
        public final void setSalesPrice(@org.jetbrains.annotations.Nullable()
        java.lang.Double salesPrice) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Double getPercentVat() {
            return null;
        }
        
        public final void setPercentVat(@org.jetbrains.annotations.Nullable()
        java.lang.Double percentVat) {
        }
        
        public ProductInfoDTO() {
            super();
        }
    }
}