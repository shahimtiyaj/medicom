package com.netizen.medicom.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0011\n\u0002\u0010\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001B\u0007\b\u0016\u00a2\u0006\u0002\u0010\u0002BW\b\u0016\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\fJ\b\u0010\r\u001a\u0004\u0018\u00010\u0004J\b\u0010\u000e\u001a\u0004\u0018\u00010\u0004J\b\u0010\u000f\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0010\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0011\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0012\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0013\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0014\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0015\u001a\u00020\u00162\b\u0010\n\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0017\u001a\u00020\u00162\b\u0010\t\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0018\u001a\u00020\u00162\b\u0010\b\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0019\u001a\u00020\u00162\b\u0010\u0007\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u001a\u001a\u00020\u00162\b\u0010\u0006\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u001b\u001a\u00020\u00162\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u001c\u001a\u00020\u00162\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u001d\u001a\u00020\u00162\b\u0010\u000b\u001a\u0004\u0018\u00010\u0004R\u0014\u0010\n\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"}, d2 = {"Lcom/netizen/medicom/model/AddPatientFamilyMember;", "", "()V", "relation", "", "patientName", "patientMobile", "patientEmail", "patientDob", "gender", "bloodGroup", "religion", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getBloodGroup", "getGender", "getPatientDob", "getPatientEmail", "getPatientMobile", "getPatientName", "getRelation", "getReligion", "setBloodGroup", "", "setGender", "setPatientDob", "setPatientEmail", "setPatientMobile", "setPatientName", "setRelation", "setReligion", "app_release"})
public final class AddPatientFamilyMember {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "bloodGroup")
    private java.lang.String bloodGroup;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "gender")
    private java.lang.String gender;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "patientDob")
    private java.lang.String patientDob;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "patientEmail")
    private java.lang.String patientEmail;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "patientMobile")
    private java.lang.String patientMobile;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "patientName")
    private java.lang.String patientName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "relation")
    private java.lang.String relation;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "religion")
    private java.lang.String religion;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getBloodGroup() {
        return null;
    }
    
    public final void setBloodGroup(@org.jetbrains.annotations.Nullable()
    java.lang.String bloodGroup) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getGender() {
        return null;
    }
    
    public final void setGender(@org.jetbrains.annotations.Nullable()
    java.lang.String gender) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPatientDob() {
        return null;
    }
    
    public final void setPatientDob(@org.jetbrains.annotations.Nullable()
    java.lang.String patientDob) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPatientEmail() {
        return null;
    }
    
    public final void setPatientEmail(@org.jetbrains.annotations.Nullable()
    java.lang.String patientEmail) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPatientMobile() {
        return null;
    }
    
    public final void setPatientMobile(@org.jetbrains.annotations.Nullable()
    java.lang.String patientMobile) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPatientName() {
        return null;
    }
    
    public final void setPatientName(@org.jetbrains.annotations.Nullable()
    java.lang.String patientName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getRelation() {
        return null;
    }
    
    public final void setRelation(@org.jetbrains.annotations.Nullable()
    java.lang.String relation) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getReligion() {
        return null;
    }
    
    public final void setReligion(@org.jetbrains.annotations.Nullable()
    java.lang.String religion) {
    }
    
    public AddPatientFamilyMember() {
        super();
    }
    
    public AddPatientFamilyMember(@org.jetbrains.annotations.Nullable()
    java.lang.String relation, @org.jetbrains.annotations.Nullable()
    java.lang.String patientName, @org.jetbrains.annotations.Nullable()
    java.lang.String patientMobile, @org.jetbrains.annotations.Nullable()
    java.lang.String patientEmail, @org.jetbrains.annotations.Nullable()
    java.lang.String patientDob, @org.jetbrains.annotations.Nullable()
    java.lang.String gender, @org.jetbrains.annotations.Nullable()
    java.lang.String bloodGroup, @org.jetbrains.annotations.Nullable()
    java.lang.String religion) {
        super();
    }
}