package com.netizen.medicom.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bJ\u0006\u0010\u001c\u001a\u00020\u0019J\u000e\u0010\u001d\u001a\u00020\u00192\u0006\u0010\u001e\u001a\u00020\tJ\u000e\u0010\u001f\u001a\u00020\u00192\u0006\u0010 \u001a\u00020\tR\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\rR \u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000f0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000b\"\u0004\b\u0010\u0010\rR\u001f\u0010\u0011\u001a\u0010\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00130\u00120\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u000bR \u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u000b\"\u0004\b\u0017\u0010\r\u00a8\u0006!"}, d2 = {"Lcom/netizen/medicom/repository/PatientRepository;", "", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "appPreferences", "Lcom/netizen/medicom/utils/AppPreferences;", "fullName", "Landroidx/lifecycle/MutableLiveData;", "", "getFullName", "()Landroidx/lifecycle/MutableLiveData;", "setFullName", "(Landroidx/lifecycle/MutableLiveData;)V", "isPatientFamilyMemberFound", "", "setPatientFamilyMemberFound", "patientFamilyMemberArrayList", "", "Lcom/netizen/medicom/model/PatientFamilyMemberList$Item;", "getPatientFamilyMemberArrayList", "phoneNo", "getPhoneNo", "setPhoneNo", "addPatientFamilyMember", "", "addPatient", "Lcom/netizen/medicom/model/AddPatientFamilyMember;", "getPatientFamilyMemberData", "getPersonInfoForPatient", "customNetiID", "patientRegistration", "referenceId", "app_release"})
public final class PatientRepository {
    private final com.netizen.medicom.utils.AppPreferences appPreferences = null;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> fullName;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> phoneNo;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.medicom.model.PatientFamilyMemberList.Item>> patientFamilyMemberArrayList = null;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isPatientFamilyMemberFound;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getFullName() {
        return null;
    }
    
    public final void setFullName(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getPhoneNo() {
        return null;
    }
    
    public final void setPhoneNo(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.medicom.model.PatientFamilyMemberList.Item>> getPatientFamilyMemberArrayList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isPatientFamilyMemberFound() {
        return null;
    }
    
    public final void setPatientFamilyMemberFound(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    public final void patientRegistration(@org.jetbrains.annotations.NotNull()
    java.lang.String referenceId) {
    }
    
    public final void getPersonInfoForPatient(@org.jetbrains.annotations.NotNull()
    java.lang.String customNetiID) {
    }
    
    public final void addPatientFamilyMember(@org.jetbrains.annotations.NotNull()
    com.netizen.medicom.model.AddPatientFamilyMember addPatient) {
    }
    
    public final void getPatientFamilyMemberData() {
    }
    
    public PatientRepository(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        super();
    }
}