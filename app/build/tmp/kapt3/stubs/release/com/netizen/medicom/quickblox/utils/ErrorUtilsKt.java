package com.netizen.medicom.quickblox.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u00000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a:\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\b\b\u0001\u0010\t\u001a\u00020\n2\u000e\u0010\u000b\u001a\n\u0018\u00010\fj\u0004\u0018\u0001`\r2\b\b\u0001\u0010\u000e\u001a\u00020\n2\u0006\u0010\u000f\u001a\u00020\u0010\u001a2\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\b\b\u0001\u0010\t\u001a\u00020\n2\u0006\u0010\u0011\u001a\u00020\u00012\b\b\u0001\u0010\u000e\u001a\u00020\n2\u0006\u0010\u000f\u001a\u00020\u0010\u001a*\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0012\u001a\u00020\u00012\b\b\u0001\u0010\u000e\u001a\u00020\n2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"}, d2 = {"EMPTY_STRING", "", "NO_CONNECTION_ERROR", "NO_INTERNET_CONNECTION", "NO_RESPONSE_TIMEOUT", "showErrorSnackbar", "Lcom/google/android/material/snackbar/Snackbar;", "view", "Landroid/view/View;", "errorMessage", "", "e", "Ljava/lang/Exception;", "Lkotlin/Exception;", "actionLabel", "clickListener", "Landroid/view/View$OnClickListener;", "error", "message", "app_release"})
public final class ErrorUtilsKt {
    private static final java.lang.String EMPTY_STRING = "";
    private static final java.lang.String NO_CONNECTION_ERROR = null;
    private static final java.lang.String NO_RESPONSE_TIMEOUT = null;
    private static final java.lang.String NO_INTERNET_CONNECTION = null;
    
    @org.jetbrains.annotations.NotNull()
    public static final com.google.android.material.snackbar.Snackbar showErrorSnackbar(@org.jetbrains.annotations.NotNull()
    android.view.View view, @androidx.annotation.StringRes()
    int errorMessage, @org.jetbrains.annotations.Nullable()
    java.lang.Exception e, @androidx.annotation.StringRes()
    int actionLabel, @org.jetbrains.annotations.NotNull()
    android.view.View.OnClickListener clickListener) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final com.google.android.material.snackbar.Snackbar showErrorSnackbar(@org.jetbrains.annotations.NotNull()
    android.view.View view, @androidx.annotation.StringRes()
    int errorMessage, @org.jetbrains.annotations.NotNull()
    java.lang.String error, @androidx.annotation.StringRes()
    int actionLabel, @org.jetbrains.annotations.NotNull()
    android.view.View.OnClickListener clickListener) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final com.google.android.material.snackbar.Snackbar showErrorSnackbar(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.NotNull()
    java.lang.String message, @androidx.annotation.StringRes()
    int actionLabel, @org.jetbrains.annotations.Nullable()
    android.view.View.OnClickListener clickListener) {
        return null;
    }
}