package com.netizen.medicom.quickblox.activities;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\b\u0007\n\u0002\u0010\u0015\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0007\u0018\u0000 \u001f2\u00020\u0001:\u0002\u001f B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0005\u001a\u00020\u0006H\u0002J\u0010\u0010\u0007\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\tH\u0002J\u001b\u0010\n\u001a\u00020\u00062\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\t0\fH\u0002\u00a2\u0006\u0002\u0010\rJ\b\u0010\u000e\u001a\u00020\u0006H\u0002J\b\u0010\u000f\u001a\u00020\u0004H\u0002J\u0013\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\t0\fH\u0002\u00a2\u0006\u0002\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J\u0012\u0010\u0015\u001a\u00020\u00062\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u0014J+\u0010\u0018\u001a\u00020\u00062\u0006\u0010\u0019\u001a\u00020\u001a2\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\t0\f2\u0006\u0010\u0013\u001a\u00020\u0014H\u0016\u00a2\u0006\u0002\u0010\u001bJ\b\u0010\u001c\u001a\u00020\u0006H\u0014J!\u0010\u001d\u001a\u00020\u00062\u0012\u0010\u000b\u001a\n\u0012\u0006\b\u0001\u0012\u00020\t0\f\"\u00020\tH\u0002\u00a2\u0006\u0002\u0010\rJ\u0010\u0010\u001e\u001a\u00020\u00062\u0006\u0010\u0013\u001a\u00020\u0014H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"}, d2 = {"Lcom/netizen/medicom/quickblox/activities/PermissionsActivity;", "Lcom/netizen/medicom/quickblox/activities/BaseActivity;", "()V", "requiresCheck", "", "allPermissionsGranted", "", "checkPermissionAudio", "audioPermission", "", "checkPermissionAudioVideo", "permissions", "", "([Ljava/lang/String;)V", "checkPermissions", "getCheckOnlyAudio", "getPermissions", "()[Ljava/lang/String;", "hasAllPermissionsGranted", "grantResults", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onRequestPermissionsResult", "requestCode", "", "(I[Ljava/lang/String;[I)V", "onResume", "requestPermissions", "showDeniedResponse", "Companion", "PermissionFeatures", "app_release"})
public final class PermissionsActivity extends com.netizen.medicom.quickblox.activities.BaseActivity {
    private boolean requiresCheck = false;
    public static final com.netizen.medicom.quickblox.activities.PermissionsActivity.Companion Companion = null;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    private final void checkPermissions() {
    }
    
    private final void checkPermissionAudio(java.lang.String audioPermission) {
    }
    
    private final void checkPermissionAudioVideo(java.lang.String[] permissions) {
    }
    
    private final java.lang.String[] getPermissions() {
        return null;
    }
    
    private final boolean getCheckOnlyAudio() {
        return false;
    }
    
    private final void requestPermissions(java.lang.String... permissions) {
    }
    
    private final void allPermissionsGranted() {
    }
    
    @java.lang.Override()
    public void onRequestPermissionsResult(int requestCode, @org.jetbrains.annotations.NotNull()
    java.lang.String[] permissions, @org.jetbrains.annotations.NotNull()
    int[] grantResults) {
    }
    
    private final void showDeniedResponse(int[] grantResults) {
    }
    
    private final boolean hasAllPermissionsGranted(int[] grantResults) {
        return false;
    }
    
    public PermissionsActivity() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0082\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004\u00a8\u0006\u0005"}, d2 = {"Lcom/netizen/medicom/quickblox/activities/PermissionsActivity$PermissionFeatures;", "", "(Ljava/lang/String;I)V", "CAMERA", "MICROPHONE", "app_release"})
    static enum PermissionFeatures {
        /*public static final*/ CAMERA /* = new CAMERA() */,
        /*public static final*/ MICROPHONE /* = new MICROPHONE() */;
        
        PermissionFeatures() {
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J)\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n\u00a2\u0006\u0002\u0010\f\u00a8\u0006\r"}, d2 = {"Lcom/netizen/medicom/quickblox/activities/PermissionsActivity$Companion;", "", "()V", "startForResult", "", "activity", "Landroid/app/Activity;", "checkOnlyAudio", "", "permissions", "", "", "(Landroid/app/Activity;Z[Ljava/lang/String;)V", "app_release"})
    public static final class Companion {
        
        public final void startForResult(@org.jetbrains.annotations.NotNull()
        android.app.Activity activity, boolean checkOnlyAudio, @org.jetbrains.annotations.NotNull()
        java.lang.String[] permissions) {
        }
        
        private Companion() {
            super();
        }
    }
}