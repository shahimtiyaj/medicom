package com.netizen.medicom.quickblox.activities;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0003X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0004"}, d2 = {"INCOME_CALL_FRAGMENT", "", "REQUEST_PERMISSION_SETTING", "", "app_release"})
public final class CallActivityKt {
    private static final java.lang.String INCOME_CALL_FRAGMENT = "income_call_fragment";
    private static final int REQUEST_PERMISSION_SETTING = 545;
}