package com.netizen.medicom.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B%\b\u0016\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0006B\u001b\b\u0016\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0007J\b\u0010\b\u001a\u0004\u0018\u00010\u0003J\b\u0010\t\u001a\u0004\u0018\u00010\u0003J\b\u0010\n\u001a\u0004\u0018\u00010\u0003J\u0010\u0010\u000b\u001a\u00020\f2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0003J\u0010\u0010\r\u001a\u00020\f2\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003J\u0010\u0010\u000e\u001a\u00020\f2\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00038\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u00038\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"}, d2 = {"Lcom/netizen/medicom/model/ResetPassword;", "", "userName", "", "password", "newPassword", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "(Ljava/lang/String;Ljava/lang/String;)V", "getNewPassword", "getPassword", "getUserName", "setNewPassword", "", "setPassword", "setUserName", "app_release"})
public final class ResetPassword {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "userName")
    private java.lang.String userName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "password")
    private java.lang.String password;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "newPassword")
    private java.lang.String newPassword;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getUserName() {
        return null;
    }
    
    public final void setUserName(@org.jetbrains.annotations.Nullable()
    java.lang.String userName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPassword() {
        return null;
    }
    
    public final void setPassword(@org.jetbrains.annotations.Nullable()
    java.lang.String password) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getNewPassword() {
        return null;
    }
    
    public final void setNewPassword(@org.jetbrains.annotations.Nullable()
    java.lang.String newPassword) {
    }
    
    public ResetPassword(@org.jetbrains.annotations.Nullable()
    java.lang.String userName, @org.jetbrains.annotations.Nullable()
    java.lang.String password, @org.jetbrains.annotations.Nullable()
    java.lang.String newPassword) {
        super();
    }
    
    public ResetPassword(@org.jetbrains.annotations.Nullable()
    java.lang.String password, @org.jetbrains.annotations.Nullable()
    java.lang.String newPassword) {
        super();
    }
}