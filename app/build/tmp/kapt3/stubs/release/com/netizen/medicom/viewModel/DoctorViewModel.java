package com.netizen.medicom.viewModel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0010\u0018\u00002\u00020\u0001:\u0001DB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u00101\u001a\u000202J\u000e\u0010\u000b\u001a\u0002022\u0006\u00103\u001a\u00020\nJ\u0006\u00104\u001a\u000202J&\u0010-\u001a\u0012\u0012\u0004\u0012\u00020\n0+j\b\u0012\u0004\u0012\u00020\n`52\f\u00106\u001a\b\u0012\u0004\u0012\u00020\b0\u0007H\u0002J&\u00107\u001a\u0012\u0012\u0004\u0012\u00020\n0+j\b\u0012\u0004\u0012\u00020\n`52\f\u00108\u001a\b\u0012\u0004\u0012\u00020\b0\u0007H\u0002Jj\u00109\u001a\u0002022\b\u0010:\u001a\u0004\u0018\u00010\n2\b\u0010;\u001a\u0004\u0018\u00010\n2\b\u0010<\u001a\u0004\u0018\u00010\n2\b\u0010=\u001a\u0004\u0018\u00010\n2\b\u0010>\u001a\u0004\u0018\u00010\n2\b\u0010?\u001a\u0004\u0018\u00010\n2\b\u0010@\u001a\u0004\u0018\u00010\n2\b\u0010A\u001a\u0004\u0018\u00010\n2\b\u0010B\u001a\u0004\u0018\u00010\n2\b\u0010C\u001a\u0004\u0018\u00010\nR\u001a\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\t\u001a\u0004\u0018\u00010\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0011\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00130\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0014\"\u0004\b\u0015\u0010\u0016R \u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00130\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0014\"\u0004\b\u0018\u0010\u0016R \u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00130\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u0014\"\u0004\b\u001a\u0010\u0016R \u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u00130\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001b\u0010\u0014\"\u0004\b\u001c\u0010\u0016R \u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00130\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\u0014\"\u0004\b\u001e\u0010\u0016R \u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00130\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010\u0014\"\u0004\b \u0010\u0016R \u0010!\u001a\b\u0012\u0004\u0012\u00020\u00130\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b!\u0010\u0014\"\u0004\b\"\u0010\u0016R \u0010#\u001a\b\u0012\u0004\u0012\u00020\u00130\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b#\u0010\u0014\"\u0004\b$\u0010\u0016R \u0010%\u001a\b\u0012\u0004\u0012\u00020\u00130\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b%\u0010\u0014\"\u0004\b&\u0010\u0016R \u0010\'\u001a\b\u0012\u0004\u0012\u00020\u00130\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\'\u0010\u0014\"\u0004\b(\u0010\u0016R+\u0010)\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\n ,*\n\u0012\u0004\u0012\u00020\n\u0018\u00010+0+0*\u00a2\u0006\b\n\u0000\u001a\u0004\b-\u0010.R+\u0010/\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\n ,*\n\u0012\u0004\u0012\u00020\n\u0018\u00010+0+0*\u00a2\u0006\b\n\u0000\u001a\u0004\b0\u0010.\u00a8\u0006E"}, d2 = {"Lcom/netizen/medicom/viewModel/DoctorViewModel;", "Landroidx/lifecycle/AndroidViewModel;", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "doctorDesignationArrayList", "Landroidx/lifecycle/MutableLiveData;", "", "Lcom/netizen/medicom/model/DoctorDesignation;", "doctorDesignationId", "", "getDoctorDesignationId", "()Ljava/lang/String;", "setDoctorDesignationId", "(Ljava/lang/String;)V", "doctorRepository", "Lcom/netizen/medicom/repository/DoctorRepository;", "doctorSpecialistArrayList", "isBmdcNumber", "", "()Landroidx/lifecycle/MutableLiveData;", "setBmdcNumber", "(Landroidx/lifecycle/MutableLiveData;)V", "isDegreeInfo", "setDegreeInfo", "isDescription", "setDescription", "isDesignationId", "setDesignationId", "isInstituteName", "setInstituteName", "isNewPatientFeesAmount", "setNewPatientFeesAmount", "isPartnerReferenceId", "setPartnerReferenceId", "isReportPatientFeesAmount", "setReportPatientFeesAmount", "isReturnPatientFeesAmount", "setReturnPatientFeesAmount", "isSpecialistId", "setSpecialistId", "tempDoctorDesignationList", "Landroidx/lifecycle/LiveData;", "Ljava/util/ArrayList;", "kotlin.jvm.PlatformType", "getTempDoctorDesignationList", "()Landroidx/lifecycle/LiveData;", "tempSpecialistList", "getTempSpecialistList", "getDoctorDesignation", "", "selectItemName", "getSpecialist", "Lkotlin/collections/ArrayList;", "designationList", "getTempSpecialisList", "specialistList", "onSignUpClick", "partnerReferenceId", "designationId", "specialistId", "instituteName", "bmdcNumber", "description", "degreeInfo", "newPatientFeesAmount", "returnPatientFeesAmount", "reportPatientFeesAmount", "DoctorViewModelFactory", "app_release"})
public final class DoctorViewModel extends androidx.lifecycle.AndroidViewModel {
    private final com.netizen.medicom.repository.DoctorRepository doctorRepository = null;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isPartnerReferenceId;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDesignationId;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isSpecialistId;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isInstituteName;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isBmdcNumber;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDescription;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDegreeInfo;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isNewPatientFeesAmount;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isReturnPatientFeesAmount;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isReportPatientFeesAmount;
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.medicom.model.DoctorDesignation>> doctorDesignationArrayList = null;
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.medicom.model.DoctorDesignation>> doctorSpecialistArrayList = null;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String doctorDesignationId;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> tempDoctorDesignationList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> tempSpecialistList = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isPartnerReferenceId() {
        return null;
    }
    
    public final void setPartnerReferenceId(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDesignationId() {
        return null;
    }
    
    public final void setDesignationId(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isSpecialistId() {
        return null;
    }
    
    public final void setSpecialistId(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isInstituteName() {
        return null;
    }
    
    public final void setInstituteName(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isBmdcNumber() {
        return null;
    }
    
    public final void setBmdcNumber(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDescription() {
        return null;
    }
    
    public final void setDescription(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDegreeInfo() {
        return null;
    }
    
    public final void setDegreeInfo(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isNewPatientFeesAmount() {
        return null;
    }
    
    public final void setNewPatientFeesAmount(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isReturnPatientFeesAmount() {
        return null;
    }
    
    public final void setReturnPatientFeesAmount(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isReportPatientFeesAmount() {
        return null;
    }
    
    public final void setReportPatientFeesAmount(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDoctorDesignationId() {
        return null;
    }
    
    public final void setDoctorDesignationId(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> getTempDoctorDesignationList() {
        return null;
    }
    
    private final java.util.ArrayList<java.lang.String> getTempDoctorDesignationList(java.util.List<com.netizen.medicom.model.DoctorDesignation> designationList) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> getTempSpecialistList() {
        return null;
    }
    
    private final java.util.ArrayList<java.lang.String> getTempSpecialisList(java.util.List<com.netizen.medicom.model.DoctorDesignation> specialistList) {
        return null;
    }
    
    public final void getDoctorDesignationId(@org.jetbrains.annotations.NotNull()
    java.lang.String selectItemName) {
    }
    
    public final void onSignUpClick(@org.jetbrains.annotations.Nullable()
    java.lang.String partnerReferenceId, @org.jetbrains.annotations.Nullable()
    java.lang.String designationId, @org.jetbrains.annotations.Nullable()
    java.lang.String specialistId, @org.jetbrains.annotations.Nullable()
    java.lang.String instituteName, @org.jetbrains.annotations.Nullable()
    java.lang.String bmdcNumber, @org.jetbrains.annotations.Nullable()
    java.lang.String description, @org.jetbrains.annotations.Nullable()
    java.lang.String degreeInfo, @org.jetbrains.annotations.Nullable()
    java.lang.String newPatientFeesAmount, @org.jetbrains.annotations.Nullable()
    java.lang.String returnPatientFeesAmount, @org.jetbrains.annotations.Nullable()
    java.lang.String reportPatientFeesAmount) {
    }
    
    public final void getDoctorDesignation() {
    }
    
    public final void getSpecialist() {
    }
    
    public DoctorViewModel(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        super(null);
    }
    
    @kotlin.Suppress(names = {"UNCHECKED_CAST"})
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\'\u0010\u0005\u001a\u0002H\u0006\"\n\b\u0000\u0010\u0006*\u0004\u0018\u00010\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00060\tH\u0016\u00a2\u0006\u0002\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"}, d2 = {"Lcom/netizen/medicom/viewModel/DoctorViewModel$DoctorViewModelFactory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "app_release"})
    public static final class DoctorViewModelFactory implements androidx.lifecycle.ViewModelProvider.Factory {
        private final android.app.Application application = null;
        
        @java.lang.Override()
        public <T extends androidx.lifecycle.ViewModel>T create(@org.jetbrains.annotations.NotNull()
        java.lang.Class<T> modelClass) {
            return null;
        }
        
        public DoctorViewModelFactory(@org.jetbrains.annotations.NotNull()
        android.app.Application application) {
            super();
        }
    }
}