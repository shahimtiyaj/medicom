package com.netizen.medicom.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001:\u0002\u000f\u0010B\u0007\b\u0016\u00a2\u0006\u0002\u0010\u0002Ba\b\u0016\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\f\u001a\u0004\u0018\u00010\r\u00a2\u0006\u0002\u0010\u000eR\u0014\u0010\t\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\u0004\u0018\u00010\r8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"}, d2 = {"Lcom/netizen/medicom/model/RegistrationPost;", "", "()V", "fullName", "", "gender", "religion", "dateOfBirth", "basicMobile", "basicEmail", "userName", "userPassword", "globalAreaInfoDTO", "Lcom/netizen/medicom/model/RegistrationPost$GlobalAreaInfoDTO;", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/netizen/medicom/model/RegistrationPost$GlobalAreaInfoDTO;)V", "GlobalAreaInfoDTO", "ProductRoleAssignDTO", "app_release"})
public final class RegistrationPost {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "fullName")
    private java.lang.String fullName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "gender")
    private java.lang.String gender;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "religion")
    private java.lang.String religion;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "dateOfBirth")
    private java.lang.String dateOfBirth;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "basicMobile")
    private java.lang.String basicMobile;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "basicEmail")
    private java.lang.String basicEmail;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "userName")
    private java.lang.String userName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "userPassword")
    private java.lang.String userPassword;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "globalAreaInfoDTO")
    private com.netizen.medicom.model.RegistrationPost.GlobalAreaInfoDTO globalAreaInfoDTO;
    
    public RegistrationPost() {
        super();
    }
    
    public RegistrationPost(@org.jetbrains.annotations.Nullable()
    java.lang.String fullName, @org.jetbrains.annotations.Nullable()
    java.lang.String gender, @org.jetbrains.annotations.Nullable()
    java.lang.String religion, @org.jetbrains.annotations.Nullable()
    java.lang.String dateOfBirth, @org.jetbrains.annotations.Nullable()
    java.lang.String basicMobile, @org.jetbrains.annotations.Nullable()
    java.lang.String basicEmail, @org.jetbrains.annotations.Nullable()
    java.lang.String userName, @org.jetbrains.annotations.Nullable()
    java.lang.String userPassword, @org.jetbrains.annotations.Nullable()
    com.netizen.medicom.model.RegistrationPost.GlobalAreaInfoDTO globalAreaInfoDTO) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0011\b\u0016\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004J\r\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0007J\u0015\u0010\b\u001a\u00020\t2\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005\u00a8\u0006\n"}, d2 = {"Lcom/netizen/medicom/model/RegistrationPost$GlobalAreaInfoDTO;", "", "coreCategoryID", "", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "getCoreCategoryID", "()Ljava/lang/Integer;", "setCoreCategoryID", "", "app_release"})
    public static final class GlobalAreaInfoDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "coreCategoryID")
        private java.lang.Integer coreCategoryID;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getCoreCategoryID() {
            return null;
        }
        
        public final void setCoreCategoryID(@org.jetbrains.annotations.Nullable()
        java.lang.Integer coreCategoryID) {
        }
        
        public GlobalAreaInfoDTO(@org.jetbrains.annotations.Nullable()
        java.lang.Integer coreCategoryID) {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\r\u0010\u0006\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0007J\u0015\u0010\b\u001a\u00020\t2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\nR\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005\u00a8\u0006\u000b"}, d2 = {"Lcom/netizen/medicom/model/RegistrationPost$ProductRoleAssignDTO;", "", "()V", "productRoleAssignID", "", "Ljava/lang/Integer;", "getProductRoleAssignID", "()Ljava/lang/Integer;", "setProductRoleAssignID", "", "(Ljava/lang/Integer;)V", "app_release"})
    public static final class ProductRoleAssignDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "productRoleAssignID")
        private java.lang.Integer productRoleAssignID;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getProductRoleAssignID() {
            return null;
        }
        
        public final void setProductRoleAssignID(@org.jetbrains.annotations.Nullable()
        java.lang.Integer productRoleAssignID) {
        }
        
        public ProductRoleAssignDTO() {
            super();
        }
    }
}