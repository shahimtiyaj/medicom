package com.netizen.medicom.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"}, d2 = {"Lcom/netizen/medicom/utils/AppUtilsClass;", "", "()V", "Companion", "app_release"})
public final class AppUtilsClass {
    private static final int IMAGE_PICK_CODE = 1000;
    private static final int PERMISSION_CODE_READ = 1001;
    private static final int PERMISSION_CODE_WRITE = 1002;
    private static final int PERMISSION_CODE_CAMERA = 1003;
    public static final com.netizen.medicom.utils.AppUtilsClass.Companion Companion = null;
    
    public AppUtilsClass() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0010\u0006\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bJ\u0017\u0010\f\u001a\u0004\u0018\u00010\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000f\u00a2\u0006\u0002\u0010\u0010J\u000e\u0010\u0011\u001a\u00020\r2\u0006\u0010\u0012\u001a\u00020\rJ\u000e\u0010\u0013\u001a\u00020\r2\u0006\u0010\u0012\u001a\u00020\rJ\u000e\u0010\u0014\u001a\u00020\r2\u0006\u0010\u0015\u001a\u00020\u0016J\u000e\u0010\u0017\u001a\u00020\t2\u0006\u0010\u0018\u001a\u00020\rJ\u0010\u0010\u0019\u001a\u00020\t2\b\u0010\u001a\u001a\u0004\u0018\u00010\rJ\u0016\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u000bJ\u0016\u0010 \u001a\u00020\u001c2\u0006\u0010\u001f\u001a\u00020!2\u0006\u0010\"\u001a\u00020\rJ\u0016\u0010#\u001a\u00020\u001c2\u0006\u0010\u001f\u001a\u00020!2\u0006\u0010$\u001a\u00020%J\u0010\u0010&\u001a\u00020\t2\b\u0010\'\u001a\u0004\u0018\u00010\rR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006("}, d2 = {"Lcom/netizen/medicom/utils/AppUtilsClass$Companion;", "", "()V", "IMAGE_PICK_CODE", "", "PERMISSION_CODE_CAMERA", "PERMISSION_CODE_READ", "PERMISSION_CODE_WRITE", "checkPermissionForImage", "", "activity", "Landroid/app/Activity;", "getDate", "", "time", "", "(Ljava/lang/Long;)Ljava/lang/String;", "getDateFormat", "date", "getDateFormatForSerachData", "getDecimalFormattedValue", "value", "", "isValidEmail", "email", "isValidPassword", "password", "requestFocus", "", "view", "Landroid/view/View;", "context", "showErrorToasty", "Landroid/content/Context;", "message", "showsDatePicker", "dateListener", "Landroid/app/DatePickerDialog$OnDateSetListener;", "validCellPhone", "number", "app_release"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getDateFormat(@org.jetbrains.annotations.NotNull()
        java.lang.String date) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getDateFormatForSerachData(@org.jetbrains.annotations.NotNull()
        java.lang.String date) {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getDate(@org.jetbrains.annotations.Nullable()
        java.lang.Long time) {
            return null;
        }
        
        public final void showErrorToasty(@org.jetbrains.annotations.NotNull()
        android.content.Context context, @org.jetbrains.annotations.NotNull()
        java.lang.String message) {
        }
        
        public final void showsDatePicker(@org.jetbrains.annotations.NotNull()
        android.content.Context context, @org.jetbrains.annotations.NotNull()
        android.app.DatePickerDialog.OnDateSetListener dateListener) {
        }
        
        public final boolean isValidPassword(@org.jetbrains.annotations.Nullable()
        java.lang.String password) {
            return false;
        }
        
        public final boolean isValidEmail(@org.jetbrains.annotations.NotNull()
        java.lang.String email) {
            return false;
        }
        
        public final boolean validCellPhone(@org.jetbrains.annotations.Nullable()
        java.lang.String number) {
            return false;
        }
        
        public final void requestFocus(@org.jetbrains.annotations.NotNull()
        android.view.View view, @org.jetbrains.annotations.NotNull()
        android.app.Activity context) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getDecimalFormattedValue(double value) {
            return null;
        }
        
        public final boolean checkPermissionForImage(@org.jetbrains.annotations.NotNull()
        android.app.Activity activity) {
            return false;
        }
        
        private Companion() {
            super();
        }
    }
}