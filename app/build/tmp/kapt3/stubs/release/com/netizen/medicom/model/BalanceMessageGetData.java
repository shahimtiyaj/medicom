package com.netizen.medicom.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0006\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\n\u0018\u00002\u00020\u0001:\u0002\"#B\u0005\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u0017\u001a\u00020\u0004J\r\u0010\u001b\u001a\u0004\u0018\u00010\u0015\u00a2\u0006\u0002\u0010\u001cJ\r\u0010\u001d\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u001eJ\b\u0010\u001f\u001a\u0004\u0018\u00010\rJ\u000e\u0010 \u001a\u00020\u001a2\u0006\u0010\u0014\u001a\u00020\u0015J\u000e\u0010!\u001a\u00020\u001a2\u0006\u0010\u0018\u001a\u00020\rR\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R \u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR\u0014\u0010\f\u001a\u0004\u0018\u00010\r8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R \u0010\u000e\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013R\u0016\u0010\u0014\u001a\u0004\u0018\u00010\u00158\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0016R\u0016\u0010\u0017\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u0014\u0010\u0018\u001a\u0004\u0018\u00010\r8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006$"}, d2 = {"Lcom/netizen/medicom/model/BalanceMessageGetData;", "", "()V", "payableAmount", "", "Ljava/lang/Double;", "productInfoDTO", "Lcom/netizen/medicom/model/BalanceMessageGetData$ProductInfoDTO;", "getProductInfoDTO", "()Lcom/netizen/medicom/model/BalanceMessageGetData$ProductInfoDTO;", "setProductInfoDTO", "(Lcom/netizen/medicom/model/BalanceMessageGetData$ProductInfoDTO;)V", "productName", "", "productPurchaseLogDTO", "Lcom/netizen/medicom/model/BalanceMessageGetData$ProductPurchaseLogDTO;", "getProductPurchaseLogDTO", "()Lcom/netizen/medicom/model/BalanceMessageGetData$ProductPurchaseLogDTO;", "setProductPurchaseLogDTO", "(Lcom/netizen/medicom/model/BalanceMessageGetData$ProductPurchaseLogDTO;)V", "quantity", "", "Ljava/lang/Long;", "salesPrice", "trxDate", "SetSalesPrice", "", "getQuantity", "()Ljava/lang/Long;", "getSalesPrice", "()Ljava/lang/Double;", "getTrxDate", "setQuantity", "setTrxDate", "ProductInfoDTO", "ProductPurchaseLogDTO", "app_release"})
public final class BalanceMessageGetData {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "trxDate")
    private java.lang.String trxDate;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "quantity")
    private java.lang.Long quantity;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "productName")
    private java.lang.String productName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "salesPrice")
    private java.lang.Double salesPrice;
    private java.lang.Double payableAmount;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "productInfoDTO")
    private com.netizen.medicom.model.BalanceMessageGetData.ProductInfoDTO productInfoDTO;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "productPurchaseLogDTO")
    private com.netizen.medicom.model.BalanceMessageGetData.ProductPurchaseLogDTO productPurchaseLogDTO;
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.medicom.model.BalanceMessageGetData.ProductInfoDTO getProductInfoDTO() {
        return null;
    }
    
    public final void setProductInfoDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.medicom.model.BalanceMessageGetData.ProductInfoDTO p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.medicom.model.BalanceMessageGetData.ProductPurchaseLogDTO getProductPurchaseLogDTO() {
        return null;
    }
    
    public final void setProductPurchaseLogDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.medicom.model.BalanceMessageGetData.ProductPurchaseLogDTO p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTrxDate() {
        return null;
    }
    
    public final void setTrxDate(@org.jetbrains.annotations.NotNull()
    java.lang.String trxDate) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getQuantity() {
        return null;
    }
    
    public final void setQuantity(long quantity) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Double getSalesPrice() {
        return null;
    }
    
    public final void SetSalesPrice(double salesPrice) {
    }
    
    public BalanceMessageGetData() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u000e\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u0004R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lcom/netizen/medicom/model/BalanceMessageGetData$ProductInfoDTO;", "", "()V", "productName", "", "getProductName", "setProductName", "", "app_release"})
    public static final class ProductInfoDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "productName")
        private java.lang.String productName;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getProductName() {
            return null;
        }
        
        public final void setProductName(@org.jetbrains.annotations.NotNull()
        java.lang.String productName) {
        }
        
        public ProductInfoDTO() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0006\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\r\u0010\u0006\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0007J\u000e\u0010\b\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u0004R\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005\u00a8\u0006\n"}, d2 = {"Lcom/netizen/medicom/model/BalanceMessageGetData$ProductPurchaseLogDTO;", "", "()V", "payableAmount", "", "Ljava/lang/Double;", "getPayableAmount", "()Ljava/lang/Double;", "setPayableAmount", "", "app_release"})
    public static final class ProductPurchaseLogDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "paidAmount")
        private java.lang.Double payableAmount;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Double getPayableAmount() {
            return null;
        }
        
        public final void setPayableAmount(double payableAmount) {
        }
        
        public ProductPurchaseLogDTO() {
            super();
        }
    }
}