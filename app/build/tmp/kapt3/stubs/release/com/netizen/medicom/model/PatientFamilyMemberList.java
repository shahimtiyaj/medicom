package com.netizen.medicom.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001:\u0001\u0014B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u000b\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0005\u0018\u00010\u0004J\b\u0010\f\u001a\u0004\u0018\u00010\u0007J\r\u0010\r\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\u000eJ\u0018\u0010\u000f\u001a\u00020\u00102\u0010\u0010\u0003\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0005\u0018\u00010\u0004J\u0010\u0010\u0011\u001a\u00020\u00102\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007J\u0015\u0010\u0012\u001a\u00020\u00102\b\u0010\b\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\u0013R\u001c\u0010\u0003\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0005\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\b\u001a\u0004\u0018\u00010\t8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\n\u00a8\u0006\u0015"}, d2 = {"Lcom/netizen/medicom/model/PatientFamilyMemberList;", "", "()V", "item", "", "Lcom/netizen/medicom/model/PatientFamilyMemberList$Item;", "message", "", "msgType", "", "Ljava/lang/Integer;", "getItem", "getMessage", "getMsgType", "()Ljava/lang/Integer;", "setItem", "", "setMessage", "setMsgType", "(Ljava/lang/Integer;)V", "Item", "app_release"})
public final class PatientFamilyMemberList {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "message")
    private java.lang.String message;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "msgType")
    private java.lang.Integer msgType;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "item")
    private java.util.List<com.netizen.medicom.model.PatientFamilyMemberList.Item> item;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMessage() {
        return null;
    }
    
    public final void setMessage(@org.jetbrains.annotations.Nullable()
    java.lang.String message) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getMsgType() {
        return null;
    }
    
    public final void setMsgType(@org.jetbrains.annotations.Nullable()
    java.lang.Integer msgType) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.netizen.medicom.model.PatientFamilyMemberList.Item> getItem() {
        return null;
    }
    
    public final void setItem(@org.jetbrains.annotations.Nullable()
    java.util.List<com.netizen.medicom.model.PatientFamilyMemberList.Item> item) {
    }
    
    public PatientFamilyMemberList() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0011\n\u0002\u0010\u0002\n\u0002\b\u000b\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0010\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0011\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0012\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0013\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0014\u001a\u0004\u0018\u00010\u0004J\r\u0010\u0015\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0002\u0010\u0016J\b\u0010\u0017\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0018\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0019\u001a\u0004\u0018\u00010\u0004J\b\u0010\u001a\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u001b\u001a\u00020\u001c2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u001d\u001a\u00020\u001c2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u001e\u001a\u00020\u001c2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u001f\u001a\u00020\u001c2\b\u0010\u0007\u001a\u0004\u0018\u00010\u0004J\u0010\u0010 \u001a\u00020\u001c2\b\u0010\b\u001a\u0004\u0018\u00010\u0004J\u0015\u0010!\u001a\u00020\u001c2\b\u0010\t\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0002\u0010\"J\u0010\u0010#\u001a\u00020\u001c2\b\u0010\f\u001a\u0004\u0018\u00010\u0004J\u0010\u0010$\u001a\u00020\u001c2\b\u0010\r\u001a\u0004\u0018\u00010\u0004J\u0010\u0010%\u001a\u00020\u001c2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0004J\u0010\u0010&\u001a\u00020\u001c2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0004R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\t\u001a\u0004\u0018\u00010\n8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u000bR\u0014\u0010\f\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\'"}, d2 = {"Lcom/netizen/medicom/model/PatientFamilyMemberList$Item;", "", "()V", "age", "", "bloodGroup", "gender", "patientDob", "patientEmail", "patientId", "", "Ljava/lang/Integer;", "patientMobile", "patientName", "relation", "religion", "getAge", "getBloodGroup", "getGender", "getPatientDob", "getPatientEmail", "getPatientId", "()Ljava/lang/Integer;", "getPatientMobile", "getPatientName", "getRelation", "getReligion", "setAge", "", "setBloodGroup", "setGender", "setPatientDob", "setPatientEmail", "setPatientId", "(Ljava/lang/Integer;)V", "setPatientMobile", "setPatientName", "setRelation", "setReligion", "app_release"})
    public static final class Item {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "patientId")
        private java.lang.Integer patientId;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "patientName")
        private java.lang.String patientName;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "patientEmail")
        private java.lang.String patientEmail;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "patientMobile")
        private java.lang.String patientMobile;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "patientDob")
        private java.lang.String patientDob;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "gender")
        private java.lang.String gender;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "bloodGroup")
        private java.lang.String bloodGroup;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "relation")
        private java.lang.String relation;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "religion")
        private java.lang.String religion;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "age")
        private java.lang.String age;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getPatientId() {
            return null;
        }
        
        public final void setPatientId(@org.jetbrains.annotations.Nullable()
        java.lang.Integer patientId) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getPatientName() {
            return null;
        }
        
        public final void setPatientName(@org.jetbrains.annotations.Nullable()
        java.lang.String patientName) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getPatientEmail() {
            return null;
        }
        
        public final void setPatientEmail(@org.jetbrains.annotations.Nullable()
        java.lang.String patientEmail) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getPatientMobile() {
            return null;
        }
        
        public final void setPatientMobile(@org.jetbrains.annotations.Nullable()
        java.lang.String patientMobile) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getPatientDob() {
            return null;
        }
        
        public final void setPatientDob(@org.jetbrains.annotations.Nullable()
        java.lang.String patientDob) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getGender() {
            return null;
        }
        
        public final void setGender(@org.jetbrains.annotations.Nullable()
        java.lang.String gender) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getBloodGroup() {
            return null;
        }
        
        public final void setBloodGroup(@org.jetbrains.annotations.Nullable()
        java.lang.String bloodGroup) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getRelation() {
            return null;
        }
        
        public final void setRelation(@org.jetbrains.annotations.Nullable()
        java.lang.String relation) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getReligion() {
            return null;
        }
        
        public final void setReligion(@org.jetbrains.annotations.Nullable()
        java.lang.String religion) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getAge() {
            return null;
        }
        
        public final void setAge(@org.jetbrains.annotations.Nullable()
        java.lang.String age) {
        }
        
        public Item() {
            super();
        }
    }
}