package com.netizen.medicom.quickblox;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\n\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"ACCOUNT_KEY", "", "APPLICATION_ID", "AUTH_KEY", "AUTH_SECRET", "DEFAULT_USER_PASSWORD", "app_release"})
public final class AppKt {
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DEFAULT_USER_PASSWORD = "12345678";
    private static final java.lang.String APPLICATION_ID = "80936";
    private static final java.lang.String AUTH_KEY = "ySNh9pNe8xxcP8w";
    private static final java.lang.String AUTH_SECRET = "j2b8RxMKfkggESa";
    private static final java.lang.String ACCOUNT_KEY = "SVMUGr3gbkQyoJ_oxV6y";
}