package com.netizen.medicom.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0016\u00a2\u0006\u0002\u0010\u0002B\u0019\b\u0016\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\b\u0010\t\u001a\u0004\u0018\u00010\u0006J\r\u0010\n\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u000bJ\u000e\u0010\f\u001a\u00020\r2\u0006\u0010\u0005\u001a\u00020\u0006J\u0015\u0010\u000e\u001a\u00020\r2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u000fR\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\b\u00a8\u0006\u0010"}, d2 = {"Lcom/netizen/medicom/model/MSG;", "", "()V", "success", "", "message", "", "(Ljava/lang/Integer;Ljava/lang/String;)V", "Ljava/lang/Integer;", "getMessage", "getSuccess", "()Ljava/lang/Integer;", "setMessage", "", "setSuccess", "(Ljava/lang/Integer;)V", "app_release"})
public final class MSG {
    private java.lang.Integer success;
    private java.lang.String message;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getSuccess() {
        return null;
    }
    
    public final void setSuccess(@org.jetbrains.annotations.Nullable()
    java.lang.Integer success) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMessage() {
        return null;
    }
    
    public final void setMessage(@org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    public MSG() {
        super();
    }
    
    public MSG(@org.jetbrains.annotations.Nullable()
    java.lang.Integer success, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
        super();
    }
}