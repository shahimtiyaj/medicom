package com.netizen.medicom.apiService;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u009c\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001JF\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\b\b\u0001\u0010\b\u001a\u00020\u0004H\'JF\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u00032\b\b\u0001\u0010\u000b\u001a\u00020\f2,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u0007H\'JF\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u000b\u001a\u00020\f2,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u0007H\'J\"\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000f0\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u00042\b\b\u0001\u0010\u000b\u001a\u00020\u0011H\'JL\u0010\u0012\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00140\u00130\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\b\b\u0001\u0010\u000b\u001a\u00020\u0015H\'JL\u0010\u0016\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00170\u00130\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\b\b\u0001\u0010\u000b\u001a\u00020\u0018H\'JL\u0010\u0019\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u001a0\u00130\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\b\b\u0001\u0010\u000b\u001a\u00020\u0015H\'JH\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\n0\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\n\b\u0001\u0010\u000b\u001a\u0004\u0018\u00010\u001cH\'JF\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\n0\u00032,\b\u0001\u0010\u0010\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\b\b\u0001\u0010\u000b\u001a\u00020\u001eH\'J$\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020 0\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u00042\n\b\u0001\u0010!\u001a\u0004\u0018\u00010\u0004H\'J$\u0010\"\u001a\b\u0012\u0004\u0012\u00020\n0\u00032\b\b\u0001\u0010#\u001a\u00020\u00042\n\b\u0001\u0010\u000b\u001a\u0004\u0018\u00010$H\'J\u0018\u0010%\u001a\b\u0012\u0004\u0012\u00020\u00010\u00032\b\b\u0001\u0010\b\u001a\u00020\u0004H\'JF\u0010&\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u000b\u001a\u00020\'2,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u0007H\'JF\u0010(\u001a\b\u0012\u0004\u0012\u00020)0\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\b\b\u0001\u0010*\u001a\u00020\u0004H\'J\u001e\u0010+\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020,0\u00130\u00032\b\b\u0001\u0010-\u001a\u00020\u0004H\'J*\u0010.\u001a\u0010\u0012\f\u0012\n\u0012\u0004\u0012\u00020/\u0018\u00010\u00130\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u00042\b\b\u0001\u00100\u001a\u00020\u0004H\'J$\u00101\u001a\n\u0012\u0006\u0012\u0004\u0018\u0001020\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u00042\b\b\u0001\u00103\u001a\u00020\u0004H\'J(\u00104\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002050\u00130\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u00042\b\b\u0001\u00106\u001a\u00020\u0004H\'J6\u00107\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002080\u00130\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u00042\n\b\u0001\u00109\u001a\u0004\u0018\u00010\u00042\n\b\u0001\u0010:\u001a\u0004\u0018\u00010\u0004H\'J*\u0010;\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002050\u00130\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u00042\n\b\u0001\u00106\u001a\u0004\u0018\u00010\u0004H\'J\u001e\u0010<\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020=0\u00130\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u0004H\'JF\u0010>\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\b\b\u0001\u0010\b\u001a\u00020\u0004H\'J\u0014\u0010?\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020@0\u00130\u0003H\'J2\u0010A\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020B0\u00130\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u00042\b\b\u0001\u00106\u001a\u00020\u00042\b\b\u0001\u0010C\u001a\u00020\u0004H\'JV\u0010D\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020E0\u00130\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\b\b\u0001\u0010F\u001a\u00020\u00042\b\b\u0001\u0010G\u001a\u00020\u0004H\'J\u0018\u0010H\u001a\b\u0012\u0004\u0012\u00020I0\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u0004H\'JF\u0010J\u001a\b\u0012\u0004\u0012\u00020K0\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\b\b\u0001\u0010\b\u001a\u00020\u0004H\'JF\u0010L\u001a\b\u0012\u0004\u0012\u00020M0\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\b\b\u0001\u0010\b\u001a\u00020\u0004H\'JF\u0010N\u001a\b\u0012\u0004\u0012\u00020O0\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\b\b\u0001\u0010\b\u001a\u00020\u0004H\'J\u0018\u0010P\u001a\b\u0012\u0004\u0012\u00020Q0\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u0004H\'J(\u0010R\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020S0\u00130\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u00042\b\b\u0001\u0010T\u001a\u00020\u0004H\'J(\u0010U\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020S0\u00130\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u00042\b\b\u0001\u0010-\u001a\u00020\u0004H\'JS\u0010V\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020W0\u00130\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\n\b\u0001\u0010X\u001a\u0004\u0018\u00010YH\'\u00a2\u0006\u0002\u0010ZJN\u0010[\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\\0\u00130\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\n\b\u0001\u0010\b\u001a\u0004\u0018\u00010\u0004H\'JS\u0010]\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020W0\u00130\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\n\b\u0001\u0010X\u001a\u0004\u0018\u00010YH\'\u00a2\u0006\u0002\u0010ZJL\u0010^\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020_0\u00130\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\b\b\u0001\u0010\u000b\u001a\u00020`H\'JF\u0010a\u001a\b\u0012\u0004\u0012\u00020b0\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\b\b\u0001\u0010c\u001a\u00020\u0004H\'JS\u0010d\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020e0\u00130\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\n\b\u0001\u0010f\u001a\u0004\u0018\u00010YH\'\u00a2\u0006\u0002\u0010ZJ<\u0010g\u001a\b\u0012\u0004\u0012\u00020h0\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u0007H\'J$\u0010i\u001a\b\u0012\u0004\u0012\u00020j0\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u00042\n\b\u0001\u0010k\u001a\u0004\u0018\u00010lH\'J\u001e\u0010m\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020n0\u00130\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u0004H\'J\u001e\u0010o\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020p0\u00130\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u0004H\'J(\u0010q\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020r0\u00130\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u00042\b\b\u0001\u00106\u001a\u00020\u0004H\'J\u001e\u0010s\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020n0\u00130\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u0004H\'J<\u0010t\u001a\b\u0012\u0004\u0012\u00020u0\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u0007H\'JL\u0010v\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020w0\u00130\u00032\b\b\u0001\u0010\u000b\u001a\u00020\u00182,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u0007H\'JF\u0010x\u001a\b\u0012\u0004\u0012\u00020\n0\u00032\b\b\u0001\u0010\u000b\u001a\u00020y2,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u0007H\'JF\u0010z\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u000b\u001a\u00020{2,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u0007H\'J\"\u0010|\u001a\b\u0012\u0004\u0012\u00020\u000f0\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u00042\b\b\u0001\u0010}\u001a\u00020\u0004H\'Jk\u0010~\u001a\b\u0012\u0004\u0012\u00020\u007f0\u00032-\b\u0001\u0010\u0080\u0001\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u0007H\'J\u001a\u0010\u0081\u0001\u001a\b\u0012\u0004\u0012\u00020\n0\u00032\t\b\u0001\u0010\u000b\u001a\u00030\u0082\u0001H\'J\u001b\u0010\u0083\u0001\u001a\b\u0012\u0004\u0012\u00020\n0\u00032\n\b\u0001\u0010\u000b\u001a\u0004\u0018\u00010\u001cH\'J&\u0010\u0084\u0001\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u00042\t\b\u0001\u0010\u000b\u001a\u00030\u0085\u0001H\'J%\u0010\u0086\u0001\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u00042\n\b\u0001\u0010\u0087\u0001\u001a\u00030\u0088\u0001H\'JH\u0010\u0089\u0001\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\t\b\u0001\u0010\u000b\u001a\u00030\u008a\u0001H\'J%\u0010\u008b\u0001\u001a\b\u0012\u0004\u0012\u00020\n0\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u00042\n\b\u0001\u0010\u008c\u0001\u001a\u00030\u008d\u0001H\'J%\u0010\u008e\u0001\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u00042\n\b\u0001\u0010\u008f\u0001\u001a\u00030\u0090\u0001H\'J\u001a\u0010\u0091\u0001\u001a\b\u0012\u0004\u0012\u00020$0\u00032\t\b\u0001\u0010\u0092\u0001\u001a\u00020\u0004H\'J1\u0010\u0093\u0001\u001a\t\u0012\u0005\u0012\u00030\u0094\u00010\u00032\t\b\u0001\u0010\u0095\u0001\u001a\u00020\u00042\t\b\u0001\u0010\u0096\u0001\u001a\u00020\u00042\t\b\u0001\u0010\u0097\u0001\u001a\u00020\u0004H\'\u00a8\u0006\u0098\u0001"}, d2 = {"Lcom/netizen/medicom/apiService/ApiInterface;", "", "OTPVarify", "Lretrofit2/Call;", "", "headers", "Ljava/util/HashMap;", "Lkotlin/collections/HashMap;", "code", "WalletBalanceTransfer", "Ljava/lang/Void;", "obj", "Lcom/netizen/medicom/model/WalletTransfer;", "WalletBalanceTransferFinal", "addPatientSubmit", "Lcom/netizen/medicom/model/PatientEnrollResponse;", "token", "Lcom/netizen/medicom/model/AddPatientFamilyMember;", "balanceDepositReportGetData", "", "Lcom/netizen/medicom/model/BalanceDepositReportGetData;", "Lcom/netizen/medicom/model/BalanceReportPostData;", "balanceStatementGetData", "Lcom/netizen/medicom/model/BalanceStatementGetData;", "Lcom/netizen/medicom/model/BalanceDepositPostForList;", "balanceTransferGetData", "Lcom/netizen/medicom/model/BalanceTransferGetData;", "changePassword", "Lcom/netizen/medicom/model/ResetPassword;", "doctorRegistrationSubmit", "Lcom/netizen/medicom/model/DoctorEnroll;", "downloadTokenAttachment", "Lcom/google/gson/JsonObject;", "imageUrl", "forgotPasswordOTP", "userContactNo", "Lcom/netizen/medicom/model/UserCheckForgotPass;", "forgotPasswordOTPVarify", "generalProductPostData", "Lcom/netizen/medicom/model/GeneralProductPostData;", "getAccountNoInfo", "Lcom/netizen/medicom/model/MobileBankAcNo;", "localBankCategoryID", "getArea", "Lcom/netizen/medicom/model/AreaModel;", "parentCategoryID", "getBalanceDepositAccountInfo", "Lcom/netizen/medicom/model/BalanceDepositAccountInfo;", "defaultCode", "getBalanceWithdrawInfo", "Lcom/netizen/medicom/model/WithDrawInfo;", "codeCategoryDefaultCode", "getBankAccountList", "Lcom/netizen/medicom/model/BankAccountInfo;", "typeDefaultCode", "getBankBranchList", "Lcom/netizen/medicom/model/BankBranchInfo;", "bankId", "districtId", "getBankDistrictList", "getBankList", "Lcom/netizen/medicom/model/BankList;", "getDataSearchByNetiID", "getDistrict", "Lcom/netizen/medicom/model/DistrictModel;", "getDoctorDesignation", "Lcom/netizen/medicom/model/DoctorDesignation;", "typeStatus", "getGeneralProductReportData", "Lcom/netizen/medicom/model/GeneralProductGetData;", "startDate", "endDate", "getHomePageInfo", "Lcom/netizen/medicom/model/HomePageInfo;", "getMessageType", "Lcom/netizen/medicom/model/MessageType;", "getMessageType1", "Lcom/netizen/medicom/model/MessageType1;", "getOfferProduct", "Lcom/netizen/medicom/model/OfferProduct;", "getPatientFamilyMemberList", "Lcom/netizen/medicom/model/PatientFamilyMemberList;", "getProblemModuleList", "Lcom/netizen/medicom/model/Problem;", "catDefaultCode", "getProblemTypeList", "getProductGDetailsData", "Lcom/netizen/medicom/model/ProductGDetailsGetData;", "purchaseID", "", "(Ljava/util/HashMap;Ljava/lang/Integer;)Lretrofit2/Call;", "getProductName", "Lcom/netizen/medicom/model/Products;", "getProductODetailsData", "getProductOfferReportData", "Lcom/netizen/medicom/model/ProductsOfferGetData;", "Lcom/netizen/medicom/model/ProductsOfferReportPostData;", "getProfile", "Lcom/netizen/medicom/model/ProfileImage;", "filePath", "getPurchaseCodeData", "Lcom/netizen/medicom/model/PurchaseCodeLogGetdata;", "usedStatus", "getPurchasePoint", "Lcom/netizen/medicom/model/PurchasePoint;", "getRevenueLogData", "Lcom/netizen/medicom/model/RevenueLogGetData;", "revenueLogPostData", "Lcom/netizen/medicom/model/RevenueLogPostData;", "getSolvedAndPendingToken", "Lcom/netizen/medicom/model/TokenList;", "getTagList", "Lcom/netizen/medicom/model/TagGetData;", "getTagTypeList", "Lcom/netizen/medicom/model/TagType;", "getTokenList", "getUserProfileInfo", "Lcom/netizen/medicom/model/ProfileInformation;", "messageRechargeGetData", "Lcom/netizen/medicom/model/BalanceMessageGetData;", "messageRechargepostData", "Lcom/netizen/medicom/model/MessageRecharge;", "offerProductPostData", "Lcom/netizen/medicom/model/OfferProductPost;", "patientRegistrationSubmit", "partnerReferenceID", "postData", "Lcom/netizen/medicom/model/PostResponse;", "params", "registrationSubmit", "Lcom/netizen/medicom/model/RegistrationPost;", "resetPasswordAfterForgot", "submitBalanceWithdrawData", "Lcom/netizen/medicom/model/WithdrawPostData;", "submitBankAccount", "bankAccountPostData", "Lcom/netizen/medicom/model/BankAccountPostData;", "submitDepositPostData", "Lcom/netizen/medicom/model/DepositPostData;", "submitNewToken", "tokenSubmitPostData", "Lcom/netizen/medicom/model/TokenSubmitPostData;", "tagBankAccount", "tagPostData", "Lcom/netizen/medicom/model/TagPostData;", "userCheckForgotPaasword", "username", "userLogIn", "Lcom/netizen/medicom/model/MSG;", "name", "password", "grant_type", "app_release"})
public abstract interface ApiInterface {
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "oauth/token")
    @retrofit2.http.FormUrlEncoded()
    public abstract retrofit2.Call<com.netizen.medicom.model.PostResponse> postData(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.FieldMap()
    java.util.HashMap<java.lang.String, java.lang.String> params, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/profile/details")
    public abstract retrofit2.Call<com.netizen.medicom.model.ProfileInformation> getUserProfileInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/account/by/corebankid")
    public abstract retrofit2.Call<com.netizen.medicom.model.MobileBankAcNo> getAccountNoInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "coreBankID")
    java.lang.String localBankCategoryID);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/category/by/type/2nd_parent_type")
    public abstract retrofit2.Call<java.util.List<com.netizen.medicom.model.BankAccountInfo>> getBankAccountList(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "typeDefaultCode")
    java.lang.String typeDefaultCode);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/message/types/by/point")
    public abstract retrofit2.Call<com.netizen.medicom.model.MessageType> getMessageType(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "roleID")
    java.lang.String code);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/message/types/by/point")
    public abstract retrofit2.Call<com.netizen.medicom.model.MessageType1> getMessageType1(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "roleID")
    java.lang.String code);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/roles/assigned")
    public abstract retrofit2.Call<com.netizen.medicom.model.PurchasePoint> getPurchasePoint(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/products/by/role")
    public abstract retrofit2.Call<java.util.List<com.netizen.medicom.model.Products>> getProductName(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Query(value = "roleID")
    java.lang.String code);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/balance/deposit")
    public abstract retrofit2.Call<java.lang.String> submitDepositPostData(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.medicom.model.DepositPostData obj);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/message/recharge")
    public abstract retrofit2.Call<java.lang.Void> messageRechargepostData(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.medicom.model.MessageRecharge obj, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/product/purchase")
    public abstract retrofit2.Call<java.lang.String> generalProductPostData(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.medicom.model.GeneralProductPostData obj, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/product/offer/by/code")
    public abstract retrofit2.Call<com.netizen.medicom.model.OfferProduct> getOfferProduct(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "code")
    java.lang.String code);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/product/purchase/offer")
    public abstract retrofit2.Call<java.lang.String> offerProductPostData(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.medicom.model.OfferProductPost obj, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/profile/by/custom_id")
    public abstract retrofit2.Call<java.lang.String> getDataSearchByNetiID(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "custom_id")
    java.lang.String code);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/transfer/check/balance")
    public abstract retrofit2.Call<java.lang.Void> WalletBalanceTransfer(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.medicom.model.WalletTransfer obj, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "guest/core/check-otp")
    public abstract retrofit2.Call<java.lang.String> OTPVarify(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "code")
    java.lang.String code);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/balance/transfer")
    public abstract retrofit2.Call<java.lang.String> WalletBalanceTransferFinal(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.medicom.model.WalletTransfer obj, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/balance/requests/by/date_range")
    public abstract retrofit2.Call<java.util.List<com.netizen.medicom.model.BalanceDepositReportGetData>> balanceDepositReportGetData(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.medicom.model.BalanceReportPostData obj);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/balance/transfer/by/date")
    public abstract retrofit2.Call<java.util.List<com.netizen.medicom.model.BalanceTransferGetData>> balanceTransferGetData(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.medicom.model.BalanceReportPostData obj);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/message/by/date-range")
    public abstract retrofit2.Call<java.util.List<com.netizen.medicom.model.BalanceMessageGetData>> messageRechargeGetData(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.medicom.model.BalanceDepositPostForList obj, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/balance/statement/for/android")
    public abstract retrofit2.Call<java.util.List<com.netizen.medicom.model.BalanceStatementGetData>> balanceStatementGetData(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.medicom.model.BalanceDepositPostForList obj);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/product/purchases/by/date-range")
    public abstract retrofit2.Call<java.util.List<com.netizen.medicom.model.GeneralProductGetData>> getGeneralProductReportData(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "startDate")
    java.lang.String startDate, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "endDate")
    java.lang.String endDate);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/purchase/codes/by/purchaseid")
    public abstract retrofit2.Call<java.util.List<com.netizen.medicom.model.ProductGDetailsGetData>> getProductGDetailsData(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Query(value = "purchaseID")
    java.lang.Integer purchaseID);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/product/offer/by/date-range")
    public abstract retrofit2.Call<java.util.List<com.netizen.medicom.model.ProductsOfferGetData>> getProductOfferReportData(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.medicom.model.ProductsOfferReportPostData obj);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/purchase/codes/by/purchaseid")
    public abstract retrofit2.Call<java.util.List<com.netizen.medicom.model.ProductGDetailsGetData>> getProductODetailsData(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Query(value = "purchaseID")
    java.lang.Integer purchaseID);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/purchase/codes/by/usedstatus")
    public abstract retrofit2.Call<java.util.List<com.netizen.medicom.model.PurchaseCodeLogGetdata>> getPurchaseCodeData(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Query(value = "usedStatus")
    java.lang.Integer usedStatus);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "guest/file/find")
    public abstract retrofit2.Call<com.netizen.medicom.model.ProfileImage> getProfile(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "filePath")
    java.lang.String filePath);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "guest/user/register-user")
    public abstract retrofit2.Call<java.lang.Void> registrationSubmit(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.medicom.model.RegistrationPost obj);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "guest/addresses/district")
    public abstract retrofit2.Call<java.util.List<com.netizen.medicom.model.DistrictModel>> getDistrict();
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "guest/addresses/area")
    public abstract retrofit2.Call<java.util.List<com.netizen.medicom.model.AreaModel>> getArea(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "parentCategoryID")
    java.lang.String parentCategoryID);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "guest/user/profile/by")
    public abstract retrofit2.Call<com.netizen.medicom.model.UserCheckForgotPass> userCheckForgotPaasword(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "username")
    java.lang.String username);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "guest/recover/password/check_contact")
    public abstract retrofit2.Call<java.lang.Void> forgotPasswordOTP(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "userContactNo")
    java.lang.String userContactNo, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Body()
    com.netizen.medicom.model.UserCheckForgotPass obj);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "guest/core/check-otp")
    public abstract retrofit2.Call<java.lang.Object> forgotPasswordOTPVarify(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "code")
    java.lang.String code);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "guest/recover/password/reset")
    public abstract retrofit2.Call<java.lang.Void> resetPasswordAfterForgot(@org.jetbrains.annotations.Nullable()
    @retrofit2.http.Body()
    com.netizen.medicom.model.ResetPassword obj);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/change/password")
    public abstract retrofit2.Call<java.lang.Void> changePassword(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Body()
    com.netizen.medicom.model.ResetPassword obj);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "oauth/token")
    @retrofit2.http.FormUrlEncoded()
    @retrofit2.http.Headers(value = {"Authorization: Basic ZGV2Z2xhbi1jbGllbnQ6ZGV2Z2xhbi1zZWNyZXQ=", "Content-Type:application/x-www-form-urlencoded", "NZUSER:absiddik:123:password"})
    public abstract retrofit2.Call<com.netizen.medicom.model.MSG> userLogIn(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Field(value = "name")
    java.lang.String name, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Field(value = "password")
    java.lang.String password, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Field(value = "grant_type")
    java.lang.String grant_type);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/home/dashboard/info")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<com.netizen.medicom.model.HomePageInfo> getHomePageInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/core/bank/accounts/by")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<java.util.List<com.netizen.medicom.model.BalanceDepositAccountInfo>> getBalanceDepositAccountInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "defaultCode")
    java.lang.String defaultCode);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/tagged/bank-account/by")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<com.netizen.medicom.model.WithDrawInfo> getBalanceWithdrawInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "coreCategoryDefaultCode")
    java.lang.String codeCategoryDefaultCode);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/balance/withdraw")
    public abstract retrofit2.Call<java.lang.String> submitBalanceWithdrawData(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.medicom.model.WithdrawPostData obj);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/report/revenue/log")
    public abstract retrofit2.Call<com.netizen.medicom.model.RevenueLogGetData> getRevenueLogData(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Body()
    com.netizen.medicom.model.RevenueLogPostData revenueLogPostData);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/category/by/typeDefaultCode")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<java.util.List<com.netizen.medicom.model.BankAccountInfo>> getBankDistrictList(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Query(value = "typeDefaultCode")
    java.lang.String typeDefaultCode);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/bank/branches/by")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<java.util.List<com.netizen.medicom.model.BankBranchInfo>> getBankBranchList(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Query(value = "bankID")
    java.lang.String bankId, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Query(value = "districtID")
    java.lang.String districtId);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/bank/add-account")
    public abstract retrofit2.Call<java.lang.String> submitBankAccount(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.medicom.model.BankAccountPostData bankAccountPostData);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/bank/accounts/by/user")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<java.util.List<com.netizen.medicom.model.BankList>> getBankList(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/category/by/typeDefaultCode")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<java.util.List<com.netizen.medicom.model.TagType>> getTagTypeList(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "typeDefaultCode")
    java.lang.String typeDefaultCode);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/bank/account/taggings")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<java.util.List<com.netizen.medicom.model.TagGetData>> getTagList(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/bank/account/tag")
    public abstract retrofit2.Call<java.lang.String> tagBankAccount(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.medicom.model.TagPostData tagPostData);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/tokens/modules/by")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<java.util.List<com.netizen.medicom.model.Problem>> getProblemModuleList(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "catDefaultCode")
    java.lang.String catDefaultCode);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/tokens/types/by")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<java.util.List<com.netizen.medicom.model.Problem>> getProblemTypeList(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "parentCategoryID")
    java.lang.String parentCategoryID);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/tokens/by/pending")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<java.util.List<com.netizen.medicom.model.TokenList>> getSolvedAndPendingToken(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/tokens/new")
    public abstract retrofit2.Call<java.lang.Void> submitNewToken(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.medicom.model.TokenSubmitPostData tokenSubmitPostData);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/tokens/by/top10")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<java.util.List<com.netizen.medicom.model.TokenList>> getTokenList(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "guest/file/find/")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<com.google.gson.JsonObject> downloadTokenAttachment(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Query(value = "filePath")
    java.lang.String imageUrl);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "guest/doctor/registration")
    public abstract retrofit2.Call<java.lang.Void> doctorRegistrationSubmit(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.util.HashMap<java.lang.String, java.lang.String> token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.medicom.model.DoctorEnroll obj);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "guest/patient/registration")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<com.netizen.medicom.model.PatientEnrollResponse> patientRegistrationSubmit(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "partnerReferenceId")
    java.lang.String partnerReferenceID);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/patient/info/save")
    public abstract retrofit2.Call<com.netizen.medicom.model.PatientEnrollResponse> addPatientSubmit(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.medicom.model.AddPatientFamilyMember obj);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/patient/info/list")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<com.netizen.medicom.model.PatientFamilyMemberList> getPatientFamilyMemberList(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/doctor/core/category/types/by-typeDefaultCode")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<java.util.List<com.netizen.medicom.model.DoctorDesignation>> getDoctorDesignation(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "typeDefaultCode")
    java.lang.String typeDefaultCode, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "typeStatus")
    java.lang.String typeStatus);
}