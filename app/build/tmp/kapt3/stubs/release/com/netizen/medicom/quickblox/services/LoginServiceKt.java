package com.netizen.medicom.quickblox.services;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0005X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0005X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0007\u001a\u00020\u0005X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"COMMAND_LOGIN", "", "COMMAND_LOGOUT", "COMMAND_NOT_FOUND", "EXTRA_COMMAND_TO_SERVICE", "", "EXTRA_PENDING_INTENT", "EXTRA_QB_USER", "app_release"})
public final class LoginServiceKt {
    private static final java.lang.String EXTRA_COMMAND_TO_SERVICE = "command_for_service";
    private static final java.lang.String EXTRA_QB_USER = "qb_user";
    private static final int COMMAND_NOT_FOUND = 0;
    private static final int COMMAND_LOGIN = 1;
    private static final int COMMAND_LOGOUT = 2;
    private static final java.lang.String EXTRA_PENDING_INTENT = "pending_Intent";
}