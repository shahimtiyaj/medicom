package com.netizen.medicom.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0011\b\u0016\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004R \u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\u0004\u00a8\u0006\b"}, d2 = {"Lcom/netizen/medicom/model/ProductPurchaseLogDTO;", "", "productRoleAssignDTO", "Lcom/netizen/medicom/model/ProductRoleAssignDTO;", "(Lcom/netizen/medicom/model/ProductRoleAssignDTO;)V", "getProductRoleAssignDTO", "()Lcom/netizen/medicom/model/ProductRoleAssignDTO;", "setProductRoleAssignDTO", "app_release"})
public final class ProductPurchaseLogDTO {
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "productRoleAssignDTO")
    private com.netizen.medicom.model.ProductRoleAssignDTO productRoleAssignDTO;
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.medicom.model.ProductRoleAssignDTO getProductRoleAssignDTO() {
        return null;
    }
    
    public final void setProductRoleAssignDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.medicom.model.ProductRoleAssignDTO p0) {
    }
    
    public ProductPurchaseLogDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.medicom.model.ProductRoleAssignDTO productRoleAssignDTO) {
        super();
    }
}