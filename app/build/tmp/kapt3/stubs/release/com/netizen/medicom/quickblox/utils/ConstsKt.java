package com.netizen.medicom.quickblox.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u001a\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0011\n\u0002\b\u0004\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0005X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0005X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0007\u001a\u00020\u0005X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\b\u001a\u00020\u0005X\u0086T\u00a2\u0006\u0002\n\u0000\"\u0019\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00010\n\u00a2\u0006\n\n\u0002\u0010\r\u001a\u0004\b\u000b\u0010\f\u00a8\u0006\u000e"}, d2 = {"EXTRA_IS_INCOMING_CALL", "", "EXTRA_LOGIN_ERROR_MESSAGE", "EXTRA_LOGIN_RESULT", "EXTRA_LOGIN_RESULT_CODE", "", "MAX_FULLNAME_LENGTH", "MAX_LOGIN_LENGTH", "MAX_OPPONENTS_COUNT", "PERMISSIONS", "", "getPERMISSIONS", "()[Ljava/lang/String;", "[Ljava/lang/String;", "app_release"})
public final class ConstsKt {
    @org.jetbrains.annotations.NotNull()
    private static final java.lang.String[] PERMISSIONS = {"android.permission.CAMERA", "android.permission.RECORD_AUDIO"};
    public static final int MAX_OPPONENTS_COUNT = 6;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String EXTRA_LOGIN_RESULT = "login_result";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String EXTRA_LOGIN_ERROR_MESSAGE = "login_error_message";
    public static final int EXTRA_LOGIN_RESULT_CODE = 1002;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String EXTRA_IS_INCOMING_CALL = "conversation_reason";
    public static final int MAX_LOGIN_LENGTH = 15;
    public static final int MAX_FULLNAME_LENGTH = 20;
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String[] getPERMISSIONS() {
        return null;
    }
}