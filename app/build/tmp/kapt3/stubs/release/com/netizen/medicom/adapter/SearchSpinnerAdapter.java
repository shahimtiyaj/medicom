package com.netizen.medicom.adapter;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0015B%\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u0012\b\u0010\b\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\nJ\b\u0010\f\u001a\u00020\tH\u0016J\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\tH\u0016J\u0018\u0010\u0011\u001a\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\tH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\b\u001a\u0004\u0018\u00010\tX\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u000b\u00a8\u0006\u0016"}, d2 = {"Lcom/netizen/medicom/adapter/SearchSpinnerAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/netizen/medicom/adapter/SearchSpinnerAdapter$ViewHolder;", "context", "Landroid/content/Context;", "sectionList", "", "", "spinnerType", "", "(Landroid/content/Context;Ljava/util/List;Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "getItemCount", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "ViewHolder", "app_release"})
public final class SearchSpinnerAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.netizen.medicom.adapter.SearchSpinnerAdapter.ViewHolder> {
    private final android.content.Context context = null;
    private final java.util.List<java.lang.String> sectionList = null;
    private final java.lang.Integer spinnerType = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.netizen.medicom.adapter.SearchSpinnerAdapter.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.netizen.medicom.adapter.SearchSpinnerAdapter.ViewHolder holder, int position) {
    }
    
    public SearchSpinnerAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> sectionList, @org.jetbrains.annotations.Nullable()
    java.lang.Integer spinnerType) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/netizen/medicom/adapter/SearchSpinnerAdapter$ViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemBinding", "Lcom/netizen/medicom/databinding/SingleSpinnerLayoutBinding;", "(Lcom/netizen/medicom/databinding/SingleSpinnerLayoutBinding;)V", "getItemBinding", "()Lcom/netizen/medicom/databinding/SingleSpinnerLayoutBinding;", "app_release"})
    public static final class ViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private final com.netizen.medicom.databinding.SingleSpinnerLayoutBinding itemBinding = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.netizen.medicom.databinding.SingleSpinnerLayoutBinding getItemBinding() {
            return null;
        }
        
        public ViewHolder(@org.jetbrains.annotations.NotNull()
        com.netizen.medicom.databinding.SingleSpinnerLayoutBinding itemBinding) {
            super(null);
        }
    }
}