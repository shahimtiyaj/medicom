package com.netizen.medicom.quickblox.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u001c\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\u001a0\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\u0001H\u0002\u001a\u0016\u0010\n\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005\u001a\u0016\u0010\u000b\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005\u00a8\u0006\f"}, d2 = {"isEnteredTextValid", "", "context", "Landroid/content/Context;", "editText", "Landroid/widget/EditText;", "resFieldName", "", "maxLength", "checkName", "isFoolNameValid", "isLoginValid", "app_release"})
public final class ValidationUtilsKt {
    
    private static final boolean isEnteredTextValid(android.content.Context context, android.widget.EditText editText, int resFieldName, int maxLength, boolean checkName) {
        return false;
    }
    
    public static final boolean isLoginValid(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    android.widget.EditText editText) {
        return false;
    }
    
    public static final boolean isFoolNameValid(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    android.widget.EditText editText) {
        return false;
    }
}