package com.netizen.medicom.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010 \n\u0000\n\u0002\u0010\u0006\n\u0002\b\u000b\n\u0002\u0010\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0007\b\u0016\u00a2\u0006\u0002\u0010\u0002B\u001b\b\u0016\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0007B\u0011\b\u0016\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\bJ\u000e\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u0000\u0018\u00010\nJ\r\u0010\u0011\u001a\u0004\u0018\u00010\f\u00a2\u0006\u0002\u0010\u0012J\r\u0010\u0013\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0014J\b\u0010\u0015\u001a\u0004\u0018\u00010\u0006J\r\u0010\u0016\u001a\u0004\u0018\u00010\f\u00a2\u0006\u0002\u0010\u0012J\u0014\u0010\u0017\u001a\u00020\u00182\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00000\nJ\u0015\u0010\u0019\u001a\u00020\u00182\b\u0010\u000b\u001a\u0004\u0018\u00010\f\u00a2\u0006\u0002\u0010\u001aJ\u0015\u0010\u001b\u001a\u00020\u00182\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u001cJ\u0010\u0010\u001d\u001a\u00020\u00182\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006J\u0015\u0010\u001e\u001a\u00020\u00182\b\u0010\u000f\u001a\u0004\u0018\u00010\f\u00a2\u0006\u0002\u0010\u001aR\u0016\u0010\t\u001a\n\u0012\u0004\u0012\u00020\u0000\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000b\u001a\u0004\u0018\u00010\f8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\rR\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u000eR\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000f\u001a\u0004\u0018\u00010\f8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\r\u00a8\u0006\u001f"}, d2 = {"Lcom/netizen/medicom/model/MessageType;", "", "()V", "productID", "", "productName", "", "(Ljava/lang/Long;Ljava/lang/String;)V", "(Ljava/lang/String;)V", "messageTypeList", "", "percentVat", "", "Ljava/lang/Double;", "Ljava/lang/Long;", "salesPrice", "getMessageTypelist", "getPercentVat", "()Ljava/lang/Double;", "getProductID", "()Ljava/lang/Long;", "getProductName", "getSalesPrice", "setMessageTypest", "", "setPercentVat", "(Ljava/lang/Double;)V", "setProductID", "(Ljava/lang/Long;)V", "setProductName", "setSalesPrice", "app_release"})
public final class MessageType {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "productID")
    private java.lang.Long productID;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "productName")
    private java.lang.String productName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "salesPrice")
    private java.lang.Double salesPrice;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "percentVat")
    private java.lang.Double percentVat;
    private java.util.List<com.netizen.medicom.model.MessageType> messageTypeList;
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.netizen.medicom.model.MessageType> getMessageTypelist() {
        return null;
    }
    
    public final void setMessageTypest(@org.jetbrains.annotations.NotNull()
    java.util.List<com.netizen.medicom.model.MessageType> messageTypeList) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getProductID() {
        return null;
    }
    
    public final void setProductID(@org.jetbrains.annotations.Nullable()
    java.lang.Long productID) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getProductName() {
        return null;
    }
    
    public final void setProductName(@org.jetbrains.annotations.Nullable()
    java.lang.String productName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Double getSalesPrice() {
        return null;
    }
    
    public final void setSalesPrice(@org.jetbrains.annotations.Nullable()
    java.lang.Double salesPrice) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Double getPercentVat() {
        return null;
    }
    
    public final void setPercentVat(@org.jetbrains.annotations.Nullable()
    java.lang.Double percentVat) {
    }
    
    public MessageType() {
        super();
    }
    
    public MessageType(@org.jetbrains.annotations.Nullable()
    java.lang.Long productID, @org.jetbrains.annotations.Nullable()
    java.lang.String productName) {
        super();
    }
    
    public MessageType(@org.jetbrains.annotations.Nullable()
    java.lang.String productName) {
        super();
    }
}