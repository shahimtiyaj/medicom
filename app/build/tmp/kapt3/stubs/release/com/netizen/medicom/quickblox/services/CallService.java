package com.netizen.medicom.quickblox.services;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u00ee\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010%\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010$\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b \n\u0002\u0018\u0002\n\u0002\b\f\u0018\u0000 \u0087\u00012\u00020\u0001:\u0016\u0083\u0001\u0084\u0001\u0085\u0001\u0086\u0001\u0087\u0001\u0088\u0001\u0089\u0001\u008a\u0001\u008b\u0001\u008c\u0001\u008d\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u001a\u0010.\u001a\u00020/2\u0012\u00100\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u000401J\u0010\u00102\u001a\u00020/2\b\u0010\u0012\u001a\u0004\u0018\u000103J\u0010\u00104\u001a\u00020/2\b\u00105\u001a\u0004\u0018\u000106J\u0014\u00107\u001a\u00020/2\f\u00105\u001a\b\u0012\u0002\b\u0003\u0018\u000108J\u0010\u00109\u001a\u00020/2\b\u00105\u001a\u0004\u0018\u00010:J\u0018\u0010;\u001a\u00020/2\u0006\u0010<\u001a\u00020,2\u0006\u0010=\u001a\u00020-H\u0002J\u0016\u0010>\u001a\u00020/2\u000e\u00105\u001a\n\u0012\u0004\u0012\u00020\u0015\u0018\u00010?J\u0006\u0010@\u001a\u00020/J\u0006\u0010A\u001a\u00020/J\u0018\u0010B\u001a\u00020\u00042\u0006\u0010C\u001a\u00020\u00042\u0006\u0010D\u001a\u00020\u0004H\u0003J\u0006\u0010E\u001a\u00020\u0018J\b\u0010F\u001a\u00020\u0004H\u0002J\r\u0010G\u001a\u0004\u0018\u00010,\u00a2\u0006\u0002\u0010HJ\b\u0010I\u001a\u0004\u0018\u00010JJ\u000e\u0010K\u001a\n\u0012\u0004\u0012\u00020,\u0018\u00010LJ\u0010\u0010M\u001a\u0004\u0018\u00010N2\u0006\u0010<\u001a\u00020,J\u0010\u0010O\u001a\u0004\u0018\u00010-2\u0006\u0010<\u001a\u00020,J\u0012\u0010P\u001a\u000e\u0012\u0004\u0012\u00020,\u0012\u0004\u0012\u00020-0+J\u001a\u0010Q\u001a\u00020\u00182\u0012\u00100\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u000401J\u0006\u0010R\u001a\u00020/J\b\u0010S\u001a\u00020/H\u0002J\b\u0010T\u001a\u00020/H\u0002J\b\u0010U\u001a\u00020VH\u0002J\b\u0010W\u001a\u00020/H\u0002J\u0006\u0010X\u001a\u00020\u0018J\u0010\u0010Y\u001a\u00020\u00182\b\u0010Z\u001a\u0004\u0018\u00010\u0015J\u0006\u0010[\u001a\u00020\u0018J\u0006\u0010\\\u001a\u00020\u0018J\u0006\u0010]\u001a\u00020\u0018J\u0014\u0010^\u001a\u0004\u0018\u00010_2\b\u0010`\u001a\u0004\u0018\u00010aH\u0016J\b\u0010b\u001a\u00020/H\u0016J\b\u0010c\u001a\u00020/H\u0016J\"\u0010d\u001a\u00020,2\b\u0010`\u001a\u0004\u0018\u00010a2\u0006\u0010e\u001a\u00020,2\u0006\u0010f\u001a\u00020,H\u0016J\u0006\u0010g\u001a\u00020/J\u001a\u0010h\u001a\u00020/2\u0012\u00100\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u000401J\u0006\u0010i\u001a\u00020/J\b\u0010j\u001a\u00020/H\u0002J\u0006\u0010k\u001a\u00020/J\u0010\u0010l\u001a\u00020/2\b\u0010\u0012\u001a\u0004\u0018\u000103J\u0010\u0010m\u001a\u00020/2\b\u00105\u001a\u0004\u0018\u000106J\u0014\u0010n\u001a\u00020/2\f\u00105\u001a\b\u0012\u0002\b\u0003\u0018\u000108J\u0010\u0010o\u001a\u00020/2\b\u00105\u001a\u0004\u0018\u00010:J\u0010\u0010p\u001a\u00020/2\u0006\u0010<\u001a\u00020,H\u0002J\u0016\u0010q\u001a\u00020/2\u000e\u00105\u001a\n\u0012\u0004\u0012\u00020\u0015\u0018\u00010?J\b\u0010r\u001a\u00020/H\u0002J\u000e\u0010s\u001a\u00020/2\u0006\u0010t\u001a\u00020\u0018J\u000e\u0010u\u001a\u00020/2\u0006\u00105\u001a\u00020\u000fJ\u000e\u0010v\u001a\u00020/2\u0006\u0010w\u001a\u00020\u0018J\u001a\u0010x\u001a\u00020/2\u0012\u00100\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u000401J\b\u0010y\u001a\u00020/H\u0002J\u000e\u0010z\u001a\u00020/2\u0006\u0010{\u001a\u00020aJ\b\u0010|\u001a\u00020/H\u0002J\u0006\u0010}\u001a\u00020/J\u0006\u0010~\u001a\u00020/J\u0006\u0010\u007f\u001a\u00020/J\u0011\u0010\u0080\u0001\u001a\u00020/2\b\u0010\u0081\u0001\u001a\u00030\u0082\u0001R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0007\u001a\u00060\bR\u00020\u0000X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\u000bR\u000e\u0010\f\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0010\u001a\u00060\u0011R\u00020\u0000X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0012\u001a\u00060\u0013R\u00020\u0000X\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082.\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u001b\u001a\u00060\u001cR\u00020\u0000X\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001d\u001a\u0004\u0018\u00010\u001eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020 X\u0082.\u00a2\u0006\u0002\n\u0000R\u0012\u0010!\u001a\u00060\"R\u00020\u0000X\u0082.\u00a2\u0006\u0002\n\u0000R\u0012\u0010#\u001a\u00060$R\u00020\u0000X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020\u0018X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0012\u0010&\u001a\u00060\'R\u00020\u0000X\u0082.\u00a2\u0006\u0002\n\u0000R\u0012\u0010(\u001a\u00060)R\u00020\u0000X\u0082.\u00a2\u0006\u0002\n\u0000R\u001a\u0010*\u001a\u000e\u0012\u0004\u0012\u00020,\u0012\u0004\u0012\u00020-0+X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u008e\u0001"}, d2 = {"Lcom/netizen/medicom/quickblox/services/CallService;", "Landroid/app/Service;", "()V", "TAG", "", "appRTCAudioManager", "Lcom/quickblox/videochat/webrtc/AppRTCAudioManager;", "callServiceBinder", "Lcom/netizen/medicom/quickblox/services/CallService$CallServiceBinder;", "callTime", "", "Ljava/lang/Long;", "callTimer", "Ljava/util/Timer;", "callTimerListener", "Lcom/netizen/medicom/quickblox/services/CallService$CallTimerListener;", "callTimerTask", "Lcom/netizen/medicom/quickblox/services/CallService$CallTimerTask;", "connectionListener", "Lcom/netizen/medicom/quickblox/services/CallService$ConnectionListenerImpl;", "currentSession", "Lcom/quickblox/videochat/webrtc/QBRTCSession;", "expirationReconnectionTime", "isCallState", "", "networkConnectionChecker", "Lcom/netizen/medicom/quickblox/util/NetworkConnectionChecker;", "networkConnectionListener", "Lcom/netizen/medicom/quickblox/services/CallService$NetworkConnectionListener;", "ringtonePlayer", "Lcom/netizen/medicom/quickblox/utils/RingtonePlayer;", "rtcClient", "Lcom/quickblox/videochat/webrtc/QBRTCClient;", "sessionEventsListener", "Lcom/netizen/medicom/quickblox/services/CallService$SessionEventsListener;", "sessionStateListener", "Lcom/netizen/medicom/quickblox/services/CallService$SessionStateListener;", "sharingScreenState", "signalingListener", "Lcom/netizen/medicom/quickblox/services/CallService$QBRTCSignalingListener;", "videoTrackListener", "Lcom/netizen/medicom/quickblox/services/CallService$VideoTrackListener;", "videoTrackMap", "", "", "Lcom/quickblox/videochat/webrtc/view/QBRTCVideoTrack;", "acceptCall", "", "userInfo", "", "addConnectionListener", "Lorg/jivesoftware/smack/ConnectionListener;", "addSessionEventsListener", "callback", "Lcom/quickblox/videochat/webrtc/callbacks/QBRTCSessionEventsCallback;", "addSessionStateListener", "Lcom/quickblox/videochat/webrtc/callbacks/QBRTCSessionStateCallback;", "addSignalingListener", "Lcom/quickblox/videochat/webrtc/callbacks/QBRTCSignalingCallback;", "addVideoTrack", "userId", "videoTrack", "addVideoTrackListener", "Lcom/quickblox/videochat/webrtc/callbacks/QBRTCClientVideoTracksCallbacks;", "clearButtonsState", "clearCallState", "createNotificationChannel", "channelId", "channelName", "currentSessionExist", "getCallTime", "getCallerId", "()Ljava/lang/Integer;", "getCurrentSessionState", "Lcom/quickblox/videochat/webrtc/BaseSession$QBRTCSessionState;", "getOpponents", "", "getPeerChannel", "Lcom/quickblox/videochat/webrtc/QBRTCTypes$QBRTCConnectionState;", "getVideoTrack", "getVideoTrackMap", "hangUpCurrentSession", "initAudioManager", "initListeners", "initNetworkChecker", "initNotification", "Landroid/app/Notification;", "initRTCClient", "isCallMode", "isCurrentSession", "session", "isMediaStreamManagerExist", "isSharingScreenState", "isVideoCall", "onBind", "Landroid/os/IBinder;", "intent", "Landroid/content/Intent;", "onCreate", "onDestroy", "onStartCommand", "flags", "startId", "playRingtone", "rejectCurrentSession", "releaseAudioManager", "releaseCurrentSession", "removeCallTimerCallback", "removeConnectionListener", "removeSessionEventsListener", "removeSessionStateListener", "removeSignalingListener", "removeVideoTrack", "removeVideoTrackListener", "removeVideoTrackRenders", "setAudioEnabled", "enabled", "setCallTimerCallback", "setVideoEnabled", "videoEnabled", "startCall", "startCallTimer", "startScreenSharing", "data", "stopCallTimer", "stopRingtone", "stopScreenSharing", "switchAudio", "switchCamera", "cameraSwitchHandler", "Lorg/webrtc/CameraVideoCapturer$CameraSwitchHandler;", "CallServiceBinder", "CallTimerListener", "CallTimerTask", "CameraEventsListener", "Companion", "ConnectionListenerImpl", "NetworkConnectionListener", "QBRTCSignalingListener", "SessionEventsListener", "SessionStateListener", "VideoTrackListener", "app_release"})
public final class CallService extends android.app.Service {
    private java.lang.String TAG;
    private final com.netizen.medicom.quickblox.services.CallService.CallServiceBinder callServiceBinder = null;
    private java.util.Map<java.lang.Integer, com.quickblox.videochat.webrtc.view.QBRTCVideoTrack> videoTrackMap;
    private com.netizen.medicom.quickblox.services.CallService.NetworkConnectionListener networkConnectionListener;
    private com.netizen.medicom.quickblox.util.NetworkConnectionChecker networkConnectionChecker;
    private com.netizen.medicom.quickblox.services.CallService.SessionEventsListener sessionEventsListener;
    private com.netizen.medicom.quickblox.services.CallService.ConnectionListenerImpl connectionListener;
    private com.netizen.medicom.quickblox.services.CallService.SessionStateListener sessionStateListener;
    private com.netizen.medicom.quickblox.services.CallService.QBRTCSignalingListener signalingListener;
    private com.netizen.medicom.quickblox.services.CallService.VideoTrackListener videoTrackListener;
    private com.quickblox.videochat.webrtc.AppRTCAudioManager appRTCAudioManager;
    private com.netizen.medicom.quickblox.services.CallService.CallTimerListener callTimerListener;
    private com.netizen.medicom.quickblox.utils.RingtonePlayer ringtonePlayer;
    private com.quickblox.videochat.webrtc.QBRTCSession currentSession;
    private long expirationReconnectionTime = 0L;
    private boolean sharingScreenState = false;
    private boolean isCallState = false;
    private com.quickblox.videochat.webrtc.QBRTCClient rtcClient;
    private final com.netizen.medicom.quickblox.services.CallService.CallTimerTask callTimerTask = null;
    private java.lang.Long callTime;
    private final java.util.Timer callTimer = null;
    public static final com.netizen.medicom.quickblox.services.CallService.Companion Companion = null;
    
    @java.lang.Override()
    public void onCreate() {
    }
    
    @java.lang.Override()
    public int onStartCommand(@org.jetbrains.annotations.Nullable()
    android.content.Intent intent, int flags, int startId) {
        return 0;
    }
    
    @java.lang.Override()
    public void onDestroy() {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.os.IBinder onBind(@org.jetbrains.annotations.Nullable()
    android.content.Intent intent) {
        return null;
    }
    
    private final android.app.Notification initNotification() {
        return null;
    }
    
    @androidx.annotation.RequiresApi(value = android.os.Build.VERSION_CODES.O)
    private final java.lang.String createNotificationChannel(java.lang.String channelId, java.lang.String channelName) {
        return null;
    }
    
    private final java.lang.String getCallTime() {
        return null;
    }
    
    public final void playRingtone() {
    }
    
    public final void stopRingtone() {
    }
    
    private final void initNetworkChecker() {
    }
    
    private final void initRTCClient() {
    }
    
    private final void initListeners() {
    }
    
    public final void initAudioManager() {
    }
    
    public final void releaseAudioManager() {
    }
    
    public final boolean currentSessionExist() {
        return false;
    }
    
    private final void releaseCurrentSession() {
    }
    
    public final void addConnectionListener(@org.jetbrains.annotations.Nullable()
    org.jivesoftware.smack.ConnectionListener connectionListener) {
    }
    
    public final void removeConnectionListener(@org.jetbrains.annotations.Nullable()
    org.jivesoftware.smack.ConnectionListener connectionListener) {
    }
    
    public final void addSessionStateListener(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.callbacks.QBRTCSessionStateCallback<?> callback) {
    }
    
    public final void removeSessionStateListener(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.callbacks.QBRTCSessionStateCallback<?> callback) {
    }
    
    public final void addVideoTrackListener(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.callbacks.QBRTCClientVideoTracksCallbacks<com.quickblox.videochat.webrtc.QBRTCSession> callback) {
    }
    
    public final void removeVideoTrackListener(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.callbacks.QBRTCClientVideoTracksCallbacks<com.quickblox.videochat.webrtc.QBRTCSession> callback) {
    }
    
    public final void addSignalingListener(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.callbacks.QBRTCSignalingCallback callback) {
    }
    
    public final void removeSignalingListener(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.callbacks.QBRTCSignalingCallback callback) {
    }
    
    public final void addSessionEventsListener(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.callbacks.QBRTCSessionEventsCallback callback) {
    }
    
    public final void removeSessionEventsListener(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.callbacks.QBRTCSessionEventsCallback callback) {
    }
    
    public final void acceptCall(@org.jetbrains.annotations.NotNull()
    java.util.Map<java.lang.String, java.lang.String> userInfo) {
    }
    
    public final void startCall(@org.jetbrains.annotations.NotNull()
    java.util.Map<java.lang.String, java.lang.String> userInfo) {
    }
    
    public final void rejectCurrentSession(@org.jetbrains.annotations.NotNull()
    java.util.Map<java.lang.String, java.lang.String> userInfo) {
    }
    
    public final boolean hangUpCurrentSession(@org.jetbrains.annotations.NotNull()
    java.util.Map<java.lang.String, java.lang.String> userInfo) {
        return false;
    }
    
    public final void setAudioEnabled(boolean enabled) {
    }
    
    public final void startScreenSharing(@org.jetbrains.annotations.NotNull()
    android.content.Intent data) {
    }
    
    public final void stopScreenSharing() {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getCallerId() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.Integer> getOpponents() {
        return null;
    }
    
    public final boolean isVideoCall() {
        return false;
    }
    
    public final void setVideoEnabled(boolean videoEnabled) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.quickblox.videochat.webrtc.BaseSession.QBRTCSessionState getCurrentSessionState() {
        return null;
    }
    
    public final boolean isMediaStreamManagerExist() {
        return false;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.quickblox.videochat.webrtc.QBRTCTypes.QBRTCConnectionState getPeerChannel(int userId) {
        return null;
    }
    
    public final boolean isCurrentSession(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.QBRTCSession session) {
        return false;
    }
    
    public final void switchCamera(@org.jetbrains.annotations.NotNull()
    org.webrtc.CameraVideoCapturer.CameraSwitchHandler cameraSwitchHandler) {
    }
    
    public final void switchAudio() {
    }
    
    public final boolean isSharingScreenState() {
        return false;
    }
    
    public final boolean isCallMode() {
        return false;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.Map<java.lang.Integer, com.quickblox.videochat.webrtc.view.QBRTCVideoTrack> getVideoTrackMap() {
        return null;
    }
    
    private final void addVideoTrack(int userId, com.quickblox.videochat.webrtc.view.QBRTCVideoTrack videoTrack) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.quickblox.videochat.webrtc.view.QBRTCVideoTrack getVideoTrack(int userId) {
        return null;
    }
    
    private final void removeVideoTrack(int userId) {
    }
    
    private final void removeVideoTrackRenders() {
    }
    
    public final void setCallTimerCallback(@org.jetbrains.annotations.NotNull()
    com.netizen.medicom.quickblox.services.CallService.CallTimerListener callback) {
    }
    
    public final void removeCallTimerCallback() {
    }
    
    private final void startCallTimer() {
    }
    
    private final void stopCallTimer() {
    }
    
    public final void clearButtonsState() {
    }
    
    public final void clearCallState() {
    }
    
    public CallService() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\b\u0082\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\b\u001a\u00020\tH\u0016R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0003\u0010\u0005\"\u0004\b\u0006\u0010\u0007\u00a8\u0006\n"}, d2 = {"Lcom/netizen/medicom/quickblox/services/CallService$CallTimerTask;", "Ljava/util/TimerTask;", "(Lcom/netizen/medicom/quickblox/services/CallService;)V", "isRunning", "", "()Z", "setRunning", "(Z)V", "run", "", "app_release"})
    final class CallTimerTask extends java.util.TimerTask {
        private boolean isRunning = false;
        
        public final boolean isRunning() {
            return false;
        }
        
        public final void setRunning(boolean p0) {
        }
        
        @java.lang.Override()
        public void run() {
        }
        
        public CallTimerTask() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0005"}, d2 = {"Lcom/netizen/medicom/quickblox/services/CallService$CallServiceBinder;", "Landroid/os/Binder;", "(Lcom/netizen/medicom/quickblox/services/CallService;)V", "getService", "Lcom/netizen/medicom/quickblox/services/CallService;", "app_release"})
    public final class CallServiceBinder extends android.os.Binder {
        
        @org.jetbrains.annotations.NotNull()
        public final com.netizen.medicom.quickblox.services.CallService getService() {
            return null;
        }
        
        public CallServiceBinder() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0082\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u000e\u0010\u0005\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0007H\u0016J\u0010\u0010\b\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\nH\u0016J\b\u0010\u000b\u001a\u00020\u0004H\u0016\u00a8\u0006\f"}, d2 = {"Lcom/netizen/medicom/quickblox/services/CallService$ConnectionListenerImpl;", "Lorg/jivesoftware/smack/AbstractConnectionListener;", "(Lcom/netizen/medicom/quickblox/services/CallService;)V", "connectionClosedOnError", "", "e", "Ljava/lang/Exception;", "Lkotlin/Exception;", "reconnectingIn", "seconds", "", "reconnectionSuccessful", "app_release"})
    final class ConnectionListenerImpl extends org.jivesoftware.smack.AbstractConnectionListener {
        
        @java.lang.Override()
        public void connectionClosedOnError(@org.jetbrains.annotations.Nullable()
        java.lang.Exception e) {
        }
        
        @java.lang.Override()
        public void reconnectionSuccessful() {
        }
        
        @java.lang.Override()
        public void reconnectingIn(int seconds) {
        }
        
        public ConnectionListenerImpl() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0002\b\r\b\u0082\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J7\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\u0014\u0010\t\u001a\u0010\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000b\u0018\u00010\nH\u0016\u00a2\u0006\u0002\u0010\fJ7\u0010\r\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\u0014\u0010\t\u001a\u0010\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000b\u0018\u00010\nH\u0016\u00a2\u0006\u0002\u0010\fJ7\u0010\u000e\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\u0010\u000f\u001a\u0004\u0018\u00010\b2\u0014\u0010\t\u001a\u0010\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000b\u0018\u00010\nH\u0016\u00a2\u0006\u0002\u0010\fJ\u0012\u0010\u0010\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0016J\u0012\u0010\u0011\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0016J\u0012\u0010\u0012\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0016J!\u0010\u0013\u001a\u00020\u00042\b\u0010\u0014\u001a\u0004\u0018\u00010\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0016\u00a2\u0006\u0002\u0010\u0015J!\u0010\u0016\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\u0010\u0017\u001a\u0004\u0018\u00010\bH\u0016\u00a2\u0006\u0002\u0010\u0015\u00a8\u0006\u0018"}, d2 = {"Lcom/netizen/medicom/quickblox/services/CallService$SessionEventsListener;", "Lcom/quickblox/videochat/webrtc/callbacks/QBRTCClientSessionCallbacks;", "(Lcom/netizen/medicom/quickblox/services/CallService;)V", "onCallAcceptByUser", "", "session", "Lcom/quickblox/videochat/webrtc/QBRTCSession;", "p1", "", "p2", "", "", "(Lcom/quickblox/videochat/webrtc/QBRTCSession;Ljava/lang/Integer;Ljava/util/Map;)V", "onCallRejectByUser", "onReceiveHangUpFromUser", "userID", "onReceiveNewSession", "onSessionClosed", "onSessionStartClose", "onUserNoActions", "p0", "(Lcom/quickblox/videochat/webrtc/QBRTCSession;Ljava/lang/Integer;)V", "onUserNotAnswer", "userId", "app_release"})
    final class SessionEventsListener implements com.quickblox.videochat.webrtc.callbacks.QBRTCClientSessionCallbacks {
        
        @java.lang.Override()
        public void onUserNotAnswer(@org.jetbrains.annotations.Nullable()
        com.quickblox.videochat.webrtc.QBRTCSession session, @org.jetbrains.annotations.Nullable()
        java.lang.Integer userId) {
        }
        
        @java.lang.Override()
        public void onSessionStartClose(@org.jetbrains.annotations.Nullable()
        com.quickblox.videochat.webrtc.QBRTCSession session) {
        }
        
        @java.lang.Override()
        public void onReceiveHangUpFromUser(@org.jetbrains.annotations.Nullable()
        com.quickblox.videochat.webrtc.QBRTCSession session, @org.jetbrains.annotations.Nullable()
        java.lang.Integer userID, @org.jetbrains.annotations.Nullable()
        java.util.Map<java.lang.String, java.lang.String> p2) {
        }
        
        @java.lang.Override()
        public void onCallAcceptByUser(@org.jetbrains.annotations.Nullable()
        com.quickblox.videochat.webrtc.QBRTCSession session, @org.jetbrains.annotations.Nullable()
        java.lang.Integer p1, @org.jetbrains.annotations.Nullable()
        java.util.Map<java.lang.String, java.lang.String> p2) {
        }
        
        @java.lang.Override()
        public void onReceiveNewSession(@org.jetbrains.annotations.Nullable()
        com.quickblox.videochat.webrtc.QBRTCSession session) {
        }
        
        @java.lang.Override()
        public void onUserNoActions(@org.jetbrains.annotations.Nullable()
        com.quickblox.videochat.webrtc.QBRTCSession p0, @org.jetbrains.annotations.Nullable()
        java.lang.Integer p1) {
        }
        
        @java.lang.Override()
        public void onSessionClosed(@org.jetbrains.annotations.Nullable()
        com.quickblox.videochat.webrtc.QBRTCSession session) {
        }
        
        @java.lang.Override()
        public void onCallRejectByUser(@org.jetbrains.annotations.Nullable()
        com.quickblox.videochat.webrtc.QBRTCSession session, @org.jetbrains.annotations.Nullable()
        java.lang.Integer p1, @org.jetbrains.annotations.Nullable()
        java.util.Map<java.lang.String, java.lang.String> p2) {
        }
        
        public SessionEventsListener() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\b\u0082\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J!\u0010\u0004\u001a\u00020\u00052\b\u0010\u0006\u001a\u0004\u0018\u00010\u00022\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0016\u00a2\u0006\u0002\u0010\tJ!\u0010\n\u001a\u00020\u00052\b\u0010\u0006\u001a\u0004\u0018\u00010\u00022\b\u0010\u000b\u001a\u0004\u0018\u00010\bH\u0016\u00a2\u0006\u0002\u0010\tJ!\u0010\f\u001a\u00020\u00052\b\u0010\u0006\u001a\u0004\u0018\u00010\u00022\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0016\u00a2\u0006\u0002\u0010\tJ\u001c\u0010\r\u001a\u00020\u00052\b\u0010\u0006\u001a\u0004\u0018\u00010\u00022\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0016\u00a8\u0006\u0010"}, d2 = {"Lcom/netizen/medicom/quickblox/services/CallService$SessionStateListener;", "Lcom/quickblox/videochat/webrtc/callbacks/QBRTCSessionStateCallback;", "Lcom/quickblox/videochat/webrtc/QBRTCSession;", "(Lcom/netizen/medicom/quickblox/services/CallService;)V", "onConnectedToUser", "", "session", "userId", "", "(Lcom/quickblox/videochat/webrtc/QBRTCSession;Ljava/lang/Integer;)V", "onConnectionClosedForUser", "userID", "onDisconnectedFromUser", "onStateChanged", "sessionState", "Lcom/quickblox/videochat/webrtc/BaseSession$QBRTCSessionState;", "app_release"})
    final class SessionStateListener implements com.quickblox.videochat.webrtc.callbacks.QBRTCSessionStateCallback<com.quickblox.videochat.webrtc.QBRTCSession> {
        
        @java.lang.Override()
        public void onDisconnectedFromUser(@org.jetbrains.annotations.Nullable()
        com.quickblox.videochat.webrtc.QBRTCSession session, @org.jetbrains.annotations.Nullable()
        java.lang.Integer userId) {
        }
        
        @java.lang.Override()
        public void onConnectedToUser(@org.jetbrains.annotations.Nullable()
        com.quickblox.videochat.webrtc.QBRTCSession session, @org.jetbrains.annotations.Nullable()
        java.lang.Integer userId) {
        }
        
        @java.lang.Override()
        public void onConnectionClosedForUser(@org.jetbrains.annotations.Nullable()
        com.quickblox.videochat.webrtc.QBRTCSession session, @org.jetbrains.annotations.Nullable()
        java.lang.Integer userID) {
        }
        
        @java.lang.Override()
        public void onStateChanged(@org.jetbrains.annotations.Nullable()
        com.quickblox.videochat.webrtc.QBRTCSession session, @org.jetbrains.annotations.Nullable()
        com.quickblox.videochat.webrtc.BaseSession.QBRTCSessionState sessionState) {
        }
        
        public SessionStateListener() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0082\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J+\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\u0010\t\u001a\u0004\u0018\u00010\nH\u0016\u00a2\u0006\u0002\u0010\u000bJ!\u0010\f\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0016\u00a2\u0006\u0002\u0010\r\u00a8\u0006\u000e"}, d2 = {"Lcom/netizen/medicom/quickblox/services/CallService$QBRTCSignalingListener;", "Lcom/quickblox/videochat/webrtc/callbacks/QBRTCSignalingCallback;", "(Lcom/netizen/medicom/quickblox/services/CallService;)V", "onErrorSendingPacket", "", "p0", "Lcom/quickblox/videochat/webrtc/QBSignalingSpec$QBSignalCMD;", "p1", "", "p2", "Lcom/quickblox/videochat/webrtc/exception/QBRTCSignalException;", "(Lcom/quickblox/videochat/webrtc/QBSignalingSpec$QBSignalCMD;Ljava/lang/Integer;Lcom/quickblox/videochat/webrtc/exception/QBRTCSignalException;)V", "onSuccessSendingPacket", "(Lcom/quickblox/videochat/webrtc/QBSignalingSpec$QBSignalCMD;Ljava/lang/Integer;)V", "app_release"})
    final class QBRTCSignalingListener implements com.quickblox.videochat.webrtc.callbacks.QBRTCSignalingCallback {
        
        @java.lang.Override()
        public void onSuccessSendingPacket(@org.jetbrains.annotations.Nullable()
        com.quickblox.videochat.webrtc.QBSignalingSpec.QBSignalCMD p0, @org.jetbrains.annotations.Nullable()
        java.lang.Integer p1) {
        }
        
        @java.lang.Override()
        public void onErrorSendingPacket(@org.jetbrains.annotations.Nullable()
        com.quickblox.videochat.webrtc.QBSignalingSpec.QBSignalCMD p0, @org.jetbrains.annotations.Nullable()
        java.lang.Integer p1, @org.jetbrains.annotations.Nullable()
        com.quickblox.videochat.webrtc.exception.QBRTCSignalException p2) {
        }
        
        public QBRTCSignalingListener() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\b\u0082\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016\u00a8\u0006\u0007"}, d2 = {"Lcom/netizen/medicom/quickblox/services/CallService$NetworkConnectionListener;", "Lcom/netizen/medicom/quickblox/util/NetworkConnectionChecker$OnConnectivityChangedListener;", "(Lcom/netizen/medicom/quickblox/services/CallService;)V", "connectivityChanged", "", "availableNow", "", "app_release"})
    final class NetworkConnectionListener implements com.netizen.medicom.quickblox.util.NetworkConnectionChecker.OnConnectivityChangedListener {
        
        @java.lang.Override()
        public void connectivityChanged(boolean availableNow) {
        }
        
        public NetworkConnectionListener() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0004\b\u0082\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016J\b\u0010\u0005\u001a\u00020\u0004H\u0016J\u0010\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\bH\u0016J\u0010\u0010\t\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\bH\u0016J\u0010\u0010\n\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\bH\u0016J\b\u0010\u000b\u001a\u00020\u0004H\u0016\u00a8\u0006\f"}, d2 = {"Lcom/netizen/medicom/quickblox/services/CallService$CameraEventsListener;", "Lorg/webrtc/CameraVideoCapturer$CameraEventsHandler;", "(Lcom/netizen/medicom/quickblox/services/CallService;)V", "onCameraClosed", "", "onCameraDisconnected", "onCameraError", "s", "", "onCameraFreezed", "onCameraOpening", "onFirstFrameAvailable", "app_release"})
    final class CameraEventsListener implements org.webrtc.CameraVideoCapturer.CameraEventsHandler {
        
        @java.lang.Override()
        public void onCameraError(@org.jetbrains.annotations.NotNull()
        java.lang.String s) {
        }
        
        @java.lang.Override()
        public void onCameraDisconnected() {
        }
        
        @java.lang.Override()
        public void onCameraFreezed(@org.jetbrains.annotations.NotNull()
        java.lang.String s) {
        }
        
        @java.lang.Override()
        public void onCameraOpening(@org.jetbrains.annotations.NotNull()
        java.lang.String s) {
        }
        
        @java.lang.Override()
        public void onFirstFrameAvailable() {
        }
        
        @java.lang.Override()
        public void onCameraClosed() {
        }
        
        public CameraEventsListener() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0082\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u001c\u0010\u0004\u001a\u00020\u00052\b\u0010\u0006\u001a\u0004\u0018\u00010\u00022\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0016J+\u0010\t\u001a\u00020\u00052\b\u0010\u0006\u001a\u0004\u0018\u00010\u00022\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\u0010\n\u001a\u0004\u0018\u00010\u000bH\u0016\u00a2\u0006\u0002\u0010\f\u00a8\u0006\r"}, d2 = {"Lcom/netizen/medicom/quickblox/services/CallService$VideoTrackListener;", "Lcom/quickblox/videochat/webrtc/callbacks/QBRTCClientVideoTracksCallbacks;", "Lcom/quickblox/videochat/webrtc/QBRTCSession;", "(Lcom/netizen/medicom/quickblox/services/CallService;)V", "onLocalVideoTrackReceive", "", "session", "videoTrack", "Lcom/quickblox/videochat/webrtc/view/QBRTCVideoTrack;", "onRemoteVideoTrackReceive", "userId", "", "(Lcom/quickblox/videochat/webrtc/QBRTCSession;Lcom/quickblox/videochat/webrtc/view/QBRTCVideoTrack;Ljava/lang/Integer;)V", "app_release"})
    final class VideoTrackListener implements com.quickblox.videochat.webrtc.callbacks.QBRTCClientVideoTracksCallbacks<com.quickblox.videochat.webrtc.QBRTCSession> {
        
        @java.lang.Override()
        public void onLocalVideoTrackReceive(@org.jetbrains.annotations.Nullable()
        com.quickblox.videochat.webrtc.QBRTCSession session, @org.jetbrains.annotations.Nullable()
        com.quickblox.videochat.webrtc.view.QBRTCVideoTrack videoTrack) {
        }
        
        @java.lang.Override()
        public void onRemoteVideoTrackReceive(@org.jetbrains.annotations.Nullable()
        com.quickblox.videochat.webrtc.QBRTCSession session, @org.jetbrains.annotations.Nullable()
        com.quickblox.videochat.webrtc.view.QBRTCVideoTrack videoTrack, @org.jetbrains.annotations.Nullable()
        java.lang.Integer userId) {
        }
        
        public VideoTrackListener() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lcom/netizen/medicom/quickblox/services/CallService$CallTimerListener;", "", "onCallTimeUpdate", "", "time", "", "app_release"})
    public static abstract interface CallTimerListener {
        
        public abstract void onCallTimeUpdate(@org.jetbrains.annotations.NotNull()
        java.lang.String time);
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\b"}, d2 = {"Lcom/netizen/medicom/quickblox/services/CallService$Companion;", "", "()V", "start", "", "context", "Landroid/content/Context;", "stop", "app_release"})
    public static final class Companion {
        
        public final void start(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
        }
        
        public final void stop(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
        }
        
        private Companion() {
            super();
        }
    }
}