package com.netizen.medicom.quickblox.fragments;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u0018\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0003X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0003X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0007X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\b\u001a\u00020\u0003X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"CAMERA_ENABLED", "", "FULL_SCREEN_CLICK_DELAY", "", "IS_CURRENT_CAMERA_FRONT", "LOCAL_TRACK_INITIALIZE_DELAY", "RECYCLE_VIEW_PADDING", "", "UPDATING_USERS_DELAY", "app_release"})
public final class VideoConversationFragmentKt {
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CAMERA_ENABLED = "is_camera_enabled";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String IS_CURRENT_CAMERA_FRONT = "is_camera_front";
    private static final long LOCAL_TRACK_INITIALIZE_DELAY = 800L;
    private static final int RECYCLE_VIEW_PADDING = 2;
    private static final long UPDATING_USERS_DELAY = 2000L;
    private static final long FULL_SCREEN_CLICK_DELAY = 1000L;
}