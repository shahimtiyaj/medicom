package com.netizen.medicom.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\r\n\u0002\u0010\u0007\n\u0002\b!\n\u0002\u0010\u0002\n\u0002\b\u001e\u0018\u00002\u00020\u0001:\u0001`B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010%\u001a\u0004\u0018\u00010\u0004J\b\u0010&\u001a\u0004\u0018\u00010\u0004J\r\u0010\'\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010(J\b\u0010)\u001a\u0004\u0018\u00010\u0004J\r\u0010*\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010(J\b\u0010+\u001a\u0004\u0018\u00010\u0004J\b\u0010,\u001a\u0004\u0018\u00010\u0004J\b\u0010-\u001a\u0004\u0018\u00010\u000eJ\b\u0010.\u001a\u0004\u0018\u00010\u0001J\b\u0010/\u001a\u0004\u0018\u00010\u0004J\b\u00100\u001a\u0004\u0018\u00010\u0004J\r\u00101\u001a\u0004\u0018\u00010\u0013\u00a2\u0006\u0002\u00102J\r\u00103\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010(J\b\u00104\u001a\u0004\u0018\u00010\u0004J\b\u00105\u001a\u0004\u0018\u00010\u0004J\r\u00106\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010(J\r\u00107\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010(J\b\u00108\u001a\u0004\u0018\u00010\u0004J\r\u00109\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010(J\r\u0010:\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010(J\b\u0010;\u001a\u0004\u0018\u00010\u0001J\b\u0010<\u001a\u0004\u0018\u00010\u0004J\r\u0010=\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010(J\r\u0010>\u001a\u0004\u0018\u00010!\u00a2\u0006\u0002\u0010?J\r\u0010@\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010(J\r\u0010A\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010(J\u0010\u0010B\u001a\u00020C2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u0010D\u001a\u00020C2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0015\u0010E\u001a\u00020C2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010FJ\u0010\u0010G\u001a\u00020C2\b\u0010\t\u001a\u0004\u0018\u00010\u0004J\u0015\u0010H\u001a\u00020C2\b\u0010\n\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010FJ\u0010\u0010I\u001a\u00020C2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0004J\u0010\u0010J\u001a\u00020C2\b\u0010\f\u001a\u0004\u0018\u00010\u0004J\u0010\u0010K\u001a\u00020C2\b\u0010\r\u001a\u0004\u0018\u00010\u000eJ\u0010\u0010L\u001a\u00020C2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0001J\u0010\u0010M\u001a\u00020C2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0004J\u0010\u0010N\u001a\u00020C2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0004J\u0015\u0010O\u001a\u00020C2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013\u00a2\u0006\u0002\u0010PJ\u0015\u0010Q\u001a\u00020C2\b\u0010\u0015\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010FJ\u0010\u0010R\u001a\u00020C2\b\u0010\u0016\u001a\u0004\u0018\u00010\u0004J\u0010\u0010S\u001a\u00020C2\b\u0010\u0017\u001a\u0004\u0018\u00010\u0004J\u0015\u0010T\u001a\u00020C2\b\u0010\u0018\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010FJ\u0015\u0010U\u001a\u00020C2\b\u0010\u0019\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010FJ\u0010\u0010V\u001a\u00020C2\b\u0010\u001a\u001a\u0004\u0018\u00010\u0004J\u0015\u0010W\u001a\u00020C2\b\u0010\u001b\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010FJ\u0015\u0010X\u001a\u00020C2\b\u0010\u001c\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010FJ\u0010\u0010Y\u001a\u00020C2\b\u0010\u001d\u001a\u0004\u0018\u00010\u0001J\u0010\u0010Z\u001a\u00020C2\b\u0010\u001e\u001a\u0004\u0018\u00010\u0004J\u0015\u0010[\u001a\u00020C2\b\u0010\u001f\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010FJ\u0015\u0010\\\u001a\u00020C2\b\u0010 \u001a\u0004\u0018\u00010!\u00a2\u0006\u0002\u0010]J\u0015\u0010^\u001a\u00020C2\b\u0010#\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010FJ\u0015\u0010_\u001a\u00020C2\b\u0010$\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010FR\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\bR\u0014\u0010\t\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\n\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\bR\u0014\u0010\u000b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0004\u0018\u00010\u000e8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0012\u001a\u0004\u0018\u00010\u00138\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0014R\u0016\u0010\u0015\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\bR\u0014\u0010\u0016\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0017\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0018\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\bR\u0016\u0010\u0019\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\bR\u0014\u0010\u001a\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u001b\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\bR\u0016\u0010\u001c\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\bR\u0014\u0010\u001d\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001e\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u001f\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\bR\u0016\u0010 \u001a\u0004\u0018\u00010!8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\"R\u0016\u0010#\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\bR\u0016\u0010$\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\b\u00a8\u0006a"}, d2 = {"Lcom/netizen/medicom/model/UserProfileUpdate;", "", "()V", "basicEmail", "", "basicMobile", "customNetiID", "", "Ljava/lang/Integer;", "dateOfBirth", "emailBalance", "fullName", "gender", "globalAreaInfoDTO", "Lcom/netizen/medicom/model/UserProfileUpdate$GlobalAreaInfoDTO;", "imageContent", "imageName", "imagePath", "imageSaveOrEditable", "", "Ljava/lang/Boolean;", "lastDateExecuted", "lastIpExecuted", "lastUserExecuted", "netiID", "registrationDate", "religion", "smsBalance", "userEnableStatus", "userName", "userPassword", "userReserveBalance", "userWalletBalance", "", "Ljava/lang/Float;", "validationStatus", "voiceBalance", "getBasicEmail", "getBasicMobile", "getCustomNetiID", "()Ljava/lang/Integer;", "getDateOfBirth", "getEmailBalance", "getFullName", "getGender", "getGlobalAreaInfoDTO", "getImageContent", "getImageName", "getImagePath", "getImageSaveOrEditable", "()Ljava/lang/Boolean;", "getLastDateExecuted", "getLastIpExecuted", "getLastUserExecuted", "getNetiID", "getRegistrationDate", "getReligion", "getSmsBalance", "getUserEnableStatus", "getUserName", "getUserPassword", "getUserReserveBalance", "getUserWalletBalance", "()Ljava/lang/Float;", "getValidationStatus", "getVoiceBalance", "setBasicEmail", "", "setBasicMobile", "setCustomNetiID", "(Ljava/lang/Integer;)V", "setDateOfBirth", "setEmailBalance", "setFullName", "setGender", "setGlobalAreaInfoDTO", "setImageContent", "setImageName", "setImagePath", "setImageSaveOrEditable", "(Ljava/lang/Boolean;)V", "setLastDateExecuted", "setLastIpExecuted", "setLastUserExecuted", "setNetiID", "setRegistrationDate", "setReligion", "setSmsBalance", "setUserEnableStatus", "setUserName", "setUserPassword", "setUserReserveBalance", "setUserWalletBalance", "(Ljava/lang/Float;)V", "setValidationStatus", "setVoiceBalance", "GlobalAreaInfoDTO", "app_release"})
public final class UserProfileUpdate {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "netiID")
    private java.lang.Integer netiID;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "customNetiID")
    private java.lang.Integer customNetiID;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "fullName")
    private java.lang.String fullName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "gender")
    private java.lang.String gender;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "religion")
    private java.lang.String religion;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "dateOfBirth")
    private java.lang.String dateOfBirth;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "basicMobile")
    private java.lang.String basicMobile;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "basicEmail")
    private java.lang.String basicEmail;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "imagePath")
    private java.lang.String imagePath;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "imageName")
    private java.lang.String imageName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "userWalletBalance")
    private java.lang.Float userWalletBalance;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "userReserveBalance")
    private java.lang.Integer userReserveBalance;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "smsBalance")
    private java.lang.Integer smsBalance;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "voiceBalance")
    private java.lang.Integer voiceBalance;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "emailBalance")
    private java.lang.Integer emailBalance;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "validationStatus")
    private java.lang.Integer validationStatus;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "userEnableStatus")
    private java.lang.Integer userEnableStatus;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "registrationDate")
    private java.lang.Integer registrationDate;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "userName")
    private java.lang.Object userName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "userPassword")
    private java.lang.String userPassword;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "imageContent")
    private java.lang.Object imageContent;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "imageSaveOrEditable")
    private java.lang.Boolean imageSaveOrEditable;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "globalAreaInfoDTO")
    private com.netizen.medicom.model.UserProfileUpdate.GlobalAreaInfoDTO globalAreaInfoDTO;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "lastUserExecuted")
    private java.lang.String lastUserExecuted;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "lastIpExecuted")
    private java.lang.String lastIpExecuted;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "lastDateExecuted")
    private java.lang.Integer lastDateExecuted;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getNetiID() {
        return null;
    }
    
    public final void setNetiID(@org.jetbrains.annotations.Nullable()
    java.lang.Integer netiID) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getCustomNetiID() {
        return null;
    }
    
    public final void setCustomNetiID(@org.jetbrains.annotations.Nullable()
    java.lang.Integer customNetiID) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getFullName() {
        return null;
    }
    
    public final void setFullName(@org.jetbrains.annotations.Nullable()
    java.lang.String fullName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getGender() {
        return null;
    }
    
    public final void setGender(@org.jetbrains.annotations.Nullable()
    java.lang.String gender) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getReligion() {
        return null;
    }
    
    public final void setReligion(@org.jetbrains.annotations.Nullable()
    java.lang.String religion) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDateOfBirth() {
        return null;
    }
    
    public final void setDateOfBirth(@org.jetbrains.annotations.Nullable()
    java.lang.String dateOfBirth) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getBasicMobile() {
        return null;
    }
    
    public final void setBasicMobile(@org.jetbrains.annotations.Nullable()
    java.lang.String basicMobile) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getBasicEmail() {
        return null;
    }
    
    public final void setBasicEmail(@org.jetbrains.annotations.Nullable()
    java.lang.String basicEmail) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getImagePath() {
        return null;
    }
    
    public final void setImagePath(@org.jetbrains.annotations.Nullable()
    java.lang.String imagePath) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getImageName() {
        return null;
    }
    
    public final void setImageName(@org.jetbrains.annotations.Nullable()
    java.lang.String imageName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Float getUserWalletBalance() {
        return null;
    }
    
    public final void setUserWalletBalance(@org.jetbrains.annotations.Nullable()
    java.lang.Float userWalletBalance) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getUserReserveBalance() {
        return null;
    }
    
    public final void setUserReserveBalance(@org.jetbrains.annotations.Nullable()
    java.lang.Integer userReserveBalance) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getSmsBalance() {
        return null;
    }
    
    public final void setSmsBalance(@org.jetbrains.annotations.Nullable()
    java.lang.Integer smsBalance) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getVoiceBalance() {
        return null;
    }
    
    public final void setVoiceBalance(@org.jetbrains.annotations.Nullable()
    java.lang.Integer voiceBalance) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getEmailBalance() {
        return null;
    }
    
    public final void setEmailBalance(@org.jetbrains.annotations.Nullable()
    java.lang.Integer emailBalance) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getValidationStatus() {
        return null;
    }
    
    public final void setValidationStatus(@org.jetbrains.annotations.Nullable()
    java.lang.Integer validationStatus) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getUserEnableStatus() {
        return null;
    }
    
    public final void setUserEnableStatus(@org.jetbrains.annotations.Nullable()
    java.lang.Integer userEnableStatus) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getRegistrationDate() {
        return null;
    }
    
    public final void setRegistrationDate(@org.jetbrains.annotations.Nullable()
    java.lang.Integer registrationDate) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getUserName() {
        return null;
    }
    
    public final void setUserName(@org.jetbrains.annotations.Nullable()
    java.lang.Object userName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getUserPassword() {
        return null;
    }
    
    public final void setUserPassword(@org.jetbrains.annotations.Nullable()
    java.lang.String userPassword) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getImageContent() {
        return null;
    }
    
    public final void setImageContent(@org.jetbrains.annotations.Nullable()
    java.lang.Object imageContent) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean getImageSaveOrEditable() {
        return null;
    }
    
    public final void setImageSaveOrEditable(@org.jetbrains.annotations.Nullable()
    java.lang.Boolean imageSaveOrEditable) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.medicom.model.UserProfileUpdate.GlobalAreaInfoDTO getGlobalAreaInfoDTO() {
        return null;
    }
    
    public final void setGlobalAreaInfoDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.medicom.model.UserProfileUpdate.GlobalAreaInfoDTO globalAreaInfoDTO) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getLastUserExecuted() {
        return null;
    }
    
    public final void setLastUserExecuted(@org.jetbrains.annotations.Nullable()
    java.lang.String lastUserExecuted) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getLastIpExecuted() {
        return null;
    }
    
    public final void setLastIpExecuted(@org.jetbrains.annotations.Nullable()
    java.lang.String lastIpExecuted) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getLastDateExecuted() {
        return null;
    }
    
    public final void setLastDateExecuted(@org.jetbrains.annotations.Nullable()
    java.lang.Integer lastDateExecuted) {
    }
    
    public UserProfileUpdate() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u001b\n\u0002\u0010\u0002\n\u0002\b\u000e\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0013\u001a\u0004\u0018\u00010\u0004J\r\u0010\u0014\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0015J\b\u0010\u0016\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0017\u001a\u0004\u0018\u00010\u0004J\r\u0010\u0018\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0015J\r\u0010\u0019\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0015J\r\u0010\u001a\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0015J\b\u0010\u001b\u001a\u0004\u0018\u00010\u0004J\b\u0010\u001c\u001a\u0004\u0018\u00010\u0004J\b\u0010\u001d\u001a\u0004\u0018\u00010\u0001J\r\u0010\u001e\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0015J\b\u0010\u001f\u001a\u0004\u0018\u00010\u0001J\r\u0010 \u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0015J\u0010\u0010!\u001a\u00020\"2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0015\u0010#\u001a\u00020\"2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010$J\u0010\u0010%\u001a\u00020\"2\b\u0010\b\u001a\u0004\u0018\u00010\u0004J\u0010\u0010&\u001a\u00020\"2\b\u0010\t\u001a\u0004\u0018\u00010\u0004J\u0015\u0010\'\u001a\u00020\"2\b\u0010\n\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010$J\u0015\u0010(\u001a\u00020\"2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010$J\u0015\u0010)\u001a\u00020\"2\b\u0010\f\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010$J\u0010\u0010*\u001a\u00020\"2\b\u0010\r\u001a\u0004\u0018\u00010\u0004J\u0010\u0010+\u001a\u00020\"2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0004J\u0010\u0010,\u001a\u00020\"2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0001J\u0015\u0010-\u001a\u00020\"2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010$J\u0010\u0010.\u001a\u00020\"2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001J\u0015\u0010/\u001a\u00020\"2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010$R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0014\u0010\b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\n\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0016\u0010\u000b\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0016\u0010\f\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0014\u0010\r\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0010\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0014\u0010\u0011\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0012\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007\u00a8\u00060"}, d2 = {"Lcom/netizen/medicom/model/UserProfileUpdate$GlobalAreaInfoDTO;", "", "()V", "categoryDefaultCode", "", "categoryEnableStatus", "", "Ljava/lang/Integer;", "categoryName", "categoryNote", "categorySerial", "coreCategoryID", "lastDateExecuted", "lastIpExecuted", "lastUserExecuted", "parentCoreCategoryInfoDTO", "parentStatus", "parentTypeInfoDTO", "typeStatus", "getCategoryDefaultCode", "getCategoryEnableStatus", "()Ljava/lang/Integer;", "getCategoryName", "getCategoryNote", "getCategorySerial", "getCoreCategoryID", "getLastDateExecuted", "getLastIpExecuted", "getLastUserExecuted", "getParentCoreCategoryInfoDTO", "getParentStatus", "getParentTypeInfoDTO", "getTypeStatus", "setCategoryDefaultCode", "", "setCategoryEnableStatus", "(Ljava/lang/Integer;)V", "setCategoryName", "setCategoryNote", "setCategorySerial", "setCoreCategoryID", "setLastDateExecuted", "setLastIpExecuted", "setLastUserExecuted", "setParentCoreCategoryInfoDTO", "setParentStatus", "setParentTypeInfoDTO", "setTypeStatus", "app_release"})
    public static final class GlobalAreaInfoDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "coreCategoryID")
        private java.lang.Integer coreCategoryID;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "categoryDefaultCode")
        private java.lang.String categoryDefaultCode;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "categoryName")
        private java.lang.String categoryName;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "categoryNote")
        private java.lang.String categoryNote;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "categoryEnableStatus")
        private java.lang.Integer categoryEnableStatus;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "categorySerial")
        private java.lang.Integer categorySerial;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "typeStatus")
        private java.lang.Integer typeStatus;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "parentStatus")
        private java.lang.Integer parentStatus;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "parentTypeInfoDTO")
        private java.lang.Object parentTypeInfoDTO;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "parentCoreCategoryInfoDTO")
        private java.lang.Object parentCoreCategoryInfoDTO;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "lastUserExecuted")
        private java.lang.String lastUserExecuted;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "lastIpExecuted")
        private java.lang.String lastIpExecuted;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "lastDateExecuted")
        private java.lang.Integer lastDateExecuted;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getCoreCategoryID() {
            return null;
        }
        
        public final void setCoreCategoryID(@org.jetbrains.annotations.Nullable()
        java.lang.Integer coreCategoryID) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCategoryDefaultCode() {
            return null;
        }
        
        public final void setCategoryDefaultCode(@org.jetbrains.annotations.Nullable()
        java.lang.String categoryDefaultCode) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCategoryName() {
            return null;
        }
        
        public final void setCategoryName(@org.jetbrains.annotations.Nullable()
        java.lang.String categoryName) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCategoryNote() {
            return null;
        }
        
        public final void setCategoryNote(@org.jetbrains.annotations.Nullable()
        java.lang.String categoryNote) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getCategoryEnableStatus() {
            return null;
        }
        
        public final void setCategoryEnableStatus(@org.jetbrains.annotations.Nullable()
        java.lang.Integer categoryEnableStatus) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getCategorySerial() {
            return null;
        }
        
        public final void setCategorySerial(@org.jetbrains.annotations.Nullable()
        java.lang.Integer categorySerial) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getTypeStatus() {
            return null;
        }
        
        public final void setTypeStatus(@org.jetbrains.annotations.Nullable()
        java.lang.Integer typeStatus) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getParentStatus() {
            return null;
        }
        
        public final void setParentStatus(@org.jetbrains.annotations.Nullable()
        java.lang.Integer parentStatus) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Object getParentTypeInfoDTO() {
            return null;
        }
        
        public final void setParentTypeInfoDTO(@org.jetbrains.annotations.Nullable()
        java.lang.Object parentTypeInfoDTO) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Object getParentCoreCategoryInfoDTO() {
            return null;
        }
        
        public final void setParentCoreCategoryInfoDTO(@org.jetbrains.annotations.Nullable()
        java.lang.Object parentCoreCategoryInfoDTO) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getLastUserExecuted() {
            return null;
        }
        
        public final void setLastUserExecuted(@org.jetbrains.annotations.Nullable()
        java.lang.String lastUserExecuted) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getLastIpExecuted() {
            return null;
        }
        
        public final void setLastIpExecuted(@org.jetbrains.annotations.Nullable()
        java.lang.String lastIpExecuted) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getLastDateExecuted() {
            return null;
        }
        
        public final void setLastDateExecuted(@org.jetbrains.annotations.Nullable()
        java.lang.Integer lastDateExecuted) {
        }
        
        public GlobalAreaInfoDTO() {
            super();
        }
    }
}