package com.netizen.medicom.quickblox.fragments;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rH\u0014J\u0010\u0010\u000e\u001a\u00020\u000b2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\b\u0010\u0011\u001a\u00020\u000bH\u0014J\b\u0010\u0012\u001a\u00020\u000bH\u0014J\b\u0010\u0013\u001a\u00020\u000bH\u0014J\r\u0010\u0014\u001a\u00020\u0015H\u0010\u00a2\u0006\u0002\b\u0016J\b\u0010\u0017\u001a\u00020\u0018H\u0002J\b\u0010\u0019\u001a\u00020\u000bH\u0014J\u0012\u0010\u001a\u001a\u00020\u000b2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0014J\u0016\u0010\u001d\u001a\u00020\u00182\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020 0\u001fH\u0002J\u0010\u0010!\u001a\u00020\u000b2\u0006\u0010\"\u001a\u00020\u0018H\u0016J\u0016\u0010#\u001a\u00020\u000b2\f\u0010$\u001a\b\u0012\u0004\u0012\u00020 0\u001fH\u0016J\b\u0010%\u001a\u00020\u000bH\u0016J\b\u0010&\u001a\u00020\u000bH\u0016J\b\u0010\'\u001a\u00020\u000bH\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0005X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0005X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006("}, d2 = {"Lcom/netizen/medicom/quickblox/fragments/AudioConversationFragment;", "Lcom/netizen/medicom/quickblox/fragments/BaseConversationFragment;", "Lcom/netizen/medicom/quickblox/activities/CallActivity$OnChangeAudioDevice;", "()V", "alsoOnCallText", "Landroid/widget/TextView;", "audioSwitchToggleButton", "Landroid/widget/ToggleButton;", "firstOpponentNameTextView", "otherOpponentsTextView", "actionButtonsEnabled", "", "inability", "", "audioDeviceChanged", "newAudioDevice", "Lcom/quickblox/videochat/webrtc/AppRTCAudioManager$AudioDevice;", "configureActionBar", "configureOutgoingScreen", "configureToolbar", "getFragmentLayout", "", "getFragmentLayout$app_release", "getOtherOpponentsNames", "", "initButtonsListener", "initViews", "view", "Landroid/view/View;", "makeStringFromUsersFullNames", "allUsers", "Ljava/util/ArrayList;", "Lcom/quickblox/users/model/QBUser;", "onCallTimeUpdate", "time", "onOpponentsListUpdated", "newUsers", "onStart", "onStop", "setVisibilityAlsoOnCallTextView", "app_release"})
public final class AudioConversationFragment extends com.netizen.medicom.quickblox.fragments.BaseConversationFragment implements com.netizen.medicom.quickblox.activities.CallActivity.OnChangeAudioDevice {
    private android.widget.ToggleButton audioSwitchToggleButton;
    private android.widget.TextView alsoOnCallText;
    private android.widget.TextView firstOpponentNameTextView;
    private android.widget.TextView otherOpponentsTextView;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    public void onStart() {
    }
    
    @java.lang.Override()
    protected void configureOutgoingScreen() {
    }
    
    @java.lang.Override()
    protected void configureToolbar() {
    }
    
    @java.lang.Override()
    protected void configureActionBar() {
    }
    
    @java.lang.Override()
    protected void initViews(@org.jetbrains.annotations.Nullable()
    android.view.View view) {
    }
    
    private final void setVisibilityAlsoOnCallTextView() {
    }
    
    private final java.lang.String getOtherOpponentsNames() {
        return null;
    }
    
    private final java.lang.String makeStringFromUsersFullNames(java.util.ArrayList<com.quickblox.users.model.QBUser> allUsers) {
        return null;
    }
    
    @java.lang.Override()
    public void onStop() {
    }
    
    @java.lang.Override()
    protected void initButtonsListener() {
    }
    
    @java.lang.Override()
    protected void actionButtonsEnabled(boolean inability) {
    }
    
    @java.lang.Override()
    public int getFragmentLayout$app_release() {
        return 0;
    }
    
    @java.lang.Override()
    public void onOpponentsListUpdated(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.quickblox.users.model.QBUser> newUsers) {
    }
    
    @java.lang.Override()
    public void onCallTimeUpdate(@org.jetbrains.annotations.NotNull()
    java.lang.String time) {
    }
    
    @java.lang.Override()
    public void audioDeviceChanged(@org.jetbrains.annotations.NotNull()
    com.quickblox.videochat.webrtc.AppRTCAudioManager.AudioDevice newAudioDevice) {
    }
    
    public AudioConversationFragment() {
        super();
    }
}