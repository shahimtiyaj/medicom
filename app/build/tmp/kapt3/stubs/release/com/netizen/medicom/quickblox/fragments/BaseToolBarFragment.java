package com.netizen.medicom.quickblox.fragments;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b&\u0018\u00002\u00020\u0001:\u0001#B\u0005\u00a2\u0006\u0002\u0010\u0002J\r\u0010\u0015\u001a\u00020\u0016H \u00a2\u0006\u0002\b\u0017J\b\u0010\u0018\u001a\u00020\u0019H\u0002J\u0012\u0010\u001a\u001a\u00020\u00192\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016J&\u0010\u001d\u001a\u0004\u0018\u00010\u001e2\u0006\u0010\u001f\u001a\u00020 2\b\u0010!\u001a\u0004\u0018\u00010\"2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016R\u001a\u0010\u0003\u001a\u00020\u0004X\u0084.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\u00020\nX\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001a\u0010\u000f\u001a\u00020\u0010X\u0084.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014\u00a8\u0006$"}, d2 = {"Lcom/netizen/medicom/quickblox/fragments/BaseToolBarFragment;", "Landroidx/fragment/app/Fragment;", "()V", "actionBar", "Landroidx/appcompat/app/ActionBar;", "getActionBar", "()Landroidx/appcompat/app/ActionBar;", "setActionBar", "(Landroidx/appcompat/app/ActionBar;)V", "mainHandler", "Landroid/os/Handler;", "getMainHandler", "()Landroid/os/Handler;", "setMainHandler", "(Landroid/os/Handler;)V", "toolbar", "Landroidx/appcompat/widget/Toolbar;", "getToolbar", "()Landroidx/appcompat/widget/Toolbar;", "setToolbar", "(Landroidx/appcompat/widget/Toolbar;)V", "getFragmentLayout", "", "getFragmentLayout$app_release", "initActionBar", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "FragmentLifeCycleHandler", "app_release"})
public abstract class BaseToolBarFragment extends androidx.fragment.app.Fragment {
    @org.jetbrains.annotations.NotNull()
    protected androidx.appcompat.app.ActionBar actionBar;
    @org.jetbrains.annotations.NotNull()
    protected androidx.appcompat.widget.Toolbar toolbar;
    @org.jetbrains.annotations.NotNull()
    private android.os.Handler mainHandler;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    protected final androidx.appcompat.app.ActionBar getActionBar() {
        return null;
    }
    
    protected final void setActionBar(@org.jetbrains.annotations.NotNull()
    androidx.appcompat.app.ActionBar p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final androidx.appcompat.widget.Toolbar getToolbar() {
        return null;
    }
    
    protected final void setToolbar(@org.jetbrains.annotations.NotNull()
    androidx.appcompat.widget.Toolbar p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final android.os.Handler getMainHandler() {
        return null;
    }
    
    protected final void setMainHandler(@org.jetbrains.annotations.NotNull()
    android.os.Handler p0) {
    }
    
    public abstract int getFragmentLayout$app_release();
    
    @java.lang.Override()
    public void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    private final void initActionBar() {
    }
    
    public BaseToolBarFragment() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00030\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"}, d2 = {"Lcom/netizen/medicom/quickblox/fragments/BaseToolBarFragment$FragmentLifeCycleHandler;", "Landroid/os/Handler;", "fragment", "Landroidx/fragment/app/Fragment;", "(Landroidx/fragment/app/Fragment;)V", "fragmentRef", "Ljava/lang/ref/WeakReference;", "dispatchMessage", "", "msg", "Landroid/os/Message;", "app_release"})
    public static final class FragmentLifeCycleHandler extends android.os.Handler {
        private final java.lang.ref.WeakReference<androidx.fragment.app.Fragment> fragmentRef = null;
        
        @java.lang.Override()
        public void dispatchMessage(@org.jetbrains.annotations.NotNull()
        android.os.Message msg) {
        }
        
        public FragmentLifeCycleHandler(@org.jetbrains.annotations.NotNull()
        androidx.fragment.app.Fragment fragment) {
            super();
        }
    }
}