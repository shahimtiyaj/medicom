// Generated by data binding compiler. Do not edit!
package com.netizen.medicom.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.netizen.medicom.R;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentDoctorIncomingCallBinding extends ViewDataBinding {
  @NonNull
  public final CardView layoutBottom;

  @NonNull
  public final RelativeLayout layoutReceive;

  @NonNull
  public final LinearLayout layoutTop;

  protected FragmentDoctorIncomingCallBinding(Object _bindingComponent, View _root,
      int _localFieldCount, CardView layoutBottom, RelativeLayout layoutReceive,
      LinearLayout layoutTop) {
    super(_bindingComponent, _root, _localFieldCount);
    this.layoutBottom = layoutBottom;
    this.layoutReceive = layoutReceive;
    this.layoutTop = layoutTop;
  }

  @NonNull
  public static FragmentDoctorIncomingCallBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_doctor_incoming_call, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentDoctorIncomingCallBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentDoctorIncomingCallBinding>inflateInternal(inflater, R.layout.fragment_doctor_incoming_call, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentDoctorIncomingCallBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_doctor_incoming_call, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentDoctorIncomingCallBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentDoctorIncomingCallBinding>inflateInternal(inflater, R.layout.fragment_doctor_incoming_call, null, false, component);
  }

  public static FragmentDoctorIncomingCallBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentDoctorIncomingCallBinding bind(@NonNull View view,
      @Nullable Object component) {
    return (FragmentDoctorIncomingCallBinding)bind(component, view, R.layout.fragment_doctor_incoming_call);
  }
}
