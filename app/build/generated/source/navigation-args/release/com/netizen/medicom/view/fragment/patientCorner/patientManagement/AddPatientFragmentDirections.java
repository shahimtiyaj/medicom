package com.netizen.medicom.view.fragment.patientCorner.patientManagement;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.netizen.medicom.R;

public class AddPatientFragmentDirections {
  private AddPatientFragmentDirections() {
  }

  @NonNull
  public static NavDirections actionAddPatientFragmentToPatientListFragment() {
    return new ActionOnlyNavDirections(R.id.action_addPatientFragment_to_patientListFragment);
  }
}
