package com.netizen.medicom.view.fragment.signIn;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.netizen.medicom.R;

public class FragmentForgotPasswordTwoDirections {
  private FragmentForgotPasswordTwoDirections() {
  }

  @NonNull
  public static NavDirections actionFragmentForgotPasswordTwoToFragmentForgotPasswordThree() {
    return new ActionOnlyNavDirections(R.id.action_fragmentForgotPasswordTwo_to_fragmentForgotPasswordThree);
  }
}
