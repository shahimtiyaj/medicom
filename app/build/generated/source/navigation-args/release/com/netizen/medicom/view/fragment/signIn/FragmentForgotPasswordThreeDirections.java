package com.netizen.medicom.view.fragment.signIn;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.netizen.medicom.R;

public class FragmentForgotPasswordThreeDirections {
  private FragmentForgotPasswordThreeDirections() {
  }

  @NonNull
  public static NavDirections actionFragmentForgotPasswordThreeToFragmentForgotPasswordFour() {
    return new ActionOnlyNavDirections(R.id.action_fragmentForgotPasswordThree_to_fragmentForgotPasswordFour);
  }
}
