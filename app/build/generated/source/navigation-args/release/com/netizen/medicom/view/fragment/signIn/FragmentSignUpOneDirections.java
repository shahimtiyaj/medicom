package com.netizen.medicom.view.fragment.signIn;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import com.netizen.medicom.R;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class FragmentSignUpOneDirections {
  private FragmentSignUpOneDirections() {
  }

  @NonNull
  public static ActionFragmentSignUpOneToFragmentSignUpTwo actionFragmentSignUpOneToFragmentSignUpTwo(
      @NonNull String username, @NonNull String password) {
    return new ActionFragmentSignUpOneToFragmentSignUpTwo(username, password);
  }

  public static class ActionFragmentSignUpOneToFragmentSignUpTwo implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionFragmentSignUpOneToFragmentSignUpTwo(@NonNull String username,
        @NonNull String password) {
      if (username == null) {
        throw new IllegalArgumentException("Argument \"username\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("username", username);
      if (password == null) {
        throw new IllegalArgumentException("Argument \"password\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("password", password);
    }

    @NonNull
    public ActionFragmentSignUpOneToFragmentSignUpTwo setUsername(@NonNull String username) {
      if (username == null) {
        throw new IllegalArgumentException("Argument \"username\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("username", username);
      return this;
    }

    @NonNull
    public ActionFragmentSignUpOneToFragmentSignUpTwo setPassword(@NonNull String password) {
      if (password == null) {
        throw new IllegalArgumentException("Argument \"password\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("password", password);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("username")) {
        String username = (String) arguments.get("username");
        __result.putString("username", username);
      }
      if (arguments.containsKey("password")) {
        String password = (String) arguments.get("password");
        __result.putString("password", password);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_fragmentSignUpOne_to_fragmentSignUpTwo;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public String getUsername() {
      return (String) arguments.get("username");
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public String getPassword() {
      return (String) arguments.get("password");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionFragmentSignUpOneToFragmentSignUpTwo that = (ActionFragmentSignUpOneToFragmentSignUpTwo) object;
      if (arguments.containsKey("username") != that.arguments.containsKey("username")) {
        return false;
      }
      if (getUsername() != null ? !getUsername().equals(that.getUsername()) : that.getUsername() != null) {
        return false;
      }
      if (arguments.containsKey("password") != that.arguments.containsKey("password")) {
        return false;
      }
      if (getPassword() != null ? !getPassword().equals(that.getPassword()) : that.getPassword() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getUsername() != null ? getUsername().hashCode() : 0);
      result = 31 * result + (getPassword() != null ? getPassword().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionFragmentSignUpOneToFragmentSignUpTwo(actionId=" + getActionId() + "){"
          + "username=" + getUsername()
          + ", password=" + getPassword()
          + "}";
    }
  }
}
