package com.netizen.medicom.view.fragment.doctorCorner;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.netizen.medicom.R;

public class FragmentDoctorDashboardDirections {
  private FragmentDoctorDashboardDirections() {
  }

  @NonNull
  public static NavDirections actionFragmentDoctorDashboardToFragmentCallDetailsHistory() {
    return new ActionOnlyNavDirections(R.id.action_fragmentDoctorDashboard_to_fragmentCallDetailsHistory);
  }
}
