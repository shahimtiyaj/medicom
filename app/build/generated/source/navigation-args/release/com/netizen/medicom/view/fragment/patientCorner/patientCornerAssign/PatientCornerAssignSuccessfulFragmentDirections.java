package com.netizen.medicom.view.fragment.patientCorner.patientCornerAssign;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.netizen.medicom.R;

public class PatientCornerAssignSuccessfulFragmentDirections {
  private PatientCornerAssignSuccessfulFragmentDirections() {
  }

  @NonNull
  public static NavDirections actionPartnerCornerAssignSuccessfulFragmentToPatientDashboardFragment(
      ) {
    return new ActionOnlyNavDirections(R.id.action_partnerCornerAssignSuccessfulFragment_to_patientDashboardFragment);
  }
}
