package com.netizen.medicom.view.fragment.doctorCorner;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.netizen.medicom.R;

public class FragmentDoctorCornerAssignThreeDirections {
  private FragmentDoctorCornerAssignThreeDirections() {
  }

  @NonNull
  public static NavDirections actionFragmentDoctorCornerAssignThreeToFragmentDoctorCornerAssignFour(
      ) {
    return new ActionOnlyNavDirections(R.id.action_fragmentDoctorCornerAssignThree_to_fragmentDoctorCornerAssignFour);
  }
}
