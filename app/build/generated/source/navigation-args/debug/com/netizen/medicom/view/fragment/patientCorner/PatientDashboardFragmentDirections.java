package com.netizen.medicom.view.fragment.patientCorner;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.netizen.medicom.R;

public class PatientDashboardFragmentDirections {
  private PatientDashboardFragmentDirections() {
  }

  @NonNull
  public static NavDirections actionPatientDashboardFragmentToAddPatientFragment() {
    return new ActionOnlyNavDirections(R.id.action_patientDashboardFragment_to_addPatientFragment);
  }
}
