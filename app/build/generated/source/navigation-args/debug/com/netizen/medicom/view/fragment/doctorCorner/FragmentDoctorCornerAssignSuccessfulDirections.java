package com.netizen.medicom.view.fragment.doctorCorner;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.netizen.medicom.R;

public class FragmentDoctorCornerAssignSuccessfulDirections {
  private FragmentDoctorCornerAssignSuccessfulDirections() {
  }

  @NonNull
  public static NavDirections actionFragmentDoctorCornerAssignSuccessfulToFragmentDoctorDashboard(
      ) {
    return new ActionOnlyNavDirections(R.id.action_fragmentDoctorCornerAssignSuccessful_to_fragmentDoctorDashboard);
  }
}
