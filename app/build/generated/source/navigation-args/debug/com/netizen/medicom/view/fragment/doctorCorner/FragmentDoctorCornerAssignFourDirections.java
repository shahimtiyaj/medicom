package com.netizen.medicom.view.fragment.doctorCorner;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.netizen.medicom.R;

public class FragmentDoctorCornerAssignFourDirections {
  private FragmentDoctorCornerAssignFourDirections() {
  }

  @NonNull
  public static NavDirections actionFragmentDoctorCornerAssignFourToFragmentDoctorCornerAssignSuccessful(
      ) {
    return new ActionOnlyNavDirections(R.id.action_fragmentDoctorCornerAssignFour_to_fragmentDoctorCornerAssignSuccessful);
  }
}
