package com.netizen.medicom.view.fragment;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.netizen.medicom.R;

public class HomeFragmentDirections {
  private HomeFragmentDirections() {
  }

  @NonNull
  public static NavDirections actionHomeFragmentToPatientCornerAssignOneFragment() {
    return new ActionOnlyNavDirections(R.id.action_homeFragment_to_patientCornerAssignOneFragment);
  }

  @NonNull
  public static NavDirections actionHomeFragmentToFragmentDoctorCornerAssignOne() {
    return new ActionOnlyNavDirections(R.id.action_homeFragment_to_fragmentDoctorCornerAssignOne);
  }

  @NonNull
  public static NavDirections actionHomeFragmentToAddPatientFragment() {
    return new ActionOnlyNavDirections(R.id.action_homeFragment_to_addPatientFragment);
  }

  @NonNull
  public static NavDirections actionHomeFragmentToPatientListFragment() {
    return new ActionOnlyNavDirections(R.id.action_homeFragment_to_patientListFragment);
  }

  @NonNull
  public static NavDirections actionHomeFragmentToPatientDashboardFragment() {
    return new ActionOnlyNavDirections(R.id.action_homeFragment_to_patientDashboardFragment);
  }
}
