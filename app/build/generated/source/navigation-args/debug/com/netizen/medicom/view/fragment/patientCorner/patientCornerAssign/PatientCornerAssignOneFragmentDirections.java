package com.netizen.medicom.view.fragment.patientCorner.patientCornerAssign;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.netizen.medicom.R;

public class PatientCornerAssignOneFragmentDirections {
  private PatientCornerAssignOneFragmentDirections() {
  }

  @NonNull
  public static NavDirections actionPatientCornerAssignOneFragmentToPatientCornerAssignTwoFragment(
      ) {
    return new ActionOnlyNavDirections(R.id.action_patientCornerAssignOneFragment_to_patientCornerAssignTwoFragment);
  }
}
