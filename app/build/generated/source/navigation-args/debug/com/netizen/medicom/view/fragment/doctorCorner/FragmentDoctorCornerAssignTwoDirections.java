package com.netizen.medicom.view.fragment.doctorCorner;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.netizen.medicom.R;

public class FragmentDoctorCornerAssignTwoDirections {
  private FragmentDoctorCornerAssignTwoDirections() {
  }

  @NonNull
  public static NavDirections actionFragmentDoctorCornerAssignTwoToFragmentDoctorCornerAssignThree(
      ) {
    return new ActionOnlyNavDirections(R.id.action_fragmentDoctorCornerAssignTwo_to_fragmentDoctorCornerAssignThree);
  }
}
