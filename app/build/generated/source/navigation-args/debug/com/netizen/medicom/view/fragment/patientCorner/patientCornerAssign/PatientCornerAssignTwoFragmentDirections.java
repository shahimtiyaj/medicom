package com.netizen.medicom.view.fragment.patientCorner.patientCornerAssign;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.netizen.medicom.R;

public class PatientCornerAssignTwoFragmentDirections {
  private PatientCornerAssignTwoFragmentDirections() {
  }

  @NonNull
  public static NavDirections actionPatientCornerAssignTwoFragmentToPartnerCornerAssignSuccessfulFragment(
      ) {
    return new ActionOnlyNavDirections(R.id.action_patientCornerAssignTwoFragment_to_partnerCornerAssignSuccessfulFragment);
  }
}
