package com.netizen.medicom.databinding;
import com.netizen.medicom.R;
import com.netizen.medicom.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentPatientDashboardBindingImpl extends FragmentPatientDashboardBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = new androidx.databinding.ViewDataBinding.IncludedLayouts(8);
        sIncludes.setIncludes(1, 
            new String[] {"single_approved_appointment_request"},
            new int[] {4},
            new int[] {com.netizen.medicom.R.layout.single_approved_appointment_request});
        sIncludes.setIncludes(2, 
            new String[] {"single_pending_appointment_request"},
            new int[] {5},
            new int[] {com.netizen.medicom.R.layout.single_pending_appointment_request});
        sIncludes.setIncludes(3, 
            new String[] {"single_completed_calls"},
            new int[] {6},
            new int[] {com.netizen.medicom.R.layout.single_completed_calls});
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.add_patient, 7);
    }
    // views
    @NonNull
    private final android.widget.LinearLayout mboundView0;
    @NonNull
    private final android.widget.LinearLayout mboundView1;
    @Nullable
    private final com.netizen.medicom.databinding.SingleApprovedAppointmentRequestBinding mboundView11;
    @NonNull
    private final android.widget.LinearLayout mboundView2;
    @Nullable
    private final com.netizen.medicom.databinding.SinglePendingAppointmentRequestBinding mboundView21;
    @NonNull
    private final android.widget.LinearLayout mboundView3;
    @Nullable
    private final com.netizen.medicom.databinding.SingleCompletedCallsBinding mboundView31;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentPatientDashboardBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 8, sIncludes, sViewsWithIds));
    }
    private FragmentPatientDashboardBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.TextView) bindings[7]
            );
        this.mboundView0 = (android.widget.LinearLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.LinearLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView11 = (com.netizen.medicom.databinding.SingleApprovedAppointmentRequestBinding) bindings[4];
        setContainedBinding(this.mboundView11);
        this.mboundView2 = (android.widget.LinearLayout) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView21 = (com.netizen.medicom.databinding.SinglePendingAppointmentRequestBinding) bindings[5];
        setContainedBinding(this.mboundView21);
        this.mboundView3 = (android.widget.LinearLayout) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView31 = (com.netizen.medicom.databinding.SingleCompletedCallsBinding) bindings[6];
        setContainedBinding(this.mboundView31);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        mboundView11.invalidateAll();
        mboundView21.invalidateAll();
        mboundView31.invalidateAll();
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        if (mboundView11.hasPendingBindings()) {
            return true;
        }
        if (mboundView21.hasPendingBindings()) {
            return true;
        }
        if (mboundView31.hasPendingBindings()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    public void setLifecycleOwner(@Nullable androidx.lifecycle.LifecycleOwner lifecycleOwner) {
        super.setLifecycleOwner(lifecycleOwner);
        mboundView11.setLifecycleOwner(lifecycleOwner);
        mboundView21.setLifecycleOwner(lifecycleOwner);
        mboundView31.setLifecycleOwner(lifecycleOwner);
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
        executeBindingsOn(mboundView11);
        executeBindingsOn(mboundView21);
        executeBindingsOn(mboundView31);
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}