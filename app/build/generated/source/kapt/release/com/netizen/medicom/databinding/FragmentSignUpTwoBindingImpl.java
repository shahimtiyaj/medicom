package com.netizen.medicom.databinding;
import com.netizen.medicom.R;
import com.netizen.medicom.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentSignUpTwoBindingImpl extends FragmentSignUpTwoBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.logo, 1);
        sViewsWithIds.put(R.id.welcome, 2);
        sViewsWithIds.put(R.id.signTitle, 3);
        sViewsWithIds.put(R.id.signTitleIcon, 4);
        sViewsWithIds.put(R.id.userTitle, 5);
        sViewsWithIds.put(R.id.inputUserFullName, 6);
        sViewsWithIds.put(R.id.rdGroup, 7);
        sViewsWithIds.put(R.id.rdbMale, 8);
        sViewsWithIds.put(R.id.rdbFemale, 9);
        sViewsWithIds.put(R.id.rdbOthers, 10);
        sViewsWithIds.put(R.id.passwordTitle, 11);
        sViewsWithIds.put(R.id.spinnerReligion, 12);
        sViewsWithIds.put(R.id.view, 13);
        sViewsWithIds.put(R.id.txtbirthday, 14);
        sViewsWithIds.put(R.id.birthday, 15);
        sViewsWithIds.put(R.id.txtmobile, 16);
        sViewsWithIds.put(R.id.mobile, 17);
        sViewsWithIds.put(R.id.txtemail, 18);
        sViewsWithIds.put(R.id.email, 19);
        sViewsWithIds.put(R.id.txtdistrict, 20);
        sViewsWithIds.put(R.id.district, 21);
        sViewsWithIds.put(R.id.txtArea, 22);
        sViewsWithIds.put(R.id.area, 23);
        sViewsWithIds.put(R.id.signup, 24);
        sViewsWithIds.put(R.id.all_ready_have_a_password, 25);
        sViewsWithIds.put(R.id.login_here, 26);
        sViewsWithIds.put(R.id.lottie_progressbar, 27);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentSignUpTwoBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 28, sIncludes, sViewsWithIds));
    }
    private FragmentSignUpTwoBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.TextView) bindings[25]
            , (android.widget.EditText) bindings[23]
            , (android.widget.EditText) bindings[15]
            , (android.widget.EditText) bindings[21]
            , (android.widget.EditText) bindings[19]
            , (android.widget.EditText) bindings[6]
            , (android.widget.TextView) bindings[26]
            , (android.widget.ImageView) bindings[1]
            , (android.widget.RelativeLayout) bindings[27]
            , (android.widget.EditText) bindings[17]
            , (android.widget.TextView) bindings[11]
            , (android.widget.RadioGroup) bindings[7]
            , (android.widget.RadioButton) bindings[9]
            , (android.widget.RadioButton) bindings[8]
            , (android.widget.RadioButton) bindings[10]
            , (android.widget.TextView) bindings[3]
            , (android.widget.ImageView) bindings[4]
            , (android.widget.Button) bindings[24]
            , (android.widget.Spinner) bindings[12]
            , (android.widget.TextView) bindings[22]
            , (android.widget.TextView) bindings[14]
            , (android.widget.TextView) bindings[20]
            , (android.widget.TextView) bindings[18]
            , (android.widget.TextView) bindings[16]
            , (android.widget.TextView) bindings[5]
            , (android.view.View) bindings[13]
            , (android.widget.TextView) bindings[2]
            );
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}