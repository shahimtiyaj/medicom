package com.netizen.medicom.databinding;
import com.netizen.medicom.R;
import com.netizen.medicom.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentNavBindingImpl extends FragmentNavBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.scroll_main_view, 1);
        sViewsWithIds.put(R.id.image_view_user, 2);
        sViewsWithIds.put(R.id.text_view_user_name, 3);
        sViewsWithIds.put(R.id.text_view_neti_id, 4);
        sViewsWithIds.put(R.id.text_view_dashboard, 5);
        sViewsWithIds.put(R.id.text_view_my_dashboard, 6);
        sViewsWithIds.put(R.id.text_view_patient_info, 7);
        sViewsWithIds.put(R.id.text_view_add_patient, 8);
        sViewsWithIds.put(R.id.text_view_patient_list, 9);
        sViewsWithIds.put(R.id.text_view_find_doctor, 10);
        sViewsWithIds.put(R.id.text_view_my_start_finding, 11);
        sViewsWithIds.put(R.id.text_view_balance_report, 12);
        sViewsWithIds.put(R.id.text_view_wallet_report, 13);
        sViewsWithIds.put(R.id.text_view_statement_report, 14);
        sViewsWithIds.put(R.id.text_view_message_report, 15);
        sViewsWithIds.put(R.id.text_view_revenue_report, 16);
        sViewsWithIds.put(R.id.text_view_purchase_report, 17);
        sViewsWithIds.put(R.id.text_view_general_product_report, 18);
        sViewsWithIds.put(R.id.text_view_offer_product_report, 19);
        sViewsWithIds.put(R.id.text_view_code_log_report, 20);
        sViewsWithIds.put(R.id.image_view_back, 21);
    }
    // views
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentNavBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 22, sIncludes, sViewsWithIds));
    }
    private FragmentNavBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.ImageView) bindings[21]
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[2]
            , (android.widget.RelativeLayout) bindings[0]
            , (android.widget.ScrollView) bindings[1]
            , (android.widget.TextView) bindings[8]
            , (android.widget.TextView) bindings[12]
            , (android.widget.TextView) bindings[20]
            , (android.widget.TextView) bindings[5]
            , (android.widget.TextView) bindings[10]
            , (android.widget.TextView) bindings[18]
            , (android.widget.TextView) bindings[15]
            , (android.widget.TextView) bindings[6]
            , (android.widget.TextView) bindings[11]
            , (android.widget.TextView) bindings[4]
            , (android.widget.TextView) bindings[19]
            , (android.widget.TextView) bindings[7]
            , (android.widget.TextView) bindings[9]
            , (android.widget.TextView) bindings[17]
            , (android.widget.TextView) bindings[16]
            , (android.widget.TextView) bindings[14]
            , (android.widget.TextView) bindings[3]
            , (android.widget.TextView) bindings[13]
            );
        this.layoutMainView.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}