package com.netizen.medicom.databinding;
import com.netizen.medicom.R;
import com.netizen.medicom.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentSignUpOneBindingImpl extends FragmentSignUpOneBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.logo, 1);
        sViewsWithIds.put(R.id.welcome, 2);
        sViewsWithIds.put(R.id.signTitle, 3);
        sViewsWithIds.put(R.id.signTitleIcon, 4);
        sViewsWithIds.put(R.id.userTitle, 5);
        sViewsWithIds.put(R.id.inputUserName, 6);
        sViewsWithIds.put(R.id.passwordTitle, 7);
        sViewsWithIds.put(R.id.inputUserPasswordLayout, 8);
        sViewsWithIds.put(R.id.inputUserPassword, 9);
        sViewsWithIds.put(R.id.image_view_password_toggle, 10);
        sViewsWithIds.put(R.id.repasswordTitle, 11);
        sViewsWithIds.put(R.id.inputUserRePasswordLayout, 12);
        sViewsWithIds.put(R.id.inputUserRePassword, 13);
        sViewsWithIds.put(R.id.image_view_re_password_toggle, 14);
        sViewsWithIds.put(R.id.next, 15);
        sViewsWithIds.put(R.id.all_ready_have_a_password, 16);
        sViewsWithIds.put(R.id.login_here, 17);
        sViewsWithIds.put(R.id.lottie_progressbar, 18);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentSignUpOneBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 19, sIncludes, sViewsWithIds));
    }
    private FragmentSignUpOneBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.TextView) bindings[16]
            , (android.widget.ImageView) bindings[10]
            , (android.widget.ImageView) bindings[14]
            , (android.widget.EditText) bindings[6]
            , (android.widget.EditText) bindings[9]
            , (android.widget.RelativeLayout) bindings[8]
            , (android.widget.EditText) bindings[13]
            , (android.widget.RelativeLayout) bindings[12]
            , (android.widget.TextView) bindings[17]
            , (android.widget.ImageView) bindings[1]
            , (android.widget.RelativeLayout) bindings[18]
            , (android.widget.Button) bindings[15]
            , (android.widget.TextView) bindings[7]
            , (android.widget.TextView) bindings[11]
            , (android.widget.TextView) bindings[3]
            , (android.widget.ImageView) bindings[4]
            , (android.widget.TextView) bindings[5]
            , (android.widget.TextView) bindings[2]
            );
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}