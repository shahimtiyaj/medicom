package com.netizen.medicom;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.netizen.medicom.databinding.ActivityMainBindingImpl;
import com.netizen.medicom.databinding.CustomNotificationLayoutBindingImpl;
import com.netizen.medicom.databinding.FragmentActiveCallBindingImpl;
import com.netizen.medicom.databinding.FragmentAddPatientBindingImpl;
import com.netizen.medicom.databinding.FragmentCallDetailsHistoryBindingImpl;
import com.netizen.medicom.databinding.FragmentChangePasswordBindingImpl;
import com.netizen.medicom.databinding.FragmentCustomerFeedbackBindingImpl;
import com.netizen.medicom.databinding.FragmentDoctorActiveCallBindingImpl;
import com.netizen.medicom.databinding.FragmentDoctorCornerAssignFourBindingImpl;
import com.netizen.medicom.databinding.FragmentDoctorCornerAssignOneBindingImpl;
import com.netizen.medicom.databinding.FragmentDoctorCornerAssignSuccessfulBindingImpl;
import com.netizen.medicom.databinding.FragmentDoctorCornerAssignThreeBindingImpl;
import com.netizen.medicom.databinding.FragmentDoctorCornerAssignTwoBindingImpl;
import com.netizen.medicom.databinding.FragmentDoctorDashboardBindingImpl;
import com.netizen.medicom.databinding.FragmentDoctorFoundBindingImpl;
import com.netizen.medicom.databinding.FragmentDoctorIncomingCallBindingImpl;
import com.netizen.medicom.databinding.FragmentDoctorSearchBindingImpl;
import com.netizen.medicom.databinding.FragmentFeedbackThanksBindingImpl;
import com.netizen.medicom.databinding.FragmentForgotPasswordFourBindingImpl;
import com.netizen.medicom.databinding.FragmentForgotPasswordOneBindingImpl;
import com.netizen.medicom.databinding.FragmentForgotPasswordThreeBindingImpl;
import com.netizen.medicom.databinding.FragmentForgotPasswordTwoBindingImpl;
import com.netizen.medicom.databinding.FragmentHomeBindingImpl;
import com.netizen.medicom.databinding.FragmentIncomingCallBindingImpl;
import com.netizen.medicom.databinding.FragmentLogOutDialogBindingImpl;
import com.netizen.medicom.databinding.FragmentMakeCallBindingImpl;
import com.netizen.medicom.databinding.FragmentNavBindingImpl;
import com.netizen.medicom.databinding.FragmentPatientActiveCallBindingImpl;
import com.netizen.medicom.databinding.FragmentPatientCornerAssignOneBindingImpl;
import com.netizen.medicom.databinding.FragmentPatientCornerAssignSuccessfulBindingImpl;
import com.netizen.medicom.databinding.FragmentPatientCornerAssignTwoBindingImpl;
import com.netizen.medicom.databinding.FragmentPatientDashboardBindingImpl;
import com.netizen.medicom.databinding.FragmentPatientIncomingCallBindingImpl;
import com.netizen.medicom.databinding.FragmentPatientListBindingImpl;
import com.netizen.medicom.databinding.FragmentPrescriptionAddMedicineBindingImpl;
import com.netizen.medicom.databinding.FragmentPrescriptionAddTestBindingImpl;
import com.netizen.medicom.databinding.FragmentPrescriptionHomeBindingImpl;
import com.netizen.medicom.databinding.FragmentPrescriptionMedicineListBindingImpl;
import com.netizen.medicom.databinding.FragmentPrescriptionTestListBindingImpl;
import com.netizen.medicom.databinding.FragmentSearchInformationBindingImpl;
import com.netizen.medicom.databinding.FragmentSearchSpinnerBindingImpl;
import com.netizen.medicom.databinding.FragmentSelectPatientBindingImpl;
import com.netizen.medicom.databinding.FragmentSignInBindingImpl;
import com.netizen.medicom.databinding.FragmentSignUpOneBindingImpl;
import com.netizen.medicom.databinding.FragmentSignUpTwoBindingImpl;
import com.netizen.medicom.databinding.FragmentSplashFindDoctorBindingImpl;
import com.netizen.medicom.databinding.FragmentSplashGetTreatmentBindingImpl;
import com.netizen.medicom.databinding.FragmentSplashMainBindingImpl;
import com.netizen.medicom.databinding.FragmentSplashSelectDoctorBindingImpl;
import com.netizen.medicom.databinding.FragmentSplashVideoChatBindingImpl;
import com.netizen.medicom.databinding.FragmentToolBarBindingImpl;
import com.netizen.medicom.databinding.LoginInvalidBindingImpl;
import com.netizen.medicom.databinding.MessageRechargePriceLayoutBindingImpl;
import com.netizen.medicom.databinding.RegistrationSucessLayoutBindingImpl;
import com.netizen.medicom.databinding.ResetPasswordSucessLayoutBindingImpl;
import com.netizen.medicom.databinding.SignleCallHistiryDoctorLayoutBindingImpl;
import com.netizen.medicom.databinding.SingleApprovedAppointmentRequestBindingImpl;
import com.netizen.medicom.databinding.SingleCompletedCallsBindingImpl;
import com.netizen.medicom.databinding.SingleDoctorFoundLayoutBindingImpl;
import com.netizen.medicom.databinding.SingleMedicineListLayoutBindingImpl;
import com.netizen.medicom.databinding.SinglePatientLayoutBindingImpl;
import com.netizen.medicom.databinding.SinglePendingAppointmentRequestBindingImpl;
import com.netizen.medicom.databinding.SingleSelectPatientBindingImpl;
import com.netizen.medicom.databinding.SingleSpinnerLayoutBindingImpl;
import com.netizen.medicom.databinding.SingleTestListLayoutBindingImpl;
import com.netizen.medicom.databinding.ToolbarBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_ACTIVITYMAIN = 1;

  private static final int LAYOUT_CUSTOMNOTIFICATIONLAYOUT = 2;

  private static final int LAYOUT_FRAGMENTACTIVECALL = 3;

  private static final int LAYOUT_FRAGMENTADDPATIENT = 4;

  private static final int LAYOUT_FRAGMENTCALLDETAILSHISTORY = 5;

  private static final int LAYOUT_FRAGMENTCHANGEPASSWORD = 6;

  private static final int LAYOUT_FRAGMENTCUSTOMERFEEDBACK = 7;

  private static final int LAYOUT_FRAGMENTDOCTORACTIVECALL = 8;

  private static final int LAYOUT_FRAGMENTDOCTORCORNERASSIGNFOUR = 9;

  private static final int LAYOUT_FRAGMENTDOCTORCORNERASSIGNONE = 10;

  private static final int LAYOUT_FRAGMENTDOCTORCORNERASSIGNSUCCESSFUL = 11;

  private static final int LAYOUT_FRAGMENTDOCTORCORNERASSIGNTHREE = 12;

  private static final int LAYOUT_FRAGMENTDOCTORCORNERASSIGNTWO = 13;

  private static final int LAYOUT_FRAGMENTDOCTORDASHBOARD = 14;

  private static final int LAYOUT_FRAGMENTDOCTORFOUND = 15;

  private static final int LAYOUT_FRAGMENTDOCTORINCOMINGCALL = 16;

  private static final int LAYOUT_FRAGMENTDOCTORSEARCH = 17;

  private static final int LAYOUT_FRAGMENTFEEDBACKTHANKS = 18;

  private static final int LAYOUT_FRAGMENTFORGOTPASSWORDFOUR = 19;

  private static final int LAYOUT_FRAGMENTFORGOTPASSWORDONE = 20;

  private static final int LAYOUT_FRAGMENTFORGOTPASSWORDTHREE = 21;

  private static final int LAYOUT_FRAGMENTFORGOTPASSWORDTWO = 22;

  private static final int LAYOUT_FRAGMENTHOME = 23;

  private static final int LAYOUT_FRAGMENTINCOMINGCALL = 24;

  private static final int LAYOUT_FRAGMENTLOGOUTDIALOG = 25;

  private static final int LAYOUT_FRAGMENTMAKECALL = 26;

  private static final int LAYOUT_FRAGMENTNAV = 27;

  private static final int LAYOUT_FRAGMENTPATIENTACTIVECALL = 28;

  private static final int LAYOUT_FRAGMENTPATIENTCORNERASSIGNONE = 29;

  private static final int LAYOUT_FRAGMENTPATIENTCORNERASSIGNSUCCESSFUL = 30;

  private static final int LAYOUT_FRAGMENTPATIENTCORNERASSIGNTWO = 31;

  private static final int LAYOUT_FRAGMENTPATIENTDASHBOARD = 32;

  private static final int LAYOUT_FRAGMENTPATIENTINCOMINGCALL = 33;

  private static final int LAYOUT_FRAGMENTPATIENTLIST = 34;

  private static final int LAYOUT_FRAGMENTPRESCRIPTIONADDMEDICINE = 35;

  private static final int LAYOUT_FRAGMENTPRESCRIPTIONADDTEST = 36;

  private static final int LAYOUT_FRAGMENTPRESCRIPTIONHOME = 37;

  private static final int LAYOUT_FRAGMENTPRESCRIPTIONMEDICINELIST = 38;

  private static final int LAYOUT_FRAGMENTPRESCRIPTIONTESTLIST = 39;

  private static final int LAYOUT_FRAGMENTSEARCHINFORMATION = 40;

  private static final int LAYOUT_FRAGMENTSEARCHSPINNER = 41;

  private static final int LAYOUT_FRAGMENTSELECTPATIENT = 42;

  private static final int LAYOUT_FRAGMENTSIGNIN = 43;

  private static final int LAYOUT_FRAGMENTSIGNUPONE = 44;

  private static final int LAYOUT_FRAGMENTSIGNUPTWO = 45;

  private static final int LAYOUT_FRAGMENTSPLASHFINDDOCTOR = 46;

  private static final int LAYOUT_FRAGMENTSPLASHGETTREATMENT = 47;

  private static final int LAYOUT_FRAGMENTSPLASHMAIN = 48;

  private static final int LAYOUT_FRAGMENTSPLASHSELECTDOCTOR = 49;

  private static final int LAYOUT_FRAGMENTSPLASHVIDEOCHAT = 50;

  private static final int LAYOUT_FRAGMENTTOOLBAR = 51;

  private static final int LAYOUT_LOGININVALID = 52;

  private static final int LAYOUT_MESSAGERECHARGEPRICELAYOUT = 53;

  private static final int LAYOUT_REGISTRATIONSUCESSLAYOUT = 54;

  private static final int LAYOUT_RESETPASSWORDSUCESSLAYOUT = 55;

  private static final int LAYOUT_SIGNLECALLHISTIRYDOCTORLAYOUT = 56;

  private static final int LAYOUT_SINGLEAPPROVEDAPPOINTMENTREQUEST = 57;

  private static final int LAYOUT_SINGLECOMPLETEDCALLS = 58;

  private static final int LAYOUT_SINGLEDOCTORFOUNDLAYOUT = 59;

  private static final int LAYOUT_SINGLEMEDICINELISTLAYOUT = 60;

  private static final int LAYOUT_SINGLEPATIENTLAYOUT = 61;

  private static final int LAYOUT_SINGLEPENDINGAPPOINTMENTREQUEST = 62;

  private static final int LAYOUT_SINGLESELECTPATIENT = 63;

  private static final int LAYOUT_SINGLESPINNERLAYOUT = 64;

  private static final int LAYOUT_SINGLETESTLISTLAYOUT = 65;

  private static final int LAYOUT_TOOLBAR = 66;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(66);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.activity_main, LAYOUT_ACTIVITYMAIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.custom_notification_layout, LAYOUT_CUSTOMNOTIFICATIONLAYOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_active_call, LAYOUT_FRAGMENTACTIVECALL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_add_patient, LAYOUT_FRAGMENTADDPATIENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_call_details_history, LAYOUT_FRAGMENTCALLDETAILSHISTORY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_change_password, LAYOUT_FRAGMENTCHANGEPASSWORD);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_customer_feedback, LAYOUT_FRAGMENTCUSTOMERFEEDBACK);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_doctor_active_call, LAYOUT_FRAGMENTDOCTORACTIVECALL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_doctor_corner_assign_four, LAYOUT_FRAGMENTDOCTORCORNERASSIGNFOUR);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_doctor_corner_assign_one, LAYOUT_FRAGMENTDOCTORCORNERASSIGNONE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_doctor_corner_assign_successful, LAYOUT_FRAGMENTDOCTORCORNERASSIGNSUCCESSFUL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_doctor_corner_assign_three, LAYOUT_FRAGMENTDOCTORCORNERASSIGNTHREE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_doctor_corner_assign_two, LAYOUT_FRAGMENTDOCTORCORNERASSIGNTWO);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_doctor_dashboard, LAYOUT_FRAGMENTDOCTORDASHBOARD);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_doctor_found, LAYOUT_FRAGMENTDOCTORFOUND);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_doctor_incoming_call, LAYOUT_FRAGMENTDOCTORINCOMINGCALL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_doctor_search, LAYOUT_FRAGMENTDOCTORSEARCH);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_feedback_thanks, LAYOUT_FRAGMENTFEEDBACKTHANKS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_forgot_password_four, LAYOUT_FRAGMENTFORGOTPASSWORDFOUR);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_forgot_password_one, LAYOUT_FRAGMENTFORGOTPASSWORDONE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_forgot_password_three, LAYOUT_FRAGMENTFORGOTPASSWORDTHREE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_forgot_password_two, LAYOUT_FRAGMENTFORGOTPASSWORDTWO);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_home, LAYOUT_FRAGMENTHOME);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_incoming_call, LAYOUT_FRAGMENTINCOMINGCALL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_log_out_dialog, LAYOUT_FRAGMENTLOGOUTDIALOG);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_make_call, LAYOUT_FRAGMENTMAKECALL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_nav, LAYOUT_FRAGMENTNAV);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_patient_active_call, LAYOUT_FRAGMENTPATIENTACTIVECALL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_patient_corner_assign_one, LAYOUT_FRAGMENTPATIENTCORNERASSIGNONE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_patient_corner_assign_successful, LAYOUT_FRAGMENTPATIENTCORNERASSIGNSUCCESSFUL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_patient_corner_assign_two, LAYOUT_FRAGMENTPATIENTCORNERASSIGNTWO);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_patient_dashboard, LAYOUT_FRAGMENTPATIENTDASHBOARD);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_patient_incoming_call, LAYOUT_FRAGMENTPATIENTINCOMINGCALL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_patient_list, LAYOUT_FRAGMENTPATIENTLIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_prescription_add_medicine, LAYOUT_FRAGMENTPRESCRIPTIONADDMEDICINE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_prescription_add_test, LAYOUT_FRAGMENTPRESCRIPTIONADDTEST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_prescription_home, LAYOUT_FRAGMENTPRESCRIPTIONHOME);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_prescription_medicine_list, LAYOUT_FRAGMENTPRESCRIPTIONMEDICINELIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_prescription_test_list, LAYOUT_FRAGMENTPRESCRIPTIONTESTLIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_search_information, LAYOUT_FRAGMENTSEARCHINFORMATION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_search_spinner, LAYOUT_FRAGMENTSEARCHSPINNER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_select_patient, LAYOUT_FRAGMENTSELECTPATIENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_sign_in, LAYOUT_FRAGMENTSIGNIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_sign_up_one, LAYOUT_FRAGMENTSIGNUPONE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_sign_up_two, LAYOUT_FRAGMENTSIGNUPTWO);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_splash_find_doctor, LAYOUT_FRAGMENTSPLASHFINDDOCTOR);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_splash_get_treatment, LAYOUT_FRAGMENTSPLASHGETTREATMENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_splash_main, LAYOUT_FRAGMENTSPLASHMAIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_splash_select_doctor, LAYOUT_FRAGMENTSPLASHSELECTDOCTOR);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_splash_video_chat, LAYOUT_FRAGMENTSPLASHVIDEOCHAT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.fragment_tool_bar, LAYOUT_FRAGMENTTOOLBAR);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.login_invalid, LAYOUT_LOGININVALID);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.message_recharge_price_layout, LAYOUT_MESSAGERECHARGEPRICELAYOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.registration_sucess_layout, LAYOUT_REGISTRATIONSUCESSLAYOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.reset_password_sucess_layout, LAYOUT_RESETPASSWORDSUCESSLAYOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.signle_call_histiry_doctor_layout, LAYOUT_SIGNLECALLHISTIRYDOCTORLAYOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.single_approved_appointment_request, LAYOUT_SINGLEAPPROVEDAPPOINTMENTREQUEST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.single_completed_calls, LAYOUT_SINGLECOMPLETEDCALLS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.single_doctor_found_layout, LAYOUT_SINGLEDOCTORFOUNDLAYOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.single_medicine_list_layout, LAYOUT_SINGLEMEDICINELISTLAYOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.single_patient_layout, LAYOUT_SINGLEPATIENTLAYOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.single_pending_appointment_request, LAYOUT_SINGLEPENDINGAPPOINTMENTREQUEST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.single_select_patient, LAYOUT_SINGLESELECTPATIENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.single_spinner_layout, LAYOUT_SINGLESPINNERLAYOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.single_test_list_layout, LAYOUT_SINGLETESTLISTLAYOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.medicom.R.layout.toolbar, LAYOUT_TOOLBAR);
  }

  private final ViewDataBinding internalGetViewDataBinding0(DataBindingComponent component,
      View view, int internalId, Object tag) {
    switch(internalId) {
      case  LAYOUT_ACTIVITYMAIN: {
        if ("layout/activity_main_0".equals(tag)) {
          return new ActivityMainBindingImpl(component, new View[]{view});
        }
        throw new IllegalArgumentException("The tag for activity_main is invalid. Received: " + tag);
      }
      case  LAYOUT_CUSTOMNOTIFICATIONLAYOUT: {
        if ("layout/custom_notification_layout_0".equals(tag)) {
          return new CustomNotificationLayoutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for custom_notification_layout is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTACTIVECALL: {
        if ("layout/fragment_active_call_0".equals(tag)) {
          return new FragmentActiveCallBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_active_call is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTADDPATIENT: {
        if ("layout/fragment_add_patient_0".equals(tag)) {
          return new FragmentAddPatientBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_add_patient is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTCALLDETAILSHISTORY: {
        if ("layout/fragment_call_details_history_0".equals(tag)) {
          return new FragmentCallDetailsHistoryBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_call_details_history is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTCHANGEPASSWORD: {
        if ("layout/fragment_change_password_0".equals(tag)) {
          return new FragmentChangePasswordBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_change_password is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTCUSTOMERFEEDBACK: {
        if ("layout/fragment_customer_feedback_0".equals(tag)) {
          return new FragmentCustomerFeedbackBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_customer_feedback is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTDOCTORACTIVECALL: {
        if ("layout/fragment_doctor_active_call_0".equals(tag)) {
          return new FragmentDoctorActiveCallBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_doctor_active_call is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTDOCTORCORNERASSIGNFOUR: {
        if ("layout/fragment_doctor_corner_assign_four_0".equals(tag)) {
          return new FragmentDoctorCornerAssignFourBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_doctor_corner_assign_four is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTDOCTORCORNERASSIGNONE: {
        if ("layout/fragment_doctor_corner_assign_one_0".equals(tag)) {
          return new FragmentDoctorCornerAssignOneBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_doctor_corner_assign_one is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTDOCTORCORNERASSIGNSUCCESSFUL: {
        if ("layout/fragment_doctor_corner_assign_successful_0".equals(tag)) {
          return new FragmentDoctorCornerAssignSuccessfulBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_doctor_corner_assign_successful is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTDOCTORCORNERASSIGNTHREE: {
        if ("layout/fragment_doctor_corner_assign_three_0".equals(tag)) {
          return new FragmentDoctorCornerAssignThreeBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_doctor_corner_assign_three is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTDOCTORCORNERASSIGNTWO: {
        if ("layout/fragment_doctor_corner_assign_two_0".equals(tag)) {
          return new FragmentDoctorCornerAssignTwoBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_doctor_corner_assign_two is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTDOCTORDASHBOARD: {
        if ("layout/fragment_doctor_dashboard_0".equals(tag)) {
          return new FragmentDoctorDashboardBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_doctor_dashboard is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTDOCTORFOUND: {
        if ("layout/fragment_doctor_found_0".equals(tag)) {
          return new FragmentDoctorFoundBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_doctor_found is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTDOCTORINCOMINGCALL: {
        if ("layout/fragment_doctor_incoming_call_0".equals(tag)) {
          return new FragmentDoctorIncomingCallBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_doctor_incoming_call is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTDOCTORSEARCH: {
        if ("layout/fragment_doctor_search_0".equals(tag)) {
          return new FragmentDoctorSearchBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_doctor_search is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTFEEDBACKTHANKS: {
        if ("layout/fragment_feedback_thanks_0".equals(tag)) {
          return new FragmentFeedbackThanksBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_feedback_thanks is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTFORGOTPASSWORDFOUR: {
        if ("layout/fragment_forgot_password_four_0".equals(tag)) {
          return new FragmentForgotPasswordFourBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_forgot_password_four is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTFORGOTPASSWORDONE: {
        if ("layout/fragment_forgot_password_one_0".equals(tag)) {
          return new FragmentForgotPasswordOneBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_forgot_password_one is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTFORGOTPASSWORDTHREE: {
        if ("layout/fragment_forgot_password_three_0".equals(tag)) {
          return new FragmentForgotPasswordThreeBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_forgot_password_three is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTFORGOTPASSWORDTWO: {
        if ("layout/fragment_forgot_password_two_0".equals(tag)) {
          return new FragmentForgotPasswordTwoBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_forgot_password_two is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTHOME: {
        if ("layout/fragment_home_0".equals(tag)) {
          return new FragmentHomeBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_home is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTINCOMINGCALL: {
        if ("layout/fragment_incoming_call_0".equals(tag)) {
          return new FragmentIncomingCallBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_incoming_call is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTLOGOUTDIALOG: {
        if ("layout/fragment_log_out_dialog_0".equals(tag)) {
          return new FragmentLogOutDialogBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_log_out_dialog is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTMAKECALL: {
        if ("layout/fragment_make_call_0".equals(tag)) {
          return new FragmentMakeCallBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_make_call is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTNAV: {
        if ("layout/fragment_nav_0".equals(tag)) {
          return new FragmentNavBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_nav is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPATIENTACTIVECALL: {
        if ("layout/fragment_patient_active_call_0".equals(tag)) {
          return new FragmentPatientActiveCallBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_patient_active_call is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPATIENTCORNERASSIGNONE: {
        if ("layout/fragment_patient_corner_assign_one_0".equals(tag)) {
          return new FragmentPatientCornerAssignOneBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_patient_corner_assign_one is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPATIENTCORNERASSIGNSUCCESSFUL: {
        if ("layout/fragment_patient_corner_assign_successful_0".equals(tag)) {
          return new FragmentPatientCornerAssignSuccessfulBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_patient_corner_assign_successful is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPATIENTCORNERASSIGNTWO: {
        if ("layout/fragment_patient_corner_assign_two_0".equals(tag)) {
          return new FragmentPatientCornerAssignTwoBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_patient_corner_assign_two is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPATIENTDASHBOARD: {
        if ("layout/fragment_patient_dashboard_0".equals(tag)) {
          return new FragmentPatientDashboardBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_patient_dashboard is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPATIENTINCOMINGCALL: {
        if ("layout/fragment_patient_incoming_call_0".equals(tag)) {
          return new FragmentPatientIncomingCallBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_patient_incoming_call is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPATIENTLIST: {
        if ("layout/fragment_patient_list_0".equals(tag)) {
          return new FragmentPatientListBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_patient_list is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPRESCRIPTIONADDMEDICINE: {
        if ("layout/fragment_prescription_add_medicine_0".equals(tag)) {
          return new FragmentPrescriptionAddMedicineBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_prescription_add_medicine is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPRESCRIPTIONADDTEST: {
        if ("layout/fragment_prescription_add_test_0".equals(tag)) {
          return new FragmentPrescriptionAddTestBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_prescription_add_test is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPRESCRIPTIONHOME: {
        if ("layout/fragment_prescription_home_0".equals(tag)) {
          return new FragmentPrescriptionHomeBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_prescription_home is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPRESCRIPTIONMEDICINELIST: {
        if ("layout/fragment_prescription_medicine_list_0".equals(tag)) {
          return new FragmentPrescriptionMedicineListBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_prescription_medicine_list is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPRESCRIPTIONTESTLIST: {
        if ("layout/fragment_prescription_test_list_0".equals(tag)) {
          return new FragmentPrescriptionTestListBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_prescription_test_list is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSEARCHINFORMATION: {
        if ("layout/fragment_search_information_0".equals(tag)) {
          return new FragmentSearchInformationBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_search_information is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSEARCHSPINNER: {
        if ("layout/fragment_search_spinner_0".equals(tag)) {
          return new FragmentSearchSpinnerBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_search_spinner is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSELECTPATIENT: {
        if ("layout/fragment_select_patient_0".equals(tag)) {
          return new FragmentSelectPatientBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_select_patient is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSIGNIN: {
        if ("layout/fragment_sign_in_0".equals(tag)) {
          return new FragmentSignInBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_sign_in is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSIGNUPONE: {
        if ("layout/fragment_sign_up_one_0".equals(tag)) {
          return new FragmentSignUpOneBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_sign_up_one is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSIGNUPTWO: {
        if ("layout/fragment_sign_up_two_0".equals(tag)) {
          return new FragmentSignUpTwoBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_sign_up_two is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSPLASHFINDDOCTOR: {
        if ("layout/fragment_splash_find_doctor_0".equals(tag)) {
          return new FragmentSplashFindDoctorBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_splash_find_doctor is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSPLASHGETTREATMENT: {
        if ("layout/fragment_splash_get_treatment_0".equals(tag)) {
          return new FragmentSplashGetTreatmentBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_splash_get_treatment is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSPLASHMAIN: {
        if ("layout/fragment_splash_main_0".equals(tag)) {
          return new FragmentSplashMainBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_splash_main is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSPLASHSELECTDOCTOR: {
        if ("layout/fragment_splash_select_doctor_0".equals(tag)) {
          return new FragmentSplashSelectDoctorBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_splash_select_doctor is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSPLASHVIDEOCHAT: {
        if ("layout/fragment_splash_video_chat_0".equals(tag)) {
          return new FragmentSplashVideoChatBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_splash_video_chat is invalid. Received: " + tag);
      }
    }
    return null;
  }

  private final ViewDataBinding internalGetViewDataBinding1(DataBindingComponent component,
      View view, int internalId, Object tag) {
    switch(internalId) {
      case  LAYOUT_FRAGMENTTOOLBAR: {
        if ("layout/fragment_tool_bar_0".equals(tag)) {
          return new FragmentToolBarBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_tool_bar is invalid. Received: " + tag);
      }
      case  LAYOUT_LOGININVALID: {
        if ("layout/login_invalid_0".equals(tag)) {
          return new LoginInvalidBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for login_invalid is invalid. Received: " + tag);
      }
      case  LAYOUT_MESSAGERECHARGEPRICELAYOUT: {
        if ("layout/message_recharge_price_layout_0".equals(tag)) {
          return new MessageRechargePriceLayoutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for message_recharge_price_layout is invalid. Received: " + tag);
      }
      case  LAYOUT_REGISTRATIONSUCESSLAYOUT: {
        if ("layout/registration_sucess_layout_0".equals(tag)) {
          return new RegistrationSucessLayoutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for registration_sucess_layout is invalid. Received: " + tag);
      }
      case  LAYOUT_RESETPASSWORDSUCESSLAYOUT: {
        if ("layout/reset_password_sucess_layout_0".equals(tag)) {
          return new ResetPasswordSucessLayoutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for reset_password_sucess_layout is invalid. Received: " + tag);
      }
      case  LAYOUT_SIGNLECALLHISTIRYDOCTORLAYOUT: {
        if ("layout/signle_call_histiry_doctor_layout_0".equals(tag)) {
          return new SignleCallHistiryDoctorLayoutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for signle_call_histiry_doctor_layout is invalid. Received: " + tag);
      }
      case  LAYOUT_SINGLEAPPROVEDAPPOINTMENTREQUEST: {
        if ("layout/single_approved_appointment_request_0".equals(tag)) {
          return new SingleApprovedAppointmentRequestBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for single_approved_appointment_request is invalid. Received: " + tag);
      }
      case  LAYOUT_SINGLECOMPLETEDCALLS: {
        if ("layout/single_completed_calls_0".equals(tag)) {
          return new SingleCompletedCallsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for single_completed_calls is invalid. Received: " + tag);
      }
      case  LAYOUT_SINGLEDOCTORFOUNDLAYOUT: {
        if ("layout/single_doctor_found_layout_0".equals(tag)) {
          return new SingleDoctorFoundLayoutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for single_doctor_found_layout is invalid. Received: " + tag);
      }
      case  LAYOUT_SINGLEMEDICINELISTLAYOUT: {
        if ("layout/single_medicine_list_layout_0".equals(tag)) {
          return new SingleMedicineListLayoutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for single_medicine_list_layout is invalid. Received: " + tag);
      }
      case  LAYOUT_SINGLEPATIENTLAYOUT: {
        if ("layout/single_patient_layout_0".equals(tag)) {
          return new SinglePatientLayoutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for single_patient_layout is invalid. Received: " + tag);
      }
      case  LAYOUT_SINGLEPENDINGAPPOINTMENTREQUEST: {
        if ("layout/single_pending_appointment_request_0".equals(tag)) {
          return new SinglePendingAppointmentRequestBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for single_pending_appointment_request is invalid. Received: " + tag);
      }
      case  LAYOUT_SINGLESELECTPATIENT: {
        if ("layout/single_select_patient_0".equals(tag)) {
          return new SingleSelectPatientBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for single_select_patient is invalid. Received: " + tag);
      }
      case  LAYOUT_SINGLESPINNERLAYOUT: {
        if ("layout/single_spinner_layout_0".equals(tag)) {
          return new SingleSpinnerLayoutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for single_spinner_layout is invalid. Received: " + tag);
      }
      case  LAYOUT_SINGLETESTLISTLAYOUT: {
        if ("layout/single_test_list_layout_0".equals(tag)) {
          return new SingleTestListLayoutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for single_test_list_layout is invalid. Received: " + tag);
      }
      case  LAYOUT_TOOLBAR: {
        if ("layout/toolbar_0".equals(tag)) {
          return new ToolbarBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for toolbar is invalid. Received: " + tag);
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      // find which method will have it. -1 is necessary becausefirst id starts with 1;
      int methodIndex = (localizedLayoutId - 1) / 50;
      switch(methodIndex) {
        case 0: {
          return internalGetViewDataBinding0(component, view, localizedLayoutId, tag);
        }
        case 1: {
          return internalGetViewDataBinding1(component, view, localizedLayoutId, tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
        case LAYOUT_ACTIVITYMAIN: {
          if("layout/activity_main_0".equals(tag)) {
            return new ActivityMainBindingImpl(component, views);
          }
          throw new IllegalArgumentException("The tag for activity_main is invalid. Received: " + tag);
        }
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(1);
    result.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(1);

    static {
      sKeys.put(0, "_all");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(66);

    static {
      sKeys.put("layout/activity_main_0", com.netizen.medicom.R.layout.activity_main);
      sKeys.put("layout/custom_notification_layout_0", com.netizen.medicom.R.layout.custom_notification_layout);
      sKeys.put("layout/fragment_active_call_0", com.netizen.medicom.R.layout.fragment_active_call);
      sKeys.put("layout/fragment_add_patient_0", com.netizen.medicom.R.layout.fragment_add_patient);
      sKeys.put("layout/fragment_call_details_history_0", com.netizen.medicom.R.layout.fragment_call_details_history);
      sKeys.put("layout/fragment_change_password_0", com.netizen.medicom.R.layout.fragment_change_password);
      sKeys.put("layout/fragment_customer_feedback_0", com.netizen.medicom.R.layout.fragment_customer_feedback);
      sKeys.put("layout/fragment_doctor_active_call_0", com.netizen.medicom.R.layout.fragment_doctor_active_call);
      sKeys.put("layout/fragment_doctor_corner_assign_four_0", com.netizen.medicom.R.layout.fragment_doctor_corner_assign_four);
      sKeys.put("layout/fragment_doctor_corner_assign_one_0", com.netizen.medicom.R.layout.fragment_doctor_corner_assign_one);
      sKeys.put("layout/fragment_doctor_corner_assign_successful_0", com.netizen.medicom.R.layout.fragment_doctor_corner_assign_successful);
      sKeys.put("layout/fragment_doctor_corner_assign_three_0", com.netizen.medicom.R.layout.fragment_doctor_corner_assign_three);
      sKeys.put("layout/fragment_doctor_corner_assign_two_0", com.netizen.medicom.R.layout.fragment_doctor_corner_assign_two);
      sKeys.put("layout/fragment_doctor_dashboard_0", com.netizen.medicom.R.layout.fragment_doctor_dashboard);
      sKeys.put("layout/fragment_doctor_found_0", com.netizen.medicom.R.layout.fragment_doctor_found);
      sKeys.put("layout/fragment_doctor_incoming_call_0", com.netizen.medicom.R.layout.fragment_doctor_incoming_call);
      sKeys.put("layout/fragment_doctor_search_0", com.netizen.medicom.R.layout.fragment_doctor_search);
      sKeys.put("layout/fragment_feedback_thanks_0", com.netizen.medicom.R.layout.fragment_feedback_thanks);
      sKeys.put("layout/fragment_forgot_password_four_0", com.netizen.medicom.R.layout.fragment_forgot_password_four);
      sKeys.put("layout/fragment_forgot_password_one_0", com.netizen.medicom.R.layout.fragment_forgot_password_one);
      sKeys.put("layout/fragment_forgot_password_three_0", com.netizen.medicom.R.layout.fragment_forgot_password_three);
      sKeys.put("layout/fragment_forgot_password_two_0", com.netizen.medicom.R.layout.fragment_forgot_password_two);
      sKeys.put("layout/fragment_home_0", com.netizen.medicom.R.layout.fragment_home);
      sKeys.put("layout/fragment_incoming_call_0", com.netizen.medicom.R.layout.fragment_incoming_call);
      sKeys.put("layout/fragment_log_out_dialog_0", com.netizen.medicom.R.layout.fragment_log_out_dialog);
      sKeys.put("layout/fragment_make_call_0", com.netizen.medicom.R.layout.fragment_make_call);
      sKeys.put("layout/fragment_nav_0", com.netizen.medicom.R.layout.fragment_nav);
      sKeys.put("layout/fragment_patient_active_call_0", com.netizen.medicom.R.layout.fragment_patient_active_call);
      sKeys.put("layout/fragment_patient_corner_assign_one_0", com.netizen.medicom.R.layout.fragment_patient_corner_assign_one);
      sKeys.put("layout/fragment_patient_corner_assign_successful_0", com.netizen.medicom.R.layout.fragment_patient_corner_assign_successful);
      sKeys.put("layout/fragment_patient_corner_assign_two_0", com.netizen.medicom.R.layout.fragment_patient_corner_assign_two);
      sKeys.put("layout/fragment_patient_dashboard_0", com.netizen.medicom.R.layout.fragment_patient_dashboard);
      sKeys.put("layout/fragment_patient_incoming_call_0", com.netizen.medicom.R.layout.fragment_patient_incoming_call);
      sKeys.put("layout/fragment_patient_list_0", com.netizen.medicom.R.layout.fragment_patient_list);
      sKeys.put("layout/fragment_prescription_add_medicine_0", com.netizen.medicom.R.layout.fragment_prescription_add_medicine);
      sKeys.put("layout/fragment_prescription_add_test_0", com.netizen.medicom.R.layout.fragment_prescription_add_test);
      sKeys.put("layout/fragment_prescription_home_0", com.netizen.medicom.R.layout.fragment_prescription_home);
      sKeys.put("layout/fragment_prescription_medicine_list_0", com.netizen.medicom.R.layout.fragment_prescription_medicine_list);
      sKeys.put("layout/fragment_prescription_test_list_0", com.netizen.medicom.R.layout.fragment_prescription_test_list);
      sKeys.put("layout/fragment_search_information_0", com.netizen.medicom.R.layout.fragment_search_information);
      sKeys.put("layout/fragment_search_spinner_0", com.netizen.medicom.R.layout.fragment_search_spinner);
      sKeys.put("layout/fragment_select_patient_0", com.netizen.medicom.R.layout.fragment_select_patient);
      sKeys.put("layout/fragment_sign_in_0", com.netizen.medicom.R.layout.fragment_sign_in);
      sKeys.put("layout/fragment_sign_up_one_0", com.netizen.medicom.R.layout.fragment_sign_up_one);
      sKeys.put("layout/fragment_sign_up_two_0", com.netizen.medicom.R.layout.fragment_sign_up_two);
      sKeys.put("layout/fragment_splash_find_doctor_0", com.netizen.medicom.R.layout.fragment_splash_find_doctor);
      sKeys.put("layout/fragment_splash_get_treatment_0", com.netizen.medicom.R.layout.fragment_splash_get_treatment);
      sKeys.put("layout/fragment_splash_main_0", com.netizen.medicom.R.layout.fragment_splash_main);
      sKeys.put("layout/fragment_splash_select_doctor_0", com.netizen.medicom.R.layout.fragment_splash_select_doctor);
      sKeys.put("layout/fragment_splash_video_chat_0", com.netizen.medicom.R.layout.fragment_splash_video_chat);
      sKeys.put("layout/fragment_tool_bar_0", com.netizen.medicom.R.layout.fragment_tool_bar);
      sKeys.put("layout/login_invalid_0", com.netizen.medicom.R.layout.login_invalid);
      sKeys.put("layout/message_recharge_price_layout_0", com.netizen.medicom.R.layout.message_recharge_price_layout);
      sKeys.put("layout/registration_sucess_layout_0", com.netizen.medicom.R.layout.registration_sucess_layout);
      sKeys.put("layout/reset_password_sucess_layout_0", com.netizen.medicom.R.layout.reset_password_sucess_layout);
      sKeys.put("layout/signle_call_histiry_doctor_layout_0", com.netizen.medicom.R.layout.signle_call_histiry_doctor_layout);
      sKeys.put("layout/single_approved_appointment_request_0", com.netizen.medicom.R.layout.single_approved_appointment_request);
      sKeys.put("layout/single_completed_calls_0", com.netizen.medicom.R.layout.single_completed_calls);
      sKeys.put("layout/single_doctor_found_layout_0", com.netizen.medicom.R.layout.single_doctor_found_layout);
      sKeys.put("layout/single_medicine_list_layout_0", com.netizen.medicom.R.layout.single_medicine_list_layout);
      sKeys.put("layout/single_patient_layout_0", com.netizen.medicom.R.layout.single_patient_layout);
      sKeys.put("layout/single_pending_appointment_request_0", com.netizen.medicom.R.layout.single_pending_appointment_request);
      sKeys.put("layout/single_select_patient_0", com.netizen.medicom.R.layout.single_select_patient);
      sKeys.put("layout/single_spinner_layout_0", com.netizen.medicom.R.layout.single_spinner_layout);
      sKeys.put("layout/single_test_list_layout_0", com.netizen.medicom.R.layout.single_test_list_layout);
      sKeys.put("layout/toolbar_0", com.netizen.medicom.R.layout.toolbar);
    }
  }
}
